# -*- coding: utf-8 -*-
#import simplejson
import werkzeug
import requests
import random
from datetime import datetime, timedelta
import urllib2
import urllib
from odoo import api
from odoo import exceptions
from odoo import fields
from odoo import models
from odoo.tools import scan_languages
from odoo.tools.translate import _
from odoo.addons.base.res.res_partner import _tz_get
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
#from odoo.addons.saas_base.exceptions import MaximumDBException, MaximumTrialDBException

#from odoo.addons.saas_base.exceptions import MaximumDBException
from werkzeug.exceptions import Forbidden
import string
from random import randint

import json
import logging
_logger = logging.getLogger(__name__)

def rendomDigits():
	N = 4

	random_string = ''.join(random.choice(string.ascii_uppercase) for alpha in range(N))
	random_digists = ''.join(["%s" % randint(0, 9) for num in range(0, N)])
	return random_string+'-'+random_digists



class ResUsers(models.Model):
    _inherit = 'res.users'
    

    allow_platform_login = fields.Boolean(default=False, string='Allow external login')
    session_id = fields.Char('session id')
    random_key = fields.Char('Random Key')
    plain_text_password = fields.Char('Plain Text Password')


    @api.model
    def create(self, vals):
    	

        if vals.get('password', False):
            vals['plain_text_password'] = vals.get('password', False)
        

        db = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        db = db.replace("http://","")
        db = db.replace("www.","")
       
        
        url = 'http://api.odesco.net/api/open/odoousers'
        headers = {'content-type': 'application/json'}

        if vals.get('allow_platform_login', False):
            vals['random_key'] = rendomDigits()
            result={}
            result['code']=vals['random_key']
            result['domaine']=db
            print"#####################result",result
            response = requests.post(url, data=json.dumps(result),headers= headers)

            print"###########""response",response    
    	return super(ResUsers, self).create(vals)

    @api.multi
    def write(self, vals):
        print "##### VALS ",vals
        if vals.get('password', False):
            vals['plain_text_password'] = vals.get('password', False)
            
        return super(ResUsers, self).write(vals)
    @api.multi
    def write(self, vals):
        print"#######user"
       
        
        db = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
        db = db.replace("http://","")
        db = db.replace("www.","")
       
        url = 'http://api.odesco.net/api/open/odoousers'
        headers = {'content-type': 'application/json'}

        if vals.get('allow_platform_login', False):  
            vals['random_key'] = rendomDigits() 
            result={}
            result['code']=vals['random_key']
            result['domaine']=db
            print"#####################result",result
            response = requests.post(url, data=json.dumps(result),headers= headers)

            print"###########""response",response

        if vals.get('password', False):
            vals['plain_text_password'] = vals.get('password', False)
            print"####password",vals.get('password', False)
        
        return super(ResUsers, self).write(vals)


