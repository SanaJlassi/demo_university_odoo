# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)

class ExamPVWizard(models.TransientModel):
    _name = "school.exam.pv.wizard"
    _description = "Report Exam wizard"


    academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)
    periode_config_id               = fields.Many2one('school.exam.periode.config', string="Session", domain="[('educational_stage_id','=',educational_stage_id), ('academic_year','=',academic_year)]", required=True)
    periode_ids                     = fields.Many2one('school.exam.periode', string="Période", domain="[('periode_config_id','=',periode_config_id)]", required=True)
    grade_id                        = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]",required=True)
    class_id                        = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)
    


    def _build_contexts(self, data):
        result = {}
        result['academic_year'] 			= data['form']['academic_year'] or False
        result['educational_stage_id'] 		= data['form']['educational_stage_id'] or False
        result['periode_config_id'] 		= data['form']['periode_config_id'] or False
        result['periode_ids'] 				= data['form']['periode_ids'] or False
        result['grade_id']					= data['form']['grade_id'] or False
        result['class_id'] 					= data['form']['class_id'] or False
        return result

    def _print_report(self, data):
        return self.env['report'].get_action(self, 'school_exams.report_schoolexampv', data=data)

    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['academic_year', 'educational_stage_id','periode_config_id', 'periode_ids', 'grade_id','class_id'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        return self._print_report(data)