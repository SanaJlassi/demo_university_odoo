# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)


class StudentCertificateWizard(models.TransientModel):
    _name = 'student.certificate.wizard'

    grade_id                                = fields.Many2one('school.grade', ondelete='set null', string='Niveau scolaire', required=True)
    class_id                                = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", ondelete="set null", string="Classe", required=True)
    student_ids                             = fields.Many2many('school.student', string="Étudiants", domain="[('class_id','=',class_id)]")  
    certificate_type 						= fields.Selection([('attendance','Attestation de présence'), ('registration',"Attestation d'inscription")], string="Type d'attestation", default='attendance')
    # student_ids                             = fields.Many2many(comodel_name='school.student', relation='school_task_school_student_rel', domain="[('class_id','=',class_id)]", string="Élèves")  

    def _build_contexts(self, data):
        result = {}
        result['grade_id'] = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        result['class_id'] = 'class_id' in data['form'] and data['form']['class_id'] or False
        result['student_ids'] = 'student_ids' in data['form'] and data['form']['student_ids'] or False
        result['certificate_type'] = 'certificate_type' in data['form'] and data['form']['certificate_type'] or False
        return result

    def _print_report(self, data):
        return self.env['report'].get_action(self, 'genext_school.report_studentcertificate', data=data)

    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['grade_id','class_id','student_ids', 'certificate_type'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        return self._print_report(data)


class StudentCertificateReport(models.AbstractModel):
    _name = 'report.genext_school.report_studentcertificate'

    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        no_student = True
        student_id = None
        if used_context['student_ids'] and len(used_context['student_ids']):
            no_student = False
            student_id = self.env['school.student'].browse(used_context['student_ids'])
        name_etab = ' '
        image_etab = ''
        email = ''
        site = ''
        phone = ''
        city = ''
        street = ''
        etablissement_obj= self.env['school.etablissement'].search([])
        for etablissement in etablissement_obj:
            if etablissement.school_active == True:
                name_etab = etablissement.name
                image_etab = etablissement.image
                email = etablissement.email
                site = etablissement.site_web
                phone = etablissement.phone
                city = etablissement.city
                street = etablissement.street
        certificate_type = 'certificate_type' in data['form'] and data['form']['certificate_type'] or False
        certificate = str(certificate_type)
        titre = 'ATTESTATION DE PRÉSENCE'
        if certificate =='registration':
            titre = 'ATTESTATION D'+"'" +'INSCRIPTION'

        print(titre)
            
        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            'docs': student_id,
            'no_student': no_student,
            'time': time,
            'certificate_title':titre,
            'nameetab': name_etab,
            'image'   : image_etab,
            'email'   : email,
            'site'    : site,
            'phone'   : phone,
            'street'  : street,
            'city'    : city,
            'image'   : image_etab,
        }
        return self.env['report'].render('genext_school.report_studentcertificate', docargs)
