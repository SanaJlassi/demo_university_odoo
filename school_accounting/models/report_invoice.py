import time

from openerp.report import report_sxw
from openerp.osv import osv

class account_invoice(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(account_invoice, self).__init__(cr, uid, name, context=context)
        ids = context.get('active_ids')
        invoice_obj = self.pool['account.invoice.line']
        docs = invoice_obj.browse(cr, uid, ids, context)
        self.localcontext.update({
            'docs': docs,
            'time': time,

        })
        self.context = context

    
class report_invoice_jhon(osv.AbstractModel):

    _name = 'report.shool_accounting.report_template'
    _inherit = 'report.abstract_report'
    _template = 'school_accounting.report_invoice_jhon'

class receipt_payment(osv.AbstractModel):

    _name = 'report.shool_accounting.report_receipt_payment'
    _inherit = 'report.abstract_report'
    _template = 'shool_accounting.receipt_payment'