# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)


class Note(models.Model):
    _name =  'class.note'

    name                    = fields.Char('Nom')
    date                    = fields.Date('date',default=fields.Date.today())
    educational_stage_id    = fields.Many2one('educational.stage', string="Cycle", required=True)
    grade_id                = fields.Many2one('school.grade',domain="[('educational_stage_id','=',educational_stage_id)]", 
                                                string="Niveau scolaire", required=True)
    class_id                = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", string="Classe", required=True)
    module_id               = fields.Many2one('module.module', string="Module", required=True)
    skill_id                = fields.Many2one('module.skill',domain="[('module_id','=',module_id)]", string="Compétence", required=True)
    annual_calendar_id      = fields.Many2one('annual.calendar', string="Année scolaire",default=lambda self: self.env['annual.calendar'].get_current_year())
    note_line_ids           = fields.One2many('note.line','class_note_id', string="Notes")
    presconote_line_ids     = fields.One2many('presco.note.line','class_note_id', string="Notes")
    comment_line_ids        = fields.One2many('comment.line','class_note_id', string="Commentaires")
    semestre_id             = fields.Many2one('annual.periode', domain="[('educational_stage_id','=',educational_stage_id)]", string="Semestre")
    periode_id              = fields.Many2one('annual.periode', domain="['&',('last_level','=',True),('parent_id','=',semestre_id)]", string="Evaluation")
    average                 = fields.Float('Moyenne du groupe', compute='_compute_average')
    teache_id               = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", string="Enseignant", 
                                    default=lambda self: self.env['hr.employee'].search([('partner_id','=',self.env.user.partner_id.id)]))
    evaluation_methode      = fields.Selection([('preschool','Préscolaire'),('not_preschool','Autre')], default="not_preschool", string="Méthode d'évaluation")
    

    _sql_constraints = [ ('name_uniq', 'unique(class_id,module_id,skill_id,annual_calendar_id,priode_id)', 'Erreur de duplication de données'),]

    @api.model
    def _compute_teacher(self):
        teacher_id = self.env['hr.employee'].search([('partner_id','=',self.env.user.partner_id.id)])
        if teacher_id:
            print "TEACHER FOUND"
            return teacher_id.id

    @api.returns('self')
    @api.multi
    def get_notes(self, student_id, class_id, module_id,skill_id):
        class_note_id = self.env['class.note'].search(['&',('module_id','=',module_id),'&',('class_id','=',class_id),('skill_id','=',skill_id)])
        print "########## class_note_id ",class_note_id
        return class_note_id.filtered(lambda r: student_id in [rec.student_id.id for rec in r.note_line_ids])

    @api.multi
    @api.depends('note_line_ids')
    def _compute_average(self):
        for rec in self:
            average = 0.0
            for line in rec.note_line_ids:
                average+=line.note
            if len(rec.note_line_ids):
                average = average/len(rec.note_line_ids)
            print "######### computing average ",average
            rec.average = average


    @api.onchange('class_id')
    def _onchange_class_id(self):
        note_list= []
        presco_note_list = []
        comment_list= []
        if len(self.class_id):
            student_id = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in student_id:
                note_list.append((0,0,{'student_id':student.id,'note':None, 'comment':None}))
                presco_note_list.append((0,0,{'student_id':student.id}))
                comment_list.append((0,0,{'student_id':student.id, 'comment':None}))

        return {'value':{'note_line_ids':note_list,'presconote_line_ids':presco_note_list,'comment_line_ids':comment_list}}



    @api.onchange('educational_stage_id')
    def _onchang_module_id(self):
        module_id = self.env['module.module'].get_modules_contains_cycle(self.educational_stage_id.id)
        _logger.warning('#### FOUND MODULES %s', module_id)

        return {    
                    'value':{'evaluation_methode':self.educational_stage_id.evaluation_methode},
                    'domain':{
                        'module_id':[('id','in',module_id.ids)],
                    }
                }

class PrescoNoteLine(models.Model):
    _name = 'presco.note.line'

    class_note_id                   = fields.Many2one('class.note', ondelete='cascade')
    student_id                      = fields.Many2one('school.student', string="Étudiants", required=True, readonly=True)
    mark                            = fields.Many2one('module.mark', sting="Remarque")

class NoteLine(models.Model):
    _name = 'note.line'

    class_note_id                   = fields.Many2one('class.note', ondelete='cascade')
    student_id                      = fields.Many2one('school.student', string="Étudiants", required=True, readonly=True)
    note                            = fields.Float('Note', default=0.0, required=True)
    comment                         = fields.Char('Commentaire')




class ModuleComment(models.Model):
    _name = 'comment.line'

    class_note_id                   = fields.Many2one('class.note', ondelete='cascade')
    student_id                      = fields.Many2one('school.student', string="Étudiants", required=True, readonly=True)
    comment                         = fields.Char('Commentaire')



