{
    'name': "Calendar extention",
    'version': '1.0',
    'category': 'calendar',
    'author': 'Genext-IT Team',
    'website': 'https://www.genext-it.com',
    'depends': ['calendar'],
    'data': [
        'views/calendar_view.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': False,
}