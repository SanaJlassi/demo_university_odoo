# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import smtplib
import string
import logging
_logger = logging.getLogger(__name__)

class SanctionType(models.Model):
    _name = 'school.sanction.type'

    name                    = fields.Char(string="Nom de sanction", required=True)
    description             = fields.Char(string="Description de sanction")


class Sanction(models.Model):
    _name = 'school.sanction'
    _description = 'School Sanction'


    name                    = fields.Char('Titre', required=True)
    description             = fields.Text(string="Description")
    sanction_type_id        = fields.Many2one('school.sanction.type', string="Type de sanction", required=True)
    date                    = fields.Datetime(default=fields.Datetime.now(), string="Date", required=True)
    datetime                = fields.Datetime(default=fields.Datetime.now(), string="Datetime")
    teacher_id              = fields.Many2one('hr.employee', string="Enseignant", required=True,domain="[('is_school_teacher','=',True)]")
    class_id                = fields.Many2one('school.class', string="Classe")
    student_ids             = fields.Many2many('school.student', string="Étudiant", domain="[('class_id','=',class_id)]", required=True)
    file                    = fields.Binary(string='Fichier', attachment=True, help="Vous pouvez attacher un document avec la sanction")
    file_name               = fields.Char(string="File name")
    state                   = fields.Selection([('draft','Brouillon'),('confirmed','Confirmé')],default='draft')

    @api.multi
    def set_to_draft(self):
        return self.write({'state':'draft'})

    @api.multi
    def set_to_confirmed(self):
        emailparent=[]
        sender = 'contact@ulysse.ens.tn'
        email_config  = self.env['email.config'].search([],limit=1)
        for rec in self:
            for student in rec.student_ids:
                for parent in student.parent_ids:
                    receiver = parent.email
                    emailparent.append(receiver)
        From = email_config.sender_email
        receivers=emailparent
        Subj = email_config.subject
        print "receivers#####",receivers
        sanction_type = self.sanction_type_id.name
        sanction_type = str(sanction_type)
        date = self.date
        date = str(date)
        #for receiver in receivers:
            #for student in self.student_ids:
               # if receiver in student.parent_ids.email:
                    #student = student.name + student.last_name
                    #Text = "Votre éleve "+ student +" a reçu une sanction de type " + sanction_type + " ,le " + date
           # print(Text)
             #Body = string.join((
              #  "From: %s" % From,
              #"Subject: %s" % Subj,
              #  "",
              #  Text,
              #  ""
              # ),"\r\n")
            #try:
            #  print("-------------------------------------------------------")
             # smtpObj = smtplib.SMTP(host='smtp.gmail.com', port=587)
             # smtpObj.ehlo()
            #  smtpObj.starttls()
              #smtpObj.ehlo()
             # print("+++++++++++")
              #smtpObj.login(user=email_config.sender_email, password=email_config.sender_password)
              #print("+++++++++++")
            #  smtpObj.sendmail(sender, receiver, Body)
             # smtpObj.close()        
              #_logger.warning("Successfully sent email")
           # except:
            #  _logger.warning("#### ERRROR while sending email")

        #print("here ------------ sms-------------------")
       # sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
      #  if not sender:
          #  print("Vous devez valider une configuration sms pour envoyer des sms")
           #raise Warning('Vous devez valider une configuration sms pour envoyer des sms')
       # else:
            #number_list = []
           # for student in self.student_ids:
              #  for parent in student.parent_ids:
                  #  phone = parent.mobile if parent.mobile else parent.phone
                    #if phone:
                     #  number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                      # _logger.warning("### NUMBER %s",number)
                      # number_list.append(number)
           # message = "Votre éleve a reçu une sanction de type " + sanction_type + " ,le " + date
           # _logger.warning("NUMBERS %s",number_list)
          #  response = sender.send_lot_sms(message, number_list)
           # if response['success'] == True:
              # self.env.cr.commit()
               ###raise Warning('Messages envoyés avec succès')
               #print("Messages envoyés avec succès")
           # else:
              #### #raise Warning('Messages non envoyés')
              # print("Messages non envoyés")

       # print("**********notification**************************")
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            ''' foreach sanction in [self --> recordset of sanction, can contain one record or a set of records]'''
            for rec in self:
                tokens = []
                for student in rec.student_ids:
                    for parent in student.parent_ids:
                        for token in parent.token_ids:
                           _logger.warning("#### TOKEN %s",parent.token)
                           tokens.append(token.token)
                           sanction = {
                               'name':rec.name,
                               'description':rec.description,
                               'sanction_type':{'id':rec.sanction_type_id.id,'name':rec.sanction_type_id.name,'description':rec.sanction_type_id.description},
                               'date':rec.date,
                               'teacher':{
                                    'id':rec.teacher_id.id,
                                    'name':rec.teacher_id.name,
                                    'last_name':rec.teacher_id.last_name,
                                },
                               'class':{
                                  'id':rec.class_id.id,
                                  'name':rec.class_id.name,
                                  'code':rec.class_id.code,
                                },
                                'file':{
                                   'base64':rec.file,
                                   'filename':rec.file_name,
                                }
                            }

                           data = {'module':'SanctionsPage','data':sanction, 'kid':{'id':student.id,'name': student.name, 'last_name':student.last_name}}
                           content = {'en':'Your child got a sanction', 'fr':'Votre enfant a reçu une sanction'}
                            #sending push notification for each sanction'''
                           notif.sendPushNotification(data=data ,content=content, tokens=[token.token])
        return self.write({'state':'confirmed'})
        

   





