# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class SchoolMedicalConsultation(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }

    @http.route('/api/student/<int:student_id>/consultaion', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_children_consultations(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER)
              
        res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
        if student_id not in res_partner.student_ids.ids:
            response['success'] = False
            response['error'] = {'code':404, 'message':'You need to be the parent of the student in order to get his info !'} 
            return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER)

        studnet_id = request.env['school.student'].search([('id','=',student_id)])
        if not studnet_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER)     

        consultation_ids = request.env['school.medical'].search([('student_id','=',student_id)])
        consultaion_list = []

        for consultaion in consultation_ids: 
            partners = []
            vaccins = []
            allergies = []
            exemptions = []
            for partner in consultaion.partner_ids:
                obj = {
                    'id':partner.id,
                    'name':partner.name,
                    'last_name':partner.last_name,
                }  
                partners.append(obj)
            for vaccin in consultaion.vaccin_ids:
                obj = {
                    'id':vaccin.id,
                    'name':vaccin.name,
                    'date':vaccin.date,
                }  
                vaccins.append(obj)
            for allergy in consultaion.allergy_ids:
                obj = {
                    'id':allergy.id,
                    'name':allergy.name,
                    'date':allergy.date,
                }  
                allergies.append(obj)
            for exemption in consultaion.exemption_ids:
                obj = {
                    'id':exemption.id,
                    'subject':{
                        'subject_id':exemption.subject_id.name,
                        },
                    'start_date':exemption.start_date,
                    'end_date': exemption.end_date,
                }  
                exemptions.append(obj)
            json_object = {
                'id':consultaion.id,
                'nature': consultaion.nature,
                'start_date': consultaion.start_date,
                'end_date': consultaion.end_date,  
                'place': consultaion.consultation_place,
                'symptom': consultaion.symptom,
                'undertaken_act' : consultaion.undertaken_act,
                'intervention_type' : consultaion.intervention_type,
                'intervention_decision' : consultaion.intervention_decision,
                'description' : consultaion.description,
                'accompanist_type' : consultaion.accompanist_type,
                'partner_ids' : partners,
                'vaccins': vaccins,
                'allergies' : allergies,
                'dispenses': exemptions,
            }
            consultaion_list.append(json_object)
            
        response['success'] = True
        response['data'] = consultaion_list
        return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER)


    @http.route('/api/teacher/<int:class_id>/<int:student_id>/consultaion', type='http', auth="user", methods=['GET'],  csrf=False)
    def teacher_children_consultations(self, class_id, student_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER)
              
        teacher_id = request.env['hr.employee'].search([('partner_id','=',request.env.user.partner_id.id)])
        if not teacher_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to find data related to logged teacher'} 
            return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER)

        classe_id = request.env['school.class'].search([('id','=',class_id)]) 
        if not classe_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No class found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER)

        studnet_id = request.env['school.student'].search([('class_id','=',class_id),('id','=',student_id)])
        if not studnet_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found in the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER) 

        consultation_ids = request.env['school.medical'].search([('student_id','=',student_id)])
        consultaion_list = []      

        for consultaion in consultation_ids: 
            partners = []
            vaccins = []
            allergies = []
            exemptions = []
            for partner in consultaion.partner_ids:
                obj = {
                    'id':partner.id,
                    'name':partner.name,
                    'last_name':partner.last_name,
                }  
                partners.append(obj)
            for vaccin in consultaion.vaccin_ids:
                obj = {
                    'id':vaccin.id,
                    'name':vaccin.name,
                    'date':vaccin.date,
                }  
                vaccins.append(obj)
            for allergy in consultaion.allergy_ids:
                obj = {
                    'id':allergy.id,
                    'name':allergy.name,
                    'date':allergy.date,
                }  
                allergies.append(obj)
            for exemption in consultaion.exemption_ids:
                obj = {
                    'id':exemption.id,
                    'subject':exemption.subject_id,
                    'start_date':exemption.start_date,
                    'end_date': exemption.end_date,
                }
                exemptions.append(obj)
            json_object = {
                'id':consultaion.id,
                'nature': consultaion.nature,
                'start_date': consultaion.start_date,
                'end_date': consultaion.end_date,  
                'place': consultaion.consultation_place,
                'symptom': consultaion.symptom,
                'undertaken_act' : consultaion.undertaken_act,
                'intervention_type' : consultaion.intervention_type,
                'intervention_decision' : consultaion.intervention_decision,
                'description' : consultaion.description,
                'accompanist_type' : consultaion.accompanist_type,
                'partner_ids' : partners,
                'vaccins': vaccins,
                'allergies' : allergies,
                'dispenses': exemptions,
            }
            consultaion_list.append(json_object)
            
        response['success'] = True
        response['data'] = consultaion_list
        return http.request.make_response(json.dumps(response),SchoolMedicalConsultation.HEADER)

