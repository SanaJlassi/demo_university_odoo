# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import ValidationError
from datetime import datetime

class TimeTable(models.Model):
    _name = 'time.table'
    _description = 'School Time Table'


    @api.depends('timing_periode','class_id')
    @api.multi
    def _compute_name(self):
        for rec in self:
            if rec.timing_periode and rec.class_id:
                rec.name = rec.class_id.code+' / '+rec.timing_periode.name

    name            = fields.Char("Name", readonly=True, compute=_compute_name)
    academic_year   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) , required=True)
    class_id        = fields.Many2one('school.class', required=True)
    grade_id        = fields.Many2one(related='class_id.grade_id')
    timing_system   = fields.Many2one(related='class_id.grade_id.educational_stage_id.timing_system_id')
    timing_periode  = fields.Many2one('school.timing.system.period', domain="[('timing_system_id','=',timing_system)]", required=True)
    

    calendar        = fields.Char("calendar",default="default values")

    time_table_session_ids  = fields.One2many('time.table.session', 'time_table_id', string="Emploi de temps")
    table_line_ids          = fields.One2many('time.table.line','time_table_id')



    '''  
        add SQL constraint to make unique ( academic year, class, timing periode ) 
    '''

    # @api.depends('timing_periode','class_id')
    # @api.multi
    # def _compute_name(self):
    #     for rec in self:
    #         if rec.timing_periode and rec.class_id:
    #             rec.name = rec.class_id+' / '+rec.timing_periode

    @api.multi
    def unlink(self):
        for record in self:
            self.env['time.table.session'].search([('time_table_id','=',record.id)]).unlink()
        return super(TimeTable,self).unlink()

   

    @api.onchange('class_id','timing_system')
    def _default_timing_periode(self):
        # check if there's a selected timing system
        if len(self.timing_system):
            for rec in self.env['school.timing.system.period'].search([('timing_system_id','=',self.timing_system.id)]):
                # reconstruct date with the current year
                start_period = str(datetime.now().year)+'-'+rec.start_date_month
                end_period = str(datetime.now().year)+'-'+rec.end_date_month
                # convert date sting to date object
                start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
                end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
                # check in which period we are right now
                if start_period <= datetime.now().date() <= end_period:
                    self.timing_periode = rec
                    break
        

    @api.multi
    def open_table_line_session_wizard(self):
        if self.id:
            if not self.class_id.id:
                raise ValidationError('You need to select a class !')

        return {
            'type': 'ir.actions.act_window',
            'name': 'Name ',
            'view_mode': 'form', 
            'target': 'new',
            'res_model': 'table.line.session.wizard',
            'context': {'time_table_id': self.id, 'update':False} 
        }


class TimeTableSession(models.Model):
    _name = 'time.table.session'
    _description = 'Session Grouping a teacher a class and a subject'


    @api.depends('subject_id','class_id')
    @api.multi
    def _compute_name(self):
        for record in self:
            subject = ''
            subject = record.subject_id.name if record.subject_id.name else record.subject_id.code
            record.name = record.class_id.code+"/"+subject


    name            = fields.Char('name', compute=_compute_name)
    time_table_id   = fields.Many2one('time.table', ondelete="cascade")
    # used for filtering session related to teacher for current timing period 
    timing_periode  = fields.Many2one(related='time_table_id.timing_periode', readonly=True, store=True)
    class_id        = fields.Many2one(related="time_table_id.class_id", readonly=True, store=True)    
    subject_id      = fields.Many2one('school.subject', domain="[('id','in',class_id.grade_id.subject_ids.ids)]")
    teacher_id      = fields.Many2one('hr.employee', domain="[('id','in',subject_id.teacher_ids.ids)]")
    classroom_id    = fields.Many2one('school.classroom', required=True)
    start_time      = fields.Char('Start time')
    end_time        = fields.Char('End time')
    day_of_week     = fields.Integer('Day Number in a week')
    time_table_teacher_id   = fields.Many2one('time.table.teacher', ondelete="cascade")


    


    #@api.multi
    #def name_get(self):
    #    return [(record.id,str(record.class_id.name)+'/'+str(record.subject_id.name)+'/'+str(record.teacher_id.name)+'/'+str(record.classroom_id.name))for record in self]

    @api.onchange('class_id')
    def _onchange_subjects(self):
        return {'domain':{'subject_id':[('id','in',self.class_id.grade_id.subject_ids.ids)]}}

    @api.onchange('subject_id')
    def _onchange_subject(self):
        subject = self.env['school.subject'].search([('id','=',self.subject_id.id)])
        return {'domain':{'teacher_id':[('id','in',subject.teacher_ids.ids)]}}

    @api.multi
    def write(self, vals):
        """ Called when updating the sessions by drag and drop """
        if vals.get('start_time',False) and vals.get('end_time',False):
            for record in self.env['time.table.session'].search([('id','!=',self.id)]):
                """ convert string time to datetime object to compare them """
                current_start_time = datetime.strptime(vals['start_time'],'%H:%M:%S')
                current_end_time = datetime.strptime(vals['end_time'],'%H:%M:%S')

                start_time = datetime.strptime(record.start_time,'%H:%M:%S')
                end_time = datetime.strptime(record.end_time,'%H:%M:%S')
                """ Check if teacher is teaching simultaneously two classes"""
                if (record.teacher_id == self.teacher_id) and (record.day_of_week == vals['day_of_week']):
                    if (start_time <= current_start_time <= end_time) or (start_time <= current_end_time <= end_time):
                        raise ValidationError("Teacher cannot teach two classes in the same time")

                """ check if classroom is free """
                if (record.classroom_id == self.classroom_id) and (record.day_of_week == vals['day_of_week']):
                    if (start_time <= current_start_time <= end_time) or (start_time <= current_end_time <= end_time):
                        raise ValidationError("Classroom is already allocated in the same day and time")
        return  super(TimeTableSession, self).write(vals)




class TimeTableLine(models.Model):
    _name = 'time.table.line'
    _description = 'Time Table Line'


    name = fields.Char("Name")

    time_table_id   = fields.Many2one('time.table')
    class_id        = fields.Many2one('school.class')
    subject_id      = fields.Many2one('school.subject')
    teacher_id      = fields.Many2one('hr.employee')
    classroom_id    = fields.Many2one('school.classroom')
    start_time      = fields.Integer('Start time')
    end_time        = fields.Integer('End time')
    day             = fields.Integer('Day Number in a week')


class TimeTableTeacher(models.Model):
    _name = 'time.table.teacher'
    _description = 'School Time Table Teacher'


    @api.depends('timing_periode','teacher_id')
    @api.multi
    def _compute_name(self):
        for rec in self:
            if rec.timing_periode and rec.teacher_id:
                rec.name = rec.teacher_id.name+' / '+rec.timing_periode.name


    @api.depends('teacher_id','timing_periode')
    @api.multi
    def _compute_sessions(self):
        for rec in self:
            if rec.teacher_id and rec.timing_periode:
               rec.time_table_session_ids = self.env['time.table.session'].search([('teacher_id','=',rec.teacher_id.id),('timing_periode','=',rec.timing_periode.id)])
               print "###################### TIME TABLE SESSION",rec.time_table_session_ids

    name                    = fields.Char("Name", readonly=True, compute=_compute_name)
    academic_year           = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) , required=True)
    # class_id                = fields.Many2one('school.class', required=True)
    # grade_id                = fields.Many2one(related='class_id.grade_id')
    # timing_system           = fields.Many2one(related='class_id.grade_id.educational_stage_id.timing_system_id')
    timing_periode          = fields.Many2one('school.timing.system.period', required=True)
    

    calendar                = fields.Char("calendar",default="default values")

    teacher_id              = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", string="Enseignant", required=True)
    time_table_session_ids  = fields.One2many('time.table.session', 'time_table_teacher_id', compute=_compute_sessions, string="Emploi de temps")
    
    table_line_ids          = fields.One2many('time.table.line','time_table_id')



    '''  
        add SQL constraint to make unique ( academic year, class, timing periode ) 
    '''

    # @api.depends('timing_periode','class_id')
    # @api.multi
    # def _compute_name(self):
    #     for rec in self:
    #         if rec.timing_periode and rec.class_id:
    #             rec.name = rec.class_id+' / '+rec.timing_periode

    @api.multi
    def unlink(self):
        for record in self:
            self.env['time.table.session'].search([('time_table_teacher_id','=',record.id)]).unlink()
        return super(TimeTableTeacher,self).unlink()

   

    # @api.onchange('timing_periode')
    # def _get_time_table_session(self):
    #     if len(self.timing_system):
    #         for rec in self.env['school.timing.system.period'].search([('timing_system_id','=',self.timing_system.id)]):
    #             # reconstruct date with the current year
    #             start_period = str(datetime.now().year)+'-'+rec.start_date_month
    #             end_period = str(datetime.now().year)+'-'+rec.end_date_month
    #             # convert date sting to date object
    #             start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
    #             end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
    #             # check in which period we are right now
    #             if start_period <= datetime.now().date() <= end_period:
    #                 self.timing_periode = rec
    #                 break
        

    # @api.multi
    # def open_table_line_session_wizard(self):
    #     if self.id:
    #         if not self.class_id.id:
    #             raise ValidationError('You need to select a class !')

    #     return {
    #         'type': 'ir.actions.act_window',
    #         'name': 'Name ',
    #         'view_mode': 'form', 
    #         'target': 'new',
    #         'res_model': 'table.line.session.wizard',
    #         'context': {'time_table_id': self.id, 'update':False} 
    #     }