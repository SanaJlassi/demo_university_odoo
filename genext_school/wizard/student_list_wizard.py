# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)


class StudentListWizard(models.TransientModel):
    _name = 'student.list.wizard'


    grade_id                                = fields.Many2one('school.grade', ondelete='set null', string='Niveau scolaire')
    class_id                                = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", ondelete="set null", string="Classe")
    # student_ids                             = fields.Many2many(comodel_name='school.student', relation='school_task_school_student_rel', domain="[('class_id','=',class_id)]", string="Élèves")  
    # student_ids                             = fields.Many2many('school.student', string="Élèves", compute='_compute_students', domain="[('class_id','=',class_id)]") 
    student_ids                             = fields.Many2many('school.student', string="Étudiants", compute='_compute_students', domain="[('class_id','=',class_id)]") 
    student_info                            = fields.Many2many(comodel_name='ir.model.fields', relation='student_field_rel', domain="[('model_name','=','school.student')]", string="Formulaire Étudiant")
    


    @api.depends('class_id','grade_id')
    @api.one
    def _compute_students(self):
        student_list = []
        if self.class_id.id:
           students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
           for student in students:
              student_list.append(student.id)
        else:
           students = self.env['school.student'].search([('grade_id','=',self.grade_id.id)])
           for student in students:
               student_list.append(student.id)
        _logger.warning("STUDENT %s",student_list)
        self.update({'student_ids':[(6,False,[y for y in student_list])]})
    
    

    def _build_contexts(self, data):
        result = {}
        result['grade_id'] = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        result['class_id'] = 'class_id' in data['form'] and data['form']['class_id'] or False
        result['student_ids'] = 'student_ids' in data['form'] and data['form']['student_ids'] or False
        result['student_info'] = 'student_info' in data['form'] and data['form']['student_info'] or False
        return result

    def _print_report(self, data):
        return self.env['report'].get_action(self, 'genext_school.report_studentlist', data=data)

    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['grade_id','class_id','student_ids','student_info'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        return self._print_report(data)

    

class StudentListReport(models.AbstractModel):
    _name = 'report.genext_school.report_studentlist'

    def _get_student_val(self,data):
        used_context = data['form'].get('used_context', {})
        list_val = {}
        if used_context['student_info']:
            print "###### after if ",used_context['student_info']
            # student = self.env['school.student'].search([('id','=',student_id.id)])
            for line in used_context['student_info']:
                obj = self.env['ir.model.fields'].browse(line)
                print "########### STUDENT INFORMATION NAME", obj.name
                # print "##########", employee_id[obj.name]
                key = str(obj.name)
                print "######## KEY",key
                # value = str(employee_id[obj.name])
                # print "######## VALUE",value
                list_val[key] = ''
        print "########## list_val",list_val
        return list_val 

    
    def _listclass(self,grade_id):
        listclass=[]
        
      #  students = self.env['school.student'].search([('grade_id','=',grade_id.id)])
        #print "################################################ students",students
       # class_ids= students.mapped('class_id')  
       #listclass.append(class_ids.name)
       # print "################################################ list class",listclass
      #  return listclass
        student_list = []
        #data = self.get_student_data()
        student_obj = self.env['student.list.wizard'].search([('grade_id.name','=',grade_name)])
        students = student_obj.mapped('student_ids')   
        for student in students:
            student_list.append(student)
        student_exemple = student_list[0]
        for student in student_list:
            if not(student.class_id in listclass):
                listclass.append(student.class_id)
        return listclass

    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        no_student = True
        student_id = None
        class_list = []
        if used_context['student_ids'] and len(used_context['student_ids']):
            no_student = False
            student_id = self.env['school.student'].browse(used_context['student_ids'])
            for student in student_id:
                if not(student.class_id.name in class_list):
                    class_list.append(student.class_id.name)
           
        grade_id = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        grade_id = self.env['school.grade'].search([('id','=',grade_id[0])])

        class_id = 'class_id' in data['form'] and data['form']['class_id'] or False

        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            'docs': student_id,
            'no_student': no_student,
            'time': time,
            'student_information': self._get_student_val,
            'class_id':class_id,
            'grade_id':grade_id,
            'list_class':class_list,
        }
        return self.env['report'].render('genext_school.report_studentlist', docargs)
