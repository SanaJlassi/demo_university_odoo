# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
from odoo import fields
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)

class SchoolExam(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/exams', type='http', auth="user", methods=['GET'],  csrf=False)
    def exams(self, **fields):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        student_id = request.env['school.student'].search([('user_id.id','=',request.env.user.id)])
        _logger.warning("Profile student %s",len(student_id))
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        #exam_id = request.env['school.exam'].search(['&',('teacher_id','=',teacher_id.id),('state','=','approved')])
        exam_id = request.env['school.exam.note'].search([('class_id','=',student_id.class_id.id)])
        _logger.warning('### EXAM ID :  %s', exam_id)
        exam_list = []
        for exam in exam_id:
            exam_json = {
                'id':exam.id,
                'subject':{
                    'id':exam.subject_id.id,
                    'name':exam.subject_id.name,
                    'code':exam.subject_id.code,
                },
                'grade':{
                    'id':exam.grade_id.id,
                    'name':exam.grade_id.name,
                    'code':exam.grade_id.code,
                },
                'classe':{
                    'id':exam.class_id.id,
                    'name':exam.class_id.name,
                    'code':exam.class_id.code,
                },
                'academic_year_id': exam.academic_year.id,
                'timing_system_periode_id': exam.timing_system_periode_id.id,
                'exam_lines': self._load_exam_lines_as_json(exam, 'student', student_id),
                'exam_moyenne_lines': self._load_exam_moyenne_lines_as_json(exam, 'student', student_id),
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

    @http.route('/api/exams/class/<int:class_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def exams_by_class(self, class_id, **fields):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        teacher_id = request.env['hr.employee'].search([('partner_id','=',request.env.user.partner_id.id)])
        if not teacher_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Teacher found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        #exam_id = request.env['school.exam'].search(['&',('teacher_id','=',teacher_id.id),'&',('class_id','=',class_id),('state','=','approved')])
        
        exam_id = request.env['school.exam.note'].search(['&',('teacher_id','=',teacher_id.id),('class_id','=',class_id)])
       # exam_id = request.env['school.exam.subject.config'].search([('class_id','=',class_id)])
        exam_list = []
        for exam in exam_id:
            exam_json = {
                'id':exam.id,
                'subject':{
                    'id':exam.subject_id.id,
                    'name':exam.subject_id.name,
                    'code':exam.subject_id.code,
                },
                'grade':{
                    'id':exam.grade_id.id,
                    'name':exam.grade_id.name,
                    'code':exam.grade_id.code,
                },
                'classe':{
                    'id':exam.class_id.id,
                    'name':exam.class_id.name,
                    'code':exam.class_id.code,
                },
                'academic_year_id': exam.academic_year.id,
                #'timing_system_periode_id': exam.timing_system_periode_id.id,
                #'exam_lines': self._load_exam_lines_as_json(exam, 'teacher', teacher_id),
                #'exam_moyenne_lines': self._load_exam_moyenne_lines_as_json(exam, 'teacher', teacher_id),
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 

    # returns a list of exam lines dict 
    def _load_exam_lines_as_json(self, exam, user_type, user_id):
        exam_line = []
        for line in exam.exam_line_ids:
            #print "EXAM LINE ",line.name
            if line.state == 'approved':
                exam_line.append({
                    'id':line.id, 
                    'name':line.name, 
                    'type':line.exam_type.id, 
                    'session': line.exam_session, 
                    'from_date': line.from_date, 
                    'to_date': line.to_date,
                    'description': line.description,
                    'scale': line.scale ,
                    'exam_notes_line_ids': self._load_exam_line_notes_as_json(line, user_type, user_id),
                    })
        return exam_line

    # returns a list of exam moyennes lines dict 
    def _load_exam_moyenne_lines_as_json(self, exam, user_type, user_id):
        moyenne_line = []
        if user_type == 'teacher':
            for line in exam.exam_moyenne_ids:
                moyenne_line.append({
                    'id':line.id, 
                    #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'moyenne': line.moyenne, 
                    'result': line.result, 
                    })
        if user_type == 'student':
            for line in exam.exam_moyenne_ids:
                if line.student_id.id == user_id.id:
                    moyenne_line.append({
                    'id':line.id, 
                    #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'moyenne': line.moyenne, 
                    'result': line.result, 
                    })

        return moyenne_line


    # returns a list of exam lines marks dict 
    def _load_exam_line_notes_as_json(self, line, user_type, user_id):
        exam_line_note = []
        if user_type == 'teacher':
            for i in line.exam_notes_line_ids:
                exam_line_note.append({
                    'id':i.id, 
                    'student_id':i.student_id.id, 
                    'name': i.student_id.name,
                    'last_name': i.student_id.last_name,
                    'note': i.note, 
                    'comment': i.comment, 
                    })
        if user_type == 'student':
            for i in line.exam_notes_line_ids:
                if i.student_id.id == user_id.id:
                    exam_line_note.append({
                    'id':i.id, 
                    'student_id':i.student_id.id, 
                    'name': i.student_id.name,
                    'last_name': i.student_id.last_name,
                    'note': i.note, 
                    'comment': i.comment, 
                    })

        return exam_line_note

    @http.route('/api/exams/<int:exam_line_id>/notes', type='http', auth="user", methods=['POST'],  csrf=False)
    def exams_note(self, exam_line_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
                       
        teacher = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        _logger.warning('### DATAAAAAAA :  (%s).', request.params.get('notes',False))       
        json_object = None
        if not request.params.get('notes',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'notes object not submited'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        exam_line = request.env['school.exam.note'].search([('&',('teacher_id','=',teacher_id.id),'id','=',exam_line_id)])
        if not exam_line:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No exam found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        json_object = None
        try:
            json_object = json.loads(request.params.get('notes',False))
            _logger.warning('###  JSON-OBJECT  :  %s', json_object)
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize notes object'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        # if self._validate_object(json_object, response)['valide'] == False:
        #     response.pop('valide', None)
        #     return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
        # else:
        #     response.pop('valide', None)

        _logger.warning('### JSON-OBJECT  :  (%s).', json_object)
        list_note = []
        for el in json_object:
            _logger.warning('### JSON-OBJECT el :  (%s).', el)
            rec = request.env['school.exam.note.line'].browse(el['id'])
            res = rec.write({'note':el['note'],'comment':el['comment']})
            if not res:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Error while updating note'} 
                return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
            else:
                response['success'] = True
                response['data'] = False


        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)




    @http.route('/api/exams/student/<int:student_id>/notes', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_exams_by_student(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)  

        print "######## EXAM student name ", student_id.name
        print "######## EXAM student last_name", student_id.last_name
        

        exam_line_ids = request.env['school.exam.note'].search([('class_id','=',student_id.class_id.id)])
        _logger.warning('####### EXAM IDS :  %s', exam_line_ids)

        exam_list = []
        for exam in exam_line_ids:
            exam_json = {
                'id':exam.id,                
                'academic_year': {
                    'id':exam.academic_year.id,
                    'name':exam.academic_year.name,
                    'code':exam.academic_year.code,
                },
                'session': {
                    'id':exam.periode_config_id.id,
                    'name':exam.periode_config_id.name,
                },
                'periode': {
                    'id':exam.periode_ids.id,
                    'name':exam.periode_ids.name,
                },
                'grade':{
                    'id':exam.grade_id.id,
                    'name':exam.grade_id.name,
                    'code':exam.grade_id.code,
                },
                'classe':{
                    'id':exam.class_id.id,
                    'name':exam.class_id.name,
                    'code':exam.class_id.code,
                },                
                'subject':{
                    'id':exam.subject_id.id,
                    'name':exam.subject_id.name,
                    'code':exam.subject_id.code,
                },
                'teacher':{
                    'id':exam.teacher_id.id,
                    'name':exam.teacher_id.name,
                    'last_name':exam.teacher_id.last_name,
                },
                'exam_type':{
                    'id':exam.exam_type_id.id,
                    'name':exam.exam_type_id.type,
                    'coefficient':exam.exam_type_id.coefficient,
                },
                'date': exam.date,
                'scale': exam.scale,
                'exam_note': self._load_exam_note_as_json(exam, 'student', student_id),
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 



    @http.route('/api/exams/teacher/<int:class_id>/notes', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_exams_by_class(self, class_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id

        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
                       
        teacher_id = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)         

        exam_line_ids = request.env['school.exam.note'].search([('class_id','=',class_id.id), ('teacher_id','=',teacher_id.id)])
        _logger.warning('####### EXAM IDS :  %s', exam_line_ids)

        exam_list = []
        for exam in exam_line_ids:
            exam_json = {
                'id':exam.id,                
                'academic_year': {
                    'id':exam.academic_year.id,
                    'name':exam.academic_year.name,
                    'code':exam.academic_year.code,
                },
                'session': {
                    'id':exam.periode_config_id.id,
                    'name':exam.periode_config_id.name,
                },
                'periode': {
                    'id':exam.periode_ids.id,
                    'name':exam.periode_ids.name,
                },
                'grade':{
                    'id':exam.grade_id.id,
                    'name':exam.grade_id.name,
                    'code':exam.grade_id.code,
                },
                'classe':{
                    'id':exam.class_id.id,
                    'name':exam.class_id.name,
                    'code':exam.class_id.code,
                },                
                'subject':{
                    'id':exam.subject_id.id,
                    'name':exam.subject_id.name,
                    'code':exam.subject_id.code,
                },
                'teacher':{
                    'id':exam.teacher_id.id,
                    'name':exam.teacher_id.name,
                    'last_name':exam.teacher_id.last_name,
                },
                'exam_type':{
                    'id':exam.exam_type_id.id,
                    'name':exam.exam_type_id.type,
                    'coefficient':exam.exam_type_id.coefficient,
                },
                'date': exam.date,
                'scale': exam.scale,
                'exam_note': self._load_exam_note_as_json(exam, 'teacher', teacher_id),
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 


    # returns a list of exam note dict 
    def _load_exam_note_as_json(self, exam, user_type, user_id):
        note_line = []
        if user_type == 'teacher':
            for line in exam.exam_notes_line_ids:
                note_line.append({
                    'id':line.id, 
                    #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'note': line.note,
                    # 'comment': line.comment, 
                    })
        if user_type == 'student':
            for line in exam.exam_notes_line_ids:
                if line.student_id.id == user_id.id:
                    note_line.append({
                    'id':line.id, 
                    #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'note': line.note,
                    'comment': line.comment, 
                    })

        return note_line

    #####periods_id is the period of exam not the period of academic_year
    @http.route('/api/exams/teacher/<int:class_id>/notes/academic_year/<int:academic_year_id>/period_ids/<int:periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_exams_by_class_academic_year_period(self, class_id,academic_year_id,periode_id,**kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id

        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
                       
        teacher_id = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)         
        
     
        exam_line_ids = request.env['school.exam.note'].search([('class_id','=',class_id.id),('teacher_id','=',teacher_id.id),('academic_year.id','=',academic_year_id),('periode_ids.id','=',periode_id)])
        _logger.warning('####### EXAM IDS :  %s', exam_line_ids)

        exam_list = []
        for exam in exam_line_ids:
            exam_json = {
                'id':exam.id,                
                'academic_year': {
                'id':exam.academic_year.id,
                'name':exam.academic_year.name,
                'code':exam.academic_year.code,
                },
                'session': {
                'id':exam.periode_config_id.id,
                'name':exam.periode_config_id.name,
                 },
                'periode': {
                'id':exam.periode_ids.id,
                'name':exam.periode_ids.name,
                 },
                'grade':{
                'id':exam.grade_id.id,
                'name':exam.grade_id.name,
                'code':exam.grade_id.code,
                },
                'classe':{
                'id':exam.class_id.id,
                'name':exam.class_id.name,
                'code':exam.class_id.code,
                },                
                'subject':{
                'id':exam.subject_id.id,
                'name':exam.subject_id.name,
                'code':exam.subject_id.code,
                 },
                'teacher':{
                'id':exam.teacher_id.id,
                'name':exam.teacher_id.name,
                'last_name':exam.teacher_id.last_name,
                },
                'exam_type':{
                'id':exam.exam_type_id.id,
                'name':exam.exam_type_id.type,
                'coefficient':exam.exam_type_id.coefficient,
                },
                'date': exam.date,
                'scale': exam.scale,
                'exam_note': self._load_exam_note_as_json(exam, 'teacher', teacher_id),
            }
            exam_list.append(exam_json)
        response['data'] = exam_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 


        # returns a list of exam note dict 
    def _load_exam_note_as_json(self, exam, user_type, user_id):
        note_line = []
        if user_type == 'teacher':
            for line in exam.exam_notes_line_ids:
                note_line.append({
                    'id':line.id, 
                        #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'note': line.note,
                        # 'comment': line.comment, 
                    })
        if user_type == 'student':
            for line in exam.exam_notes_line_ids:
                if line.student_id.id == user_id.id:
                    note_line.append({
                    'id':line.id, 
                        #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'note': line.note,
                    'comment': line.comment, 
                    })

        return note_line


    @http.route('/api/exams/student/<int:student_id>/appreciation', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_module_appreciation_by_student(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)  

        appreciation_line_ids = request.env['exam.appreciation.module'].search([('class_id','=',student_id.class_id.id)])
        _logger.warning('####### EXAM APPRECIATION IDS :  %s', appreciation_line_ids)

        appreciation_list = []
        for appreciation in appreciation_line_ids:
            appreciation_json = {
                'id':appreciation.id,                
                'academic_year': {
                    'id':appreciation.academic_year.id,
                    'name':appreciation.academic_year.name,
                    'code':appreciation.academic_year.code,
                },
                'session': {
                    'id':appreciation.periode_config_id.id,
                    'name':appreciation.periode_config_id.name,
                },
                'periode': {
                    'id':appreciation.periode_ids.id,
                    'name':appreciation.periode_ids.name,
                },
                'grade':{
                    'id':appreciation.grade_id.id,
                    'name':appreciation.grade_id.name,
                    'code':appreciation.grade_id.code,
                },
                'classe':{
                    'id':appreciation.class_id.id,
                    'name':appreciation.class_id.name,
                    'code':appreciation.class_id.code,
                },                
                'module':{
                    'id':appreciation.module_id.id,
                    'name':appreciation.module_id.name,
                },
                'module_comment': self._load_comment_line_as_json(appreciation, 'student', student_id),
                'module_average': self._load_average_line_as_json(appreciation, 'student', student_id),
            }
            appreciation_list.append(appreciation_json)
        response['data'] = appreciation_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 


    def _load_comment_line_as_json(self, appreciation, user_type, user_id):
        comment_line = []
        # if user_type == 'teacher':
        #     for line in exam.exam_moyenne_ids:
        #         moyenne_line.append({
        #             'id':line.id, 
        #             #'name':line.name, 
        #             'student_id':line.student_id.id, 
        #             'moyenne': line.moyenne, 
        #             'result': line.result, 
        #             })
        if user_type == 'student':
            for line in appreciation.comment_line_ids:
                if line.student_id.id == user_id.id:
                    comment_line.append({
                    'id':line.id, 
                    'student_id':line.student_id.id, 
                    'comment': line.comment,
                    })

        return comment_line

    def _load_average_line_as_json(self, appreciation, user_type, user_id):
        average_line = []
        # if user_type == 'teacher':
        #     for line in exam.exam_moyenne_ids:
        #         moyenne_line.append({
        #             'id':line.id, 
        #             #'name':line.name, 
        #             'student_id':line.student_id.id, 
        #             'moyenne': line.moyenne, 
        #             'result': line.result, 
        #             })
        if user_type == 'student':
            for line in appreciation.module_average_line_ids:
                if line.student_id.id == user_id.id:
                    average_line.append({
                    'id':line.id, 
                    #'name':line.name, 
                    'student_id':line.student_id.id, 
                    'average': line.average,
                    })

        return average_line



    @http.route('/api/exams/student/<int:student_id>/council', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_appreciation_council_by_student(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)  

        council_line_ids = request.env['exam.appreciation.council'].search([('class_id','=',student_id.class_id.id)])
        _logger.warning('####### EXAM APPRECIATION IDS :  %s', council_line_ids)

        council_list = []
        for council in council_line_ids:
            council_json = {
                'id':council.id,                
                'academic_year': {
                    'id':council.academic_year.id,
                    'name':council.academic_year.name,
                    'code':council.academic_year.code,
                },
                'session': {
                    'id':council.periode_config_id.id,
                    'name':council.periode_config_id.name,
                },
                'periode': {
                    'id':council.periode_ids.id,
                    'name':council.periode_ids.name,
                },
                'grade':{
                    'id':council.grade_id.id,
                    'name':council.grade_id.name,
                    'code':council.grade_id.code,
                },
                'classe':{
                    'id':council.class_id.id,
                    'name':council.class_id.name,
                    'code':council.class_id.code,
                },
                'comment_line': self._load_comment_council_as_json(council, 'student', student_id),
                'average_line': self._load_average_council_as_json(council, 'student', student_id),
            }
            council_list.append(council_json)
        response['data'] = council_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 


    def _load_comment_council_as_json(self, council, user_type, user_id):
        comment_line = []
        if user_type == 'student':
            for line in council.comment_line_ids:
                if line.student_id.id == user_id.id:
                    comment_line.append({
                    'id':line.id, 
                    'student_id':line.student_id.id, 
                    'comment': line.comment,
                    })

        return comment_line

    def _load_average_council_as_json(self, council, user_type, user_id):
        average_line = []
        if user_type == 'student':
            for line in council.period_average_line_ids:
                if line.student_id.id == user_id.id:
                    average_line.append({
                    'id':line.id, 
                    'student_id':line.student_id.id, 
                    'average': line.average,
                    })

        return average_line

    

    @http.route('/api/exams/teacher/<int:class_id>/sessions', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_exam_sessions(self, class_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)  

        print "######## EXAM class name ", class_id.name
        
        grade_id                = class_id.grade_id
        educational_stage_id    = grade_id.educational_stage_id
        academic_year           = request.env['academic.year'].search([('active_year','=',True)])
        print "########## GRADE ID", grade_id.name
        print "########## EDUCATIONAL STAGE ID",educational_stage_id.name
        print "########## ACADEMIC YEAR ID",academic_year.name

        session_ids = request.env['school.exam.periode.config'].search([('educational_stage_id','=',educational_stage_id.id),('academic_year','=',academic_year.id)])
        print "########## SESSION IDS",session_ids

        sessions_list = []
        for session in session_ids:
            object_json = {
                'id':session.id,  
                'name':session.name,              
                'academic_year': {
                    'id':session.academic_year.id,
                    'name':session.academic_year.name,
                    'code':session.academic_year.code,
                },
            }
            sessions_list.append(object_json)
        response['data'] = sessions_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 
    @http.route('/api/exams/teacher/<int:class_id>/sessions/academic_year/<int:academic_year_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_exam_sessions_byacademic_year(self, class_id,academic_year_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)  

        print "######## EXAM class name ", class_id.name
        
        grade_id                = class_id.grade_id
        educational_stage_id    = grade_id.educational_stage_id
        academic_year           = request.env['academic.year'].search([('id','=',academic_year_id)])
        print "########## GRADE ID", grade_id.name
        print "########## EDUCATIONAL STAGE ID",educational_stage_id.name
        print "########## ACADEMIC YEAR ID",academic_year.name

        session_ids = request.env['school.exam.periode.config'].search([('educational_stage_id','=',educational_stage_id.id),('academic_year','=',academic_year.id)])
        print "########## SESSION IDS",session_ids

        sessions_list = []
        for session in session_ids:
            object_json = {
                'id':session.id,  
                'name':session.name,              
                'academic_year': {
                    'id':session.academic_year.id,
                    'name':session.academic_year.name,
                    'code':session.academic_year.code,
                },
            }
            sessions_list.append(object_json)
        response['data'] = sessions_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

    @http.route('/api/exams/teacher/<int:session_id>/periods', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_exam_periods(self, session_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        session_id = request.env['school.exam.periode.config'].browse(session_id)
        if not session_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Session found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)  

        print "######## EXAM Session name ", session_id.name

        period_ids = request.env['school.exam.periode'].search([('periode_config_id','=',session_id.id)])
        print "########## PERIOD IDS",period_ids

        periods_list = []
        for period in period_ids:
            object_json = {
                'id':period.id,  
                'name':period.name,    
                'code':period.code,
                'start_date':period.start_date,
                'end_date':period.end_date,   
            }
            periods_list.append(object_json)
        response['data'] = periods_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 

    @http.route('/api/exams/teacher/<int:class_id>/subjects', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_exam_subjects(self, class_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)  

        print "######## EXAM class name ", class_id.name
        grade_id    = class_id.grade_id
        subjects_list = []
        for subject in grade_id.subject_ids:
            object_json = {
                'id':subject.id,  
                'name':subject.name,    
                'code':subject.code,
                'coefficient':subject.coefficient,
            }
            subjects_list.append(object_json)
        response['data'] = subjects_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 

    @http.route('/api/exams/teacher/<int:class_id>/types', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_exam_types(self, class_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)  

        print "######## EXAM class name ", class_id.name
        grade_id                = class_id.grade_id
        educational_stage_id    = grade_id.educational_stage_id

        exam_type_ids = request.env['school.exam.type'].search([('educational_stage_id','=',educational_stage_id.id)])
        exam_type_list = []
        for exam_type in exam_type_ids:
            object_json = {
                'id':exam_type.id,  
                'name':exam_type.type,  
                'coefficient':exam_type.coefficient,
                'scale': exam_type.scale,
            }
            exam_type_list.append(object_json)
        response['data'] = exam_type_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolExam.HEADER) 


    @http.route('/api/exams/class/<int:class_id>/notes', type='http', auth="user", methods=['POST'],  csrf=False)
    def exams_note(self, class_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
                       
        teacher_id = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        _logger.warning('### DATAAAAAAA :  (%s).', request.params.get('notes',False))       
        json_object = None
        if not request.params.get('notes',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'notes object is not submited'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        class_id                = request.env['school.class'].browse(class_id)
        grade_id                = class_id.grade_id
        educational_stage_id    = grade_id.educational_stage_id
        academic_year           = request.env['academic.year'].search([('active_year','=',True)])
        
        
        try:
            json_object = json.loads(request.params.get('notes',False))
            _logger.warning('############  JSON-OBJECT  :  %s', json_object)
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize notes object'} 
            return http.request.make_response(json.dumps(response),SchoolExam.HEADER)

        # list_note = []
        for el in json_object:
            _logger.warning('############# JSON-OBJECT el :  (%s).', el)       


        note_object = {
            'academic_year':academic_year.id,
            'educational_stage_id':educational_stage_id.id,
            'periode_config_id':json_object['periode_config_id'],
            'periode_ids':json_object['periode_ids'],
            'grade_id':grade_id.id,
            'class_id':class_id.id,
            'subject_id':json_object['subject_id'],
            'teacher_id':teacher_id.id,
            'exam_type_id':json_object['exam_type_id'],
            'date':fields.Date.today(),
            'scale':json_object['scale'],
        }
    
        # note_ids = request.env['school.exam.note'].search([('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',json_object['periode_config_id']),('periode_ids','=',json_object['periode_ids']),('grade_id','=',grade_id.id),('class_id','=',class_id.id)
        
        context = request.env.context.copy()
        context['source'] = 'controller'
        request.env.context = context    
        note_object = request.env['school.exam.note'].create(note_object)

        
        if note_object:
            response['success'] = True
            response['data'] = False
        else: 
            response['success'] = False
            response['data'] = False

        note_list = []
        for el in json_object['exam_notes_line_ids']:
            note_list.append((0,0,el))

        res = note_object.write({'exam_notes_line_ids':note_list})

        if res:
            response['success'] = True
            response['data'] = False
        else: 
            response['success'] = False
            response['data'] = False

        #     rec = request.env['school.exam.note.line'].browse(el['id'])
        #     res = rec.write({'note':el['note'],'comment':el['comment']})
        #     if not res:
        #         response['success'] = False
        #         response['error'] = {'code':404, 'message':'Error while updating note'} 
        #         return http.request.make_response(json.dumps(response),SchoolExam.HEADER)
        #     else:
        #         response['success'] = True
        #         response['data'] = False


        return http.request.make_response(json.dumps(response),SchoolExam.HEADER)