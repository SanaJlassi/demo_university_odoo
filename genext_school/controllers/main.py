# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import models, fields, api
from odoo.http import content_disposition, dispatch_rpc, request
import json
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class SchoolProfile(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_GET = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    @http.route('/api/user/token', type='http', auth="user", methods=['POST','GET'],  csrf=False)
    def update_token(self, **kwargs):
        response = {'success':False, 'data':None}
        if request.httprequest.method == 'GET':
            print "################## GET Token ",request.env.user.partner_id.token
            data = {'token':request.env.user.partner_id.token}
            response['success'] = True
            response['data'] = data
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER_GET)
        else:
            print "################## PRE POST Token ", request.params.get('token',False)

            if not 'add' in request.params:
                print "##### ADD NOT IN PARAMS"
                response['success'] = False
                response['error'] = {'code':404, 'message':'Missing parameter init'} 
                return http.request.make_response(json.dumps(response),SchoolProfile.HEADER_GET)

            if not 'token' in request.params:
                print "##### TOKEN NOT IN PARAMS"
                response['success'] = False
                response['error'] = {'code':404, 'message':'Missing parameter token'} 
                return http.request.make_response(json.dumps(response),SchoolProfile.HEADER_GET)


            print "################## POST INIT ", request.params.get('add',False)
            print "################## POST POSTToken ", request.params.get('token',False)

            add = request.params.get('add',False)
            token = request.params.get('token',False)
            _logger.warning(' ###### ADD %s',add)
            _logger.warning(' ###### TOKEN %s',token)
            res = False
            if add == 'true':
                partner_id = request.env.user.partner_id
                res = partner_id.write({'token_ids':[(0,0,{'token':token,'partner_id':partner_id.id})]})
                _logger.warning('##### ADDING A TOKEN %s',res)
                if not res:
                    response['success'] = False
                    response['error'] = {'code':404, 'message':'Error while adding token (must be unique)'} 
                    return http.request.make_response(json.dumps(response),SchoolProfile.HEADER_GET)
            else:
                partner_id = request.env.user.partner_id
                for rec in partner_id.token_ids:
                    if rec.token == token:
                        res = rec.unlink()
                        _logger.warning('##### DELETING A TOKEN %s',res)
                        if not res:
                            response['success'] = False
                            response['error'] = {'code':404, 'message':'Error while deleting token'} 
                            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER_GET)

            if res:
                response['success'] = True
            else:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Error occured while update user token'} 
            response['success'] = True
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER_GET)

    
    @http.route('/api/<string:user_type>/profile', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_profile(self, user_type, **fields):
        ''' 
            No need to send the user id coz it's accessible from request.env.user.id since the user is already logged in 
            we only need the user type in order to find the related object [ res.partner: parent, school.student: student, hr.employee: teacher]
        '''
        response = {'success':False, 'data':None}

        if user_type == 'teacher':
            print "### TEACHER"
            return self.get_teacher_details(request.env.user.id)
        elif user_type == 'parent':
            print "### PARENT"
            return self.get_parent_details(request.env.user.partner_id.id)
        elif user_type == 'student':
            return self.get_student_details(request.env.user.id)
        else:
            print "check if this user is the admin using the method is superuser otherwise it's an error the specified user type unrecognized"

        return http.request.make_response(json.dumps({'GET':request.env.user.id}),SchoolProfile.HEADER)



    @http.route('/api/<string:user_type>/profile', type='http', auth="user", methods=['POST'],  csrf=False)
    def update_profile(self, user_type, **fields):
        ''' 
            No need to send the user id coz it's accessible from request.env.user.id since the user is already logged in 
            we only need the user type in order to find the related object [ res.partner: parent, school.student: student, hr.employee: teacher]
        '''
        response = {'success':False, 'data':None}

        if user_type == 'teacher':
            print "USER TYPE TEACHER"
            return self.update_teacher_details()
        elif user_type == 'parent':
            print "### PARENT"
            return self.get_parent_details(request.env.user.partner_id.id)
        elif user_type == 'student':
            pass
        else:
            print "check if this user is the admin using the method is superuser otherwise it's an error the specified user type unrecognized"

        return http.request.make_response(json.dumps({'POST':request.env.user.id}),SchoolProfile.HEADER)
    
    def _get_classes(self,user_id):
        class_list = []
        time_string = datetime.now().time().strftime('%H:%M:%S')
        current_time = datetime.strptime(datetime.now().time().strftime('%H:%M:%S'), '%H:%M:%S') # [time] from string to object
        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        for session in request.env['time.table.session'].search([('time_table_id.academic_year','=',current_academic_year.id)]):# <--- check current year and timing session period here
            start_period = str(datetime.now().year)+'-'+session.timing_periode.start_date_month
            end_period = str(datetime.now().year)+'-'+session.timing_periode.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                print "#### CURRENT PERIODE ",session.timing_periode.name
                # try to check day of week and teacher first to avoid unnecessary further test of datetime
                if session.teacher_id.id == user_id.id: # user.id is the hr.employee related to logged user
                    session_start_time = datetime.strptime(session.start_time,'%H:%M:%S') 
                    session_end_time = datetime.strptime(session.end_time,'%H:%M:%S')
                    print "SESSION START TIME",session_start_time
                    print "CURRENT TIME ",current_time
                    print "SESSION END TIME ",session_end_time
                    json_class = {
                        'id':session.class_id.id,
                        'name':session.class_id.name,
                        'grade_name':session.class_id.grade_id.name,
                        'educational_stage':session.class_id.grade_id.educational_stage_id.name,
                    }
                    print "### class object ",json_class
                    add = True
                    for item in class_list:
                        if item['id'] == json_class['id']:
                            add = False
                            break
                    if add:
                        class_list.append(json_class)
            else:
                print "#### NOT CURRENT PERIODE ",session.timing_periode.name
            print "--------------------------------"
        return class_list
    def get_skill_teacher(self,teacherId):
        response = {'success':False, 'data':None}
        if not teacherId:
            response['success'] = False
            response['error'] = {'code':400, 'message':'Missing parameter teacherId'}
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
        else:
            record = request.env['hr.employee'].sudo().search(['&',('is_school_teacher','=',True),('user_id.id','=',teacherId)])
            if len(record) != 1:
                response['success'] = False
                response['error'] = {'code':400, 'message':'No teacher found with the given ID'}  
                return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
            skill_list=[]
            for skill in record.skill_teachers_ids:
                skill_obj=  {'competence':skill.skill_teacher.name,
                      'categorie':skill.options,
                      'id':skill.id,
                         }
                skill_list.append(skill_obj)
            return skill_list
    def get_region_teacher(self,teacherId):
        response = {'success':False, 'data':None}
        if not teacherId:
            response['success'] = False
            response['error'] = {'code':400, 'message':'Missing parameter teacherId'}
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
        else:
            record = request.env['hr.employee'].sudo().search(['&',('is_school_teacher','=',True),('user_id.id','=',teacherId)])
            if len(record) != 1:
                response['success'] = False
                response['error'] = {'code':400, 'message':'No teacher found with the given ID'}  
                return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
            region_list=[]
            for region in record.region_ids:
                region_obj=  {'region':region.name,
                      'city':region.city,
                      'id':region.id,
                         }
                region_list.append(region_obj)
            return region_list



    def get_teacher_details(self, teacherId, **fields):
        response = {'success':False, 'data':None}
        if not teacherId:
            response['success'] = False
            response['error'] = {'code':400, 'message':'Missing parameter teacherId'}
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
        else:
            record = request.env['hr.employee'].sudo().search(['&',('is_school_teacher','=',True),('user_id.id','=',teacherId)])
            if len(record) != 1:
                response['success'] = False
                response['error'] = {'code':400, 'message':'No teacher found with the given ID'}  
                return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
            user = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])   
            domain = [
                    ('res_model', '=', record._name),
                    ('res_field', '=', 'image'),
                    ('res_id', '=',record.id),
                ]
            attachment = request.env['ir.attachment'].search(domain)
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+'?session_id='+request.env['ir.http'].session_info()['session_id']
            url = url.replace(" ", "_")
            _logger.warning('#### URL of document %s',url)
            teacher = {
                'id': record.id,
                'pid':record.pid,
                'login': record.user_id.login,
                'user_type':record.user_type,
                'name':record.name,
                'last_name':record.last_name,
                'work_email': record.work_email,
                'gender': record.gender,
                'last_login': record.last_login,
                'work_phone':record.work_phone,
                'mobile_phone': record.mobile_phone,
                'school_id':{
                    'name':record.school_id.name,
                    },
                #'image': record.image,
                'imageurl':url,
                'class':self._get_classes(user),
                'regions':self.get_region_teacher(teacherId),
                'competences_enseignant':self.get_skill_teacher(teacherId),}
            response['success'] = True
            response['data'] = teacher
        return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

    def update_teacher_details(self):
        response = {'success':False, 'data':None}
        if not request.params.get('profile',False):
            response['success'] = False
            response['error'] = {'code':400, 'message':'Object profile not found'}  
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
        teacher_json = None
        try:
            teacher_json = json.loads(request.params.get('profile',True))
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':400, 'message':'unable to deserialize the object'}  
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
        teacher = {
            'login':teacher_json['login'],
            'name':teacher_json['name'],
            'last_name':teacher_json['last_name'],
            'work_email': teacher_json['work_email'],
            'gender': teacher_json['gender'],
            'work_phone':teacher_json['work_phone'],
            'mobile_phone': teacher_json['mobile_phone'],
            'image': teacher_json['image'],
        }
        record = request.env['hr.employee'].sudo().search([('user_id.id','=', request.env.user.id)])
        result = record.write(teacher)
        print "UPDATING TEACHER RESULT ",result
        if result:
            response['success'] = True
            response['data'] = None
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
        else:
            response['success'] = False
            response['error'] = {'code':400, 'message':'error while trying to update teacher'}  
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

    def get_parent_details(self, parent_id):
        response = {'success':False, 'data':None}
        print "PARENT ID ",parent_id
        if not parent_id:
            response['success'] = False
            response['error'] = {'code':400, 'message':'Missing parameter parentId'}
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
        else:
            record = request.env['res.partner'].sudo().search([('id','=',parent_id)])
            print "LEN ",record
            if len(record) != 1:
                response['success'] = False
                response['error'] = {'code':400, 'message':'No teacher found with the given ID'}  
                return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
            
            child_list = []
            for child in record.student_ids:
                child_json = {
                                'id':child.id,
                                'name':child.name,
                                'last_name':child.last_name,
                                'image':child.image,
                                'class':{
                                    'id':child.class_id.id,
                                    'name':child.class_id.name,
                                },
                                'grade':{
                                    'id':child.grade_id.id,
                                    'grade_name':child.grade_id.name,
                                    'educational_stage_name':child.grade_id.educational_stage_id.name,
                                }
                            }
                child_list.append(child_json)




            domain = [
                    ('res_model', '=',record._name),
                    ('res_field', '=', 'image'),
                    ('res_id', '=',record.id),
                ]
            attachment = request.env['ir.attachment'].search(domain)
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'
            url = url.replace(" ", "_")
            _logger.warning('#### URL of document %s',url)

            parent = {
                'id': record.id,
                'name':record.name,
                'last_name': record.last_name,
                'cin': record.cin,
                'street': record.street,
                'street2': record.street2,
                'zip':record.zip,
                'city': record.city,
                'phone': record.phone,
                'mobile': record.mobile,
                #'image': record.image,
                'imageurl':url,
                'phone': record.phone,
                'childrens':child_list,
            }
            response['success'] = True
            response['data'] = parent
        return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

    def updateTeacher(self, teacherId, **fields):
        response = {'success':False, 'data':None}
        print "TEACHER ID ",teacherId
        print "FIELDS ",fields
        if request.params.get('teacherId',False):
            teacher_id = request.params.get('teacherId')
            record = request.env['hr.employee'].sudo().search(['&',('is_school_teacher','=',True),('user_id.id','=',teacher_id)])
            fieldKeys = record._fields.keys()
            teacher = False
            if isinstance(fields,dict) and len(fields):
                for k,v in fields.items():
                    if k not in fieldKeys:
                        response['success'] = False
                        response['error'] = {'code':404, 'message':'Invalide field name ['+k+']'}
                        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
                record.write(fields)
                teacher = record.read()
            if len(teacher):
                response['data'] = teacher
                response['success'] = True
            else:
                response['success'] = False
                response['error'] = {'code':404, 'message':'teacherId does not exist'}    
        else:
            response['success'] = False
            response['error'] = {'code':400, 'message':'Missing parameter teacherId'}

        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
    def get_student_details(self, user_id):
        response = {'success':False, 'data':None}
        #print "Student ID ",user_id
        _logger.warning("#### Student ID %s",user_id)
        if not user_id:
            response['success'] = False
            response['error'] = {'code':400, 'message':'Missing parameter parentId'}
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
        else:
            student_id = request.env['school.student'].search([('user_id.id','=',user_id)])
            # record = request.env['res.partner'].sudo().search([('id','=',parent_id)])
            _logger.warning("Profile student %s",len(student_id))
            if len(student_id) != 1:
                response['success'] = False
                response['error'] = {'code':400, 'message':'No Student found with the given ID'}  
                return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

            domain = [
                ('res_model', '=', student_id.user_id.partner_id._name),
                ('res_field', '=', 'image'),
                ('res_id', '=', student_id.user_id.partner_id.id),
            ]
            attachment = request.env['ir.attachment'].search(domain)
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+request.env['ir.http'].session_info()['session_id']

            student = {
                'id': student_id.id,
                'first_name':student_id.name,
                'last_name': student_id.last_name,
                #'cin': student_id.cin,
                'street': student_id.street_1,
                'street2': student_id.street_2,
                'zip':student_id.zip_code,
                'city': student_id.city,
                'phone': student_id.phone,
                'mobile': student_id.mobile,
                'image':url,
            }
            response['success'] = True
            response['data'] = student
        return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

    


    def old_getTeacherDetails(self, teacherId, **fields):
        response = {'success':False, 'data':None}
        if teacherId:
            records = request.env['hr.employee'].sudo().search(['&',('is_school_teacher','=',True),('user_id.id','=',teacherId)])
            fieldKeys = records._fields.keys()
            teacher = False
            if isinstance(fields,dict) and len(fields):
                fieldList = []
                for k,v in fields.items():
                    if k not in fieldKeys:
                        response['success'] = False
                        response['error'] = {'code':404, 'message':'Invalide field name ['+k+']'}
                        return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
                    else:
                        fieldList.append(k)
                print "FIELD LIST ",fieldList
                teacher = records.read(fieldList)
            else:
                # adding all fields to read to load the field image
                teacher = records.read(fieldKeys)
            if len(teacher):
                response['data'] = teacher
                response['success'] = True
            else:
                response['success'] = False
                response['error'] = {'code':404, 'message':'teacherId does not exist'}    
        else:
            response['success'] = False
            response['error'] = {'code':400, 'message':'Missing parameter teacherId'}

        return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

    @http.route('/api/user/update/password', type='http', auth="user", methods=['POST'],  csrf=False)
    def parent_update_password(self, **kwargs):
        response = {'success':False, 'data':None}
        try:
            json_object = json.loads(request.params.get('update_password',False))
            # print "JSON-OBJECT ",json_object
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize update_password object'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)


        if not 'new_password' in json_object:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Missing password '} 
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

        if not 'old_password' in json_object:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Missing old password '} 
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

        old_password = json_object['old_password']
        new_password = json_object['new_password']

        print "########### ---------- OLD PASSWORD ",old_password

        uid = request.env.user._login(request.env.cr.dbname, request.env.user.login, str(old_password))
        _logger.warning('##############----------############ UID %s',uid)

        if not uid:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Incorrect password'} 
            return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)

        user_id = request.env.user
        res = user_id.write({'password':new_password})
        if res:
            user_id.partner_id.write({'password':new_password})
        _logger.warning('#######################PASSWORD UPDATED################## %s',res)
        request.session = None
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolProfile.HEADER)
    


    @http.route('/api/login', type='http', methods=['POST'], auth="none",csrf=False)
    def web_login(self, **kw):
        print "KWARGS ",kw
        # d = json.loads(kw['data'])
        # print "### ",d['db']
        uid = request.session.authenticate(request.params['db'], request.params['login'], request.params['password'])
        request.session.uid = uid
        user = request.env['res.users'].sudo().search([('id','=',uid)])
        print "content of user UID ",uid

        responseData = {'success':False, 'data':None}
        if len(user):
            userData = {}
            userData['login'] = user.login
            userData['user_type'] = user.user_type
            userData['id'] = user.id
            userData['session_id'] = request.env['ir.http'].session_info()['session_id']
            responseData['data'] = userData
            responseData['success'] = True
        else:
            responseData['success'] = False
            responseData['error'] = {'code':401, 'message':'Authentification failed'}

        return http.request.make_response(json.dumps(responseData),SchoolProfile.HEADER)

        



        #cookies={'fileToken': token}

   