# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT



class SchoolReservation(http.Controller):

    HEADER_GET = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }




    @http.route('/api/reservation/<int:reservation_id>/cancel', type='http', auth="user", methods=['POST'],  csrf=False)
    def cancel_reservation(self, reservation_id, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        reservation = request.env['school.reservation'].search([('id','=',reservation_id)])
        if not reservation:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No reservation found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        
        # check if logged user is the parent of the student
        res_partner = request.env['res.partner'].search(['&',('id','=',request.env.user.partner_id.id),('is_school_parent','=',True)])
        if not res_partner.id in reservation.student_id.parent_ids.ids:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Only the parent of the student can cancel the reservation'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        if reservation.state != 'waiting':
            response['success'] = False
            response['error'] = {'code':404, 'message':'You can only cancel a reservation which state equal waiting'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)

        res = reservation.write({'state':'canceled'})
        if not res:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to update the reservation state'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        reservation = {
            'id': reservation.id,
            'name':reservation.name,
            'state':reservation.state,
            'student_name':reservation.student_id.name,
            'student_last_name':reservation.student_id.last_name,
            'type':reservation.reservation_type_id.name,
            'menu':{
                'id':reservation.menu_id.id,
                'name':reservation.menu_id.name,
                'date':reservation.menu_id.start_date,
                'content':self._load_meals_by_menu(reservation.menu_id),
            }
        }
        response['success'] = True
        response['data'] = reservation
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)

        






    @http.route('/api/reservation', type='http', auth="user", methods=['POST'],  csrf=False)
    def create_reservation(self, **kwargs):
        """
            @param reservation = { "student_id":1,"menu_id":2,"reservation_type_id":3}
        """
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
                response['success'] = False
                response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
                return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        # check if logged user is the parent of the student
        res_partner = request.env['res.partner'].search(['&',('id','=',request.env.user.partner_id.id),('is_school_parent','=',True)])
        
        if not request.params.get('reservation',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'No reservation object submited'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        else:
            print "PARAM ",request.params.get('reservation',False)



        json_object = None
        try:
            json_object = json.loads(request.params.get('reservation',False))
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize reservation object'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        if self._validate_reservation_object(json_object, response)['valide'] == False:
            response.pop('valide', None)
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        else:
            response.pop('valide', None)

        if not json_object['student_id'] in res_partner.student_ids.ids:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user need to be the parent of the student in order to submit a reservation for him'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)

        # student_id right here is a simple integer not a recordset
        json_object['state'] = 'waiting'
        res = request.env['school.reservation'].create(json_object)
        if not res:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unknown error occurred while creating a reservation'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)


        # make the student_id field a list to make it possible to create reservation for multiple student with one menu 


        reservation = {
            'id': res.id,
            'name':res.name,
            'student_name':res.student_id.name,
            'student_last_name':res.student_id.last_name,
            'type':res.reservation_type_id.name,
            'menu':{
                'id':res.menu_id.id,
                'name':res.menu_id.name,
                'date':res.menu_id.from_date,
                'content':self._load_meals_by_menu(res.menu_id),
            }
        }
        print "### reservation ",reservation
        response['data'] = reservation
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)


    def _validate_reservation_object(self, json_object, response):
        response['valide'] = True
        if not 'student_id' in json_object:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'student_id is missing'} 
        else:
            try:
                json_object['student_id'] = int(json_object['student_id'])
            except ValueError:
                response['valide'] = False
                response['success'] = False
                response['error'] = {'code':404, 'message':'student_id must be of type integer'}

        if not 'menu_id' in json_object:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'menu_id is missing'} 
        else:
            try:
                json_object['menu_id'] = int(json_object['menu_id'])
            except ValueError:
                response['valide'] = False
                response['success'] = False
                response['error'] = {'code':404, 'message':'menu_id must be of type integer'} 

        if not 'reservation_type_id' in json_object:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'reservation_type_id is missing'} 
        else:
            try:
                json_object['reservation_type_id'] = int(json_object['reservation_type_id'])
            except ValueError:
                response['valide'] = False
                response['success'] = False
                response['error'] = {'code':404, 'message':'reservation_type_id must be of type integer'} 

        
        menu_id = request.env['school.menu'].search([('id','=',json_object['menu_id'])])
        if not menu_id:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'No menu found with the given ID'} 

        date = datetime.strptime(fields.Date.today(), DEFAULT_SERVER_DATE_FORMAT).date()
        menu_date = datetime.strptime(menu_id.from_date, DEFAULT_SERVER_DATE_FORMAT).date()
        if date > menu_date:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':"You can only make a reservation for today's or upcoming menu"} 

        reservation_type_id = request.env['school.reservation.type'].search([('id','=',json_object['reservation_type_id'])])
        if not reservation_type_id:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':404, 'message':'No reservation type found with the given ID'} 

        res = request.env['school.reservation'].search(['&',('student_id','=',json_object['student_id']), '&',('menu_id','=',json_object['menu_id']), ('reservation_type_id','=',json_object['reservation_type_id']),('state','in',['waiting','accepted'])])
        if res:
            response['valide'] = False
            response['success'] = False
            response['error'] = {'code':403, 'message':'A reservation with the given menu id is already created for the student with the id '+str(json_object['student_id'])} 

        json_object['start_date'] = menu_date
        return response

    
    @http.route('/api/reservation/<int:reservation_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_reservation(self, reservation_id):
        """
            @apram student_id : recordset of school.student
        """
        response = {'success':False, 'data':None}
        reservation_id = request.env['school.reservation'].search([('id','=',reservation_id)])
        if not reservation_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No reservation found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        reservation = {
            'id': reservation_id.id,
            'name':reservation_id.name,
            'state':reservation_id.state,
            'student_name':reservation_id.student_id.name,
            'student_last_name':reservation_id.student_id.last_name,
            'type':reservation_id.reservation_type_id.name,
            'menu':{
                'id':reservation_id.menu_id.id,
                'name':reservation_id.menu_id.name,
                'date':reservation_id.menu_id.from_date,
                'content':self._load_meals_by_menu(reservation_id.menu_id),
            }
        }
        response['success'] = True
        response['data'] = reservation
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)


    @http.route('/api/reservation/parent', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_parent_childs_reservations(self, **kwargs):
        """
            obtain all children's reservations of the logged parent
        """
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        res_partner = request.env['res.partner'].search(['&',('id','=',request.env.user.partner_id.id),('is_school_parent','=',True)])
        reservations = []
        for student_id in res_partner.student_ids:
            res = {
                'id':student_id.id,
                'name':student_id.name,
                'last_name':student_id.last_name,
                'reservations':self._load_reservation_by_student(student_id),
            }
            reservations.append(res)
        response['success'] = True
        response['data'] = reservations
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)

    
    @http.route('/api/reservation/student/<int:student_id>/', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_student_reservation(self, student_id, **kwargs):
        """ 
            @URL:param student_id
        """
        response = {'success':False, 'data':None}
        student_id = request.env['school.student'].search([('id','=',student_id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        reservations = self._load_reservation_by_student(student_id)
        response['success'] = True
        response['data'] = reservations
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)

   
    def _load_reservation_by_student(self, student_id):
        """
            @apram student_id : recordset of school.student
        """
        date = datetime.strptime(fields.Date.today(), DEFAULT_SERVER_DATE_FORMAT).date()
        reservations = request.env['school.reservation'].search([('student_id','=',student_id.id), ('state','in',['accepted', 'waiting'])])
        reservations_list = []
        for res in reservations:
            reservation = {
                'id': res.id,
                'name':res.name,
                'state':res.state,
                'student_name':res.student_id.name,
                'student_last_name':res.student_id.last_name,
                'type':res.reservation_type_id.name,
                'menu':{
                    'id':res.menu_id.id,
                    'name':res.menu_id.name,
                    'date':res.menu_id.from_date,
                    'content':self._load_meals_by_menu(res.menu_id),
                }
            }
            reservations_list.append(reservation)
        return reservations_list

    
    def _load_meals_by_menu(self, menu_id):
        """
            @apram menu_id : recordset of school.menu
        """
        meal_type_ids = []
        meal_list = []
        for rec in menu_id.menu_line_ids:
            meal_type_ids.append(rec.meal_type_id.id)
        meal_type_ids = list(set(meal_type_ids))
        for type in request.env['school.meal.type'].browse(meal_type_ids):
            
            meal_type = {
                'id':type.id,
                'name':type.name,
                'meals':[],
            }
            for rec in menu_id.menu_line_ids:
                if rec.meal_type_id.id == type.id:
                    meal = {
                        'id':rec.id,
                        'name':rec.meal_id.name,
                    }
                    meal_type['meals'].append(meal)
            meal_list.append(meal_type)
        return meal_list

    @http.route('/api/reservation/student/<int:student_id>/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_student_reservation_by_academic_year_period(self, student_id,academic_year_id,timing_system_periode_id, **kwargs):
        """ 
            @URL:param student_id
        """
        response = {'success':False, 'data':None}
        student_id = request.env['school.student'].search([('id','=',student_id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No student found with the given ID'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        reservations = self._load_reservation_by_student_by_academic_year_period(student_id,academic_year_id,timing_system_periode_id)
        response['success'] = True
        response['data'] = reservations
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)

   
    def _load_reservation_by_student_by_academic_year_period(self, student_id,academic_year_id,timing_system_periode_id):
        """
            @apram student_id : recordset of school.student
        """
        academic_years=request.env['academic.year'].search([('id','=',academic_year_id)])
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)]) 
        date = datetime.strptime(fields.Date.today(), DEFAULT_SERVER_DATE_FORMAT).date()
        reservations = request.env['school.reservation'].search([('student_id','=',student_id.id), ('state','in',['accepted', 'waiting'])])
        reservations_list = []
        for res in reservations:
            if academic_years.start_date <= res.start_date <= academic_years.end_date:
                    if timing_periode.start_date <= res.start_date <= timing_periode.end_date:
                        reservation = {
                            'id': res.id,
                            'name':res.name,
                            'state':res.state,
                            'student_name':res.student_id.name,
                            'student_last_name':res.student_id.last_name,
                            'type':res.reservation_type_id.name,
                            'menu':{
                                'id':res.menu_id.id,
                                'name':res.menu_id.name,
                                'date':res.menu_id.from_date,
                                'content':self._load_meals_by_menu(res.menu_id),
                            }
                        }
                        reservations_list.append(reservation)
        return reservations_list



    @http.route('/api/reservation/menu', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_menus(self, **kwargs):
        response = {'success':False, 'data':None}
        menu_list = []
        if not request.env.user.has_group('genext_school.group_school_parent') and not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        else:      
            if request.env.user.user_type == 'parent':
                print "## PARENT"
                menu_list = self._load_menu()
            elif request.env.user.user_type == 'student':
                print "### Student user"
            
        response['success'] = True
        response['data'] = menu_list
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)


    def _load_menu(self):
        date = datetime.strptime(fields.Date.today(), DEFAULT_SERVER_DATE_FORMAT).date()
        menus = request.env['school.menu'].search([('from_date','>=',date)])
        print "## lenth ",menus
        menu_list =  []
        for rec in menus:
            print "## menu name ",rec.name
            menu = {
                'id':rec.id,
                'name':rec.name,
                'date':rec.from_date,
                'content':self._load_meals_by_menu(rec),
            }
            menu_list.append(menu)
        return menu_list
    @http.route('/api/reservation/menu/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_menus_year_period(self,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        menu_list = []
        if not request.env.user.has_group('genext_school.group_school_parent') and not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)
        else:      
            if request.env.user.user_type == 'parent':
                print "## PARENT"
                menu_list = self._load_menu_year_period(academic_year_id,timing_system_periode_id)
            elif request.env.user.user_type == 'student':
                print "### Student user"
            
        response['success'] = True
        response['data'] = menu_list
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)

         
    def _load_menu_year_period(self,academic_year_id,timing_system_periode_id):
        academic_years=request.env['academic.year'].search([('id','=',academic_year_id)])
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)]) 
        date = datetime.strptime(fields.Date.today(), DEFAULT_SERVER_DATE_FORMAT).date()
        menus = request.env['school.menu'].search([('from_date','>=',date)])
        print "## lenth ",menus
        menu_list =  []
        for rec in menus:
            if academic_years.start_date <= rec.from_date <= academic_years.end_date:
                    if timing_periode.start_date <= rec.from_date <= timing_periode.end_date:
                        print "## menu name ",rec.name
                        menu = {
                            'id':rec.id,
                            'name':rec.name,
                            'date':rec.from_date,
                            'content':self._load_meals_by_menu(rec),
                        }
                        menu_list.append(menu)
                    else:
                        print"period not in timing system period"
            else: 
                print"period not in academic year"
        return menu_list

    @http.route('/api/reservation/type', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_reservation_tye(self, **kwargs):
        response = {'success':False, 'data':None}
        menu_list = []
        reservations = request.env['school.reservation.type'].search([])
        reservation_lit = []
        for rec in reservations:
            reservation = {
                'id':rec.id,
                'name':rec.name,
                'price':rec.price,
            }
            reservation_lit.append(reservation)
            
        response['success'] = True
        response['data'] = reservation_lit
        return http.request.make_response(json.dumps(response),SchoolReservation.HEADER_GET)