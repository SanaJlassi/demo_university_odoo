# -*- coding: utf-8 -*-
from urllib import urlencode
import odoo
from odoo import http
from odoo import models, fields, api
from odoo.http import content_disposition, dispatch_rpc, request, Response
import json
import base64
import urllib2
import urllib
import logging
#import simplejson
import werkzeug
#from odoo.addons.saas_base.exceptions import MaximumDBException, MaximumTrialDBException
_logger = logging.getLogger(__name__)

# this url is used to created new instance for a logged in user 
# user must be already logged in 
# http://odesco/saas_portal/add_new_client?lang=en_US&dbname=whdes&tz=+0100&hosting=&app=&plan_id=1

#http://005.odesco/saas_client/new_database?access_token=ELr1hrEXJNJIs1SQM0TawUv4KnUKNQ&state=%7B%22a%22%3A+37%2C+%22p%22%3A+4%2C+%22d%22%3A+%22005.odesco%22%7D





class LoginController(http.Controller):
    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Credentials':'True'
            }

    def get_config_parameter(self, param):
        config = request.env['ir.config_parameter']
        full_param = 'saas_portal.%s' % param
        return config.get_param(full_param)

    def get_plan(self, plan_id=None):
        plan_obj = request.env['saas_portal.plan']
        if not plan_id:
            domain = [('state', '=', 'confirmed')]
            plans = request.env['saas_portal.plan'].search(domain)
            if plans:
                return plans[0]
            else:
                raise exceptions.Warning(_('There is no plan configured'))
        return plan_obj.sudo().browse(plan_id)


    def get_full_dbname(self, dbname):
        if not dbname:
            return None
        full_dbname = '%s.%s' % (dbname, self.get_config_parameter('base_saas_domain'))
        return full_dbname.replace('www.', '')


    def serialize_plans(self, plan_id):
        """
        serialize a recordset of saas_portal.plan
        :param Recordset of saas_portal.plan: the recordset to serialize
        :returns the list object of the given plans 
        """
        result = []
        for plan in plan_id:
            paln_dict = {
                'id':plan.id,
                'name':plan.name,
                'summary':plan.summary,
                'max_users':plan.max_users,
                'total_storage_limit':plan.total_storage_limit
            }
            result.append(paln_dict)
        return result

    @http.route('/api/test', type='json', methods=['POST','GET'], auth="user", csrf=False)
    def web_test(self, **kw):
        responseData = {'success':True, 'data':request.session.sid,'user':request.env.user.name,'data':'data for test'}
        
        return responseData


    @http.route('/api/generate/session', type='json', methods=['POST'], auth="public", csrf=False)
    def generate_session(self, **kw):
        responseData = {'success':False, 'data':None}
        random_key = None
        if not request.params.get('random_key'):
            responseData['success'] = False
            responseData['error'] = {'code':401, 'message':'random key value is missing '}
            return responseData
        random_key = request.params.get('random_key')
        
        user_id = request.env['res.users'].sudo().search([('random_key','=',random_key)])
        if not user_id:
            responseData['success'] = False
            responseData['error'] = {"code":401, "message":"random key doesn't exist "}
            return responseData

        if not user_id.random_key:
            responseData['success'] = False
            responseData['error'] = {"code":401, "message":"User doesn't have a randm key"}
            return responseData

        if not user_id.allow_platform_login:
            responseData['success'] = False
            responseData['error'] = {"code":401, "message":"User is not allowed to login"}
            return responseData
            

        login = user_id.login
        #password = user_id.plain_text_password
        password = user_id.login
        print"######password",password
        #db = request.env['ir.config_parameter'].sudo().get_param('web.base.url')
        #db = db.replace("http://","")
        #db = db.replace("www.","")
        db="odescoPRI"
        uid = request.session.authenticate(db, login, password)
        request.session.uid = uid
        if not uid:
            responseData['success'] = False
            responseData['error'] = {"code":401, "message":"Authentification failed"}
            return responseData
        user_id.write({'session_id':request.env['ir.http'].session_info()['session_id']})
        user = request.env['res.users'].sudo().search([('id','=',uid)])
        
        if len(user):
            userData = {}
            userData['login'] = user.login
            userData['id'] = user.id
            userData['user_type']= user.user_type
            userData['session_id'] = request.env['ir.http'].session_info()['session_id']
            responseData['data'] = userData
            print"############",userData
            responseData['success'] = True
        else:
            responseData['success'] = False
            responseData['error'] = {'code':401, 'message':'Authentification failed'}
        return responseData


    @http.route('/api/logins', type='json', methods=['POST'], auth="public", csrf=False)
    def web_logins(self, **kw):
        print '------------------------------- ',http.db_list()
        responseData = {'success':False, 'data':None}
        if request.httprequest.environ['HTTP_HOST'] not in http.db_list():
            responseData['success'] = False
            responseData['error'] = {'code':401, 'message':'Requested instance does not exist'}
            return responseData
        if not request.params.get('login'):
            responseData['success'] = False
            responseData['error'] = {'code':401, 'message':'login value is missing'}
            return responseData
        elif not request.params.get('password'):
            responseData['success'] = False
            responseData['error'] = {'code':401, 'message':'password value is missing'}
            return responseData

        uid = request.session.authenticate(request.httprequest.environ['HTTP_HOST'], request.params['login'], request.params['password'])
        request.session.uid = uid
        user = request.env['res.users'].sudo().search([('id','=',uid)])
        
        if len(user):
            userData = {}
            userData['login'] = user.login
            userData['id'] = user.id
            #userData['user_type']= user.user_type
            userData['session_id'] = request.env['ir.http'].session_info()['session_id']
            responseData['data'] = userData
            responseData['success'] = True
        else:
            responseData['success'] = False
            responseData['error'] = {'code':401, 'message':'Authentification failed'}
        return responseData

    @http.route('/api/update/password', type='json', auth="user", methods=['POST'],  csrf=False)
    def update_password(self, **kwargs):
        response = {'success':False, 'data':None}
        if not request.params.get('new_password'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Missing password '} 
            return response

        if not request.params.get('old_password'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Missing old password '} 
            return response
        

        old_password = request.params.get('old_password')
        new_password = request.params.get('new_password')
        
        uid = request.env.user._login(request.env.cr.dbname, request.env.user.login, str(old_password))
        _logger.warning('##############----------############ UID %s',uid)

        if not uid:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Incorrect password'} 
            return response

        user_id = request.env.user
        res = user_id.write({'password':new_password})
        
        _logger.warning('#######################PASSWORD UPDATED################## %s',res)
        request.session = None
        response['success'] = True
        return response


    @http.route('/api/res_company/logos', type='json', auth="public", methods=['POST'],  csrf=False)
    def update_logo(self,**kw):
        responseData = {'success':False, 'data':None}
        company_json=None
        if 'logos' not in request.params:
            responseData['success'] = False
            responseData['error'] = {'code':404, 'message':'logo object is not submited'} 
            return responseData
        company = {}
        base64 = request.params.get('logos').split(",")
        company['logo'] = base64[1].decode('utf-8').replace(' ', '+')
        company['name'] = request.params.get('name')
        result = request.env.user.company_id.write(company)
        if result:
            responseData['success'] = True
            responseData['data'] = None
        else:
            responseData['success'] = False
            responseData['error'] = {'code':400, 'message':'error while trying to update company logo'} 
        return responseData
    

