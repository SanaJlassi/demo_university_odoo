# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)

class schoolEvaluationMark(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }
    @http.route('/api/teacher/class/<int:class_id>/marks', type='http', auth="user", methods=['POST'],  csrf=False)
    def evaluations_marks(self,class_id,**kwargs):
    	response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)
        teacher_id = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        _logger.warning('### DATAAAAAAA :  (%s).', request.params.get('marks',False))       
        json_object = None               
        if not request.params.get('marks',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'mark object is not submited'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)

        class_id                = request.env['school.class'].browse(class_id)
        grade_id                = class_id.grade_id
        educational_stage_id    = grade_id.educational_stage_id
        academic_year           = request.env['academic.year'].search([('active_year','=',True)])
        try:
            json_object = json.loads(request.params.get('marks',False))
            _logger.warning('############  JSON-OBJECT  :  %s', json_object)
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize mark object'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)

        for el in json_object:
            _logger.warning('############# JSON-OBJECT el :  (%s).', el)

        mark_object = {
            'academic_year':academic_year.id,
            'educational_stage_id':educational_stage_id.id,
            #'timing_system_id':json_object['timing_system_id'],
            'timing_periode_ids':json_object['timing_periode_ids'],
            'grade_id':grade_id.id,
            'class_id':class_id.id,
            'skill_id':json_object['skill_id'],
            'teacher_id':teacher_id.id,
            'date':fields.Date.today(),
           
        }
        context = request.env.context.copy()
        context['source'] = 'controller'
        request.env.context = context    
        mark_object = request.env['school.evaluation.mark'].create(mark_object)
        if mark_object:
            response['success'] = True
            response['data'] = False
        else: 
            response['success'] = False
            response['data'] = False
        mark_list = []
        for el in json_object['evaluation_mark_line_ids']:
            mark_list.append((0,0,el))

        res = mark_object.write({'evaluation_mark_line_ids':mark_list})
        if res:
            response['success'] = True
            response['data'] = False
        else: 
            response['success'] = False
            response['data'] = False
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)

    @http.route('/api/teacher/class/<int:class_id>/marks', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluation_marks(self, class_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)

        teacher_id = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)
        evaluations_marks_ids=request.env['school.evaluation.mark'].search([('class_id','=',class_id.id),('teacher_id','=',teacher_id.id)])
        marks_list=[]
        for evalu in evaluations_marks_ids:
            eval_mark= {
                    'id':evalu.id,
                    'academic_year': {
                        'id':evalu.academic_year.id,
                        'name':evalu.academic_year.name,
                        'code':evalu.academic_year.code,
                    },
                    #'timinig_system':evalu.timing_system_id, 
                    #'timing_periode':evalu.timing_periode_ids,
                    'classe':{
                        'id':evalu.class_id.id,
                        'name':evalu.class_id.name,
                        'code':evalu.class_id.code,
                          },
                    'skill':{
                        'id':evalu.skill_id.id,
                        'name':evalu.skill_id.name,
                        'code':evalu.skill_id.code,
                        }, 
                    'teacher':{
                        'id':evalu.teacher_id.id,
                        'name':evalu.teacher_id.name,
                        'last_name':evalu.teacher_id.last_name,
                        },
                    'date': evalu.date,
                    'evaluation_marks':self._load_json_marks(evalu, 'teacher', teacher_id),  
            }
            marks_list.append(eval_mark)
        response['data'] = marks_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER) 
    def _load_json_marks(self, evalu, user_type, user_id):
        mark_line=[]
        if user_type == 'teacher':
            for line in evalu.evaluation_mark_line_ids:
                mark_line.append({
                    'id':line.id,
                    'student_id':line.student_id.id,
                    'student_name':line.student_id.name,
                    'student_lastname':line.student_id.last_name,
                    'mark':line.mark,
                    })
        return mark_line

    @http.route('/api/teacher/class/<int:class_id>/subject/<int:subject_id>/marks/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluation_marks_year_period(self, class_id,subject_id,academic_year_id,timing_system_periode_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)

        teacher_id = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        selected_academic_year = request.env['academic.year'].search([('id','=',academic_year_id)])
        evaluations_marks_ids=request.env['school.evaluation.mark'].search([('class_id','=',class_id.id),('teacher_id','=',teacher_id.id),('academic_year','=',selected_academic_year.id),('timing_periode_ids','=',timing_periode.id),('subject_id','=',subject_id)])
        marks_list=[]
        
        for evalu in evaluations_marks_ids:
            eval_mark= {
                    'id':evalu.id,
                    'academic_year': {
                        'id':evalu.academic_year.id,
                        'name':evalu.academic_year.name,
                        'code':evalu.academic_year.code,
                    },
                    #'timinig_system':evalu.timing_system_id, 
                    #'timing_periode':evalu.timing_periode_ids,
                    'classe':{
                        'id':evalu.class_id.id,
                        'name':evalu.class_id.name,
                        'code':evalu.class_id.code,
                          },
                    'subject':{
                        'id':evalu.subject_id.id,
                        'name':evalu.subject_id.name,
                        'code':evalu.subject_id.code,
                        }, 
                    'teacher':{
                        'id':evalu.teacher_id.id,
                        'name':evalu.teacher_id.name,
                        'last_name':evalu.teacher_id.last_name,
                        },
                    'date': evalu.date,
                    'evaluation_marks':self._load_json_marks_year_period(evalu, 'teacher', teacher_id),  
            }
            marks_list.append(eval_mark)
        response['data'] = marks_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER) 
    def _load_json_marks_year_period(self, evalu, user_type, user_id):
        mark_line=[]
        marks=0
        if user_type == 'teacher':
            for line in evalu.evaluation_mark_line_ids:
                marks+=line.mark
                mark_line.append({
                    'id':line.id,
                    'student_id':line.student_id.id,
                    'mark':line.mark,
                    })
            #marks=marks+line.mark
            print"####marks",marks
            
        return mark_line

    @http.route('/api/student/<int:student_id>/marks', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_marks_(self,student_id,**kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)

        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER) 
        marks = request.env['school.evaluation.mark'].search([])
        mark_ids=[]
        for mark in marks:
            for evaluation_mark_line in mark.evaluation_mark_line_ids:
                if student_id.id in evaluation_mark_line.student_id.ids:
                    mark_ids.append(mark) 

        mark_list = []
        for mark in mark_ids:
            mark_list.append(
                { 'teacher': mark.teacher_id.name,
                  'skill' : mark.skill_id.name,
                  'date'  : mark.date,
                  'skill_line_ids' : self._load_skill_note(mark,student_id)
                })

        response['success'] = True
        response['data'] = mark_list
        return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)   

    def _load_skill_note(self,mark,user_id):
        mark_line=[]
        for line in mark.evaluation_mark_line_ids:
                if line.student_id.id == user_id.id:
                    mark_line.append({
                    'id':line.id, 
                    'student_id':line.student_id.id, 
                    'student_name':line.student_id.name,
                    'student_lastname':line.student_id.last_name,
                    'mark': line.mark, 
                
                    })

        return mark_line 
    """@http.route('/api/student/<int:student_id>/class/<int:class_id>/skill_id/<int:skill_id>/marks', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_marks_moyenne(self,student_id,class_id,skill_id,**kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)

        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER) 
        marks = request.env['school.evaluation.mark'].search([('class_id','=',class_id),('skill_id','=',skill_id)])
        
        mark_ids=[]
        for mark in marks:
                if student_id.id in mark.evaluation_mark_line_ids.student_id.ids:
                    mark_ids.append(mark) 


        mark_list = []
        mark_note=0.0
        for mark in mark_ids:
            mark_note +=self._load_skill_note_student(mark,student_id)
        moyenne_mark=mark_note/len(mark_ids)
        mark_list.append({
                'moyenne_skill':moyenne_mark}
                )

        response['success'] = True
        response['data'] = mark_list
        return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)   

    def _load_skill_note_student(self,mark,user_id):
        mark_line=[]
        marks=0
        for line in mark.evaluation_mark_line_ids:
                if line.student_id.id == user_id.id:
                    mark_line.append({
                    'id':line.id, 
                    'student_id':line.student_id.id, 
                    'student_name':line.student_id.name,
                    'student_lastname':line.student_id.last_name,
                    'mark': line.mark, 
                
                    })
                    marks =line.mark
        return marks"""
    @http.route('/api/student/<int:student_id>/class/<int:class_id>/skill_id/<int:skill_id>/marks', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_marks_moyenne(self,student_id,class_id,skill_id,**kwargs):
        response = {'success':False, 'data':None}
        """if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)"""

        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER) 
        marks = request.env['school.evaluation.mark'].search([('class_id','=',class_id),('skill_id','=',skill_id)])
        
        mark_ids=[]
        for mark in marks:
                if student_id.id in mark.evaluation_mark_line_ids.student_id.ids:
                    mark_ids.append(mark) 


        mark_list = []
        
        for mark in mark_ids:
           
            mark_list.append(
                { 'teacher': mark.teacher_id.name,
                  'skill' : mark.skill_id.name,
                  'date'  : mark.date,
                  'skill_line_ids' : self._load_skill_note_student(mark,student_id)
                })

        response['success'] = True
        response['data'] = mark_list
        return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)   

    def _load_skill_note_student(self,mark,user_id):
        mark_line=[]
        
        for line in mark.evaluation_mark_line_ids:
                if line.student_id.id == user_id.id:
                    mark_line.append({
                    'id':line.id, 
                    'student_id':line.student_id.id, 
                    'student_name':line.student_id.name,
                    'student_lastname':line.student_id.last_name,
                    'mark': line.mark, 
                
                    })
                    
        return mark_line


    @http.route('/api/student/<int:student_id>/marks/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_marks_academic_year_period(self,student_id,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        """if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)"""
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        selected_academic_year = request.env['academic.year'].search([('id','=',academic_year_id)])
        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER) 
        marks = request.env['school.evaluation.mark'].search([('academic_year','=',selected_academic_year.id),('timing_periode_ids','=',timing_periode.id)])
       
        mark_ids=[]
        for mark in marks:
           
            for evaluation_mark_line in mark.evaluation_mark_line_ids:
                if student_id.id in evaluation_mark_line.student_id.ids:
                    
                    mark_ids.append(mark)
                    print"########mark_ids",mark_ids 

        mark_list = []
        for mark in mark_ids:
            mark_list.append(
                { 'teacher': mark.teacher_id.name,
                  'skill' : mark.skill_id.name,
                  'date'  : mark.date,
                  'skill_line_ids' : self._load_skill_note_year_period(mark,student_id)
                })

        response['success'] = True
        response['data'] = mark_list
        return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)   

    def _load_skill_note_year_period(self,mark,user_id):
        mark_line=[]
        for line in mark.evaluation_mark_line_ids:
                if line.student_id.id == user_id.id:
                    mark_line.append({
                    'id':line.id, 
                    'student_id':line.student_id.id, 
                    'student_name':line.student_id.name,
                    'student_lastname':line.student_id.last_name,
                    'mark': line.mark, 
                
                    })

        return mark_line 

    @http.route('/api/teacher/class/<int:class_id>/marks/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluation_marks_by_year_period(self, class_id,academic_year_id,timing_system_periode_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)

        teacher_id = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)]) 
        
        class_id = request.env['school.class'].browse(class_id)
        if not class_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Class found with the given id'} 
            return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER)
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        selected_academic_year = request.env['academic.year'].search([('id','=',academic_year_id)])
        evaluations_marks_ids=request.env['school.evaluation.mark'].search([('class_id','=',class_id.id),('teacher_id','=',teacher_id.id),('academic_year','=',selected_academic_year.id),('timing_periode_ids','=',timing_periode.id)])
        marks_list=[]
        
        for evalu in evaluations_marks_ids:
            if self._load_json_marks_by_year_period(evalu, 'teacher', teacher_id) != []:
                eval_mark= {
                        'id':evalu.id,
                        'academic_year': {
                            'id':evalu.academic_year.id,
                            'name':evalu.academic_year.name,
                            'code':evalu.academic_year.code,
                        },
                        #'timinig_system':evalu.timing_system_id, 
                        #'timing_periode':evalu.timing_periode_ids,
                        'classe':{
                            'id':evalu.class_id.id,
                            'name':evalu.class_id.name,
                            'code':evalu.class_id.code,
                              },
                        'skill':{
                            'id':evalu.skill_id.id,
                            'name':evalu.skill_id.name,
                            'code':evalu.skill_id.code,
                            }, 
                        'teacher':{
                            'id':evalu.teacher_id.id,
                            'name':evalu.teacher_id.name,
                            'last_name':evalu.teacher_id.last_name,
                            },
                        'date': evalu.date,
                        'evaluation_marks':self._load_json_marks_by_year_period(evalu, 'teacher', teacher_id),  
                }
                marks_list.append(eval_mark)
        response['data'] = marks_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),schoolEvaluationMark.HEADER) 
    def _load_json_marks_by_year_period(self, evalu, user_type, user_id):
        mark_line=[]
        if user_type == 'teacher':
            for line in evalu.evaluation_mark_line_ids:
                if line.mark > 0:
                    print"######################",line.mark
                    mark_line.append({
                        'id':line.id,
                        'student_id':line.student_id.id,
                        'student_name':line.student_id.name,
                        'student_lastname':line.student_id.last_name,
                        'mark':line.mark,
                        })
        return mark_line
