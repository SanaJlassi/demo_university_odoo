# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
_logger = logging.getLogger(__name__)


class SchoolEvaluation(http.Controller):

    HEADER_GET = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    
    @http.route('/api/parent/evaluations', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluations(self, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        student_ids = partner_id.student_ids
        print "#### Parent : ",partner_id.name
        list_of_evaluation = []
        for student in student_ids:
            print "#### student name ", student.name
            print "#### student last_name", student.last_name
            evaulations = request.env['school.evaluation'].search(['&',('student_id','=',student.id),('state','=','confirmed')])
            eval_list = []
            for evaluation in evaulations:
                domain = [
                    ('res_model', '=', evaluation._name),
                    ('res_field', '=', 'file'),
                    ('res_id', '=', evaluation.id),
                ]
                attachment = request.env['ir.attachment'].search(domain)
                base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+evaluation.file_name
                url = url.replace(" ", "_")
                _logger.warning('#### URL of document %s',url)
                eval_list.append({
                    'id':evaluation.id,
                    'title':evaluation.name,
                    'date':evaluation.date,
                    #'file':evaluation.file,
                    'filename':evaluation.file_name,
                    'url':url,
                    'class':{
                        'id':evaluation.class_id.id,
                        'name':evaluation.class_id.name,
                        'code':evaluation.class_id.code,
                    }
                })

            list_of_evaluation.append({
                'id':student.id,
                'name':student.name,
                'last_name':student.last_name,
                'evaluations':eval_list,
            })
        response['success'] = True
        response['data'] = list_of_evaluation
        return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER_GET)


    @http.route('/api/parent/evaluations/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluations(self,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        student_ids = partner_id.student_ids
        print "#### Parent : ",partner_id.name
        list_of_evaluation = []
        academic_years=request.env['academic.year'].search([('id','=',academic_year_id)])
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        for student in student_ids:
            print "#### student name ", student.name
            print "#### student last_name", student.last_name
            evaulations = request.env['school.evaluation'].search(['&',('student_id','=',student.id),('state','=','confirmed')])
            eval_list = []
            for evaluation in evaulations:
                if academic_years.start_date <= evaluation.date <= academic_years.end_date:
                    if timing_periode.start_date <= evaluation.date <= timing_periode.end_date:
                        domain = [
                            ('res_model', '=', evaluation._name),
                            ('res_field', '=', 'file'),
                            ('res_id', '=', evaluation.id),
                        ]
                        attachment = request.env['ir.attachment'].search(domain)
                        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                        url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+evaluation.file_name
                        url = url.replace(" ", "_")
                        _logger.warning('#### URL of document %s',url)
                        eval_list.append({
                            'id':evaluation.id,
                            'title':evaluation.name,
                            'date':evaluation.date,
                            #'file':evaluation.file,
                            'filename':evaluation.file_name,
                            'url':url,
                            'class':{
                                'id':evaluation.class_id.id,
                                'name':evaluation.class_id.name,
                                'code':evaluation.class_id.code,
                            }
                        })

            list_of_evaluation.append({
                'id':student.id,
                'name':student.name,
                'last_name':student.last_name,
                'evaluations':eval_list,
            })
        response['success'] = True
        response['data'] = list_of_evaluation
        return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER_GET)



    @http.route('/api/parent/student/<int:student_id>/evaluations', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluations_by_student(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        list_of_evaluation = []
        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER)  

        print "#### student name ", student_id.name
        print "#### student last_name", student_id.last_name
        evaulations = request.env['school.evaluation'].search(['&',('student_id','=',student_id.id),('state','=','confirmed')])
        eval_list = []
        for evaluation in evaulations:
            domain = [
                ('res_model', '=', evaluation._name),
                ('res_field', '=', 'file'),
                ('res_id', '=', evaluation.id),
            ]
            attachment = request.env['ir.attachment'].search(domain)
            base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+evaluation.file_name
            url = url.replace(" ", "_")
            _logger.warning('#### URL of document %s',url)
            eval_list.append({
                'id':evaluation.id,
                'title':evaluation.name,
                'date':evaluation.date,
                #'file':evaluation.file,
                'filename':evaluation.file_name,
                'url':url,
                'class':{
                    'id':evaluation.class_id.id,
                    'name':evaluation.class_id.name,
                    'code':evaluation.class_id.code,
                }
            })
        response['success'] = True
        response['data'] = eval_list
        return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER_GET)

    @http.route('/api/parent/student/<int:student_id>/evaluations/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_evaluations_by_student_byacademic_year_period(self, student_id,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        partner_id = request.env.user.partner_id
        list_of_evaluation = []
        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER)  

        print "#### student name ", student_id.name
        print "#### student last_name", student_id.last_name
        academic_years=request.env['academic.year'].search([('id','=',academic_year_id)])
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        evaulations = request.env['school.evaluation'].search(['&',('student_id','=',student_id.id),('state','=','confirmed')])
        eval_list = []
        for evaluation in evaulations:
            if academic_years.start_date <= evaluation.date <= academic_years.end_date:
                    if timing_periode.start_date <= evaluation.date <= timing_periode.end_date:
                        domain = [
                            ('res_model', '=', evaluation._name),
                            ('res_field', '=', 'file'),
                            ('res_id', '=', evaluation.id),
                        ]
                        attachment = request.env['ir.attachment'].search(domain)
                        base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                        url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+evaluation.file_name
                        url = url.replace(" ", "_")
                        _logger.warning('#### URL of document %s',url)
                        eval_list.append({
                            'id':evaluation.id,
                            'title':evaluation.name,
                            'date':evaluation.date,
                            'file':evaluation.file,
                            'filename':evaluation.file_name,
                            'url':url,
                            'class':{
                                'id':evaluation.class_id.id,
                                'name':evaluation.class_id.name,
                                'code':evaluation.class_id.code,
                            }
                        })
        response['success'] = True
        response['data'] = eval_list
        return http.request.make_response(json.dumps(response),SchoolEvaluation.HEADER_GET)


                