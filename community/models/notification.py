# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
from odoo.exceptions import UserError, ValidationError, Warning

import cookielib
import urllib2
import json
import base64
import logging
import json
import smtplib
import string
import re

from notification_client import Client

_logger = logging.getLogger(__name__)


class NotificationConfig(models.Model):
    _name       = 'notification.config'


    name                    = fields.Char(string="Nom", required=True)
    app_id                  = fields.Char(string="APP ID", required=True)
    url                     = fields.Char(string='Url', required=True)
    default_message         = fields.Char(string="Message par défaut", default=lambda self: self.env.user.company_id.name)
    enabled                  = fields.Boolean("Active",default=True)


    @api.multi
    def toggle_enabled(self):
        for record in self:
            record.write({'enabled':not record.enabled})



    @api.model
    def create(self, vals):
        if vals.get('enabled',False):
            for rec in self.env['notification.config'].search([]):
                rec.write({'enabled':False})
        return super(NotificationConfig, self).create(vals)

    @api.multi
    def write(self, vals):
        if vals.get('enabled',False):
            if vals.get('enabled',False) == True:
                for rec in self.env['notification.config'].search([]):
                    rec.write({'enabled':False})
            else:
                rec = self.env['notification.config'].search([],limit=1)
                if rec:
                    rec.write({'enabled':True})
        return super(NotificationConfig, self).write(vals)

    @api.multi
    def unlink(self):
        res = super(NotificationConfig, self).unlink()
        rec = self.env['notification.config'].search([],limit=1)
        if rec:
            rec.write({'enabled':True})
        return res


    def sendPushNotification(self, data=None ,content=None, tokens=[]):
        enabled_config = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if enabled_config:
            app_id = enabled_config.app_id
            url =enabled_config.url
            default_message = enabled_config.default_message

            client = Client(url=url, app_id=app_id)
            response = client.sendPushNotification(data=data ,content=content, tokens=tokens)
            _logger.warning("##### API RESPONSE: %s",response)








    
   