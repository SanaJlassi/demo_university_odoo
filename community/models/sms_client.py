# -*- coding: utf-8 -*-
import cookielib
import urllib2
import json
import base64
import logging
_logger = logging.getLogger(__name__)


class Client:

    LOGIN_URL       = 'api/signin'
    SEND_SMS        = 'api/send-sms'
    COUNT_SMS       = 'api/counts-sms'
    UPDATE_DETAILS  = 'api/edit-user'
    GET_HEADER      = 'api/contact-headers'
    EDIT_HEADER     = 'api/edit-header'
    SENT_SMS        = 'api/list-sms'
    NOT_SENT_SMS    = 'api/drafts'
    

    def __init__(self, url='http://192.168.1.10:8080',debug=0):
        self.cookies = cookielib.CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPHandler(),urllib2.HTTPCookieProcessor(self.cookies))
        self.headers = {'Content-Type':'application/json', 'Accept':'application/json'}
        self.url = url
        self.login = None
        self.ticket = None
        self.data = { "userId": "admin","password": "Nevermind"}
        self.opener = opener
        self.debug = debug

    def sendSms(self, user_id, sms_header, sms, contacts):
        sendUrl = '%s/%s' % (self.url, Client.SEND_SMS)
        data = json.dumps({ "user_id":user_id, "sms_header":sms_header, "sms":sms, "contacts":contacts})
        request = AlfrescoRequest(sendUrl, data=data, headers=self.headers, method='POST')
        serverResponse = None
        response = {'success':False,'error':[]}
        try:
            if self.debug == 1:
                _logger.warning('Sending SMS')
            serverResponse = self.opener.open(request)
            if self.debug == 1:
                _logger.warning('SMS sent')
        except urllib2.HTTPError ,e:
            response['error'].append('Error while calling server')
        except urllib2.URLError, e:
            _logger.warning('Unable to connect')
            response['error'].append('Unable to open url, Connection refused')

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            _logger.warning("RESPONSE %s",data)
            if data['status'] == True:
                response['success'] = True
                return response
            else:
                response['success'] = False
            return response
        else:
            response['success'] = False
            return response

    def loginUser(self, login, password):
        loginUrl = '%s/%s' % (self.url, Client.LOGIN_URL)
        data = json.dumps({ "email": login,"pass": password})
        request = AlfrescoRequest(loginUrl, data=data, headers=self.headers, method='POST')
        serverResponse = None
        response = {'success':False,'error':[]}
        try:
            if self.debug == 1:
                _logger.warning('Try to connect as %s',login)
            serverResponse = self.opener.open(request)
            if self.debug == 1:
                _logger.warning('Connection success !!')
        except urllib2.HTTPError ,e:
            response['error'].append('Error while calling server')
        except urllib2.URLError, e:
            _logger.warning('Unable to connect')
            response['error'].append('Unable to open url, Connection refused')

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            if data['status'] == True:
                response['data'] = {
                    'id':data['data']['id'],
                    'name':data['data']['first_name'],
                    'last_name':data['data']['last_name'],
                    'password':data['data']['pass'],
                    'sms_header':data['data']['sms_header'],
                    'active':data['data']['active'],
                }
                serverResponse.close()
                response['success'] = True
                return response
            else:
                response['error'].append('Incorrect login or password')
                serverResponse.close()
                response['success'] = False
                return response
        else:
            response['success'] = False
            return response

    def updateDetails(self, json_object):
        updateUrl = '%s/%s' % (self.url, Client.UPDATE_DETAILS)
        data = json.dumps(json_object)
        request = AlfrescoRequest(updateUrl, data=data, headers=self.headers, method='POST')
        serverResponse = None
        response = {'success':False,'error':False}
        try:
            if self.debug == 1:
                _logger.warning('Updating profile data')
            serverResponse = self.opener.open(request)
        except urllib2.HTTPError ,e:
            response['error'] = 'Error while calling server'
        except urllib2.URLError, e:
            _logger.warning('Unable to connect')
            response['error']= 'Connection refusée'

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            response['success'] = True
            response['data'] = data
            return response
        else:
            response['success'] = False
            return response

    def updateHeader(self, json_object):
        updateUrl = '%s/%s' % (self.url, Client.EDIT_HEADER)
        data = json.dumps(json_object)
        request = AlfrescoRequest(updateUrl, data=data, headers=self.headers, method='POST')
        serverResponse = None
        response = {'success':False,'error':False}
        try:
            if self.debug == 1:
                _logger.warning('Updating header')
            serverResponse = self.opener.open(request)
            _logger.warning('After updatinh header')
        except urllib2.HTTPError ,e:
            response['error'] = 'Error while calling server'
        except urllib2.URLError, e:
            _logger.warning('Unable to connect')
            response['error']= 'Connection refusée'

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            response['success'] = True
            response['data'] = data
            return response
        else:
            response['success'] = False
            return response

    def get_header(self, user_id):
        getHeaderUrl = '%s/%s?user_id=%s' % (self.url, Client.GET_HEADER,user_id)
        request = AlfrescoRequest(getHeaderUrl,headers=self.headers, method='GET')
        serverResponse = None
        response = {'success':False,'error':False}
        try:
            if self.debug == 1:
                _logger.warning('Getting Headers')
            serverResponse = self.opener.open(request)
        except urllib2.HTTPError ,e:
            response['error'] = 'Error while calling server'
        except urllib2.URLError, e:
            _logger.warning('Unable to connect')
            response['error']= 'Connection refusée'

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            _logger.warning("### DATA %s",data)
            response['success'] = True
            response['data'] = data
            return response
        else:
            response['success'] = False
            return response


    def get_sms_count(self, user_id):
        countSmsUrl = '%s/%s?user_id=%s' % (self.url, Client.COUNT_SMS,user_id)
        request = AlfrescoRequest(countSmsUrl,headers=self.headers, method='GET')
        serverResponse = None
        response = {'success':False,'error':False}
        try:
            if self.debug == 1:
                _logger.warning('Getting SMS count')
            serverResponse = self.opener.open(request)
        except urllib2.HTTPError ,e:
            response['error'] = 'Error while calling server'
        except urllib2.URLError, e:
            _logger.warning('Unable to connect')
            response['error']= 'Connection refusée'

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            response['success'] = True
            response['data'] = data
            return response
        else:
            response['success'] = False
            return response

    def get_sent_sms(self, user_id):
        sentSmsUrl = '%s/%s?user_id=%s' % (self.url, Client.SENT_SMS,user_id)
        request = AlfrescoRequest(sentSmsUrl,headers=self.headers, method='GET')
        serverResponse = None
        response = {'success':False,'error':False}
        try:
            if self.debug == 1:
                _logger.warning('Getting sent SMS')
            serverResponse = self.opener.open(request)
        except urllib2.HTTPError ,e:
            response['error'] = 'Error while calling server'
        except urllib2.URLError, e:
            _logger.warning('Unable to connect')
            response['error']= 'Connection refusée'

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            response['success'] = True
            response['data'] = data
            return response
        else:
            response['success'] = False
            return response

    def get_not_sent_sms(self, user_id):
        notSentSmsUrl = '%s/%s?user_id=%s' % (self.url, Client.NOT_SENT_SMS,user_id)
        request = AlfrescoRequest(notSentSmsUrl,headers=self.headers, method='GET')
        serverResponse = None
        response = {'success':False,'error':False}
        try:
            if self.debug == 1:
                _logger.warning('Getting sent SMS')
            serverResponse = self.opener.open(request)
        except urllib2.HTTPError ,e:
            response['error'] = 'Error while calling server'
        except urllib2.URLError, e:
            _logger.warning('Unable to connect')
            response['error']= 'Connection refusée'

        if serverResponse is not None:
            jsonData = serverResponse.read()
            data = json.loads(jsonData)
            response['success'] = True
            response['data'] = data
            return response
        else:
            response['success'] = False
            return response

    def createUser(self,userData):

        createUserUrl = '%s/%s' % (self.url, Client.CREATE_USER)
        user = {}
        response = {'success':True}
        if 'id' in userData:
            user['id'] = userData['id']
        else:
            response['success']=False
            response['Error']={'Id':'Must not be empty'}

        if 'firstName' in userData:
            user['firstName'] = userData['firstName']
        else:
            response['success']=False
            response['Error']={'FirstName':'Must not be empty'}

        if 'lastName' in userData:
            user['lastName'] = userData['lastName']

        if 'email' in userData:
            user['email'] = userData['email']
        else:
            response['success']=False
            response['Error']={'Email':'Must not be empty'}

        if 'password' in userData:
            user['password'] = userData['password']
        else:
            response['success']=False
            response['Error']={'Password':'Must not be empty'}

        if response['success'] == False:
            return response
        else:
            print user

        request = AlfrescoRequest(createUserUrl,data=json.dumps(user), headers=self.headers,method='POST')
        response = None
        try:
            _logger.warning('Creating a user ...')
            response = self.opener.open(request)
            jsonResponse = json.loads(response.read())
            print json.dumps(jsonResponse, indent=4, sort_keys=True)    
        except urllib2.HTTPError, e:
            if e.code == 400:
                print 'Invalid parameter'
            elif e.code == 401:
                print 'Authentication failed'   
            elif e.code == 403:
                print 'Current user does not have permission to create a person'
            elif e.code == 409:
                print 'Person within given id already exists'
            elif e.code == 422:
                print 'Model integrity exception'
            else:
                printException(e)
        if response is not None:
            response.close()
            return {'success':True}
        else:
            return {'success':False}

    def updateUser(self,userData):

        user = {}
        response = {'success':True}
        if 'id' in userData:
            print 'OK' 
        else:
            response['success']=False
            response['Error']={'Id':'Must not be empty'}

        if 'firstName' in userData:
            user['firstName'] = userData['firstName']
        else:
            response['success']=False
            response['Error']={'FirstName':'Must not be empty'}

        if 'lastName' in userData:
            user['lastName'] = userData['lastName']

        if 'enabled' in userData:
            user['enabled'] = userData['enabled']

        if 'email' in userData:
            user['email'] = userData['email']
        else:
            response['success']=False
            response['Error']={'Email':'Must not be empty'}

        if 'password' in userData:
            user['password'] = userData['password']
        else:
            response['success']=False
            response['Error']={'Password':'Must not be empty'}

        if response['success'] == False:
            return response
        else:
            print user

        createUserUrl = '%s/%s/%s' % (self.url, Client.UPDATE_USER,userData['id'])

        request = AlfrescoRequest(createUserUrl,data=json.dumps(user), headers=self.headers,method='PUT')
        response = None
        try:
            print 'Updating a user ...'
            response = self.opener.open(request)
            jsonResponse = json.loads(response.read())
            print json.dumps(jsonResponse, indent=4, sort_keys=True)    
        except urllib2.HTTPError, e:
            if e.code == 400:
                print 'Invalid parameter'
            elif e.code == 404:
                print 'personId does not exist'
            elif e.code == 401:
                print 'Authentication failed'   
            elif e.code == 403:
                print 'Current user does not have permission to create a person'
            elif e.code == 409:
                print 'Person within given id already exists'
            elif e.code == 422:
                print 'Model integrity exception'
            else:
                printException(e)
        if response is not None:
            response.close()
            return {'success':True}
        else:
            return {'success':False}

    def createSite(self, siteData=None):
        """
        @param siteData: {"id": "SiteId","title": "SiteTitle","description": "description of site","visibility": "PRIVATE"}
        """
        createSiteUrl = '%s/%s' % (self.url,Client.CREATE_SITE)
        #site = {"id": "SiteId3","title": "SiteTitle","description": "description of site","visibility": "PRIVATE"}
        if 'id' not in siteData:
            raise ValueError('site ID is required !')
        elif 'title' not in siteData:
            raise ValueError('site Title is required !')
        elif 'visibility' not in siteData:
            raise ValueError('site Visibility is required !')

        request = AlfrescoRequest(createSiteUrl,data=json.dumps(siteData), headers=self.headers,method='POST')
        response = None
        try:
            print 'Creating a site ...'
            response = self.opener.open(request)
            jsonResponse = json.loads(response.read())
            print json.dumps(jsonResponse, indent=4, sort_keys=True)    
        except urllib2.HTTPError, e:
            if e.code == 400:
                print 'Invalid parameter: id, title, or description exceed the maximum length; or id contains invalid characters; or siteBodyCreate invalid'
            elif e.code == 401:
                print 'Authentication failed'   
            elif e.code == 409:
                print 'Site with the given identifier already exists'
            else:
                printException(e)

class AlfrescoInstance:

    def __init__(self, url):
        self.url = url.rstrip('/')

    def getBaseUrl(self):
        """ return the base url for the running alfresco instance"""
        return self.url

    def getUrl(self, path):
        """
        concat the base url with the path pointing to the needed function 
        @param path : path to the function Exp: 'sites' 
        """
        return '%s/%s' % (self.getBaseUrl(),path)



class AlfrescoRequest(urllib2.Request):
    """
    Extending the urllib2.Request class in order to add additional request method
    to GET and POST like PUT and DELETE 
    """
    def __init__(self, url, data=None, headers={}, origin_req_host=None, unverifiable=False, method=None):
        urllib2.Request.__init__(self, url, data, headers, origin_req_host, unverifiable)
        self.httpMethod = method

    def get_method(self):
        if self.httpMethod is not None:
            return self.httpMethod
        else:
            return urllib2.Request.get_method(self)

    def set_method(self, method):
        self.httpMethod = method



def printException(exception):
    print '---- > Unkown Error occured : \n\tURL : %s \n\tCODE : %s \n\tMSG : %s' % (exception.url, exception.code, exception.msg)
    jsonData = json.loads(exception.fp.read())
    if jsonData is not None:
        print json.dumps(jsonData, indent=4, sort_keys=True)    
    else:
        print 'Response is not of type JSON\n'


if __name__ == '__main__':
    client = Client(url='http://192.168.1.10:8080',debug=1)
    response = client.loginUser('admin','Nevermind')
    user = {
      "id":"amir",
      "firstName": "amir",
      "lastName": "ben ahmed",
      "email": "abdel@example.com",
      "password": "Nevermind",
    }
    print client.createUser(user)