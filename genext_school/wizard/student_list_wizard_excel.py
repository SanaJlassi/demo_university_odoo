# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)
import xlsxwriter

class StudentListWizard(models.TransientModel):
    _name = 'student.test.wizard'

    grade_id                                = fields.Many2one('school.grade', ondelete='set null', string='Niveau scolaire', required=True)
    class_id                                = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", ondelete="set null", string="Classe")
    #student_ids                             = fields.Many2many(comodel_name='school.student', relation='school_task_school_student_rel', domain="[('class_id','=',class_id)]", string="Élèves")  
    student_ids                             = fields.Many2many('school.student', string="Étudiants", compute='_compute_students', domain="[('class_id','=',class_id)]") 
    student_info                            = fields.Many2many(comodel_name='ir.model.fields', relation='student_field_relation', domain="[('model_name','=','school.student')]", string="Formulaire Étudiant")
    @api.depends('class_id','grade_id')
    @api.one
    def _compute_students(self):
        student_list = []
        if self.class_id.id:
           students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
           for student in students:
               student_list.append(student.id)
        else:
           students = self.env['school.student'].search([('grade_id','=',self.grade_id.id)])
           for student in students:
               student_list.append(student.id)
        _logger.warning("STUDENT %s",student_list)
        self.update({'student_ids':[(6,False,[y for y in student_list])]})
    
    
    def _build_contexts(self, data):
        result = {}
        result['grade_id'] = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        result['class_id'] = 'class_id' in data['form'] and data['form']['class_id'] or False
        result['student_ids'] = 'student_ids' in data['form'] and data['form']['student_ids'] or False
        result['student_info'] = 'student_info' in data['form'] and data['form']['student_info'] or False
        return result

    def _print_report(self , data):
        return self.env['report'].get_action(self, 'genext_school.report_test.xlsx' , data=data)

    @api.multi
    def check_report(self, data):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['grade_id','class_id','student_ids','student_info'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        return self._print_report(data)
    #def print_xls_report(self, data):
       # self.ensure_one()
        #rec = self.browse(data)
       # data = {}
        #data['ids'] = self.env.context.get('active_ids', [])
       # data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        #data['form'] = rec.read(['grade_id','class_id'])
       # return self.env['report'].get_action(rec,'genext_school.report_test.xlsx',
        


class StudentListReport(ReportXlsx):
     
 
    def generate_xlsx_report(self,workbook,data,obj): 

         #We can recieve the data entered in the wizard here as data
        #active_ids = data['ids']
        grade = obj.grade_id
        grade_name = obj.grade_id.name
        row = 0
        class_list = []
        student_list = []
        #data = self.get_student_data()
        student_obj = self.env['student.list.wizard'].search([('grade_id.name','=',grade_name)])
        students = student_obj.mapped('student_ids')
        infos_list = obj.student_info
        for student in students:
            student_list.append(student)
        student_exemple = student_list[0]
        print"studentexemaole",student_exemple
        for student in student_list:
            if not(student.class_id in class_list):
                class_list.append(student.class_id)
        infos = []
        for i in infos_list:
            infos.append(i.name)
        #obj = self.env['student.list.wizard'].search([])
        #grade = obj.mapped('grade_id')
        #grade = obj.grade_id
        for cl in class_list:
            row = 6
            classe = workbook.add_worksheet(cl.name)
            style_2 = workbook.add_format()
            style_2.set_bg_color('#87CEEB')
            style_2.set_font_color('blue')
            style_2.set_border()
            bold = workbook.add_format({'bold': 1})
            style_1 = workbook.add_format({'bold': 1})
            style_1.set_bg_color('#6495ED')
            style_3 = workbook.add_format()
            style_3.set_border()
            style_4 = workbook.add_format({'bold': 1})
            #blue = workbook.add_format({'color':'blue'})
            classe.set_column('A:A', 18)
            classe.set_column('F:F', 15)
            classe.set_column('C:C', 12)
            cell_format = workbook.add_format()
            cell_format.set_font_size(10)
            classe.write(0,3,'Liste Étudiants',style_1)
            classe.write(0,4,'',style_1)
            classe.write(2,0,'ANNÉE SCOLAIRE',style_4)
            classe.write(2,2,student_exemple.academic_year_id.code)
            classe.write(3,0,'NIVEAU SCOLAIRE',style_4)
            classe.write(3,2,grade_name)
            classe.write(2,5,'NIVEAU ÉDUCATIF',style_4)
            classe.write(2,7,student_exemple.educational_stage_id.name)
            classe.write(3,5,'CLASSE',style_4)
            classe.write(3,7,cl.name)
            col = 0
            if infos:
                if 'pid' in infos:
                    classe.write(5,col,'PID',style_2)
                    col += 1
                if 'state' in infos:
                    classe.write(5,col,'ÉTAT',style_2)
                    col += 1
                if 'name' in infos:
                    classe.write(5,col,'NOM',style_2)
                    col +=  1
                if 'last_name' in infos:
                    classe.write(5,col,'PRÉNOM',style_2)
                    col += 1
                if 'gender' in infos:
                    classe.write(5,col,'SEXE',style_2)
                    col +=1
                if 'age' in infos:
                    classe.write(5,col,'AGE',style_2)
                    col += 1
                if 'birth_date' in infos:                        
                    classe.write(5,col,'DATE DE NAISSANCE',style_2)
                    col += 1
                if 'birthday_city' in infos:
                    classe.write(5,col,'LIEU DE NAISSANCE',style_2)
                    col += 1
                if 'street_1' in infos:
                    classe.write(5,col,'ADRESSE',style_2)
                    col += 1
                if 'country_id' in infos:
                    classe.write(5,col,'PAYS',style_2)
                    col += 1
                if 'country_id_1' in infos:
                    classe.write(5,col,'NATIONALITÉ 1',style_2)
                    col += 1
                if 'country_id_2' in infos:
                    classe.write(5,col,'NATIONALITÉ 2',style_2)
                    col += 1
                if 'mobile' in infos:
                    classe.write(5,col,'TÉLEPHONE',style_2)
                    col += 1
                if 'parent_ids' in infos:
                    classe.write(5,col,'PARENTS',style_2)
                    col += 1
                if 'parents_phone' in infos:
                    classe.write(5,col,'TÉLEPHONE DES PARENTS',style_2)
                    classe.set_column(col,col, 27)
                    col += 1
                if 'parents_email' in  infos:
                    classe.write(5,col,'ADRESSES EMAIL DES PARENTS',style_2)
                    classe.set_column(col,col, 27)
                    col += 1
                if 'legal_responsable' in infos:
                    classe.write(5,col,'RESPONSABLE LÉGAL',style_2)
                    classe.set_column(col,col,20)
                    col += 1
            row = 6
            for i in student_list:
                col = 0
                if i.class_id == cl:
                    if infos:
                        if 'pid' in infos:
                            if i.pid:
                                classe.write(row,col,i.pid,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'state' in infos:
                            if ((i.state) and (i.state != ' ')):
                                classe.write(row,col,i.state,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'name' in infos:
                            if ((i.name) and (i.name != ' ')):
                                classe.write(row,col,i.name,style_3)
                                col +=  1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'last_name' in infos:
                            if ((i.last_name) and (i.last_name != ' ')):
                                classe.write(row,col,i.last_name,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'gender' in infos:
                            classe.write(row,col,i.gender,style_3)
                            col +=1
                        if 'age' in infos:
                            if ((i.age) and (i.age!=' ')):
                                classe.write(row,col,i.age,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'birth_date' in infos:
                            if ((i.birth_date) and (i.birth_date != ' ')):                     
                                classe.write(row,col,i.birth_date,style_3)
                                classe.set_column(col,col,20)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                classe.set_column(col,col,20)
                                col += 1
                        if 'birthday_city' in infos:
                            if ((i.birthday_city) and (i.birthday_city != ' ')):
                                classe.write(row,col,i.birthday_city,style_3)
                                classe.set_column(col,col,20)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                classe.set_column(col,col,20)
                                col += 1
                        if 'street_1' in infos:
                            if ((i.street_1) and (i.street_1 != ' ')):
                                classe.write(row,col,i.street_1,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'country_id' in infos:
                            if i.country_id:
                                classe.write(row,col,i.country_id,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'country_id_1' in infos:
                            if ((i.country_id_1) and (i.country_id_1 != ' ')):
                                classe.write(row,col,i.country_id_1,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'country_id_2' in infos:
                            if ((i.country_id_2) and (i.country_id_2 != ' ')):
                                classe.write(row,col,i.country_id_2,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'mobile' in infos:
                            if ((i.mobile) and (i.mobile != ' ')):
                                classe.write(row,col,i.mobile,style_3)
                                col += 1
                            else:
                                classe.write(row,col,'-',style_3)
                                col += 1
                        if 'parent_ids' in infos:
                            nom = ''
                            col_taille = 0
                            for p in i.parent_ids:
                                nom = nom + p.name + ' ' + p.last_name + '  '
                                col_taille += 15
                            classe.write(row,col,nom,style_3)
                            classe.set_column(col,col,col_taille)
                            col += 1
                        if 'parents_phone' in infos:
                            tel = ''
                            col_taille = 0
                            if i.parent_ids:
                                for p in i.parent_ids:
                                    tel = tel + ' ' + p.phone
                                    col_taille += 25
                            else:
                                tel = '-'
                                col_taille += 25
                            classe.write(row,col,tel,style_3)
                            classe.set_column(col,col,col_taille)
                            col += 1
                        if 'parents_email' in  infos:
                            email = ''
                            col_taille = 0
                            if i.parent_ids:
                                for p in i.parent_ids:
                                    email += p.email
                                    email += ' '
                                    col_taille += 30
                            else:
                                email = '-'
                                col_taille += 30
                            classe.write(row,col,email,style_3)
                            classe.set_column(col,col,col_taille)
                            col += 1
                        if 'legal_responsable' in infos:
                            nom = i.legal_responsable.name + ' ' + i.legal_responsable.last_name
                            classe.write(row,col,nom,style_3)
                            col += 1
                    row += 1
            row =0
            
        #col = 1
        #for x in infos:
        #    classe.write(7,col,x.name)
        #    col += 1
        #classe.set_column('B:B', 15)
       

        #We can recieve the data entered in the wizard here as data
StudentListReport('report.genext_school.report_test.xlsx','student.test.wizard')



