# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)

class ExamNote(models.Model):
    _name = 'school.exam.note'
    _description = 'Exam Note'

    name 							= fields.Char('Nom')
    academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)
    # exam_id                         = fields.Many2one('school.exam.subject.config', string="Examen Matière", domain="[('educational_stage_id','=',educational_stage_id), ('academic_year','=',academic_year)]" ,required=True)
    periode_config_id               = fields.Many2one('school.exam.periode.config', string="Session", domain="[('educational_stage_id','=',educational_stage_id), ('academic_year','=',academic_year)]", required=True)
    periode_ids                     = fields.Many2one('school.exam.periode', string="Période", domain="[('periode_config_id','=',periode_config_id)]", required=True)
    grade_id                        = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]",required=True)
    class_id                        = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)
    subject_id                      = fields.Many2one('school.subject', string="Matière",required=True)
    teacher_id                      = fields.Many2one('hr.employee', domain="['&',('is_school_teacher','=',True),('subject_ids','=',subject_id)]", string="Enseignant", required=True)
    # teacher_id                      = fields.Many2one('hr.employee', string="Enseignant", required=True, compute='_compute_default_teacher')
    exam_type_id                    = fields.Many2one('school.exam.type',string="Type examen", domain="[('educational_stage_id','=',educational_stage_id)]", required=True )
    date                            = fields.Date(string='Date', default=fields.Date.context_today, required=True)
    scale                           = fields.Float(related="exam_type_id.scale", string='Barème', required=True)
    # exam_subject_config             = fields.Many2one('school.exam.subject.config',string="Examens Matières", domain="['&',('periode_config_id','=',periode_config_id),'&',('periode_ids','=',periode_ids),'&',('grade_id','=',grade_id),('subject_id','=',subject_id)]")    
    # exam_subject_types              = fields.Many2many(related="exam_subject_config.exam_type_ids")   

    exam_notes_line_ids             = fields.One2many('school.exam.note.line', 'exam_line_id', ondelete='cascade')

    _sql_constraints = [('exam_notes', 'unique(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id, exam_type_id)',"Cette configuration est déjà saisie")] 

    @api.onchange('class_id')
    def _onchange_class_id(self):
        student_line= []
        if len(self.class_id):
            students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in students:
                student_line.append( (0,0,{'name':'name','student_id':student.id, 'note':None, 'comment':None}) )
        else:
            _logger.warning("###### NO CLASS ID"  )  
        return {'value':{'exam_notes_line_ids':student_line}}

    @api.multi
    def _compute_default_teacher(self):
        for record in self:
            teacher_list = []
            for student in record.student_ids:
                teacher_ids = self._get_class_teachers(student.class_id)
                teacher_list.extend(teacher_ids)
            record.contact_ids = self.env['hr.employee'].browse(list(set(teacher_list)))


class ExamNoteLine(models.Model):
    _name = 'school.exam.note.line'

    @api.multi
    def _compute_moyenne(self):
        ######################## fix it (if we have more than one object for the same exam type) ##########################################################################
        for rec in self:
            exam_list = []
            total = 0.0 
            div = 0.0
            average = 0.0 
            domain = [('academic_year','=',rec.exam_line_id.academic_year.id),('educational_stage_id','=',rec.exam_line_id.educational_stage_id.id),('periode_config_id','=',rec.exam_line_id.periode_config_id.id),('periode_ids','=',rec.exam_line_id.periode_ids.id),('grade_id','=',rec.exam_line_id.grade_id.id),('class_id','=',rec.exam_line_id.class_id.id),('subject_id','=',rec.exam_line_id.subject_id.id)] 
            exam_list = self.env['school.exam.note'].search(domain)
            if len(exam_list):
                for exam in exam_list:
                    note_id = exam.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == rec.student_id.id)
                    total += note_id.note * exam.exam_type_id.coefficient
                    div += exam.exam_type_id.coefficient   
                rec.average = round(total/div, 2)

    exam_line_id                = fields.Many2one('school.exam.note', string="Examen")
    student_id                  = fields.Many2one('school.student', string="Étudiants")
    note                        = fields.Float(string='Note', default=0.0)
    average                     = fields.Float(string="Moyenne Matière", default=0.0, compute=_compute_moyenne)
    comment                     = fields.Char(string="Commentaire")



class ExamAppreciationModule(models.Model):
    _name = 'exam.appreciation.module'
    _description = 'Appreciations modules'

    name                            = fields.Char('Commentaire')
    academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)
    periode_config_id               = fields.Many2one('school.exam.periode.config', string="Session", domain="[('educational_stage_id','=',educational_stage_id), ('academic_year','=',academic_year)]", required=True)
    periode_ids                     = fields.Many2one('school.exam.periode', string="Période", domain="[('periode_config_id','=',periode_config_id)]", required=True)
    grade_id                        = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]",required=True)
    module_id                       = fields.Many2one('school.module', string="Module",required=True)
    class_id                        = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)

    comment_line_ids                = fields.One2many('exam.comment.line','module_comment_id', string="Appréciations")
    module_average_line_ids         = fields.One2many('school.module.average.line', 'module_average_line_id', ondelete='cascade')

    _sql_constraints = [('module_average', 'unique(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id)',"Cette configuration est déjà saisie")] 

    @api.onchange('class_id')
    def _onchange_class_id(self):
        comment_list = []
        average_list = []
        if len(self.class_id):
            student_id = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in student_id:
                comment_list.append((0,0,{'student_id':student.id, 'comment':None}))
                average_list.append((0,0,{'student_id':student.id, 'average':0.0}))
        return {'value':{'comment_line_ids':comment_list, 'module_average_line_ids':average_list}}


class ExamCommentLine(models.Model):
    _name = 'exam.comment.line'

    council_comment_id                  = fields.Many2one('exam.appreciation.council')
    module_comment_id                   = fields.Many2one('exam.appreciation.module')
    student_id                          = fields.Many2one('school.student', string="Étudiants", required=True, readonly=True)
    comment                             = fields.Text('Commentaire')


class ExamAppreciationCouncil(models.Model):
    _name = 'exam.appreciation.council'
    _description = 'Appreciations Conseils'

    name                            = fields.Char('Commentaire')
    academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)
    periode_config_id               = fields.Many2one('school.exam.periode.config', string="Session", domain="[('educational_stage_id','=',educational_stage_id), ('academic_year','=',academic_year)]", required=True)
    periode_ids                     = fields.Many2one('school.exam.periode', string="Période", domain="[('periode_config_id','=',periode_config_id)]", required=True)
    grade_id                        = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]",required=True)
    class_id                        = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)

    comment_line_ids                = fields.One2many('exam.comment.line','council_comment_id', string="Appréciations")
    period_average_line_ids         = fields.One2many('school.period.average.line', 'period_average_line_id', ondelete='cascade')

    _sql_constraints = [('average_period', 'unique(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id)',"Cette configuration est déjà saisie")] 

    @api.onchange('class_id')
    def _onchange_class_id(self):
        comment_list = []
        average_list = []
        if len(self.class_id):
            student_id = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in student_id:
                average_list.append((0,0,{'student_id':student.id, 'average':0.0}))
                comment_list.append((0,0,{'student_id':student.id, 'comment':None}))
        return {'value':{'comment_line_ids':comment_list, 'period_average_line_ids':average_list}}



# class SubjectAverage(models.Model):
#     _name = 'school.subject.average'
#     _description = 'Subject Average'

#     name                            = fields.Char('Nom')
#     academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
#     educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)
#     periode_config_id               = fields.Many2one('school.exam.periode.config', string="Session", domain="[('educational_stage_id','=',educational_stage_id), ('academic_year','=',academic_year)]", required=True)
#     periode_ids                     = fields.Many2one('school.exam.periode', string="Période", domain="[('periode_config_id','=',periode_config_id)]", required=True)
#     grade_id                        = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]",required=True)
#     class_id                        = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)
#     subject_id                      = fields.Many2one('school.subject', string="Matière",required=True)
#     teacher_id                      = fields.Many2one('hr.employee', domain="['&',('is_school_teacher','=',True),('subject_ids','=',subject_id)]", string="Enseignant", required=True)
#     subject_average_line_ids        = fields.One2many('school.subject.average.line', 'subject_average_line_id', ondelete='cascade')

#     _sql_constraints = [('subject_average', 'unique(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id, teacher_id)',"Cette configuration est déjà saisie")] 

    
#     @api.onchange('class_id')
#     def _onchange_class_id(self):
#         student_line= []
#         if len(self.class_id):
#             students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
#             for student in students:
#                 student_line.append( (0,0,{'name':'name','student_id':student.id, 'average':0.0, 'comment':None}) )
#         else:
#             _logger.warning("###### NO CLASS ID"  )  
#         return {'value':{'subject_average_line_ids':student_line}}


# class SubjectAverageLine(models.Model):
#     _name = 'school.subject.average.line'

#     @api.multi
#     def _compute_moyenne(self):
#         for rec in self:
#             exam_list = []
#             total = 0.0 
#             div = 0.0
#             average = 0.0 
#             domain = [('academic_year','=',rec.subject_average_line_id.academic_year.id),('educational_stage_id','=',rec.subject_average_line_id.educational_stage_id.id),('periode_config_id','=',rec.subject_average_line_id.periode_config_id.id),('periode_ids','=',rec.subject_average_line_id.periode_ids.id),('grade_id','=',rec.subject_average_line_id.grade_id.id),('class_id','=',rec.subject_average_line_id.class_id.id),('subject_id','=',rec.subject_average_line_id.subject_id.id)] 
#             exam_list = self.env['school.exam.note'].search(domain)
#             if len(exam_list):
#                 for exam in exam_list:
#                     note_id = exam.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == rec.student_id.id)
#                     total += note_id.note * exam.exam_type_id.coefficient
#                     div += exam.exam_type_id.coefficient   
#                 rec.average = round(total/div, 2)
    


#     subject_average_line_id     = fields.Many2one('school.subject.average')
#     student_id                  = fields.Many2one('school.student', string="Élève")
#     average                     = fields.Float(string='Moyenne', default=0.0, compute=_compute_moyenne)
#     comment                     = fields.Char(string="Commentaire")



class ModuleAverageLine(models.Model):
    _name = 'school.module.average.line'

    @api.multi
    def _compute_moyenne(self):  
        for rec in self :             
            total_average_subject = 0.0
            total_coeff = 0.0 
            for subject in rec.subject_ids :
                print "############################# SUBJECT ID",subject.name
                exam_list = []
                average_subject = 0.0 
                domain = [('academic_year','=',rec.module_average_line_id.academic_year.id),('educational_stage_id','=',rec.module_average_line_id.educational_stage_id.id),('periode_config_id','=',rec.module_average_line_id.periode_config_id.id),('periode_ids','=',rec.module_average_line_id.periode_ids.id),('grade_id','=',rec.module_average_line_id.grade_id.id),('class_id','=',rec.module_average_line_id.class_id.id),('subject_id','=',subject.id)] 
                exam_list = self.env['school.exam.note'].search(domain)
                if len(exam_list):                    
                    for exam in exam_list:
                        print "############################## EXAM ID",exam.exam_type_id.type
                        note_id = exam.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == rec.student_id.id)
                        average_subject = note_id.average
                    print "################################### AVERAGE SUBJECT",average_subject
                total_average_subject += average_subject * subject.coefficient
                total_coeff += subject.coefficient
            if total_coeff != 0:
                rec.average = round(total_average_subject/total_coeff, 2)
                print "################################# AVERAGE MODULE",rec.average
                    

    module_average_line_id      = fields.Many2one('exam.appreciation.module')
    student_id                  = fields.Many2one('school.student', string="Étudiants")
    average                     = fields.Float(string='Moyenne', default=0.0, compute=_compute_moyenne)
    comment                     = fields.Char(string="Commentaire")
    subject_ids                 = fields.Many2many(related="module_average_line_id.module_id.subject_ids", string="Matières")

class PeriodAverageLine(models.Model):
    _name = 'school.period.average.line'

    @api.multi
    def _compute_moyenne(self):
        for rec in self:
            module_note_list = []
            total = 0.0 
            div = 0.0
            average = 0.0 
            domain = [('academic_year','=',rec.period_average_line_id.academic_year.id),('educational_stage_id','=',rec.period_average_line_id.educational_stage_id.id),('periode_config_id','=',rec.period_average_line_id.periode_config_id.id),('periode_ids','=',rec.period_average_line_id.periode_ids.id),('grade_id','=',rec.period_average_line_id.grade_id.id),('class_id','=',rec.period_average_line_id.class_id.id)] 
            module_note_list = self.env['exam.appreciation.module'].search(domain)
            if len(module_note_list):
                for line in module_note_list:
                    note_id = line.mapped('module_average_line_ids').filtered(lambda r: r.student_id.id == rec.student_id.id)
                    total += note_id.average 
                    div += 1
                if div != 0 :
                    rec.average = round(total/div, 2)  

    
                    
    period_average_line_id      = fields.Many2one('exam.appreciation.council')
    student_id                  = fields.Many2one('school.student', string="Étudiants")
    average                     = fields.Float(string='Moyenne', default=0.0, compute=_compute_moyenne)
    rank                        = fields.Integer(string='Rang', compute='_compute_rank', store=True, default=False)
    comment                     = fields.Char(string="Commentaire")

    @api.multi
    @api.depends('average')
    def _compute_rank(self):
        average_list = []
        for rec in self:
            average_list.append(rec.average)            
        average_list.sort(reverse=True)
        for rec in self:
            rec.rank = average_list.index(rec.average) +1 
            # print "############ rank"rec.rank