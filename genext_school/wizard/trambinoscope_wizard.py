# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)


class TrambinoscopeWizard(models.TransientModel):
    _name = 'student.trambinoscope.wizard'

    grade_id                                = fields.Many2one('school.grade', ondelete='set null', string='Niveau scolaire', required=True)
    class_id                                = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", ondelete="set null", string="Classe", required=True)
    student_ids                             = fields.Many2many('school.student', string="Étudiants", compute='_compute_students', domain="[('class_id','=',class_id)]")  
    
    

    @api.depends('class_id')
    @api.one
    def _compute_students(self):
        student_list = []
        students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
        for student in students:
            student_list.append(student.id)
        _logger.warning("STUDENT %s",student_list)
        self.update({'student_ids':[(6,False,[y for y in student_list])]})

    def _build_contexts(self, data):
        result = {}
        result['grade_id'] = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        result['class_id'] = 'class_id' in data['form'] and data['form']['class_id'] or False
        result['student_ids'] = 'student_ids' in data['form'] and data['form']['student_ids'] or False
        return result

    def _print_report(self, data):
        return self.env['report'].get_action(self, 'genext_school.report_trambinoscope', data=data)

    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['grade_id','class_id','student_ids'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        return self._print_report(data)


class TrambinoscopeReport(models.AbstractModel):
    _name = 'report.genext_school.report_trambinoscope'

    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})

        student_ids = 'student_ids' in data['form'] and data['form']['student_ids'] or False

        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            'students': student_ids,
            # 'docs': student_id,
            # 'no_student': no_student,
            'time': time,
        }
        return self.env['report'].render('genext_school.report_trambinoscope', docargs)
