# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo import models
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime

from passlib.context import CryptContext
import psycopg2
import getpass
import signal
import sys
import logging
_logger = logging.getLogger(__name__)



class SchoolParent(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_PUT = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'PUT',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_DELETE = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'DELETE',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/user/update/profile', type='http', auth="user", methods=['POST'],  csrf=False)
    def parent_edit_profile(self, **kwargs):
        response = {'success':False, 'data':None}
        #if not request.env.user.has_group('genext_school.group_school_parent'):
           # response['success'] = False
           # response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
          # return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

        print "########## USER TYPE ",request.env.user.user_type

        json_object = None
        try:
            json_object = json.loads(request.params.get('profile',False))
            # print "JSON-OBJECT ",json_object
        except TypeError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to profile appointment object'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        if request.env.user.user_type == 'parent':
            parent = {}
            if 'name' in json_object:
                parent['name'] = json_object['name']
            if 'last_name' in json_object:
                parent['last_name'] = json_object['last_name']
            if 'phone' in json_object:
                parent['phone'] = json_object['phone']
            if 'mobile' in json_object:
                parent['mobile'] = json_object['mobile']
            if 'email' in json_object:
                parent['email'] = json_object['email']
            if 'fax' in json_object:
                parent['fax'] = json_object['fax']
            if 'image' in json_object:
                base64 = json_object['image'].split(",")
                print "################ IMAGE METADATA ", base64[0]
                print "################ IMAGE DATA ", base64[1][:20] 
                
                parent['image'] = base64[1].decode('utf-8').replace(' ', '')
                print "################# SPACES NUMBER ",parent['image'].count(' ')
            partner_id = request.env['res.partner'].sudo().search([('id','=',request.env.user.partner_id.id)])
            res = partner_id.sudo().write(parent)

            _logger.warning('########### EDIT PARENT OBJECT %s',parent)
            print "##### EDIT RESPONSE ",res
            response['success'] = True
        elif request.env.user.user_type == 'teacher':
            'name'
            'last_name'
            'mobile_phone'
            'work_email'
            parent = {}
            if 'name' in json_object:
                parent['name'] = json_object['name']
            if 'last_name' in json_object:
                parent['last_name'] = json_object['last_name']
            if 'mobile_phone' in json_object:
                parent['phone'] = json_object['phone']
            if 'mobile' in json_object:
                parent['mobile'] = json_object['mobile']
            if 'work_email' in json_object:
                parent['email'] = json_object['email']
            if 'image' in json_object:
                base64 = json_object['image'].split(",")
                print "################ IMAGE METADATA ", base64[0]
                print "################ IMAGE DATA ", base64[1][:20] 
                
                parent['image'] = base64[1].decode('utf-8').replace(' ', '')
                print "################# SPACES NUMBER ",parent['image'].count(' ')
            partner_id = request.env['res.partner'].sudo().search([('id','=',request.env.user.partner_id.id)])
            res = partner_id.sudo().write(parent)

            _logger.warning('########### EDIT TEACHER OBJECT %s',parent)
            print "##### EDIT RESPONSE ",res
            response['success'] = True

        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)




   

    @http.route('/api/parent/payments', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_payments(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

        partner_id = request.env.user.partner_id
        student_id  = request.env.user.partner_id.student_ids
        payements_list = []
        for student in student_id:
            payments = request.env['account.payment'].search([('student_id','=',student.id)])
            lines = []
            for payment in payments:
                for payement_line in payment.account_payment_line_ids:
                    line = {
                        'id':payement_line.id,
                        'product':payement_line.product_id.name,
                        'description':payement_line.description,
                        'payment_date':payment.payment_date,
                        'month':payement_line.month,
                        'price':payement_line.price,
                        'quantity':payement_line.quantity,
                        'discount':payement_line.discount,
                        'total_ttc':payement_line.total_ttc,
                        'class':{
                            'id':payement_line.class_id.id,
                            'name':payement_line.class_id.name,
                            'code':payement_line.class_id.code,
                        }
                    }
                    lines.append(line)
            student_obj = {
                'id':payement_line.student_id.id,
                'name':payement_line.student_id.name,
                'last_name':payement_line.student_id.last_name,
                'payments':lines
            }
            payements_list.append(student_obj)
        response['data'] = payements_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)  

    @http.route('/api/parent/payments/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_paymentsby_academic_year_period(self,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        academic_years=request.env['academic.year'].search([('id','=',academic_year_id)])
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        partner_id = request.env.user.partner_id
        student_id  = request.env.user.partner_id.student_ids
        payements_list = []
        for student in student_id:
            payments = request.env['account.payment'].search([('student_id','=',student.id)])
            lines = []
            for payment in payments:
                if academic_years.start_date <= payment.payment_date <= academic_years.end_date:
                    if timing_periode.start_date <= payment.payment_date <= timing_periode.end_date:
                        for payement_line in payment.account_payment_line_ids:
                            line = {
                                'id':payement_line.id,
                                'product':payement_line.product_id.name,
                                'description':payement_line.description,
                                'payment_date':payment.payment_date,
                                'month':payement_line.month,
                                'price':payement_line.price,
                                'quantity':payement_line.quantity,
                                'discount':payement_line.discount,
                                'total_ttc':payement_line.total_ttc,
                                'class':{
                                    'id':payement_line.class_id.id,
                                    'name':payement_line.class_id.name,
                                    'code':payement_line.class_id.code,
                                }
                            }
                            lines.append(line)
            
                            student_obj = {
                            'id':payement_line.student_id.id,
                            'name':payement_line.student_id.name,
                            'last_name':payement_line.student_id.last_name,
                            'payments':lines
                            }
                            payements_list.append(student_obj)
            
        response['data'] = payements_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)       


    @http.route('/api/parent/contacts', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_contact(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        else:         
            res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
            contact_list = []
            for contact in  res_partner.contact_ids:
                subjects = []
                for subject in contact.subject_ids:
                    subjects.append({'id':subject.id, 'name':contact.name, 'code':subject.code})
                json_object = {
                    'id':contact.id,
                    'name':contact.name,
                    'last_name':contact.last_name,
                    'image':contact.image,
                    'subjects':subjects,
                }
                contact_list.append(json_object)
            
        response['success'] = True
        response['data'] = contact_list
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)


    @http.route('/api/parent/childrens', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_children(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        else:         
            res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
            print "### parent ",res_partner.name
            child_list =  []
            
        response['success'] = True
        response['data'] = data
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)


    @http.route('/api/parent/class/<int:class_id>/timetable', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_child_timetable(self,class_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        else:         
            res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
            class_id = request.env['school.class'].browse(class_id)
            if not class_id:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No class found with the given ID'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

            timetable = self._get_timetable_by_class(class_id)
            if not timetable:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No Timetable for this class yet'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
            session_list = []
            for session in timetable.time_table_session_ids:
                json_session = {
                        'class':session.class_id.name,
                        'subject':session.subject_id.name,
                        'teacher':session.teacher_id.name,
                        'classroom':session.classroom_id.name,
                        'start_time':session.start_time,
                        'end_time':session.end_time,
                        'day_of_week':session.day_of_week,
                    }
                session_list.append(json_session)

            response['success'] = True
            response['data'] = session_list
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)



    def _get_timetable_by_class(self, class_id):
        session_list = []
        time_string = datetime.now().time().strftime('%H:%M:%S')
        current_time = datetime.strptime(datetime.now().time().strftime('%H:%M:%S'), '%H:%M:%S') # [time] from string to object
        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        timetables = request.env['time.table'].search(['&',('academic_year','=',current_academic_year.id),('class_id','=',class_id.id)])
        print "########### TIMETABLE",timetables
        current_time_table = None
        for timetable in timetables:
            timing_periode = timetable.timing_periode
            print "########### TIMING PERIOD TIMETABLE",timing_periode
            # reconstruct date with the current year
            start_period = str(datetime.now().year)+'-'+timing_periode.start_date_month
            print "############ START PERIOD TIMETABLE",start_period
            end_period = str(datetime.now().year)+'-'+timing_periode.end_date_month
            print "############ END PERIOD TIMETABLE",end_period
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            print "############ START PERIOD TIMETABLE OBJECT",start_period
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            print "############ END PERIOD TIMETABLE OBJECT",end_period
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                current_time_table = timetable
                break
        return current_time_table
    @http.route('/api/parent/class/<int:class_id>/timetable/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_child_timetable_by_academic_year_period(self,class_id,academic_year_id,timing_system_periode_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        else:         
            res_partner = request.env['res.partner'].search([('id','=',request.env.user.partner_id.id)])
            class_id = request.env['school.class'].browse(class_id)
            if not class_id:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No class found with the given ID'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

            timetable = self._get_timetable_by_class_academic_year_periode(class_id,academic_year_id,timing_system_periode_id)
            if not timetable:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No Timetable for this class yet'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
            session_list = []
            for session in timetable.time_table_session_ids:
                json_session = {
                        'class':session.class_id.name,
                        'subject':session.subject_id.name,
                        'teacher':session.teacher_id.name,
                        'classroom':session.classroom_id.name,
                        'start_time':session.start_time,
                        'end_time':session.end_time,
                        'day_of_week':session.day_of_week,
                    }
                session_list.append(json_session)

            response['success'] = True
            response['data'] = session_list
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)



    def _get_timetable_by_class_academic_year_periode(self, class_id,academic_year_id,timing_system_periode_id):
        session_list = []
        time_string = datetime.now().time().strftime('%H:%M:%S')
        current_time = datetime.strptime(datetime.now().time().strftime('%H:%M:%S'), '%H:%M:%S') # [time] from string to object
        selected_academic_year = request.env['academic.year'].search([('id','=',academic_year_id)])
        timetables = request.env['time.table'].search(['&',('academic_year','=',selected_academic_year.id),('class_id','=',class_id.id),('timing_periode','=',timing_system_periode_id)])
        print "########### TIMETABLE",timetables
        return timetables


    @http.route('/api/parent/<int:class_id>/students', type='http', auth="user", methods=['GET'],  csrf=False)
    def class_student(self,class_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        else:
            class_list = self._get_students_by_class(class_id)
            data = class_list

        response['success'] = True
        response['data'] = data
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)


    def _get_students_by_class(self, class_id):
        student_list  =  []
        students = request.env['school.student'].search([('class_id','=',class_id)])
        for student in students:
            std = {'id':student.id, 'name': student.name,'last_name':student.last_name, 'birthday':student.birth_date, 'photo':student.image, 'class':student.class_id.name}
            student_list.append(std)
        return student_list

    @http.route('/api/student/payments', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_payments_student(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

        student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

        payements_list = []
        payments = request.env['account.payment'].search([('student_id','=',student_id.id)])
        lines = []
        for payment in payments:
            for payement_line in payment.account_payment_line_ids:
                line = {
                    'id':payement_line.id,
                    'product':payement_line.product_id.name,
                    'description':payement_line.description,
                    'payment_date':payment.payment_date,
                    'month':payement_line.month,
                    'price':payement_line.price,
                    'quantity':payement_line.quantity,
                    'discount':payement_line.discount,
                    'total_ttc':payement_line.total_ttc,
                    'class':{
                        'id':payement_line.class_id.id,
                        'name':payement_line.class_id.name,
                        'code':payement_line.class_id.code,
                    }
                }
                lines.append(line)
        student_obj = {
            'id':student_id.id,
            'name':student_id.name,
            'last_name':student_id.last_name,
            'payments':lines
        }
        payements_list.append(student_obj)
        response['data'] = payements_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)  
               
    @http.route('/api/student/timetable', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_child_timetable_(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        else:         
            student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
            if not student_id:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No Student found related to the connected user'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
            class_id = student_id.class_id
            if not class_id:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No class found for the logged user'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

            _logger.warning('###### class_id %s',class_id.name)
            timetable = self._get_timetable_by_class(class_id)
            if not timetable:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No Timetable for this class yet'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
            session_list = []
            for session in timetable.time_table_session_ids:
                json_session = {
                        'class':session.class_id.name,
                        'subject':session.subject_id.name,
                        'teacher':session.teacher_id.name,
                        'classroom':session.classroom_id.name,
                        'start_time':session.start_time,
                        'end_time':session.end_time,
                        'day_of_week':session.day_of_week,
                    }
                session_list.append(json_session)

            response['success'] = True
            response['data'] = session_list
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
    
    def _get_timetable_by_class(self, class_id):
        session_list = []
        time_string = datetime.now().time().strftime('%H:%M:%S')
        current_time = datetime.strptime(datetime.now().time().strftime('%H:%M:%S'), '%H:%M:%S') # [time] from string to object
        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        timetables = request.env['time.table'].search(['&',('academic_year','=',current_academic_year.id),('class_id','=',class_id.id)])
        print "########### TIMETABLE",timetables
        current_time_table = None
        for timetable in timetables:
            timing_periode = timetable.timing_periode
            print "########### TIMING PERIOD TIMETABLE",timing_periode
            # reconstruct date with the current year
            start_period = str(datetime.now().year)+'-'+timing_periode.start_date_month
            print "############ START PERIOD TIMETABLE",start_period
            end_period = str(datetime.now().year)+'-'+timing_periode.end_date_month
            print "############ END PERIOD TIMETABLE",end_period
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            print "############ START PERIOD TIMETABLE OBJECT",start_period
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            print "############ END PERIOD TIMETABLE OBJECT",end_period
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                current_time_table = timetable
                break
        return current_time_table

    @http.route('/api/student/payments/academic_year/<int:academic_year_id>/period/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def parent_payments_student_academic_year_period(self,academic_year_id,timing_system_periode_id, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        academic_years=request.env['academic.year'].search([('id','=',academic_year_id)])
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

        payements_list = []
        payments = request.env['account.payment'].search([('student_id','=',student_id.id)])
        lines = []
        for payment in payments:
            if academic_years.start_date <= payment.payment_date <= academic_years.end_date:
                    if timing_periode.start_date <= payment.payment_date <= timing_periode.end_date:
                        for payement_line in payment.account_payment_line_ids:
                            line = {
                                'id':payement_line.id,
                                'product':payement_line.product_id.name,
                                'description':payement_line.description,
                                'payment_date':payment.payment_date,
                                'month':payement_line.month,
                                'price':payement_line.price,
                                'quantity':payement_line.quantity,
                                'discount':payement_line.discount,
                                'total_ttc':payement_line.total_ttc,
                                'class':{
                                    'id':payement_line.class_id.id,
                                    'name':payement_line.class_id.name,
                                    'code':payement_line.class_id.code,
                                }
                            }
                            lines.append(line)
                            student_obj = {
                                'id':student_id.id,
                                'name':student_id.name,
                                'last_name':student_id.last_name,
                                'payments':lines
                            }
                            payements_list.append(student_obj)
        response['data'] = payements_list
        response['success'] = True
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER) 

    @http.route('/api/student/timetable/academic_year/<int:academic_year_id>/period/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_child_timetable_academic_year_period(self,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        data = []
        if not request.env.user.has_group('genext_school.group_school_student'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
        else:         
            student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
            if not student_id:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No Student found related to the connected user'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
            class_id = student_id.class_id
            if not class_id:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No class found for the logged user'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)

            _logger.warning('###### class_id %s',class_id.name)
            timetable = self._get_timetable_by_class_academic_year_periode(class_id,academic_year_id,timing_system_periode_id)
            if not timetable:
                response['success'] = False
                response['error'] = {'code':404, 'message':'No Timetable for this class yet'} 
                return http.request.make_response(json.dumps(response),SchoolParent.HEADER)
            session_list = []
            for session in timetable.time_table_session_ids:
                json_session = {
                        'class':session.class_id.name,
                        'subject':session.subject_id.name,
                        'teacher':session.teacher_id.name,
                        'classroom':session.classroom_id.name,
                        'start_time':session.start_time,
                        'end_time':session.end_time,
                        'day_of_week':session.day_of_week,
                    }
                session_list.append(json_session)

            response['success'] = True
            response['data'] = session_list
        return http.request.make_response(json.dumps(response),SchoolParent.HEADER)



    def _get_timetable_by_class_academic_year_period(self, class_id,academic_year_id,timing_system_periode_id):
        session_list = []
        time_string = datetime.now().time().strftime('%H:%M:%S')
        current_time = datetime.strptime(datetime.now().time().strftime('%H:%M:%S'), '%H:%M:%S') # [time] from string to object
        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        timetables = request.env['time.table'].search(['&',('academic_year','=',academic_year_id),('class_id','=',class_id.id),('timing_periode','=',timing_system_periode_id)])
        current_time_table = None
        for timetable in timetables:
            timing_periode = timetable.timing_periode
            # reconstruct date with the current year
            #start_period = str(datetime.now().year)+'-'+timing_periode.start_date_month
            start_period = timing_periode.start_date
            _logger.warning('#### SESSION START PERIOD %s',timing_periode.start_date)
            #end_period = str(datetime.now().year)+'-'+timing_periode.end_date_month
            end_period = timing_periode.end_date
            _logger.warning('#### SESSION END PERIOD %s',timing_periode.end_date)
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                current_time_table = timetable
                break
        return current_time_table

