# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
from odoo import exceptions
from odoo.exceptions import UserError, ValidationError, Warning

import cookielib
import urllib2
import json
import base64
import smtplib
import string
import re
import logging
_logger = logging.getLogger(__name__)

from sms_client import Client

class ParentRelation(models.Model):
    _name   = 'school.parent.relation'


    name                    = fields.Char(string='Titre')
    description             = fields.Text(string='Description')
    datetime                = fields.Datetime(string="Date", default=fields.Datetime.now())
    class_id                = fields.Many2one('school.class', string="Classe")
    file_ids                = fields.One2many('school.file','parent_relation_id', string="Fichiers")
    state                   = fields.Selection([('draft','Brouillon'),('published','Publié')], default='draft')


    @api.multi
    def set_draft(self):
        self.write({'state':'draft'})
        return True

    @api.multi
    def set_published(self):
        self.write({'state':'published'})
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            ''' foreach sanction in [self --> recordset of sanction, can contain one record or a set of records]'''
            for rec in self:
                for student in rec.class_id.student_ids:
                    tokens = []
                    for parent in student.parent_ids:
                        for token in parent.token_ids:
                            tokens.append(token.token)
                            messageEN = 'École des parents'
                            messageFR = 'École des parents'
                            data = {'module':'ParentSchoolPage','data':None}
                            content = {'en':messageEN, 'fr':messageEN}
                            ''' sending push notification for each sanction'''
                            _logger.warning('##### Sending push notification %s',content)
                            notif.sendPushNotification(data=data ,content=content, tokens=[token.token])
        return True




class File(models.Model):
    _name    = 'school.file'


    name                                    = fields.Char(string="Nom")
    description                             = fields.Char(string="Description")
    file                                    = fields.Binary(string='Pièce joint', attachment=True)
    file_name                               = fields.Char(string="Nom du fichier")
    parent_relation_id                      = fields.Many2one('school.parent.relation', string="Devoir", ondelete='cascade')
    message_sent_id                         = fields.Many2one('message.sent',string="Message", ondelete='cascade')

    @api.model
    def create(self,vals):
        #_logger.warning('##### CONTENT OF VALS %s',vals)
        # vals['file'] = '/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCACAAIADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD1XVPFD2mtSafFGh8tAxZvU0+18UZYLcxAA/xJ2/CuY1wgeNbzPUov8qx78Xyv51sxZR1XNePUxFSNVpM2UU0ewQXEVzEJInDKehFS15j4c8TSQShX4J+/H616TbzpcwpLGcqwzXfQrqqvMzlGxNRRRXSSFFFFABRRRQAUUUUAFFFFABRRRQB5r4wRrbxfbzY+WaED8jVaPhmWun8dab9q0lb2MfvrRt/Hde4rk4JRKsUo6MteJjYcs79zem7opapZbXW8gX94v3gO9dN4U1/hI5G/dOcEf3TVAgMMHkVhSCTTdXQx/wCqlPI7VjSqOLuhtHtQOaKzNDvRe6bG27Lr8rVp171OSnFMwasLRRRViCiiigAooooASio3nijGXdV+prPn16whOPN3n/YGazlUhHdjszVornW8VW4biCQj1zT4/FNozYaORR69az+s0u4+VmzcQpcW8kLgFHUqQa8nME2lajNpVyOUOY3/ALy9jXrtc74p8OrrNsJocLewjMTf3v8AZNTiqPtI6bhCVmcjG25eeoqhrkBlsS6/ejO4GktbiYStDcoY7iM4ZD1q5PtltZB6qa8SzjKzOjc1fBWoFWiR2+WVdp+vau+yK8d0PUotPtVuJpABE3T1x2FaGq+Of7TntZrGF7eS3cskjNknIwQV6Yr0sPXUINMylG7PUGdVBLMAB1yaox63psryKl9ASn3vn6V5Wbq7vWYz3UsnmNubLdTVlbXZGNo/CiePs9ECpnqMWp2U8vlxXULv/dDjNW68iiXZJumjbYOytg11mmeIobaxMYtZlwPlZ5N2T+PSrpY1S+LQTp9jqbq8hs4jJM4Ve3qa5bUvFMjKVhxCn949aw9T1vzpd9zLlj91B2rDaK51CYvI/lQ9getc9bFynpHRFRgkaN5rSKQzyGRj6nNUf7QvbggwQNj1IxVqGytogNkW8j+Jqkkmii/10qqPQVx3uaFNU1GYndKFB7Cpo2mg2WokM9xIdqL3zUtu17qTeXpdm8v+2RhfzrsPDXhRdLc3t64nvm79o/YV0UcPKo/IiUkjqqKKK9wwOV8U+Fv7XRbuzIjv4vunoHHoa4KS/NrO9tfwvDMnDqa9mrzT4sJbx2VnIiILySTbvHXbiuHE4eMlzFwlbQ4G7ukLPbWzsyFt2Ogqa0g2KB3qlYwCNGmmIUdSzdBWzbyWjhds8Zz05rgastDUt26uGHIrbhJMY3DFZcZjhZGkljVWYKuW6mna9aand6YItKv1sZ9wLSlN3y+grDlvKzGma2BUNxCZYyEYq/Y159/xcLR90q3Nrq8a9Y8fOR7dK6bwr4jm16CdbuwnsruBsSRyRsB7YJrSVCUVzJpoFK5HsSK8c3r7mXoq81rWz3l4Atlpk8vvswPzNdh4d+yvI0UkEZkPzKSgNdUqKn3VAHtXVRwsakVJszlO2h55aeEdavmH22ZLOH+6h3Ma6Gx8EaRaOJJI2uZP70xz+ldJiiu6GGpx2RDk2RxQRwIEiRUUdAoxUtFFbpJEhRRRTASvI/iNci88URWecrbxA49zXrhrxXxIN/jnUmc8h1H4bRXLinaBcNzL/wCEck8SXMWgp8iXI3Syf880Ugk/Xpiu2u/gt4YntYo7aS+spIwB5kM5+Y+pDZFYfh/V7PRvFkN7qFwILRoHh8xvuqxKkZ9OlZPxp+I+oWN3ZaZ4c1IJE8XnTXFq4Yk5wFyOnr+NThlF01cJbld/B0ug+K7mxu9RnvYbYRzW+8AZ3Z5PuNprrYYxfahplnKT5NxcbZcNtO0KzYz9QK4DwLPrF9Zz6nrV3PcSzlUiadstsXP6ZY0vxKvLlPDtrJYzSRyQ3AZnicqVG0jtXLKKliFHoWvhPWtU8BiK1km0W5lS6VcpFO5dGPpk8iub0y8e4h2zRtFMpKSRt1RhwRWb8D/H2r67NeaRrV2LhLeJXhml4frggnvW5evC3ivVDCQUM3JHdti5rTGUoxheK1FTbubGiSbNTgx3bFd2K4HRz/xM4P8AfFd9V4B+4xVNxaKKK7zMKKKKACiiigANeE+NJzD421EdCWX/ANBFe614n8UtPa08UJeY/d3UQOf9ocH+lc2JjzQKhuc82ooyGORFdT1BpluLCZwhtIyvoRxWQ5xknoOal066jaZXVgVzjNcPK0tDS5Brvj+LS717G204SGL5SWfaAfYCtDw54lh1y18zU9NtUtmnW3UmfLOzdlQ8n3rC8VeEbrVb0X2nKrsy/OmcEn2rY8BeAJdPu49V1hVWaM5ht/vbT/eatn7FU79Re9c9Eh8NaLHatDHp1vGpHVFwfzqeK0htsCMdOOanEvHqaTOea86cm1qaI1NBiMmqQ/7J3V3Irk/C0RNzJLjhVx+ddYK9XBRtTMZ7i0UUV2kBRRRQAUUUUAJXnnxZ09p9CtrxFJNvLhiOysP8cV6JVLVLFNT0u5spMbZo2Tnt6GonG8Who+ZmIIORnI5qssPlSl4RjP3l7GtC+tJbG+ms51xJC5Rh9KgAANec9NDUuWmrTQkK9vJt/wBnmuisdZLKD5TfRuK5WKJpZAq8e9b1hYbCN7Mx+tc9RRGjpLe9muG2rGoHr6VoopCgHrVeziEcfAxWpp1m97eJEv1Y+grBXnJRRWx1Xh23MWmhz1kbP4Vs96jjjWKJUUYVRgU+vfpx5YpHO3di0UUVoIKKKKACiiigAooooA8W+Kujiz12LUY1xHdr83++Ov6YrggNzDFfSOuaDY+IbEWt9GWRW3KQcFTXHj4TacszOl9MEzlUKA4/GuOrRk5XRakec6bZshDMtdHaw7nU44rqf+FdGLAivlI/2kxV228EtDgtdqT7JXDPDVX0NFOJhKu1QBXYeGrIQWRuGHzy9D/s02HwrbJgyzPJ7Dit2ONYowiDCqMAV0YXCyhLmkROd9iSiiivSMwooooAKKKKAP/Z'
        res = super(File, self).create(vals)
        if vals.get('file',False):
            domain = [
                ('res_model', '=', res._name),
                ('res_field', '=', 'file'),
                ('res_id', '=', res.id),
            ]
            attachment = self.env['ir.attachment'].search(domain)
            _logger.warning('###### FILE NAME %s',attachment['name'])
            _logger.warning('###### File Name %s',attachment['datas_fname'])
            _logger.warning('###### res_name %s',attachment['res_name'])
            _logger.warning('###### res_model %s',attachment['res_model'])
            _logger.warning('###### typeE %s',attachment['type'])
            _logger.warning('###### url %s',attachment['url'])
            _logger.warning('###### public %s',attachment['public'])
            _logger.warning('###### store_fname %s',attachment['store_fname'])
            _logger.warning('###### file_size %s',attachment['file_size'])
            _logger.warning('###### mimetype %s',attachment['mimetype'])
            _logger.warning('###### mimetype %s',attachment['id'])

        return res







class Appointment(models.Model):
    _name       = 'school.appointment'


    name                    = fields.Char(string="Nom")
    teacher_ids             = fields.Many2many('hr.employee', string="Enseignant")
    parent_id               = fields.Many2one('res.partner', domain="[('is_school_parent','=',True)]", string="Parent" )
    student_id              = fields.Many2one('school.student', string="élève")
    user_id                 = fields.Many2one('res.users', default= lambda self:self.env.user.id)
    start_date              = fields.Datetime(string="Heure de début", 
                                        states={'accepted': [('readonly', True)], 'rejected': [('readonly', True)],'canceled': [('readonly', True)] })
    end_date                = fields.Datetime(string="Heure de fin", 
                                        states={'accepted': [('readonly', True)], 'rejected': [('readonly', True)],'canceled': [('readonly', True)] })
    create_date             = fields.Datetime(string="Date création", default=fields.Datetime.now())

    type                    = fields.Selection([('administrative','Administratif'), ('pedagogic','Pédagogique')], default='administrative', string="Type")
    state                   = fields.Selection([('waiting','En attente'),('accepted','Accepté'),('rejected','Rejeté'),('canceled','Annulé'),('archived','Archivé')], default='waiting', string="état")
    subject                 = fields.Text(string="Sujet")
    note                    = fields.Char(string="Note")

    state_changed           = fields.Boolean(string="State changed", default=False)
    date_changed            = fields.Boolean(string="State changed", default=False)
    to_notify_admin         = fields.Boolean(string="To notify admin", default=False)
    to_notify_parent        = fields.Boolean(string="To notify parent", default=False)
    newly_created           = fields.Boolean(string="Just created", default=False)
    color                   = fields.Integer(string='calendar appointment color', compute='_compute_color')

    @api.onchange('parent_id')
    def _onchange_parent_id(self):
        if self.parent_id:
            student_ids = self.parent_id.student_ids
            return {'domain':{'student_id':[('id','in',student_ids.ids)]}}
        else:
            return {'domain':{'student_id':[('id','in',[])]}}



    @api.multi
    def update_appointment_state(self):
        apps = self.env['school.appointment'].search([('state','=','accepted')])
        for el in apps:
            _logger.warning("#### name of appointment %s",el.name)
            if datetime.strptime(el.start_date, DEFAULT_SERVER_DATETIME_FORMAT) <= datetime.strptime(fields.Datetime.now(),  DEFAULT_SERVER_DATETIME_FORMAT) and el.state == 'accepted':
                el.write({'state':'archived'})
                _logger.warning("#### updated appointment %s",el.name)
                _logger.warning("#### updated appointment new state %s",el.state)


    @api.depends('type')
    @api.multi
    def _compute_color(self):
        for rec in self:
            if rec.type == 'administrative':
                rec.color = 0
            else:
                rec.color = 1


    @api.multi
    def read(self, fields=None, load='_classic_read'):
        res = super(Appointment, self).read(fields,load)
        if self.env.user.has_group('genext_school.group_school_administration'):
            for rec in self:
                if rec.to_notify_admin == True:
                    rec.write({'to_notify_admin':False})
        elif self.env.user.has_group('genext_school.group_school_parent'):
            for rec in self:
                if rec.to_notify_parent == True:
                    rec.write({'to_notify_parent':False})
        return res


    @api.model
    def create(self, vals):
        _logger.warning("### CREATING AN APPOINTMENT")
        if self.env.user.has_group('genext_school.group_school_administration'):
            vals['to_notify_parent'] = True
        elif self.env.user.has_group('genext_school.group_school_parent'):
            vals['to_notify_admin'] = True
            vals['parent_id'] = self.env.user.partner_id.id
        if not vals.get('name',False):
            vals['name'] = 'Rendez-vous '+ ('Administratif' if vals.get('type') == 'administrative' else 'Pédagogique')

        return super(Appointment, self).create(vals)

    @api.multi
    def write(self, vals):
        _logger.warning("### UPDATING AN APPOINTMENT")
        if vals.get('start_date',False):
            vals['date_changed'] = True
        return super(Appointment, self).write(vals)

    @api.multi
    def unlink(self):
        _logger.warning("### DELETING AN APPOINTMENT")
        for rec in self:
            _logger.warning("##### SUBJECT %s",rec.subject)
        return super(Appointment, self).unlink()


    @api.multi
    def set_waiting(self):
        self.write({'state':'waiting','to_notify_parent':True})
        return True

    @api.multi
    def set_accepted(self):
        self.write({'state':'accepted','to_notify_parent':True})
        return True


    @api.multi
    def set_rejected(self):
        self.write({'state':'rejected','to_notify_parent':True})
        return True

    @api.multi
    def set_canceled(self):
        self.write({'state':'canceled'})
        return True

    @api.multi
    def set_archived(self):
        self.write({'state':'archived'})
        return True


class EmailConfig(models.Model):
    _name   = 'email.config'

    name                = fields.Char(string="Nom du configuration")

    sender_email        = fields.Char(string="Email", required=True)
    sender_password     = fields.Char(string="Mot de passe", required=True)
    subject             = fields.Char(string="Objet", default="Message des parents")
    email_ids           = fields.One2many('email.email', 'email_config_id', string="Emails", required=True)
    
    @api.multi
    @api.onchange('sender_email')
    def  validateEmail(self):
        for email in self:
            if email.sender_email:
                _logger.warning("### email %s",email.sender_email)
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email.sender_email) != None:
                    pass
                else:
                    raise ValidationError("Veuillez entrer une adresse e-mail d'expédition valide")

    @api.one
    @api.constrains('sender_email')
    def email_validation(self):
        for email in self:
            if email.sender_email:
                _logger.warning("### email %s",email.sender_email)
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email.sender_email) != None:
                    pass
                else:
                    raise ValidationError("Veuillez entrer une adresse e-mail d'expédition valide")


class Email(models.Model):
    _name   = 'email.email'

    name = fields.Char(string='Email', required=True)
    email_config_id = fields.Many2one('email.config')

    @api.multi
    @api.onchange('name')
    def  validateEmail(self):
        _logger.warning("#### calling validation")
        for email in self:
            if email.name:
                _logger.warning("### email %s",email.name)
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email.name) != None:
                    pass
                else:
                    raise ValidationError("Veuillez entrer une adresse e-mail valide")

    @api.one
    @api.constrains('name')
    def email_validation(self):
        for email in self:
            if email.name:
                _logger.warning("### email %s",email.name)
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email.name) != None:
                    pass
                else:
                    raise ValidationError("Veuillez entrer une adresse e-mail valide")


class ContactAs(models.Model):
    _name    ='contact.as'


    @api.depends('user_id')
    def _compute_default_partner(self):
        if self.env.user.parent_id:
            partner_id = self.env.user.partner_id
            return partner_id
        else:
            return None



    name                    = fields.Char(string="Titre")
    content                 = fields.Char(string="Contenu de message")
    user_id                 = fields.Many2one('res.users', default= lambda self:self.env.user.id, )
    partner_id              = fields.Many2one('res.partner', string="Parent")
    datetime                = fields.Datetime(string="Date de création", default=fields.Datetime.now())
    student_ids             = fields.Many2many(related='partner_id.student_ids', string="élèves")

    @api.model
    def create(self, vals):
        res = super(ContactAs,self).create(vals)
        _logger.warning("#### sending email")

        email_config  = self.env['email.config'].search([],limit=1)

        sender = email_config.sender_email
        receivers = [email.name for email in email_config.email_ids]
        
        
        From = email_config.sender_email
        Subj = res.name if res.name else email_config.subject

        childrens = ["\t"+child.name+" "+child.last_name+": "+child.class_id.name if child.class_id.name else "Non défini" for child in res.partner_id.student_ids]
        childrens = ', '.join(set(childrens))
        Text = "%s \n\n\n\n Parent : %s %s \n Enfant(s): \n %s"%(res.content,res.partner_id.name,res.partner_id.last_name, childrens)

        Body = string.join((
            "From: %s" % From,
            "Subject: %s" % Subj,
            "",
            Text,
            ), "\r\n")
        
        try:
            smtpObj = smtplib.SMTP(host='smtp.gmail.com', port=587)
            smtpObj.ehlo()
            smtpObj.starttls()
            smtpObj.ehlo()
            smtpObj.login(user=email_config.sender_email, password=email_config.sender_password)
            smtpObj.sendmail(sender, receivers, Body) 
            smtpObj.close()        
            _logger.warning("Successfully sent email")
        except:
            _logger.warning("#### ERRROR while sending email")
        return res


class MessageSend(models.Model):
    _name   = 'message.sent'

    name                    = fields.Char(string="Titre")
    content                 = fields.Char(string="Contenu de message")
    partner_id              = fields.Many2one('res.partner', string="Parent", domain="[('is_school_parent','=',True)]")
    datetime                = fields.Datetime(string="Date de création", default=fields.Datetime.now())
    seen                    = fields.Boolean(string="Vue", default=False)
    student_ids             = fields.Many2many(related='partner_id.student_ids', string="élèves")
    file_ids                = fields.One2many('school.file','message_sent_id', string="Fichiers")
    state                   = fields.Selection([('draft','Brouillon'),('confirmed','Confirmé')],default='draft')


    @api.multi
    def set_to_confirmed(self):
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            ''' foreach sanction in [self --> recordset of sanction, can contain one record or a set of records]'''
            for rec in self:
                _logger.warning('##### FOR REC IN SELF')
                tokens = []
                for token in rec.partner_id.token_ids:
                    _logger.warning('##### FOR TOKEN %s',token.token)
                    # tokens.append(token.token)
                    data = {'module':'AlertPage'}
                    content = {'en':"You've got a message from Odesco ", 'fr':'Vous avez un message de Odesco'}
                    ''' sending push notification for each sanction'''
                    _logger.warning('##### Sending push notification %s',content)
                    notif.sendPushNotification(data=data ,content=content, tokens=[token.token])
        _logger.warning('##### Sending push notification OUT of IF')
        return self.write({'state':'confirmed'})
        

    @api.multi
    def set_to_draft(self):
        return self.write({'state':'draft'})




class Contact(models.Model):
    _name  = 'sms.contact'


    name                = fields.Char(string="Nom", required=True)
    phone               = fields.Char(string="Numéro mobile", required=True)
    sms_sms_id          = fields.Many2one('sms.sms')


class SMSSender(models.Model):
    _name = 'sms.sms'



    name                = fields.Char(string='SMS',default='SMS')
    solde               = fields.Text(string='Solde SMS',compute='get_solde')
    message             = fields.Text(string="texte à envoyer", required=True)
    nb_sms              = fields.Integer(compute='default_comment')
    nb_des              = fields.Integer(compute='_compute_destinataire')
    @api.onchange('message')
    def default_comment(self):
        note_c = ' caractères'
        note_s = ' unité de SMS'
        nb_c = 0
        nb_s = 1
        m = ''
        if self.message:
            m = self.message
            m = str(m)
            nb_c = len(m)
            nb_s = (nb_c/160) + 1
            self.nb_sms = nb_s

    counte_ids          = fields.Many2many('school.sms.setup',string='choisir votre compte')
    nb_destinataires    = fields.Text(' ',compute='_compute_destinataire',readonly=True)
    @api.depends('partner_ids')
    def _compute_destinataire(self):
        self.ensure_one()
        nb = 0
        if self.partner_ids:
            for partner in self.partner_ids:
                nb += 1
        self.nb_des = nb
        nb = str(nb)
        nb = nb + ' destinataires'
        self.nb_destinataires = nb




    date                = fields.Date(string="Date", default=fields.Date.today())
    user_type           = fields.Selection([('parent','Parent'),('teacher','Enseignant'), ('student','Élève')], default='parent', string="Destinataire")
    sms_id              = fields.Many2one('school.sms')

    # class_ids           = fields.Many2many('school.class', string="Classes")
    partner_ids         = fields.Many2many('res.partner', string="Envoyé à", domain="[('is_school_parent','=',True)]")

    contact_ids         = fields.One2many('sms.contact', 'sms_sms_id', string="Particulier")

    def generate_info(self):
        message = self.message
        #destinataires = ' " sera envoyé à ' + self.nb_destinataires + '\n'
        nb_sms = self.nb_sms * self.nb_des
        nb_sms = str(nb_sms)
        sms_nombre = ' par '+str(self.nb_sms)+' SMS'
        destinataires = ' " sera envoyé à ' + self.nb_destinataires  + sms_nombre + '\n'
        nb_s = '\n' + 'le nombre total des SMS à envoyer est ' + nb_sms +' SMS'+' \n'
        solde = self.counte_ids.remaining_sms - (self.nb_sms * self.nb_des)
        solde = str(solde)
        solde = 'le solde restant aprés l ''envoi est ' + solde + ' unité de SMS' 
        infos = 'votre message : " ' + message + destinataires + nb_s + solde
        raise exceptions.except_orm(('Vérification'),(infos))

    @api.depends('counte_ids')
    def get_solde(self):
        self.ensure_one()
        message = ''
        if self.counte_ids:
            message += 'votre Solde SMS est '
            rest_sms = self.counte_ids.remaining_sms
            rest_sms = str(rest_sms)
            message = message + rest_sms + ' unité(s)'
        self.solde = message


    @api.onchange('user_type')
    def _onchange_user_type(self):
        self.class_ids = False
        self.partner_ids = False
        if self.user_type == 'parent':
            return {'domain': {'partner_ids': [('is_school_parent', '=', True)]}}
        elif self.user_type == 'teacher':
            return {'domain': {'partner_ids': [('is_school_teacher', '=', True)]}}
        else:
            return {'domain': {'partner_ids': [('is_school_student', '=', True)]}}



    @api.model
    def create(self, vals):
        vals['name'] = self.env['ir.sequence'].next_by_code('sms.sms')
        return super(SMSSender, self).create(vals)



    # @api.onchange('class_ids', 'user_type')
    # @api.multi
    # def _compute_partner_ids(self):
    #     # for rec in self:
    #     if self.user_type == 'parent':
    #         parent_list = []
    #         for classe in self.class_ids:
    #             print "### class name ",classe.name
    #             for student in classe.student_ids:
    #                 print "### student name ",student.name
    #                 for parent in student.parent_ids:
    #                     parent_list.append(parent)

    #         if len(parent_list):

    #             self.write({'partner_ids': [(6,False,[y.id for y in parent_list])]})

    #     if self.user_type == 'teacher':
    #         teacher_list = []
    #         class_list = []
    #         for classe in self.class_ids:
    #             class_list.append(classe.id)


    #         for teacher in self.env['hr.employee'].search([('is_school_teacher','=',True)]):
    #             for class_id in teacher.class_ids:
    #                 if class_id.id in class_list:
    #                     teacher_list.append(teacher.partner_id)
    #                     break

    #         if len(teacher_list):
    #             self.write({'partner_ids':[(6,False,[y.id for y in teacher_list])]})


    @api.multi
    def action_send_sms(self):
        sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
        if not sender:
            raise Warning('Vous devez valider une configuration sms pour envoyer des sms')
        number_list = []
        for partner in self.partner_ids:
            phone = partner.phone if partner.phone else partner.mobile
            if phone:
                number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                number_list.append(number)

        for contact in self.contact_ids:
            number = {'name':'','gender':1,'phone':contact.phone,'prefix':'216','type':'cell'}
            number_list.append(number)
        _logger.warning("NUMBERS %s",number_list)
        response = sender.send_lot_sms(self.message, number_list)
        if response['success'] == True:
            self.env.cr.commit()
            raise Warning('Messages envoyés avec succès')
        else:
            raise Warning('Messages non envoyés')

    @api.multi
    def action_send_login_password_sms(self):
        sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
        if not sender:
            raise Warning('Vous devez valider une configuration sms pour envoyer des sms')
        number_list = []
        for partner in self.partner_ids:
            if partner.is_school_teacher:
                teacher_id = self.env['hr.employee'].search([('partner_id','=',partner.id)])
                if teacher_id:
                    _logger.warning("#### TEACHER LOGIN PASSWORD %s",teacher_id.user_id.login)
                    phone = partner.phone if partner.phone else partner.mobile
                    if phone:
                        number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                        message = "%s Votre login : %s \n Votre mot de passe : %s"%(self.message, teacher_id.user_id.login,teacher_id.user_id.login)
                        response = sender.send_lot_sms(message, [number])
                        #response = sender.send_lot_sms(self.message, number_list)
            elif partner.is_school_parent:
                user_id = self.env['res.users'].search([('partner_id','=',partner.id)])
                if user_id:
                    _logger.warning("#### PARENT LOGIN PASSWORD %s",user_id.login)
                    phone = partner.phone if partner.phone else partner.mobile
                    if phone:
                        number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                        message = "%s Votre login : %s \n Votre mot de passe : %s"%(self.message, user_id.login,user_id.login)
                        response = sender.send_lot_sms(message, [number])
            elif partner.is_school_student:
                student_id = self.env['school.student'].search([('partner_id','=',partner.id)])
                if student_id:
                    _logger.warning("#### STUDENT LOGIN PASSWORD %s",student_id.user_id.login)
                    phone = partner.phone if partner.phone else partner.mobile
                    if phone:
                        number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                        message = "%s Votre login : %sVotre mot de passe : %s"%(self.message, student_id.user_id.login,student_id.user_id.login)
                        response = sender.send_lot_sms(message, [number])



        # for contact in self.contact_ids:
        #     number = {'name':'','gender':1,'phone':contact.phone,'prefix':'216','type':'cell'}
        #     number_list.append(number)
        # print "NUMBERS ",number_list

        # response = sender.send_lot_sms(self.message, number_list)
        # if response['success'] == True:
        #     self.env.cr.commit()
        #     raise Warning('Messages envoyés avec succès')
        # else:
        #     raise Warning('Messages non envoyés')
    @api.multi
    def action_send_randomkey_sms(self):
        sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
        if not sender:
            raise Warning('Vous devez valider une configuration sms pour envoyer des sms')
        number_list = []
        for partner in self.partner_ids:
            if partner.is_school_teacher:
                teacher_id = self.env['hr.employee'].search([('partner_id','=',partner.id)])
                if teacher_id:
                    _logger.warning("#### TEACHER RANDOM KEY %s",teacher_id.user_id.random_key)
                    phone = partner.phone if partner.phone else partner.mobile
                    if phone:
                        number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                        message = "%s Votre clé de parrainage est : %s \n "%(self.message, teacher_id.user_id.random_key)
                        response = sender.send_lot_sms(message, [number])
            elif partner.is_school_parent:
                user_id = self.env['res.users'].search([('partner_id','=',partner.id)])
                if user_id:
                    _logger.warning("#### PARENT RANDOM KEY %s",user_id.random_key)
                    phone = partner.phone if partner.phone else partner.mobile
                    if phone:
                        number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                        message = "%s Votre clé de parrainage est  : %s"%(self.message, user_id.random_key)
                        response = sender.send_lot_sms(message, [number])
            elif partner.is_school_student:
                student_id = self.env['school.student'].search([('partner_id','=',partner.id)])
                if student_id:
                    _logger.warning("#### STUDENT RANDOM KEY %s",student_id.user_id.random_key)
                    phone = partner.phone if partner.phone else partner.mobile
                    if phone:
                        number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                        message = "%sVotre clé de parrainage est: %s"%(self.message, student_id.user_id.random_key)
                        response = sender.send_lot_sms(message, [number])
class Sms(models.Model):
    _name = 'school.sms'
    _order = "send_date desc"

    name                    = fields.Char(string="Nom")
    sms_id                  = fields.Char(string="SMS ID")
    sms_header              = fields.Char(string="Entête de message")
    sent_count              = fields.Integer(string="Nombre d'envoi")
    sms_text                = fields.Char(string="Contenu de message")
    phone                   = fields.Char(string="Mobile")
    send_date               = fields.Datetime(string="Date d'envoi")
    state                   = fields.Selection([('sent','Envoyé'),('not-sent','Non envoyé')], default='not-sent')
    school_sms_setup_id     = fields.Many2one('school.sms.setup')
    sent_from_odoo          = fields.Boolean(default=False)
    partner_id              = fields.Many2one('res.partner')
    




class SmsSetup(models.Model):
    _name = 'school.sms.setup'

    @api.multi
    @api.depends('payed_sms','sent_sms')
    def _compute_sms(self):
        for rec in self:
            if rec.payed_sms and rec.sent_sms:
                rec.remaining_sms = rec.payed_sms - rec.sent_sms

    @api.multi
    def _check_for_update(self):
        client = Client(url=self.api_path, debug=1)
        response = client.get_sms_count(self.api_id) 
        if 'data' in response:
            data = response['data']
            if data['status'] == True:
                _logger.warning("## count_payed_sms %s", data['count_payed_sms'])
                _logger.warning("## count_sent %s", data['count_sent'])
                if self.payed_sms != data['count_payed_sms'] or self.sent_sms != data['count_sent']:
                    _logger.warning("## you need to call synchronize")
                    self.need_to_update = True
                else:
                    _logger.warning("## no need to call synchronize")
                    self.need_to_update = False
            response = client.get_header(self.api_id)
            if response['success'] == True and response['data']['status'] == True:
                data = response['data']
                header = data['headers'][0] if len(data['headers']) else None
                if header != None and header['sms_header'] != self.sms_header:
                     self.need_to_update = True

    @api.multi
    def _load_sms_history(self):
        for rec in self.env['school.sms'].search([]):
            _logger.warning("### DELETING ")
            _logger.warning("DELETE RESULT %s",rec.unlink())
        self.sms_ids = False
        self.env.cr.commit()
        if self.api_id and self.api_path:
            client = Client(url=self.api_path, debug=1)
            response = client.get_sent_sms(self.api_id)
            if 'data' in response: 
                data = response['data']
                if data['status'] == True:
                    sms_list = []
                    for sms in data['list_sms']:
                        sms_obj = {
                            'name':'to fix',
                            'sms_id':sms['id'],
                            'sms_header':sms['sms_header'],
                            'sent_count':sms['count_msg'],
                            'sms_text':sms['sms'],
                            'phone':sms['phone'],
                            'send_date':sms['sent_dt'],
                            'state':sms['status'] if sms['status'] == 'sent' else 'not-sent',
                            #'school_sms_setup_id':3,
                        } 
                        # sms_list.append((0,0,sms_obj))
                        #self.env['school.sms'].create(sms_obj)
                        self.sms_ids  |=  self.env['school.sms'].create(sms_obj)

            response = client.get_not_sent_sms(self.api_id) 
            if 'data' in response:
                data = response['data']
                if data['status'] == True:
                    sms_list = []
                    for sms in data['drafts']:
                        sms_obj = {
                            'name':'to fix',
                            'sms_id':sms['id'],
                            'sms_header':sms['sms_header'],
                            'sent_count':sms['count_msg'],
                            'sms_text':sms['sms'],
                            'phone':sms['phone'],
                            'send_date':sms['sent_dt'],
                            'state':sms['status'] if sms['status'] == 'sent' else 'not-sent',
                            #'school_sms_setup_id':3,
                        } 
                        # sms_list.append((0,0,sms_obj))
                        #self.env['school.sms'].create(sms_obj)
                        self.sms_ids  |=  self.env['school.sms'].create(sms_obj)
        

#    {    "id":"067b80a5-9d4d-4044-9e39-4103c03986a2",
#         "user_id":"612b8b30-1a80-4ece-8edb-6664f2456193",
#         "sms_header":"SMS HEADER",
#         "count_msg":1,
#         "sms":"A message sent from Odesco school management system solution",
#         "phone":"21621195064",
#         "gender":1,
#         "city":null,
#         "is_arabic":0,
#         "status":"sent",
#         "sent_dt":"2017-09-22T23:39:00.000Z"
#     }
            
    login           = fields.Char(string='Login', required=True)
    password        = fields.Char(string='Mot de passe', required=True)
    api_path        = fields.Char(string='URL de base', required=True, default="http://92.222.88.236:3000")
    test_phone      = fields.Char(string='Numéro de test')
    name            = fields.Char(string="Prénom")
    last_name       = fields.Char(string="Nom")
    sms_header      = fields.Char(string="Entête de message", size=11)
    sms_header_id   = fields.Char(string="Header id")
    active          = fields.Boolean(string="Actif", default=False)
    api_id          = fields.Char(string="API ID")
    test_sms        = fields.Char(string="Message de test", default="A message sent from Odesco school management system solution")

    payed_sms       = fields.Integer(string='Total des SMS')
    sent_sms        = fields.Integer(string='SMS envoyés')
    remaining_sms   = fields.Integer(string="SMS restants", compute=_compute_sms)


    state           = fields.Selection([('draft','Brouillon'),('verified','Vérifié')], default='draft', string="État")

    need_to_update  = fields.Boolean(default=True, compute=_check_for_update)

    sms_ids         = fields.One2many('school.sms', 'school_sms_setup_id', compute=_load_sms_history)



    @api.multi
    def write(self, vals):
        if vals.get('name',False) or vals.get('last_name',False):
            user = {
                'user_id':self.api_id,
                'user':{
                    'email':self.login,
                    'first_name': vals.get('name') if vals.get('name',False) else self.name,
                    'last_name': vals.get('last_name') if vals.get('last_name',False) else self.last_name,
                    'sector':'',
                    'phone':'',
                    'address':'',
                    'description':'',
                    }
                }
            client = Client(url=self.api_path, debug=1)
            response = client.updateDetails(user)

        if vals.get('sms_header',False):
            _logger.warning("CALL TO UPDATE HEAEDR")
            header = {
                'header_id': self.sms_header_id,
                'sms_header': vals.get('sms_header'),
                'user_id':self.api_id,
            }
            client = Client(url=self.api_path, debug=1)
            response = client.updateHeader(header)

        return super(SmsSetup, self).write(vals)


    @api.multi
    def set_draft(self):
        self.write({'state': 'draft'})

    

    @api.multi
    def action_synchronize(self):
        client = Client(url=self.api_path, debug=1)
        response = client.get_sms_count(self.api_id)
        if response['success'] == True:
            data = response['data']
            if data['status'] == True:
                self.payed_sms = data['count_payed_sms'] 
                self.sent_sms = data['count_sent']
            # raise exception if response status if false
        else:
            raise Warning(str(response['error']))
        response = client.get_header(self.api_id)
        if response['success'] == True and response['data']['status'] == True:
            data = response['data']
            header = data['headers'][0] if len(data['headers']) else None
            if header != None:
                 self.write({'sms_header_id': header['id'], 'sms_header': header['sms_header']})
            
    @api.multi
    def action_send_sms(self):
        if not self.test_phone:
            raise Warning('Veuillez saisir un numéro de pour tester')
        else:
            client = Client(url=self.api_path,  debug=1)
            number = {'name':'','gender':1,'phone':self.test_phone,'prefix':'216','type':'cell'}
            client.sendSms(self.api_id, self.sms_header, self.test_sms, [number])   

    @api.multi
    def send_sms(self,sms):
        client = Client(url=self.api_path,  debug=1)
        #number = {'name':'','gender':1,'phone':self.test_phone,'prefix':'216','type':'cell'}
        client.sendSms(self.api_id, self.sms_header, self.test_sms, [sms])

    @api.multi
    def send_from_wizard_sms(self,sms, contacts):
        client = Client(url=self.api_path,  debug=1)
        #number = {'name':'','gender':1,'phone':self.test_phone,'prefix':'216','type':'cell'}
        return client.sendSms(self.api_id, self.sms_header, sms, contacts)    

    @api.multi
    def send_lot_sms(self,sms, numbers):
        client = Client(url=self.api_path,  debug=1)
        _logger.warning("MESSAGE %s",sms)
        for rec in numbers:
            _logger.warning("# number object %s",rec)
        #number = {'name':'','gender':1,'phone':self.test_phone,'prefix':'216','type':'cell'}
        return client.sendSms(self.api_id, self.sms_header, sms, numbers) 
        

    @api.multi
    def set_verified(self):
        client = Client(url=self.api_path,  debug=1)
        response = client.loginUser(self.login,self.password)
        _logger.warning("## %s",response)
        if response['success'] == True:
            data = response['data']
            self.write({'state': 'verified', 'name':data['name'], 'last_name':data['last_name'], 'active':data['active'], 'sms_header':data['sms_header'], 'api_id':data['id']})
            response = client.get_header(self.api_id)
            if response['success'] == True and response['data']['status'] == True:
                data = response['data']
                header = data['headers'][0] if len(data['headers']) else None
                if header != None:
                     self.write({'sms_header_id': header['id'], 'sms_header': header['sms_header']})
            # keep only one record in verified state
            records = self.env['school.sms.setup'].search([])
            for rec in records:
                if rec.id != self.id:
                    rec.write({'state': 'draft'})
            self.env.cr.commit()
            raise Warning('Connexion établie avec succès')
        else:
            raise Warning(response['error'])



class SchoolTasks(models.Model):
    _inherit = 'school.task'


    @api.multi
    def send_sms(self):
        sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
        if not sender:
            raise Warning('Vous devez valider une configuration sms pour envoyer des sms')
        number_list = []
        for rec in self:
            for student in rec.student_ids:
                for parent in student.parent_ids:
                    phone = parent.mobile if parent.mobile else parent.phone
                    if phone:
                        number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                        number_list.append(number)
        _logger.warning("NUMBERS %s",number_list)
        response = sender.send_sms(number_list)
        if response['success'] == True:
            self.env.cr.commit()
            raise Warning('Messages envoyés avec succès')




class SchoolTaskSMSWizard(models.TransientModel):
    _name = 'school.task.sms.wizard'
        
    school_task_id      = fields.Many2one('school.task')
    parent_ids          = fields.Many2many('res.partner')
    description         = fields.Char(string="Message")

    @api.model
    def default_get(self, fields):
        res = super(SchoolTaskSMSWizard, self).default_get(fields)
        active_id = self._context['active_id']
        school_task_id = self.env['school.task'].browse(active_id)
        res['school_task_id'] = school_task_id.id
        parents = self.env['res.partner']
        for student in school_task_id.student_ids:
            for parent in student.parent_ids:
                parents += parent
        _logger.warning("## Parents %s",parents)
        res['parent_ids'] = [(6,False,[y for y in parents.ids])] 
        res['description'] = school_task_id.description
        return res


    @api.multi
    def send_sms(self):
        self.ensure_one()
        active_id = self._context['active_id']
        school_task_id = self.env['school.task'].browse(active_id)
        _logger.warning("### SchoolTaskSMSWizard %s", school_task_id )
        sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
        if not sender:
            raise Warning('Vous devez configurer le service sms pour envoyer des messages')
        number_list = []
        for rec in school_task_id:
            for student in rec.student_ids:
                for parent in student.parent_ids:
                    phone = parent.mobile if parent.mobile else parent.phone
                    if phone:
                        number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                        _logger.warning("### NUMBER %s",number)
                        number_list.append(number)
        _logger.warning("NUMBERS %s",number_list)
        response = sender.send_from_wizard_sms(self.description, number_list)
        if response['success'] == True:
            self.env.cr.commit()
            raise Warning('Messages envoyés avec succès')


# [   
#     {   "id":"067b80a5-9d4d-4044-9e39-4103c03986a2",
#         "user_id":"612b8b30-1a80-4ece-8edb-6664f2456193",
#         "sms_header":"SMS HEADER",
#         "count_msg":1,
#         "sms":"A message sent from Odesco school management system solution",
#         "phone":"21621195064",
#         "gender":1,
#         "city":null,
#         "is_arabic":0,
#         "status":"sent",
#         "sent_dt":"2017-09-22T23:39:00.000Z"
#     }
# ]

''' 
    An article can be published only by the administration
    it can be viewed by user who belongs to the specified groups
'''
class Article(models.Model):
    _name = 'school.article'


    @api.model
    def _get_school_groups(self):
        return [('category_id', '=', self.sudo().env.ref('genext_school.module_category_school').id )]

    @api.multi
    @api.depends('comment_ids')
    def _compute_comment(self):
        for rec in self:
            if rec.comment_ids:
                rec.comment_count = len(rec.comment_ids)

    @api.multi
    @api.depends('like_ids')
    def _compute_like(self):
        for rec in self:
            if rec.like_ids:
                rec.like_count = len(rec.like_ids)


    name            = fields.Char('Article Title', required=True)
    user_id         = fields.Many2one('res.users', default=lambda self: self.env.user)
    state           = fields.Selection([
                                ('unpublished','Non publié'),
                                ('published','Publié'),
                                ('canceled','Annulé')], 
                                default='unpublished')
    subtitle        = fields.Char('Subtitle')
    content         = fields.Text('Content', required=True)
    datetime        = fields.Datetime(default=fields.Datetime.now())
    group_ids       = fields.Many2many('res.groups', string='Groups', domain=_get_school_groups)
    image_ids       = fields.One2many('school.article.image', 'article_id', string="Images")
    # will be updated only from the external API
    views_nbr       = fields.Integer(string='Views Number')

    like_ids        = fields.One2many('school.article.like', 'article_id', string="Likes")
    like_count      = fields.Integer(string='Likes', compute=_compute_like)
    
    comment_ids     = fields.One2many('school.article.comment', 'article_id', string="Comments")
    comment_count   = fields.Integer(compute=_compute_comment)
    class_ids       = fields.Many2many('school.class', string="Classes")
    from_teacher    = fields.Boolean(string="Crée par enseignant", default=False)

    
    @api.multi
    def increment_view(self):
        for rec in self:
            rec.write({'views_nbr':rec.views_nbr+1})

    @api.one
    def set_to_unpublished(self):
        self.write({'state':'unpublished'})

    @api.one
    def set_to_published(self):
        self.write({'state':'published'})
        
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            ''' foreach sanction in [self --> recordset of sanction, can contain one record or a set of records]'''
            for rec in self:
                for class_id in rec.class_ids:
                    for student in class_id.student_ids:
                        tokens = []
                        for parent in student.parent_ids:
                            for token in parent.token_ids:
                                tokens.append(token.token)
                                messageEN = 'Nouvelle publication: %s'%rec.name
                                messageFR = 'New publication: %s'%rec.name
                                data = {'module':'HomePage','data':None}
                                content = {'en':messageEN, 'fr':messageEN}
                                ''' sending push notification for each sanction'''
                                _logger.warning('##### Sending push notification %s',content)
                                notif.sendPushNotification(data=data ,content=content, tokens=[token.token])
        

    @api.one
    def set_to_canceled(self):
        self.write({'state':'canceled'})    


    @api.multi
    def unlink(self):
        # for rec in self:
        #     if rec.state != 'canceled':
        #         raise UserError("You cannot delete this article, you need to cancel it first")
        return super(Article, self).unlink()

    

    @api.multi
    def write(self, vals):
        for rec in self:
            for img in rec.image_ids:
                url = 'http://localhost:8069/web/image?model='+img._name+'&id='+str(img.id)+'&field=image'
                _logger.warning("## url %s",url)
        return super(Article, self).write(vals)



class ArticleImage(models.Model):
    _name  = 'school.article.image'
    _description = 'Image Related to an article'

    name    = fields.Char('Image name')
    description = fields.Char('Description of image')
    image = fields.Binary("Image", attachment=True)
    image_url  = fields.Char('Image url')
    file_name = fields.Char("file name")
    article_id      = fields.Many2one('school.article', ondelete='cascade')

    @api.model
    def create(self,vals):
        res = super(ArticleImage, self).create(vals)
        if vals.get('image',False):
            domain = [
                ('res_model', '=', res._name),
                ('res_field', '=', 'image'),
                ('res_id', '=', res.id),
            ]
            attachment = self.env['ir.attachment'].search(domain)
            attachment.write({'public':True})
            _logger.warning("ATTACHMENT %s", attachment)
            _logger.warning("ATTACHMENT ID %s", attachment.id)
            base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
            res['image_url'] = str(base_url)+'/web/content/'+str(attachment.id)
        return res

    # updated without test: check if the new image still got the same ir.attachment id and is public or not 
    @api.multi
    def write(self, vals):
        res = super(ArticleImage, self).write(vals)
        for rec in self:
            _logger.warning("CONTENT OF VALS %s",vals)
            if vals.get('image',False):
                domain = [
                    ('res_model', '=', rec._name),
                    ('res_field', '=', 'image'),
                    ('res_id', '=', rec.id),
                ]
                attachment = self.env['ir.attachment'].search(domain)
                attachment.write({'public':True})
                _logger.warning("ATTACHMENT %s", attachment)
                _logger.warning("ATTACHMENT ID %s", attachment.id)
                _logger.warning("ATTACHMENT PUBLIC %s", attachment.public)
        return res


class Comment(models.Model):
    _name  = 'school.article.comment'
    _description = 'Comments Related to an article'

    name        = fields.Text('Comment')
    user_id     = fields.Many2one('res.users', default=lambda self: self.env.user)
    datetime    = fields.Datetime(default=fields.Datetime.now())
    article_id  = fields.Many2one('school.article', ondelete='cascade')

    

class Like(models.Model):
    _name  = 'school.article.like'
    _description = 'Likes Related to an article'

    name        = fields.Char('Like')
    user_id     = fields.Many2one('res.users', default=lambda self: self.env.user)
    article_id  = fields.Many2one('school.article', ondelete='cascade')

    _sql_constraints = [('code_uniq', 'unique(user_id,article_id)','You already liked this article')]
    

class Resgroups(models.Model):
    _inherit = 'res.groups'