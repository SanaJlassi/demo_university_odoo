# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import logging
_logger = logging.getLogger(__name__)


class ConductReportWizard(models.TransientModel):
    _name = 'conduct.report.wizard'

    @api.model
    def get_attendance(self):
        current_date = datetime.today()
        # _logger.warning("################################# CURRENT DATE %s",datetime.now().strftime('%Y-%m-%d 00:00:00'))
        # _logger.warning("################################# CURRENT DATE %s",datetime.now().strftime('%Y-%m-%d 23:23:59'))
        attendance_ids =  self.env['school.attendance'].search([('datetime','>=',datetime.now().strftime('%Y-%m-%d 00:00:00')),('datetime','<=',datetime.now().strftime('%Y-%m-%d 23:23:59'))])
        # for att in attendance_ids:
        #     print "####", att.datetime
        return attendance_ids

    @api.model
    def get_dispenses(self):
        return self.env['school.exemption'].search([('end_date','>=',datetime.now().strftime('%Y-%m-%d 23:23:59'))])

    @api.model
    def get_sanctions(self):
        return  self.env['school.sanction'].search([('date','>=',datetime.now().strftime('%Y-%m-%d 00:00:00')),('date','<=',datetime.now().strftime('%Y-%m-%d 23:23:59'))])

    attendance_ids      = fields.Many2many('school.attendance', string='Absences', default=get_attendance)
    dispense_ids        = fields.Many2many('school.exemption', string='Dispenses', default=get_dispenses)
    sanction_ids        = fields.Many2many('school.sanction', string='Sanctions', default=get_sanctions)  


    def _build_contexts(self, data):
        result = {}
        result['attendance_ids']    = 'attendance_ids' in data['form'] and data['form']['attendance_ids'] or False
        result['dispense_ids']      = 'dispense_ids' in data['form'] and data['form']['dispense_ids'] or False
        result['sanction_ids']      = 'sanction_ids' in data['form'] and data['form']['sanction_ids'] or False
        return result

    def _print_report(self, data):
        return self.env['report'].get_action(self, 'genext_school.report_conductdailyreport', data=data)

    @api.multi
    def check_report(self):
        self.ensure_one()
        data = {}
        data['ids'] = self.env.context.get('active_ids', [])
        data['model'] = self.env.context.get('active_model', 'ir.ui.menu')
        data['form'] = self.read(['attendance_ids','dispense_ids','sanction_ids'])[0]
        used_context = self._build_contexts(data)
        data['form']['used_context'] = dict(used_context, lang=self.env.context.get('lang', 'en_US'))
        return self._print_report(data)


class ConductReportReport(models.AbstractModel):
    _name = 'report.genext_school.report_conductdailyreport'

    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})

        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            # 'docs': student_id,
            'time': time,
        }
        return self.env['report'].render('genext_school.report_conductdailyreport', docargs)