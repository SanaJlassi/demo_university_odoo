{
    'name': 'Exams',
    'version': '1.0',
    'category': 'Scolarité',
    'author': 'Odesco Team',
    'website': 'https://www.genext-it.com',
    'depends': ['genext_school'],
    'data': [
        'security/ir.model.access.csv',
        'views/report_external_layout.xml',
        'views/report_bulletin.xml',

        'views/report_studentbulletin.xml',

        'views/bulletin_view.xml',
        'views/exam_report.xml',
        'views/exam_calendar_view.xml',
        'views/module_view.xml',
        'views/exam_view.xml',
        'views/config_view.xml',
        'views/note_view.xml',
        'views/comment_view.xml',
        'wizard/bulletin_report_view.xml',

        'wizard/report_bulletin.xml',

        
        # 'views/school_exams_menuitem.xml',
        'views/report_prescolaire.xml',

        
        'views/school_exam_config.xml',
        'views/school_exam_calendar.xml',
        'views/school_exam_note.xml',
        'wizard/school_exam_bulletin.xml',
        'wizard/school_exam_transcript.xml',
        'wizard/school_exam_notes.xml',
        'wizard/school_class_average.xml',
        'wizard/school_exam_pv.xml',
        'wizard/school_exam_laureat.xml',
        'report/report_schoolexambulletin.xml',
        'report/report_schoolexamtranscript.xml',
        'report/report_schoolexamnotes.xml',
        'report/report_schoolexamaverage.xml',
        'report/report_schoolexampv.xml',
        'report/report_schoolexamlaureat.xml',
        'report/report.xml',
        'views/menuitem.xml',
        
    ],

    'installable': True,
    'auto_install': False,
    'application': True,
}