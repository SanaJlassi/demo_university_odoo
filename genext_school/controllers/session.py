# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib



class SchoolSession(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/session/<date>/<teacher_id>/<class_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def session(self, **kwargs):
        response = {'success':False, 'data':None}
        data = []
        grades = request.env['school.session'].search([])
        for grade in grades:
            grade_data = {
                'id':grade.id, 
                'name': grade.name, 
                'code': grade.code, 
                'subjects': self._load_grade_subjects_as_json(grade),
                'classes': self._load_grade_classes_as_json(grade)
            }

            data.append(grade_data)
        response['success'] = True
        response['data'] = data
        return http.request.make_response(json.dumps(response),SchoolGrade.HEADER)


    # returns a list of subject dict [{subject}, {subject}]
    def _load_grade_subjects_as_json(self, grade):
        subjects = []
        for subject in grade.subject_ids:
            print "SUBJECT ",subject.name
            subjects.append({'id':subject.id, 'name':subject.name, 'code':subject.code})
        return subjects

    # returns a list of class dict [{subject}, {subject}]
    def _load_grade_classes_as_json(self, grade):
        classes = []
        records = request.env['school.class'].search([('grade_id','=',grade.id)])
        for record in records:
            print "CLASS ",record.name
            classes.append({'id':record.id, 'name':record.name, 'code':record.code})
        return classes