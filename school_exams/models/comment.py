# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)

class AdministrativCommentType(models.Model):
    _name = 'admin.comment.type'
    _description = 'Type de commentaire administratif'

    name                                = fields.Char('Nom')
    educational_stage_id                = fields.Many2many('educational.stage', string="Cycles", required=True)
    annual_calendar_id                  = fields.Many2one('annual.calendar', string='Années scolaire')
    semester_id                         = fields.Many2one('annual.periode', string="Semestre")
    periode_id                          = fields.Many2one('annual.periode', string="évaluation")
    evaluation_methode                  = fields.Selection([('preschool','Préscolaire'),('not_preschool','Autre')], default="not_preschool", string="Méthode d'évaluation")


class AdministrativComment(models.Model):
    _name = 'admin.comment'
    _description = 'Commentaire administratif'



    name                                = fields.Char('Commentaire')
    educational_stage_id                = fields.Many2one('educational.stage', string="Cycles", required=True)
    grade_id                            = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]", required=True)
    class_id                            = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)


    annual_calendar_id                  = fields.Many2one('annual.calendar', string="Année scolaire", required=True, default=lambda self: self.env['annual.calendar'].get_current_year())
    semestre_id                         = fields.Many2one('annual.periode', domain="[('educational_stage_id','=',educational_stage_id)]", string="Semestre", required=True)
    periode_id                          = fields.Many2one('annual.periode', domain="['&',('last_level','=',True),('parent_id','=',semestre_id)]", string="Evaluation")
    admin_comment_type_id               = fields.Many2one('admin.comment.type', string="Type de commentaire", required=True)
    admin_comment_line_ids              = fields.One2many('admin.comment.line','admin_comment_id', string="Commentaires")
    evaluation_methode                  = fields.Selection([('preschool','Préscolaire'),('not_preschool','Autre')], default="not_preschool", string="Méthode d'évaluation")



    _sql_constraints = [('name_uniq', 'unique(class_id,annual_calendar_id,semestre_id,periode_id,admin_comment_type_id)', 'Erreur de duplication de données'),]


    @api.onchange('educational_stage_id')
    def _onchang_educational_stage_id(self):
        admin_comment_type_id = self.env['admin.comment.type'].search([('evaluation_methode','=',self.educational_stage_id.evaluation_methode)], limit=1)
        return {
                    'value':{
                        'evaluation_methode':self.educational_stage_id.evaluation_methode,
                        'admin_comment_type_id':admin_comment_type_id.id,
                    },
                }



    @api.returns('self')
    @api.multi
    def get_presco_comment(self, student_id, class_id, annual_calendar_id, semestre_id):

        admin_comment_id = self.env['admin.comment'].search(['&',('class_id','=',class_id),('annual_calendar_id','=',annual_calendar_id)])
        print "#### FOUND COMMENTS ",admin_comment_id
        return admin_comment_id.filtered(lambda r: student_id in [rec.student_id.id for rec in r.admin_comment_line_ids])

    @api.returns('self')
    @api.multi
    def get_not_presco_comment(self, student_id, class_id, annual_calendar_id, semestre_id):
        admin_comment_id = self.env['admin.comment'].search(['&',('class_id','=',class_id),('annual_calendar_id','=',annual_calendar_id)])
        print "#### FOUND COMMENTS ",admin_comment_id
        return admin_comment_id.filtered(lambda r: student_id in [rec.student_id.id for rec in r.admin_comment_line_ids])


    @api.onchange('class_id')
    def _onchange_class_id(self):
        comment_list = []
        if len(self.class_id):
            student_id = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in student_id:
                comment_list.append((0,0,{'student_id':student.id, 'comment':None}))
        return {'value':{'admin_comment_line_ids':comment_list}}




class CommentLine(models.Model):
    _name = 'admin.comment.line'

    admin_comment_id                    = fields.Many2one('admin.comment')
    teacher_comment_id                  = fields.Many2one('teacher.comment')
    student_id                          = fields.Many2one('school.student', string="Étudiants", required=True, readonly=True)
    comment                             = fields.Text('Commentaire')



class TeacherComment(models.Model):
    _name = 'teacher.comment'
    _description = 'Commentaire enseignant'

    name                                = fields.Char('Commentaire')
    educational_stage_id                = fields.Many2one('educational.stage', string="Cycles", required=True)
    grade_id                            = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]", required=True)
    class_id                            = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)


    annual_calendar_id                  = fields.Many2one('annual.calendar', string="Année scolaire", required=True, default=lambda self: self.env['annual.calendar'].get_current_year())
    semestre_id                         = fields.Many2one('annual.periode', domain="[('educational_stage_id','=',educational_stage_id)]", string="Semestre", required=True)
    periode_id                          = fields.Many2one('annual.periode', domain="['&',('last_level','=',True),('parent_id','=',semestre_id)]", string="Evaluation")

    module                              = fields.Many2one('module.module', string="Module")
    teacher_id                          = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", string="Enseignant", 
                                    default=lambda self: self.env['hr.employee'].search([('partner_id','=',self.env.user.partner_id.id)]))
    teacher_comment_line_ids            = fields.One2many('admin.comment.line','teacher_comment_id', string="Commentaires")


    @api.onchange('class_id')
    def _onchange_class_id(self):
        comment_list = []
        if len(self.class_id):
            student_id = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in student_id:
                comment_list.append((0,0,{'student_id':student.id, 'comment':None}))
        return {'value':{'teacher_comment_line_ids':comment_list}}

    @api.onchange('module_id')
    def _onchange_module(self):
        comment_list = []
        if len(self.module_id):
            prev_comments = self.env['class.note'].search([('periode_id','=',self.periode_id.id),('module_id','=',self.module_id.id),('class_id','=',self.class_id.id),('grade_id','=',self.grade_id.id),('semestre_id','=',self.semestre_id.id),('annual_calendar_id','=',self.annual_calendar_id.id),('educational_stage_id','=',self.educational_stage_id.id)])
            comments = prev_comments.mapped('comment_line_ids').filtered(lambda r: r.comment != False)

            print "######### NEW COMMENTS",comments
            print "######### NEW COMMENTS",prev_comments
            for line in comments:
                print "##### comment ",line.comment
                print "### student ", line.student_id.name
                comment_list.append((0,0,{'student_id':line.student_id.id, 'comment':line.comment}))
        return {'value':{'teacher_comment_line_ids':comment_list}}