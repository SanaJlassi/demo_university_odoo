{
    'name': "Invoice Tax Stamp",
    'version': '1.0',
    'category': 'Accounting',
    'author': 'Genext-IT Team',
    'website': 'https://www.genext-it.com',
    'depends': ['account'],
    'data': [
        'security/ir.model.access.csv',
        'views/tax_stamp_view.xml',
    ],

    'installable': True,
    'auto_install': False,
    'application': False,
}