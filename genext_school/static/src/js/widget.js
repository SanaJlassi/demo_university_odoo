odoo.define('genext_school.widget', function(require){
    "use strict";
    var core = require('web.core');
    var QWeb = core.qweb;
    var common = require('web.form_common');
    var formats = require('web.formats');
    var framework = require('web.framework');
    var FieldChar = core.form_widget_registry.get('char');
    var FieldOne2Many = core.form_widget_registry.get('one2many');
    
    var Widget = require('web.Widget');
    var Model = require('web.Model');
    var Data = require('web.data');
    var Dialog = require('web.Dialog');
    var NotificationManager = require('web.notification').NotificationManager;
    var _t = core._t;


    /*
    the key to show the calendar in view mode 
    you need to set the calendar size after init 
    check the behavior when you first open a form and try to resize chrome inspector
    the calendar shows up this means the resize event is the key to fix it 

    try to use the calendar plug-in used by odoo, it supports drag and drop 
    fix problem related with the time zone and time format
    */

    /*
        try using dataset_changes or on form_change events and method of the field_manager object
        because when switching from one record to another tha calendar still rendering the same data 
    */


    var MultiDatePicker = common.AbstractField.extend(common.ReinitializeFieldMixin,{
        template: 'MultiDatePicker',
        events: {
            'change': 'store_dom_value',
        },
        init: function(field_manager, node) {
            this._super(field_manager, node);
            console.log('init()');
            // var dataset = self.field_manager.dataset;
            // var active_id = dataset.ids[dataset.index];
            console.log("SELF ",this);
            // console.log("ACTIVE_ID ",active_id);
            // console.log("DATASET ",dataset);
        },
        initialize_content: function() {
            console.log('initialize_content()')
            var self = this;

            var selectable = false;
            var editable = false;
            if(this.get('effective_readonly')){
                selectable = false;
                editable =  false;
            }else{
                selectable = true;
                editable = true;
            }

            this.calendar = this.$('#calendar');
            if(this.calendar)
                this.calendar.fullCalendar('destroy')
            
            this.calendar.fullCalendar({
                
               header: { // Display nothing at the top
                    left: '',
                    center: '',
                    right: ''
                },
                height: 480,
                timezone: false,
                defaultView: 'agendaWeek', // display week view
                hiddenDays: [0], // hide Saturday and Sunday
                allDaySlot: false, // don't show "all day" at the top
                weekNumbers:  false, // don't show week numbers
                columnFormat: 'dddd', // Display just full length of weekday, without dates 

                selectable:selectable,
                selectHelper:true,
                axisFormat: 'HH:mm', // change time format on axis to 24H
                slotMinutes: 15,
                defaultEventMinutes: 120,

                minTime: '08:00:00', // display from 16 to
                maxTime: '19:00:00', // 23 
                editable: editable,
                //droppable: true, // this allows things to be dropped onto the calendar
                events: function(start, end, callback) {
                     var model = new Model('time.table.session').query()
                    .filter([['time_table_id', '=', self.field_manager.datarecord.id]])
                    .all()
                    .then(function(sessions){
                        var session_list = []; var i;
                        for(i=0 ; i < sessions.length; i++){
                            session_list.push({
                                id: sessions[i].id,
                                allDay: false,
                                title: sessions[i].subject_id[1],
                                teacher: sessions[i].teacher_id[1],
                                classroom: sessions[i].classroom_id[1],
                                start: new Date('2017-07-'+(17+sessions[i].day_of_week)+' '+sessions[i].start_time),
                                end: new Date('2017-07-'+(17+sessions[i].day_of_week)+' '+sessions[i].end_time)
                            });
                        }
                        callback(session_list);
                    });

                },
                select: function(startDate, endDate, allDay) {
                    var dataset = self.field_manager.dataset;
                    var active_id = dataset.ids[dataset.index];
                    console.log("SELF ",self);
                    console.log("ACTIVE_ID ",active_id);
                    console.log("DATASET ",dataset);
                    


                    console.log("START SELECT ",startDate)
                    console.log("END SELECT ",endDate)
                    /*  using 2017-07-17 as a reference [17-->Monday] so just add selected weekday number to 17 [monday = 1 in week number order]*/
                    var ref_dow = 17; // reference dayOfWeek
                    /* we just need the time portion of datetime since we are using a static week*/
                    var start_time = (startDate.getHours()<10?'0':'') + startDate.getHours() + ":" + (startDate.getMinutes()<10?'0':'') + startDate.getMinutes() + ":00";
                    var end_time = (endDate.getHours()<10?'0':'') + endDate.getHours() + ":" + (endDate.getMinutes()<10?'0':'') + endDate.getMinutes() + ":00";
                    console.log("START SELECT ",start_time)
                    console.log("END SELECT ",end_time)

                    var selected_dow = ref_dow+startDate.getDay()-1; // dayofweek for monday = 1 so we need to substract 1 to keep it right
                    console.log("DAY OF WEEK",selected_dow)
                    var start = '2017-07-'+selected_dow+' '+start_time;
                    var end = '2017-07-'+selected_dow+' '+end_time;
                    // send start and end in context and maybe allDay attribute
                    self.do_action({
                        type: 'ir.actions.act_window',
                        res_model: 'table.line.session.wizard',
                        view_mode: 'form',
                        view_type: 'form',
                        views: [[false, 'form']],
                        target: 'new',
                        context: {'time_table_id': self.field_manager.datarecord.id,'start':start,'end':end, 'update':false, 'id':self.id, 'ids':self.id},
                        }, {
                            on_close: function() {
                                console.log("AFTER SELECT",self);
                                self.calendar.fullCalendar('rerenderEvents');
                                self.calendar.fullCalendar('refetchEvents');

                                //self.dataset.trigger("dataset_changed");
                            }
                    });
                },
                eventResize: function(event, delta, revertFunc) {
                    var startDate = event.start;
                    var endDate = event.end;
                    var id = event.id;
                    var start_time = (startDate.getHours()<10?'0':'') + startDate.getHours() + ":" + (startDate.getMinutes()<10?'0':'') + startDate.getMinutes() + ":00";
                    var end_time = (endDate.getHours()<10?'0':'') + endDate.getHours() + ":" + (endDate.getMinutes()<10?'0':'') + endDate.getMinutes() + ":00";
                    var selected_dow = event.start.getDay()-1;

                    var model = new Model('time.table.session');
                    model.call('write', [[id], {'name':'whaat', 'start_time':start_time, 'end_time':end_time, 'day_of_week':selected_dow}])
                    .then(
                        function(result){
                            console.log("RESULT: ",result)
                        },
                        function(error){
                            revertFunc();
                            //self.do_warn(error.message, error.data.message)
                        }
                    );
                },
                eventDrop: function(event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
                    var startDate = event.start;
                    var endDate = event.end;
                    var id = event.id;
                    var start_time = (startDate.getHours()<10?'0':'') + startDate.getHours() + ":" + (startDate.getMinutes()<10?'0':'') + startDate.getMinutes() + ":00";
                    var end_time = (endDate.getHours()<10?'0':'') + endDate.getHours() + ":" + (endDate.getMinutes()<10?'0':'') + endDate.getMinutes() + ":00";
                    var selected_dow = event.start.getDay()-1;

                    var model = new Model('time.table.session');
                    model.call('write', [[id], {'name':'whaat', 'start_time':start_time, 'end_time':end_time, 'day_of_week':selected_dow}])
                    .then(
                        function(result){
                            console.log("RESULT: ",result)
                        },
                        function(error){
                            revertFunc();
                            //self.do_warn(error.message, error.data.message)
                        }
                    );
                },
                eventClick:function (event, jsEvent) {
                    console.log('######## EVENT CLICKED ',event);
                    console.log('######## EVENT CLICKED ',jsEvent);
                    var startDate = event.start
                    var endDate = event.end

                    console.log("START SELECT ",startDate)
                    console.log("END SELECT ",endDate)
                    /*  using 2017-07-17 as a reference [17-->Monday] so just add selected weekday number to 17 [monday = 1 in week number order]*/
                    var ref_dow = 17; // reference dayOfWeek
                    /* we just need the time portion of datetime since we are using a static week*/
                    var start_time = (startDate.getHours()<10?'0':'') + startDate.getHours() + ":" + (startDate.getMinutes()<10?'0':'') + startDate.getMinutes() + ":00";
                    var end_time = (endDate.getHours()<10?'0':'') + endDate.getHours() + ":" + (endDate.getMinutes()<10?'0':'') + endDate.getMinutes() + ":00";
                    console.log("START SELECT ",start_time)
                    console.log("END SELECT ",end_time)

                    var selected_dow = ref_dow+startDate.getDay()-1; // dayofweek for monday = 1 so we need to substract 1 to keep it right
                    console.log("DAY OF WEEK",selected_dow)
                    var start = '2017-07-'+selected_dow+' '+start_time;
                    var end = '2017-07-'+selected_dow+' '+end_time;
                    // send start and end in context and maybe allDay attribute
                    self.do_action({
                        type: 'ir.actions.act_window',
                        res_model: 'table.line.session.wizard',
                        view_mode: 'form',
                        view_type: 'form',
                        views: [[false, 'form']],
                        target: 'new',
                        context: {'time_table_session_id':event.id,'time_table_id': self.field_manager.datarecord.id,'start':start,'end':end, 'update':true},
                        }, {
                            on_close: function() {
                                console.log("AFTER SELECT",self);
                                self.calendar.fullCalendar('rerenderEvents');
                                self.calendar.fullCalendar('refetchEvents');

                                //self.dataset.trigger("dataset_changed");
                            }
                    })
                },
                eventRender: function(event, element) {
                    element.find('.fc-event-title').append("<br/>" + event.teacher);
                    element.find('.fc-event-title').append("<br/>" + event.classroom);
                },
            });
            this.calendar.fullCalendar( 'gotoDate', new Date('2017-07-17'));
        },
        render_value: function() {
            console.log('render_value()');
            $(window).trigger('resize');
        },
        destroy: function() {
            console.log('destroy()');
            this.$('#calendar').fullCalendar('destroy');
            this._super.apply(this, arguments);
        },
    });
    core.form_widget_registry.add('MultiDatePicker', MultiDatePicker);
});


