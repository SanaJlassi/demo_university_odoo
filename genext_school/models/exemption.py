# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
from datetime import date, datetime
import cookielib
import urllib2
import smtplib
import json
import string
import logging
_logger = logging.getLogger(__name__)


class SchoolExemption(models.Model):
    _name="school.exemption"
    _description="School Exemption"

    name                     = fields.Char("Name")
    academic_year   		 = fields.Many2one('academic.year', string="Année scolaire" ,default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) )
    academic_year_code 		 = fields.Char(related='academic_year.code', string="Année scolaire" )
    class_id                 = fields.Many2one('school.class', string="Classe", required=True)
    subject_id               = fields.Many2one('school.subject', string='Matière', required=True)
    # teacher_id               = fields.Many2one('hr.employee', string='Enseignant',domain="[('is_school_teacher','=',True)]", required=True)
    start_date 				 = fields.Datetime(default=fields.Datetime.now(), string='Date de début')
    end_date 				 = fields.Datetime(default=fields.Datetime.now(), string='Date de fin')
    student_id               = fields.Many2one('school.student', string="Étudiant", domain="[('class_id','=',class_id)]", required=True)
    description              = fields.Text(string="Description")
    file                     = fields.Binary(string='Fichier', attachment=True)
    file_name                = fields.Char(string="File name")

    @api.multi
    def name_get(self):
        return [(record.id, record.class_id.name+'/'+record.subject_id.name)for record in self]

    def action_to_notify_parents(self):
        number_list = []
        emailparent=[]
        email_config  = self.env['email.config'].search([],limit=1)
        sender = 'conctactecolepetitprince@gmail.com'
        subject = self.subject_id.name
        subject = str(subject)
        start_date = self.start_date
        start_date = str(start_date)
        end_date = self.end_date
        end_date  = str(end_date)
        student = self.student_id
        nom = student.name + ' ' + student.last_name
        for parent in self.student_id.parent_ids:
            receiver = parent.email
            emailparent.append(receiver)
            phone = parent.mobile if parent.mobile else parent.phone
            if phone:
                number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                _logger.warning("### NUMBER %s",number)
                number_list.append(number)

        From = email_config.sender_email
        Subj = email_config.subject
        receivers=emailparent
        print "receivers#####",receivers
        Text = "Votre Élève " + nom + " a pris une dispence en " + subject + ' de ' + start_date + ' à '+end_date 
        Body = string.join((
                "From: %s" % From,
                "",
                Text,
                ""
                ),"\r\n")
        for receiver in receivers:
            print(Text)
            try:
              print("-------------------------------------------------------")
              smtpObj = smtplib.SMTP(host='smtp.gmail.com', port=587)
              smtpObj.ehlo()
              smtpObj.starttls()
              smtpObj.ehlo()
              print("+++++++++++")
              smtpObj.login(user=email_config.sender_email,password=email_config.sender_password)
              smtpObj.sendmail(sender, receiver, Body)
              smtpObj.close()        
              _logger.warning("Successfully sent email")
            except:
              _logger.warning("#### ERRROR while sending email")

        print("here ------------ sms-------------------")
        sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
        if not sender:
            print("Vous devez valider une configuration sms pour envoyer des sms")
           #raise Warning('Vous devez valider une configuration sms pour envoyer des sms')
        else:
            _logger.warning("NUMBERS %s",number_list)
            response = sender.send_lot_sms(Text, number_list)
            if response['success'] == True:
               self.env.cr.commit()
               #raise Warning('Messages envoyés avec succès')
               print("Messages envoyés avec succès")
            else:
               #raise Warning('Messages non envoyés')
               print("Messages non envoyés")

        print("**********notification**************************")
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            tokens = []
            for parent in self.student_id.parent_ids:
                for token in parent.token_ids:
                    _logger.warning("#### TOKEN %s",parent.token)
                    tokens.append(token.token)
                    dispence = {
                        'student':nom,
                        'description':self.description,
                        'start_date':self.start_date,
                        'end_date':self.end_date,
                        'class':{
                            'id':self.class_id.id,
                            'name':self.class_id.name,
                            'code':self.class_id.code,
                        },
                        'file':{
                            'base64':self.file,
                            'filename':self.file_name,
                        }
                    }
                    data = {'module':'ExemptionPage','data':dispence, 'kid':{'id':self.student_id.id,'name':self.student_id.name,'last_name':self.student_id.last_name}}
                    content = {'en':'Your child has taken a dispense', 'fr':'Votre enfant a pris une dispence'}
                    notif.sendPushNotification(data=data ,content=content, tokens=[token.token])