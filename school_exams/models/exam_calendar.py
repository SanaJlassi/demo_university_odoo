# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)


class AnnualCalendar(models.Model):
    _name = 'annual.calendar'
    _description = 'Annual calendar for exams'


    name                    = fields.Char("Nom de l'année scolaire", required=True)
    code                    = fields.Char("Code de l'années scolaire", required=True)
    start_date              = fields.Date('Date début', required=True)
    end_date                = fields.Date('Date fin', required=True)
    periode_ids             = fields.One2many('annual.periode','annual_calendar_id', string="Périodes")
    educational_stage_id    = fields.Many2many('educational.stage', string="Cycles")


    @api.multi
    @api.returns('self')
    def get_current_year(self):
        current_date = fields.Date.today()
        current_year = self.env['academic.year'].search(['&',('start_date','<=',current_date),('end_date','>=',current_date)], limit=1)
        if not current_year:
            raise ValidationError('vous devez définir une année scolaire !')
        return current_year


    @api.one
    @api.constrains('start_date', 'end_date')
    def _check_dates(self):
        if self.start_date >= self.end_date:
            raise ValidationError("La date de fin de période doit être supérieure à la date de début de période")

    @api.multi
    def name_get(self):
        return [(record.id, record.name+' '+record.code )for record in self]



class PeriodeConfig(models.Model):
    _name = 'periode.config'


    name                            = fields.Char('Nom', required=True)
    annual_calendar_id              = fields.Many2one('annual.calendar', string="Année scolaire", required=True,
                                        default=lambda self: self.env['annual.calendar'].get_current_year())
    periode_ids                     = fields.One2many('annual.periode','periode_config_id', string="Périodes")
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle")
    


class AnnualPeriode(models.Model):
    _name = 'annual.periode'

    name                    = fields.Char('Nom du période', required=True)
    code                    = fields.Char('Code du période', required=True)
    periode_config_id       = fields.Many2one('periode.config')
    annual_calendar_id      = fields.Many2one(related='periode_config_id.annual_calendar_id')
    educational_stage_id    = fields.Many2one(related='periode_config_id.educational_stage_id')
    start_date              = fields.Date('Date début', required=True)
    end_date                = fields.Date('Date fin', required=True)
    last_level              = fields.Boolean('Dernier niveau', default=True)
    parent_id               = fields.Many2one('annual.periode')
    child_id                = fields.One2many('annual.periode', 'parent_id')
    evaluated               = fields.Boolean('A évaluer', default=False)
    ponderation             = fields.Integer('Pondération', default=0)
    order_value             = fields.Integer('Order', default=0)


    @api.returns('self')
    @api.multi
    def get_periode_by_cycle(self, cycle_id):
        periode_id = self.env['annual.periode'].search([])
        return module_id.filtered(lambda r: cycle_id in r.educational_stage_ids.ids)



    @api.one
    @api.constrains('start_date', 'end_date')
    def _check_dates(self):
        if self.start_date >= self.end_date:
            raise ValidationError("La date de fin de période doit être supérieure à la date de début de période")
        

    @api.model
    def create(self, vals):
        res = super(AnnualPeriode, self).create(vals)
        start_date = datetime.strptime(res.start_date, DEFAULT_SERVER_DATE_FORMAT)
        end_date = datetime.strptime(res.end_date, DEFAULT_SERVER_DATE_FORMAT)
        if res.annual_calendar_id.start_date and res.annual_calendar_id.end_date:
            year_start = datetime.strptime(res.annual_calendar_id.start_date, DEFAULT_SERVER_DATE_FORMAT)
            year_end = datetime.strptime(res.annual_calendar_id.end_date, DEFAULT_SERVER_DATE_FORMAT)

            if  start_date < year_start or start_date > year_end:
                raise ValidationError("Date de début de période [%s] doit être entre l'intervalle d'année "%res.name)
            if  end_date < year_start or end_date > year_end:
                raise ValidationError("Date de fin de période [%s] doit être entre l'intervalle d'année "%res.name)

        if res.parent_id:
            parent_start = datetime.strptime(res.parent_id.start_date, DEFAULT_SERVER_DATE_FORMAT)
            parent_end = datetime.strptime(res.parent_id.end_date, DEFAULT_SERVER_DATE_FORMAT)
            if  start_date < parent_start or start_date > parent_end:
                raise ValidationError("Date de début de période [%s] doit être entre l'interval du niveau superieur"%res.name)
            if  end_date < parent_start or end_date > parent_end:
                raise ValidationError("Date de fin de période [%s] doit être entre l'interval du niveau superieur"%res.name)

        
        return res
