# -*- coding: utf-8 -*-


from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
import odoo.addons.decimal_precision as dp



 
class account_payment(models.Model):
    _inherit = 'account.payment'

    state                         = fields.Selection([('draft', 'Draft'), ('posted', 'Confirmé'), ('sent', 'Sent'), ('reconciled', 'Reconciled')], readonly=True, default='draft', copy=False, string="Status")
    account_payment_line_ids      = fields.One2many('account.payment.line', 'account_payment_id')

    student_id                    = fields.Many2one('school.student', string='élève')
    class_id                      = fields.Many2one('school.class', string="Classe", compute="_compute_class" , store=True)
    parent_ids                    = fields.Many2many(related='student_id.parent_ids')
    student_pid                   = fields.Char(related='student_id.pid')

    num_check                     = fields.Char(string="Numéro du chéque")
    check_holder                  = fields.Char(string="Titulaire du chéque")
    bank_name                     = fields.Char(string="Banque")
    rib                           = fields.Char(string="RIB")
    due_date                      = fields.Date(string="Date d'échéance")
    company_name                  = fields.Char(string="Société")
    type                          = fields.Selection(related="journal_id.type")

    


    @api.depends('student_id')
    @api.multi
    def _compute_class(self):
        for rec in self:
            if rec.student_id.class_id:
                rec.class_id = rec.student_id.class_id
        print"#########",rec.class_id

    @api.onchange('account_payment_line_ids')
    @api.multi
    def _onchange_account_payment_line_ids(self):
        for rec in self:
            total = 0.0
            for line in rec.account_payment_line_ids:
                total += line.total_ttc
            rec.amount = total


   



class account_payement_line(models.Model):
    _name='account.payment.line'

    
    academic_year           = fields.Many2one('academic.year',string='Année', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]))
    account_payment_id      = fields.Many2one('account.payment', ondelete='cascade')
    product_id              = fields.Many2one('product.product', domain=[('sale_ok', '=', True)], ondelete='restrict', required=True, string='Frais')
    description             = fields.Char(string="Description")
    price                   = fields.Float(related='product_id.price_ttc', string="Prix") # from product.template
    quantity                = fields.Integer('Quantité' , default=1)
    discount                = fields.Float(string="Remise", digits=dp.get_precision('Product Price'), default=0.000)
    discount_string         = fields.Char(string="Remise (%)", compute="_compute_discount_string")
    total_ttc               = fields.Float(string='Montant TTC', digits=dp.get_precision('Product Price'))
    month                   = fields.Selection([(1,'Janvier'),
                                                (2,'Février'),
                                                (3,'Mars'),
                                                (4,'Avril'),
                                                (5,'Mai'),
                                                (6,'Juin'),
                                                (9,'Septembre'),
                                                (10,'Octobre'),
                                                (11,'Novembre'),
                                                (12,'Décembre'),
                                                ], string="Mois")

    student_id              = fields.Many2one('school.student', compute='_compute_student_class', store=True, default=False)
    class_id                = fields.Many2one('school.class', compute='_compute_student_class', store=True, default=False)

    dummy_student_id        = fields.Many2one('school.student', compute='_compute_dummy_student_class')
    dummy_class_id          = fields.Many2one('school.class', compute='_compute_dummy_student_class')

    is_invoiced             = fields.Boolean(string="Facturée", default=False)


    @api.multi
    @api.depends('account_payment_id')
    def _compute_student_class(self):
        for record in self:
            print "### record ",record.account_payment_id.amount
            if record.account_payment_id:
                print "### student ", record.account_payment_id.student_id.name
                print "### student ", record.account_payment_id.class_id.name
                record.student_id = record.account_payment_id.student_id
                record.class_id = record.account_payment_id.class_id

    @api.multi
    @api.depends('account_payment_id')
    def _compute_dummy_student_class(self):
        for record in self:
            print "### record ",record.account_payment_id.amount
            if record.account_payment_id:
                print "### student ", record.account_payment_id.student_id.name
                print "### student ", record.account_payment_id.class_id.name
                record.dummy_student_id = record.account_payment_id.student_id
                record.dummy_class_id = record.account_payment_id.class_id



    @api.multi
    @api.depends('discount')
    def _compute_discount_string(self):
        for rec in self:
            #rec._compute_student_class()
            if rec.discount:
                rec.discount_string = unicode(rec.discount).encode('utf-8')+' %'
            


    @api.multi
    @api.onchange('product_id', 'quantity', 'price', 'discount')
    def product_id_change(self):
        for line in self:
            line.total_ttc = line.product_id.price_ttc * line.quantity * (1-(line.discount/100))



    