# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime , timedelta
import logging
import json

_logger = logging.getLogger(__name__)


class SchoolAttendance(http.Controller):

    HEADER = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Credentials':'True'
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }
    
    @http.route('/api/attendance', type='http', auth="user", methods=['GET','POST'],  csrf=False)
    def attendance(self,**fields):
        response = {'success':False, 'data':None}
        if request.httprequest.method == 'GET':
            
            if request.env.user.has_group('genext_school.group_school_administration'):
                self._handleAdminRequest()
            elif request.env.user.has_group('genext_school.group_school_teacher'):
                self._handleTeacherRequest()
            else:
                response['success'] = False
                response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
                return http.request.make_response(json.dumps(response),SchoolAttendance.HEADER)

            user = request.env['hr.employee'].search(['&',('is_school_teacher','=',True),('user_id.id','=',request.env.user.id)])
            
            session_id = self._getSession(user)

            data = {'session':None, 'class':None, 'academic_year':None,'subject':None,'timing_system_period':None, 'students':[]}
            if session_id:
                students = request.env['school.student'].search([('class_id','=',session_id.class_id.id)])
                for student in students:
               
                    domain = [
                    ('res_model', '=',student.user_id.partner_id._name),
                    ('res_field', '=', 'image'),
                    ('res_id', '=',student.user_id.partner_id.id),
                    ]
                    attachment = request.env['ir.attachment'].search(domain)
                    base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                    url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+'?session_id='+request.env['ir.http'].session_info()['session_id']
                    url = url.replace(" ", "_")
                    _logger.warning('#### URL of document %s',url)
                    data['students'].append({'id':student.id, 'pid':student.pid, 'name':student.name,'last_name':student.last_name, 'image':url, 
                            'email':student.email,
                            'attendance':{'present':True,'time_late':0}
                        })
                data['session'] = {'id':session_id.id, 'name':session_id.name, 'start_time':session_id.start_time, 'end_time':session_id.end_time, 'day_of_week':session_id.day_of_week}
                data['classroom'] = {'id':session_id.classroom_id.id, 'name':session_id.classroom_id.name}
                data['class'] = {'id':session_id.class_id.id, 'name':session_id.class_id.name, 'code':session_id.class_id.code}
                data['academic_year'] = {'id':session_id.time_table_id.academic_year.id, 'name':session_id.time_table_id.academic_year.name, 'code':session_id.time_table_id.academic_year.code}
                data['subject'] = {'id':session_id.subject_id.id, 'name':session_id.subject_id.name}
                data['timing_system_period'] = {'id':session_id.timing_periode.id, 'name':session_id.timing_periode.name}
            response['success'] = True
            response['data'] = data
        elif request.httprequest.method == 'POST':
            data = json.loads(request.params.get('attendance',False))  
            response['success'] = True
            response['data'] = 'Say Whaaaat !'

            session_id = data['session']['id']
            class_id = data['class']['id']
            timing_system_period_id = data['timing_system_period']['id']
            subject_id = data['subject']['id']
            classroom_id = data['classroom']['id']
            academic_year_id = data['academic_year']['id']

            print "### session id",data['session']['id']
            print "### class id",data['class']['id']
            print "### timing_system_period id",data['timing_system_period']['id']
            print "### subject id",data['subject']['id']
            print "### classroom id",data['classroom']['id']

            attendance_lines = []
            for el in data['students']:
                state = 'present' if el['attendance']['present'] else 'absent'
                attendance_lines.append( (0,0,{'name':'name','student_id':el['id'],'state':state}) )


            user_id = request.env.user
            teacher_id = request.env['hr.employee'].search([('user_id','=',user_id.id)])
            attendance = {
                'teacher_id':teacher_id.id,
                'session_id':request.env['time.table.session'].search([('id','=',session_id)]).id,
                'class_id':request.env['school.class'].search([('id','=',class_id)]).id,
                'subject_id':request.env['school.subject'].search([('id','=',subject_id)]).id,
                'classroom_id':request.env['school.classroom'].search([('id','=',classroom_id)]).id,
                'attendance_line_ids':attendance_lines,
            }
            res = request.env['school.attendance'].create(attendance)
            print "#### RES OF CREATE ",res
            return http.request.make_response(json.dumps(response),SchoolAttendance.HEADER_POST)
            

        return http.request.make_response(json.dumps(response),SchoolAttendance.HEADER)

   
    @http.route('/api/attendance/<date>', type='http', auth="user", methods=['GET'],  csrf=False)
    def attendancess(self,**fields):
        pass

    def _handleAdminRequest(self):
        pass

    def _handleTeacherRequest(self):
        pass



    def _getSession(self,user_id):
        dow = datetime.today().weekday()
        add_hour    =   datetime.now() + timedelta(hours=1) 
        time_string = add_hour.time().strftime('%H:%M:%S')
        current_time = datetime.strptime(time_string, '%H:%M:%S') # [time] from string to object
        print "##### CURRENT TIME",current_time
        current_academic_year = request.env['academic.year'].search([('active_year','=',True)])
        for session in request.env['time.table.session'].search([('time_table_id.academic_year','=',current_academic_year.id)]):# <--- check current year and timing session period here
            start_period = str(datetime.now().year)+'-'+session.timing_periode.start_date_month
            end_period = str(datetime.now().year)+'-'+session.timing_periode.end_date_month
            # convert date sting to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period < datetime.now().date() < end_period:
                print "#### CURRENT PERIODES ",session.timing_periode.name
                # try to check day of week and teacher first to avoid unnecessary further test of datetime
                print "DAY OF WEEK ",dow
                print "SESSION DOW ",session.day_of_week
                print "YEAR ",datetime.now().date().year
                print "MONTH ",datetime.now().date().month
                print "DAY ",datetime.now().date().day
                print "CURRENT TIME ",current_time
                if session.day_of_week == dow and session.teacher_id.id == user_id.id: # user.id is the hr.employee related to logged user
                    session_start_time = datetime.strptime(session.start_time,'%H:%M:%S') 
                    session_end_time = datetime.strptime(session.end_time,'%H:%M:%S')
                    print "SESSION START TIME",session_start_time
                    print "CURRENT TIME ",current_time
                    print "SESSION END TIME ",session_end_time
                    if session_start_time <= current_time <= session_end_time:
                        print "FOUND SESSION ", session
                        return session
            else:
                print "#### NOT CURRENT PERIODE ",session.timing_periode.name
            print "--------------------------------"
        return False

    def _getClass(self):
        pass

    def _getClassroom(self):
        pass




    def updateTeacher(self, teacherId, **fields):
        response = {'success':False, 'data':None}
        print "TEACHER ID ",teacherId
        print "FIELDS ",fields
        if request.params.get('teacherId',False):
            teacher_id = request.params.get('teacherId')
            record = request.env['hr.employee'].sudo().search(['&',('is_school_teacher','=',True),('user_id.id','=',teacher_id)])
            fieldKeys = record._fields.keys()
            teacher = False
            if isinstance(fields,dict) and len(fields):
                for k,v in fields.items():
                    if k not in fieldKeys:
                        response['success'] = False
                        response['error'] = {'code':404, 'message':'Invalide field name ['+k+']'}
                        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)
                record.write(fields)
                teacher = record.read()
            if len(teacher):
                response['data'] = teacher
                response['success'] = True
            else:
                response['success'] = False
                response['error'] = {'code':404, 'message':'teacherId does not exist'}    
        else:
            response['success'] = False
            response['error'] = {'code':400, 'message':'Missing parameter teacherId'}

        return http.request.make_response(json.dumps(response),SchoolTeacher.HEADER)




    """@http.route('/api/student/attendance', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_attendance_by_student_parent(self, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolAttendance.HEADER)

        partner_id = request.env.user.partner_id
        student_id  = request.env.user.partner_id.student_ids
        # student_id = request.env['school.student'].search([('user_id','=',request.env.user.id)])
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolAttendance.HEADER)     

        for student in student_id:
            class_id = student_id.class_id
            academic_year_id = request.env['academic.year'].search([('active_year','=',True)])
            attendance_id = request.env['school.attendance'].search(['&',('class_id','=',class_id.id),('academic_year','=',academic_year_id.id)])

            attendance_list = []
            for att in attendance_id:
                for line in att.attendance_line_ids:
                    # if line.student_id.id == student_id.id and line.state != 'present':
                    if line.student_id.id == student_id.id :
                        attendance_list.append({
                            'id':att.id,
                            'teacher':{
                                'id':att.teacher_id.id,
                                'name':att.teacher_id.name,
                                'last_name':att.teacher_id.last_name,
                            },
                            'subject':{
                                'id':att.subject_id.id,
                                'name':att.subject_id.name,
                                'code':att.subject_id.code,
                            },
                            'session':{
                                'start_time':att.session_id.start_time,
                                'end_time':att.session_id.end_time,
                                'dow':att.session_id.day_of_week,
                            },
                            'date':att.datetime,
                            'state':line.state,
                        })
                        break
        print "#### DONE "
        response['success'] = True
        response['data'] = attendance_list
        return http.request.make_response(json.dumps(response),SchoolAttendance.HEADER)"""