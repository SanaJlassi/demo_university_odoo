# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.modules import get_resource_path
from dateutil.parser import parse
import time
import locale
import calendar
from datetime import date, datetime

from odoo import fields as attributes
import cookielib
import urllib2
import json
import smtplib
import string
import re
import logging
_logger = logging.getLogger(__name__)



class CheckReportWizard(models.TransientModel):
    _name = 'check.report.wizard'

    from_date   =   fields.Date('Date début', default=datetime.today(), required=True)
    to_date     =   fields.Date('Date fin', default=datetime.today(), required=True)


    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['from_date','to_date'])[0]
        #print "######### DATA ",data
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['from_date','to_date'])[0])
        #print "####### UPDATED DATA ",data
        return self.env['report'].get_action(self, 'school_accounting.check_report', data=data)


class CheckReport(models.AbstractModel):
    _name = 'report.school_accounting.check_report'

    @api.model
    def render_html(self, docids, data=None):
        self.model      = self.env.context.get('active_model')
        docs            = self.env[self.model].browse(self.env.context.get('active_id'))
        payment_line    = self.env['account.payment'].search(['&',('type','=','bank'),'|',('payment_date','>=',docs.from_date),('due_date','<=',docs.to_date)])
        #payment_line    = self.env['account.payment'].search(['&',('journal_id.code','=','CHEK'),('payment_date','>=',docs.from_date)])
        
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'payment_line': payment_line,
        }
        _logger.warning("####### render_html ")
        #print "################# docargs ",docargs
        return self.env['report'].render('school_accounting.check_report', docargs)

class StateAccountWizard(models.TransientModel):
    _name = 'state.account.wizard'

    from_date       =   fields.Date('Date début', default=datetime.today(), required=True)
    to_date         =   fields.Date('Date fin', default=datetime.today(), required=True)
    product_type    =   fields.Selection([('primary', 'Principale'), ('auxiliary', 'Auxiliaire')], default='primary', string="Nature des frais", required=True)
    state_account   =   fields.Selection([('caisse', 'Caisse'), ('day', 'Jour'), ('month', 'Mois'), ('year', 'An')], default='caisse', string="États comptable", required=True)
    timing_periode  =   fields.Many2many('school.timing.system.period', domain="[('timing_system_id.active_period','=',True)]", required=True, default=lambda self: self.env['school.timing.system.period'].search([]))


    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['from_date','to_date','product_type','state_account', 'timing_periode'])[0]
        #print "######### DATA ",data
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['from_date','to_date','product_type','state_account', 'timing_periode'])[0])
        #print "####### UPDATED DATA ",data
        return self.env['report'].get_action(self, 'school_accounting.state_account_report', data=data)


class StateAccount(models.AbstractModel):
    _name = 'report.school_accounting.state_account_report'

    @api.model
    def render_html(self, docids, data=None):
        self.model      = self.env.context.get('active_model')
        docs            = self.env[self.model].browse(self.env.context.get('active_id'))
        product_ids     = self.env['product.template'].search([('product_type','=', docs.product_type )])
        #invoices        = self.env['account.invoice'].search(['&',('date_invoice','>=',docs.from_date),('date_invoice','<=',docs.to_date)])
        
        if docs.state_account == 'day':
            invoices        = self.env['account.invoice'].search(['&',('date_invoice','>=',docs.from_date),('date_invoice','<=',docs.to_date)])
        if docs.state_account == 'month':
            invoices        = self.env['account.invoice'].search(['&',('date_invoice.month','>=',docs.from_date.month),('date_invoice.month','<=',docs.to_date.month)])
        if docs.state_account == 'year':
            invoices        = self.env['account.invoice'].search(['&',('date_invoice.year','>=',docs.from_date.year),('date_invoice.year','<=',docs.to_date.year)])
        if docs.state_account == 'caisse':
            invoices        = self.env['account.invoice'].search([('date_invoice','=',datetime.today() )])


        _logger.warning("####### invoices %s", invoices)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            'product_ids': product_ids,
            #'invoices_today': invoices_today,
            'invoices': invoices,
        }
        _logger.warning("####### render_html ")
        #print "################# docargs ",docargs
        return self.env['report'].render('school_accounting.state_account_report', docargs)

class EmailConfig(models.Model):
    _name   = 'email.config'

    name                = fields.Char(string="Nom du configuration")

    sender_email        = fields.Char(string="Email", required=True)
    sender_password     = fields.Char(string="Mot de passe", required=True)
    subject             = fields.Char(string="Objet", default="Message des parents")
    email_ids           = fields.One2many('email.email', 'email_config_id', string="Emails", required=True)
    
    @api.multi
    @api.onchange('sender_email')
    def  validateEmail(self):
        for email in self:
            if email.sender_email:
                _logger.warning("### email %s",email.sender_email)
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email.sender_email) != None:
                    pass
                else:
                    raise ValidationError("Veuillez entrer une adresse e-mail d'expédition valide")

    @api.one
    @api.constrains('sender_email')
    def email_validation(self):
        for email in self:
            if email.sender_email:
                _logger.warning("### email %s",email.sender_email)
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email.sender_email) != None:
                    pass
                else:
                    raise ValidationError("Veuillez entrer une adresse e-mail d'expédition valide")


class Email(models.Model):
    _name   = 'email.email'

    name = fields.Char(string='Email', required=True)
    email_config_id = fields.Many2one('email.config')

    @api.multi
    @api.onchange('name')
    def  validateEmail(self):
        _logger.warning("#### calling validation")
        for email in self:
            if email.name:
                _logger.warning("### email %s",email.name)
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email.name) != None:
                    pass
                else:
                    raise ValidationError("Veuillez entrer une adresse e-mail valide")

    @api.one
    @api.constrains('name')
    def email_validation(self):
        for email in self:
            if email.name:
                _logger.warning("### email %s",email.name)
                if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email.name) != None:
                    pass
                else:
                    raise ValidationError("Veuillez entrer une adresse e-mail valide")



class PaymentReportWizard(models.TransientModel):
    _name = 'payment.report.wizard'

    # from_date   =   fields.Date('Date début', default=datetime.today(), required=True)
    # to_date     =   fields.Date('Date fin', default=datetime.today(), required=True)
   
  
    product_id    =   fields.Many2one('product.template', string="Frais scolaire", required=True)
    month         = fields.Selection([(1,'Janvier'),
                                      (2,'Février'),
                                      (3,'Mars'),
                                      (4,'Avril'),
                                      (5,'Mai'),
                                      (6,'Juin'),
                                      (7,'Juillet'),
                                      (8,'Août'),
                                      (9,'Septembre'),
                                      (10,'Octobre'),
                                      (11,'Novembre'),
                                      (12,'Décembre'),
                                       ], string="Mois",
                                         required=True)
    
    grade_id       = fields.Many2one('school.grade', ondelete='set null', string='Niveau scolaire',required=True)
    class_id       = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", ondelete="set null", string="Classe" , required=True)
    payment_statee = fields.Selection([('unpaid','Non payé'),('all_paid','payé')],string="État de paiement",required=True)
    student_ids    = fields.Many2many('school.student', string="Étudiants", compute='_compute_students', domain="[('class_id','=',class_id),('payment_state','=',payment_state)]") 
    message             = fields.Text(string="texte à envoyer")
    #date                = fields.Date(string="Date", default=fields.Date.today())
    parent_ids          = fields.Many2many('res.partner')
    
    @api.depends('class_id','payment_statee')
    def _compute_students(self):
        student_list = []
        if self.class_id.id:
            class_id = self.class_id.id
            payment_state = self.payment_statee
            students = self.env['school.student'].search([('class_id','=',class_id)])
            for student in students:
                if student.payment_state == payment_state:
                   student_list.append(student.id)
        _logger.warning("STUDENT %s",student_list)
        self.update({'student_ids':[(6,False,[y for y in student_list])]})
        
        print "#################",student_list

    #def _build_contexts(self, data):
       # result = {}
        #result['grade_id'] = 'grade_id' in data['form'] and data['form']['grade_id'] or False
       # result['class_id'] = 'class_id' in data['form'] and data['form']['class_id'] or False
        #result['student_ids'] = 'student_ids' in data['form'] and data['form']['student_ids'] or False
       # result['payment_state'] = 'payment_state' in data['form'] and data['form']['payment_state'] or False
       # result['product_id'] = 'product_id' in data['form'] and data['form']['product_id'] or False
       # result['month'] = 'month' in data['form'] and data['form']['month'] or False
       # return result    
   

   
    @api.multi
    def check_report(self):
        data = {}
        data['form'] = self.read(['student_ids', 'product_id', 'month','class_id','grade_id','payment_statee'])[0]
        # print "######### MONTH ",data['form']['month']
        # print "######### PRODUCT ID ",data['form']['product_id']
        return self._print_report(data)

    def _print_report(self, data):
        data['form'].update(self.read(['student_ids', 'product_id', 'month','class_id','grade_id','payment_statee'])[0])
        #print "####### UPDATED DATA ",data
        return self.env['report'].get_action(self, 'school_accounting.payment_fees_report', data=data)

    @api.multi
    def check_report_reminder(self):
        data = {}
        data['form'] = self.read(['student_ids', 'product_id', 'month','class_id','message'])[0]
        # print "######### MONTH ",data['form']['month']
        # print "######### PRODUCT ID ",data['form']['product_id']
        return self._print_report_reminder(data)

    def _print_report_reminder(self, data):
        data['form'].update(self.read(['student_ids', 'product_id', 'month','class_id','message'])[0])
        #print "####### UPDATED DATA ",data
        return self.env['report'].get_action(self, 'school_accounting.payment_fees_report_reminder', data=data)


    
    @api.multi
    def action_send_sms_reminder(self):
        sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
        if not sender:
           raise Warning('Vous devez valider une configuration sms pour envoyer des sms')
        number_list = []
       
        for student in self.student_ids:
            for parent in student.parent_ids:
                phone = parent.mobile if parent.mobile else parent.phone
                if phone:
                   number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                   _logger.warning("### NUMBER %s",number)
                   number_list.append(number)
        
        _logger.warning("NUMBERS %s",number_list)
        response = sender.send_lot_sms(self.message, number_list)
        if response['success'] == True:
           self.env.cr.commit()
           raise Warning('Messages envoyés avec succès')
        else:
           raise Warning('Messages non envoyés')


    
    def action_send_email_reminder(self):
        
        _logger.warning("#### sending email")

        emailparent=[]
        sender = 'contact@ulysse.ens.tn'
        email_config  = self.env['email.config'].search([],limit=1)
        for student in self.student_ids:
            for parent in student.parent_ids:
                receiver = parent.email 
                emailparent.append(receiver)
      
        From = email_config.sender_email
        Subj = email_config.subject
        receivers=emailparent
        print "receivers#####",receivers
      
        months=self.month
        service=self.product_id.name
        mes = self.message
        for receiver in receivers:
            for child in self.student_ids:
                if receiver in child.parent_ids.email:
                    childrens = ["\t"+child.name+" "+child.last_name+": "+ child.class_id.name if child.class_id.name else "Non défini"]
                    childrens = ', '.join(set(childrens))
                    Text = "\n Parent : %s  \n Enfant(s): \n %s"%(child.parent_ids.name, childrens)

            Body = string.join((
                "From: %s" % From,
                "Subject: %s" % Subj,
                "",
                Text,
                "",
                mes,
                "Service: %s"%service,
                "Mois: %s" %months,
                ), "\r\n")
            try:
              smtpObj = smtplib.SMTP(host='smtp.gmail.com', port=587)
              smtpObj.ehlo()
              smtpObj.starttls()
              smtpObj.ehlo()
              smtpObj.login(user=email_config.sender_email, password=email_config.sender_password)
              smtpObj.sendmail(sender, receiver, Body) 
              smtpObj.close()        
              _logger.warning("Successfully sent email")
            except:
              _logger.warning("#### ERRROR while sending email")
       
       #report reminder paiement
class CheckReport_reminder(models.AbstractModel):
    _name = 'report.school_accounting.payment_fees_report_reminder'

    @api.model
    def render_html(self, docids, data=None):
        self.model      = self.env.context.get('active_model')
        docs            = self.env[self.model].browse(self.env.context.get('active_id'))
        payment_list = []
       # state = 'Non payé(e)'
        reminder_paiement_obj =self.env['payment.report.wizard'].search([('message','=',docs.message)])
        reminder_paiement = reminder_paiement_obj.mapped('message')
        reminder_paiement = str(reminder_paiement[0])
        str(reminder_paiement)

        
        etablissement_obj= self.env['school.etablissement'].search([])
        for etablissement in etablissement_obj:
            if etablissement.school_active==True:
                name_etab = etablissement.name
                image_etab= etablissement.image
                email= etablissement.email
                site = etablissement.site_web
                phone= etablissement.phone
                city = etablissement.city
                street= etablissement.street


        for student in docs.student_ids:
            payment_line    = self.env['account.payment.line'].search(['&',('product_id','=',docs.product_id.id),('month','=',docs.month),('class_id','=', docs.class_id.id),('student_id','=', student.id)])
            
            #if len(payment_line):
              # state = 'Payé(e)'
            _logger.warning("####### FOR  %s", student.name)
            #_logger.warning("####### FOR  %s", state)
            _logger.warning("####### FOR  %s", payment_line.account_payment_id.payment_date)
            _logger.warning("####### FOR  %s", student.class_id.code)
            payment_list.append({'student_id':student.name,'student_idd':student.last_name,'parent':student.parent_ids.name, 'payment_date': payment_line.account_payment_id.payment_date, 'class': student.class_id.code,'payment_state':student.payment_state})
            _logger.warning("####### FOR  %s", payment_list)
        docargs = {
            'image'   : image_etab,
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            #'class_id':class_id,
            #'grade_id':grade_id,
           # 'payment_state':payment_state,
            'payments': payment_list,
            'message' : reminder_paiement,
            'nameetab': name_etab,
            'email'   : email,
            'site'    : site, 
            'phone'   : phone,
            'street'  : street,
            'city'    : city,
        }

        # print "################# docargs ",docargs
        return self.env['report'].render('school_accounting.payment_fees_report_reminder', docargs)



class CheckReport(models.AbstractModel):
    _name = 'report.school_accounting.payment_fees_report'

    @api.model
    def render_html(self, docids, data=None):
        self.model      = self.env.context.get('active_model')
        docs            = self.env[self.model].browse(self.env.context.get('active_id'))
        payment_list = []
       # state = 'Non payé(e)'
        for student in docs.student_ids:
            payment_line    = self.env['account.payment.line'].search(['&',('product_id','=',docs.product_id.id),('month','=',docs.month),('class_id','=', docs.class_id.id),('student_id','=', student.id)])
            #if len(payment_line):
              # state = 'Payé(e)'
            _logger.warning("####### FOR  %s", student.name)
            #_logger.warning("####### FOR  %s", state)
            _logger.warning("####### FOR  %s", payment_line.account_payment_id.payment_date)
            _logger.warning("####### FOR  %s", student.class_id.code)
            payment_list.append({'student_id':student.name,'payment_date': payment_line.account_payment_id.payment_date, 'class': student.class_id.code,'payment_state':student.payment_state})
            _logger.warning("####### FOR  %s", payment_list)
        docargs = {
            'doc_ids': self.ids,
            'doc_model': self.model,
            'docs': docs,
            'time': time,
            #'class_id':class_id,
            #'grade_id':grade_id,
           # 'payment_state':payment_state,
            'payments': payment_list,

        }

        # print "################# docargs ",docargs
        return self.env['report'].render('school_accounting.payment_fees_report', docargs)

