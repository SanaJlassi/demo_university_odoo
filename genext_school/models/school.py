 # -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError , Warning
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json
#import simplejson
import werkzeug
import requests
import urllib2
import urllib
import random
import string
from odoo import api
from random import randint
_logger = logging.getLogger(__name__)


class Evaluation(models.Model):
    _name   = 'school.evaluation'


    name                        = fields.Char(string="Titre de l'évaluation")
    start_date                  = fields.Date(string="Date début")
    end_date                    = fields.Date(string="Date fin")
    date                        = fields.Date(string="Date", default=fields.Date.today(), required=True)
    class_id                    = fields.Many2one('school.class', string="Classe")
    student_id                  = fields.Many2one('school.student', domain="[('class_id','=',class_id)]" , string="Étudiants" , required=True)
    file                        = fields.Binary(string="Fichier", attachment=True, required=True)
    file_name                   = fields.Char(string="File name")
    state                       = fields.Selection([('draft','Brouillon'), ('confirmed','Confirmé')], default='draft')


    @api.multi
    def set_to_confirmed(self):
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            ''' foreach sanction in [self --> recordset of sanction, can contain one record or a set of records]'''
            for rec in self:
                _logger.warning('##### FOR REC IN SELF')
                tokens = []
                for parent in rec.student_id.parent_ids:
                    for token in parent.token_ids:
                        _logger.warning('##### FOR TOKEN %s',token.token)
                        # tokens.append(token.token)
                        content = {'en':rec.name, 'fr':rec.name}
                        data = {'module':'NotesPage','kid':{'id':rec.student_id.id,'name':rec.student_id.name,'last_name':rec.student_id.last_name}}
                        ''' sending push notification for each sanction '''
                        _logger.warning('##### Sending push notification %s',content)
                        notif.sendPushNotification(data=data ,content=content, tokens=[token.token])
        _logger.warning('##### Sending push notification OUT of IF')
        return self.write({'state':'confirmed'})
        

    @api.multi
    def set_to_draft(self):
        # print "#### set_to_draft"
        return self.write({'state':'draft'})
        

    @api.model
    def create(self, vals):
    #     print "FILE ",vals.get('file',False)
    #     print "FILE ",vals.get('file_name',False)
        return super(Evaluation,self).create(vals)

    @api.multi
    def write(self, vals):
        # print "### VALS ",vals
        return super(Evaluation, self).write(vals)

class EvaluationMark(models.Model):
    _name='school.evaluation.mark'

    academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)
    grade_id                        = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]",required=True)
    class_id                        = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)
    skill_id                        = fields.Many2one('school.skill', string="Compétence",required=True)
    teacher_id                      = fields.Many2one('hr.employee', domain="['&',('is_school_teacher','=',True),('skill_ids','=',skill_id)]", string="Enseignant", required=True)
    date                            = fields.Date(string='Date', default=fields.Date.context_today, required=True)
    evaluation_mark_line_ids        = fields.One2many('school.evaluation.mark.line', 'mark_line_id', ondelete='cascade')
    timing_system_id                = fields.Many2one(related='class_id.grade_id.educational_stage_id.timing_system_id',string='système périodique')
    timing_periode_ids              = fields.Many2one('school.timing.system.period', domain="[('timing_system_id','=',timing_system_id)]",string='période')
    
    @api.onchange('class_id')
    def _onchange_class_id(self):
        student_line= []
        if len(self.class_id):
            students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in students:
                student_line.append( (0,0,{'name':'name','student_id':student.id, 'mark':None}) )
        else:
            _logger.warning("###### NO CLASS ID"  )  
        return {'value':{'evaluation_mark_line_ids':student_line}}

class EvaluationMarkLine(models.Model):
    _name='school.evaluation.mark.line'

    student_id                  = fields.Many2one('school.student', string="Étudiants")
    mark                        = fields.Float(string='Mark', default=0)
    mark_line_id                = fields.Many2one('school.evaluation.mark', string="Evaluation")


class SchoolTasks(models.Model):
    _name = 'school.task'
    _description = 'A task can be related to a student or all student in a class'
    

    name                                    = fields.Char(string="Titre de travail à faire", required=True)
    academic_year_id                        = fields.Many2one('academic.year', ondelete='set null', string="Année scolaire", default=lambda self: self.env['academic.year'].search([('active_year','=',True)]))
    
    grade_id                                = fields.Many2one('school.grade', ondelete='set null', string='Niveau scolaire', required=True)
    class_id                                = fields.Many2one('school.class', domain="[('grade_id','=',grade_id)]", ondelete="set null", string="Classe", required=True)
    subject_id                              = fields.Many2one('school.subject', ondelete='set null', string='Matiére', required=True)
    teacher_id                              = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", ondelete='set null', string="Enseignant", required=True) #domain="['&',('is_school_teacher','=',True),('id','in',subject_id.teacher_ids.ids)]"

    educational_stage_id                    = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
    timing_system_id                        = fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
    timing_system_periode_id                = fields.Many2one('school.timing.system.period', domain="[('timing_system_id','=',timing_system_id)]", ondelete='set null', string="Période") # add default value current periode

    creation_date                           = fields.Datetime(string="Date de création", default=fields.Datetime.now())
    dead_line                               = fields.Date(string='Date limite pour le devoir')
    description                             = fields.Text(string="Détails de travail à faire")
    

    all_student                             = fields.Boolean(string="Tous les Étudiants", default=False)
    # student_ids                             = fields.Many2many('school.student',compute="_compute_students", domain="[('class_id','=',class_id)]", string="Élèves")  # domain [(6, 0, ids)] ==> allowed_type_ids[0][2]
    student_ids                             = fields.Many2many('school.student', domain="[('class_id','=',class_id)]", string="Étudiants")  # domain [(6, 0, ids)] ==> allowed_type_ids[0][2]
    file_ids                                = fields.One2many('task.file', 'task_id', string='Fichiers')
    state                                   = fields.Selection([('unapproved','Non approuvé'),
                                                                 ('approved','Approuvé'),
                                                                 ('ignored','Ignoré'),
                                                                 ], default='unapproved', string="état")

    type_task                               = fields.Selection([('task','Travail à faire'), ('sanction','Sanction'),], string="Type", required=True)
    
    @api.one
    def set_unapproved(self):
        self.write({'state':'unapproved'})
        

    @api.one
    def set_approved(self):  
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            ''' foreach sanction in [self --> recordset of sanction, can contain one record or a set of records]'''
            for rec in self:
                for student in rec.student_ids:
                    tokens = []
                    for parent in student.parent_ids:
                        for token in parent.token_ids:
                            tokens.append(token.token)
                            task = {
                                'id':rec.id,
                                'name':rec.name,
                                'creation_date':rec.create_date,
                                'deadline':rec.dead_line,
                                'description':rec.description,
                                'all_student':rec.all_student,
                                'grade':{
                                    'id':rec.grade_id.id,
                                    'name':rec.grade_id.name,
                                },
                                'class':{
                                    'id':rec.class_id.id,
                                    'name':rec.class_id.name,
                                },
                                'subject':{
                                    'id':rec.subject_id.id,
                                    'name':rec.subject_id.name,
                                },
                                'teacher':{
                                    'id':rec.teacher_id.id,
                                    'name':rec.teacher_id.name,
                                },
                                'periode':{
                                    'id':rec.timing_system_periode_id.id,
                                    'name':rec.timing_system_periode_id.name,
                                },
                            }
                            all_class = ' and all his colleague' if rec.all_student else ''
                            deadline =  (' for %s'%rec.dead_line) if rec.dead_line else ''
                            messageEN = '%s has a %s homework %s'%(student.name+all_class, rec.subject_id.name, deadline)
                            messageFR = ''
                            data = {'module':'MissionsPage','data':task, 'kid':{'id':student.id,'name':student.name,'last_name':student.last_name}}
                            content = {'en':messageEN, 'fr':messageEN}
                            ''' sending push notification for each sanction'''
                            notif.sendPushNotification(data=data ,content=content, tokens=[token.token])   
        self.write({'state':'approved'})


    @api.one
    def set_ignore(self):
        self.write({'state':'ignored'})


    @api.onchange('class_id')
    def _onchange_subjects(self):
        student_list = []
        students = self.env['school.student']
        if self.all_student:
            students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in students:
                student_list.append(student.id)
        _logger.warning("STUDENT %s",student_list)
        return {'domain':{'subject_id':[('id','in',self.class_id.grade_id.subject_ids.ids)]}}
   


    @api.depends('class_id')
    @api.one
    def _compute_students(self):
        student_list = []
        students = self.env['school.student']
        if self.all_student:
            students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in students:
                student_list.append(student.id)
        _logger.warning("STUDENT %s",student_list)
        self.update({'student_ids':[(6,False,[y for y in student_list])]})

    @api.model
    def create(self, vals):
        """------------------------------------------------------------------------------------------------- NEEDS A FIX--------------------------------------------------------------------------------------"""
        # #print vals
        # if 'source' in self.env.context:
        #     print "CONTEXT ",self.env.context
        #     if vals.get('student_ids',False):
        #         ids = []
        #         for rec in vals['student_ids']:
        #             ids.append(rec[1])
        #         vals['student_ids'] = [(6,False,ids)]
        #         print "## LIST ",vals['student_ids']
        # else:
        #     print "NO CONTEXT ",self.env.context
        return super(SchoolTasks, self).create(vals)
        

    @api.multi
    def write(self, vals):
        # print "## VALS ", vals
        
        return super(SchoolTasks, self).write(vals)


    @api.onchange('all_student')
    def _onchange_all_student(self):
        student_list = []
        if self.all_student and self.class_id:
            students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in students:
                student_list.append(student.id)

        _logger.warning("STUDENT ALL STUDENT %s",student_list)
        return {
                    'domain':{'subject_id':[('id','in',self.class_id.grade_id.subject_ids.ids)]},
                    'value': {'student_ids':[(6,False,[y for y in student_list])]}
                }                     

    @api.onchange('subject_id')
    def _onchange_subject(self):
        subject = self.env['school.subject'].search([('id','=',self.subject_id.id)])
        return {'domain':{'teacher_id':['&',('is_school_teacher','=',True),('id','in',subject.teacher_ids.ids)]}}

    @api.onchange('class_id','timing_system_id')
    def _default_timing_periode(self):
        # check if there's a selected timing system
        if len(self.timing_system_id):
            for rec in self.env['school.timing.system.period'].search([('timing_system_id','=',self.timing_system_id.id)]):
                # reconstruct date with the current year
                start_period = str(datetime.now().year)+'-'+rec.start_date_month
                end_period = str(datetime.now().year)+'-'+rec.end_date_month
                # convert date sting to date object
                start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
                end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
                # check in which period we are right now
                if start_period <= datetime.now().date() <= end_period:
                    self.timing_system_periode_id = rec
                    break





class SchoolEvent(models.Model):
    _name = "school.event"


    name        = fields.Char(required=True)
    start_date  = fields.Date(required=True)
    end_date    = fields.Date(required=True)
    event_type  = fields.Selection([('Jour Fériée','Jour Fériée'),('Evénement Scolaire','Evénement Scolaire'),('Vacances','Vacances')],default="Evénement Scolaire")
    color       = fields.Integer(string='calendar Event color', compute='_compute_color_value')

    note        = fields.Char(compute='_get_event_note')

    @api.multi
    def _get_event_note(self):
        for rec in self:
            event_name = rec.name
            event_name = str(event_name)
            typee = rec.event_type
            typee = str(typee)   
            rec.note = event_name + ' : ' + typee

    @api.depends('event_type')
    @api.multi
    def _compute_color_value(self):
        for rec in self:
            if rec.event_type == 'Jour Fériée':
                rec.color = 0
            else:
                if rec.event_type == 'Evénement Scolaire':
                    rec.color = 1
                else:
                    rec.color = 2



class AcademicCalendar(models.Model):
    _name = "academic.calendar"

    academic_year     = fields.Many2many('academic.year')
    start_date        = fields.Date(compute='get_date')
    end_date          = fields.Date(compute='get_date')


    @api.depends('academic_year')
    @api.multi
    def get_date(self):
        for rec in self:
            if rec.academic_year:
                academic_year_id = rec.academic_year
                academic_year = academic_year_id[0]
                rec.start_date = academic_year.start_date
                rec.end_date   = academic_year.end_date

    event_ids    = fields.Many2many('school.event')

    def action_validate_events(self):
        if self.event_ids:
            event_list = self.event_ids
            valider = True
            for event in event_list:
                if ((event.start_date < self.start_date) or (event.end_date > self.end_date)):
                    valider = False
            if valider == True:
                raise Warning('Calendrier valideé')
            else:
                raise Warning('Calendrier Invalide : les dates des Evénements ne sont pas compatibles avec l ''Année Scolaire')








class TaskFile(models.Model):
    _name = 'task.file'

    name                                    = fields.Char(string="Nom")
    description                             = fields.Char(string="Description")
    file                                    = fields.Binary(string='Pièce joint', attachment=True)
    file_name                               = fields.Char(string="Nom du fichier")
    content_type                            = fields.Char(string="Content Type Base64")
    task_id                                 = fields.Many2one('school.task', string="Devoir")



class SchoolModule(models.Model):
    _name = 'school.module'

    name                = fields.Char(string='Nom du module')
    academic_year_id    = fields.Many2one('academic.year', ondelete='restrict', string="Année scolaire", default=lambda self: self.env['academic.year'].search([('active_year','=',True)]))
    grade_ids           = fields.Many2many('school.grade', 'school_module_school_grade_rel', 'module_id', 'grade_id')
    subject_ids         = fields.Many2many('school.subject', 'school_module_school_subject_rel', 'module_id', 'subject_id')
    coefficient         = fields.Float(string="Coefficient", default=0.0)




class AcademicYear(models.Model):

    _name = 'academic.year'
    _description = 'The academic year'
    _order = "sequence"

    name = fields.Char('Name', required=True, help="Insert a name for the academic year Exp: ('2000/2001')")
    code = fields.Char('Code', required=True, help="Insert code for the academic year Exp: (01)")
    start_date = fields.Date('Start Date', required=True, help="The start date of the academic year")
    end_date = fields.Date('End Date', required=True, help="The end date of the academic year")
    description = fields.Text('Description', help="Description of the academic year")

    setup = fields.Char('Vacation')
    month_ids = fields.One2many('academic.month', 'academic_year')

    active_year = fields.Boolean('Active Year', default=False)
    sequence            = fields.Char("sequence number")


    @api.multi
    def name_get(self):
        """
        @override : methode to change the display_name 
        """
        return [(record.id, ' [' + record.code + ']' + record.name)for record in self]

    @api.constrains('start_date', 'end_date')
    def _check_academic_year(self):
        """
            methode to check for valide year start and end [start < end]
            and verify if current year is overlapping with anothers
        """
        _logger.warning('content of self.ids : %s',self.ids)
        if(self.start_date and self.end_date and self.start_date > self.end_date):
            raise UserError("The End date of the academic year must be greater than Start date")
        # comparing the given [start,end] dates with other academic years and check if there is any conflicts 
        for old_academic_year in self.search([('id', 'not in', self.ids)]):
            if(old_academic_year.start_date <= self.start_date <= old_academic_year.end_date):
                raise UserError('Academic [start date] is overlapping with the year [%s] - [%s]'%(old_academic_year.start_date,old_academic_year.end_date))
            if(old_academic_year.start_date <= self.end_date <= old_academic_year.end_date):
                raise UserError('Academic [end date] is overlapping with the year [%s] - [%s]'%(old_academic_year.start_date,old_academic_year.end_date))
        return True

    @api.model
    def create(self,vals):
        recordset = self.env['academic.year'].search([])
        if vals.get('active_year',False):
            if vals['active_year'] == True:
                for record in recordset:
                    record.write({'active_year':False})
        vals['sequence'] = self.env['ir.sequence'].next_by_code('academic.year')
        return super(AcademicYear,self).create(vals)



    @api.multi
    def write(self,vals):
        self.ensure_one()
        res = super(AcademicYear,self).write(vals)
        recordset = self.env['academic.year'].search([])
        if vals.get('active_year',False):
            if self.active_year == True:
                for record in recordset:
                    # update all records except current one
                    if record.id != self.id:
                        record.write({'active_year':False})
        return res


class Month(models.Model):
    _name='academic.month'
    _description='Academic month related to academic year'

    name = fields.Char("Month's full name", required=True, help="The full name of the academic month")
    code = fields.Char("Month's code", required=True, help="Short name of the academic month")
    start_day = fields.Integer('Start of month', required=True, help="The start of the academic month")
    end_day = fields.Integer('End of month', required=True, help="The end of the academic month")
    description = fields.Text('Description')
    academic_year = fields.Many2one('academic.year')


class User(models.Model):
    ''' adding user type [Teacher, Parent, Student]'''
    _name = 'res.users'
    _inherit = 'res.users'

    user_type = fields.Selection([
                                    ('student','Élève'),
                                    ('parent','Parent'),
                                    ('teacher','Enseignant'),
                                    ], default='student'
                                )

class SchoolContact(models.Model):
    _name  = 'school.contact'


    user_id                 = fields.Many2one('res.users')
    contact_user_id         = fields.Many2one('res.users')
    user_type               = fields.Selection(related='user_id.user_type')



class Token(models.Model):
    _name = 'user.token'


    token                = fields.Text(string='Token', required=True)
    datetime             = fields.Datetime(string="Creation datetime")
    partner_id           = fields.Many2one('res.partner','partner')

    # _sql_constraints = [('field_unique', 'unique(token)','Choose another value - it has to be unique!')]


def rendomDigits():
    N = 4

    random_string = ''.join(random.choice(string.ascii_uppercase) for alpha in range(N))
    random_digists = ''.join(["%s" % randint(0, 9) for num in range(0, N)])
    return random_string+'-'+random_digists




class Parent(models.Model):
    '''Defining an address information '''
    _name = 'res.partner'
    _inherit = 'res.partner'
    _description = 'Address Information'
    _order = "pid asc"

    last_name           = fields.Char("Prénom")
    pid                 = fields.Char('Identifiant', help="Numéro d'identification personnelle")
    is_school_parent    = fields.Boolean('Is A Parent')
    is_school_student   = fields.Boolean('Is A Student')
    is_school_teacher   = fields.Boolean('Is A Teacher')
    student_ids         = fields.Many2many('school.student', 'res_partner_school_student_rel', 'res_partner_id', 'school_student_id')
    cin                 = fields.Integer(string="CIN")
    job                 = fields.Char(string="Profession")
    contact_ids         = fields.Many2many('hr.employee', compute='_compute_contact_ids')
    gender              = fields.Selection([('Masculin', 'Masculin'),('Feminin', 'Feminin'),('other', 'Autre') ], string='Genre')

    class_id            = fields.Many2one('school.class', compute='_compute_class_id', store=True)
    token               = fields.Text(string="Token")
    password            = fields.Char(string="Mot de passe" ,compute='_get_password')
    token_ids           = fields.One2many('user.token','partner_id',string="Tokens")


    allow_platform_login = fields.Boolean(default=False, string='Allow external login')
    user_id              = fields.Many2one('res.users',readonly=True,  auto_join=True)
    random_key           = fields.Char(related='user_ids.random_key')
    
    
    @api.multi
    def _compute_class_id(self):
        _logger.warning("#### TRIGGGER ")
        for record in self:
            if record.is_school_student:
                student_id = self.env['school.student'].search([('partner_id','=',record.id)])
                _logger.warning("#### STUDENT NAME %s",student_id.name)
                if student_id:
                    _logger.warning("#### STUDENT CLASS %s",student_id.class_id.name)
                    record.class_id = student_id.class_id
            elif record.is_school_teacher:
                teacher_id = self.env['hr.employee'].search([('partner_id','=',record.id)])
                _logger.warning("#### TEACHER NAME %s",teacher_id.name)
                if teacher_id:
                    for classe in teacher_id.class_ids:
                        record.class_id = classe
                        break
            elif record.is_school_parent:
                class_set = self.env['school.class']
                for student in record.student_ids:
                    _logger.warning("#### CHILD NAME %s",student.name)
                    # class_set = class_set | student.class_id
                    record.class_id = student.class_id
                    break


    @api.multi
    def _compute_contact_ids(self):
        for record in self:
            teacher_list = []
            for student in record.student_ids:
                teacher_ids = self._get_class_teachers(student.class_id)
                teacher_list.extend(teacher_ids)
            record.contact_ids = self.env['hr.employee'].browse(list(set(teacher_list)))

    @api.multi
    def write(self,vals):
       
        for rec in self:
            if rec.is_school_parent:
                if vals.get('allow_platform_login', False):
                   rec.user_ids.write({'allow_platform_login': vals.get('allow_platform_login')})
        return super(Parent,self).write(vals)
    @api.multi
    def name_get(self):
        return [(record.id, record.name+' '+record.last_name if record.last_name else record.name)for record in self]

    def _get_password(self):
        for rec in self:
            rec.password = rec.pid      

    @api.model
    def create(self, vals):
        '''Method creates parents assign group parents'''
        vals['pid'] = self.env['ir.sequence'].next_by_code('res.partner')
        #if vals.get('allow_platform_login', False):
                #vals['random_key'] = rendomDigits()
        res = super(Parent, self).create(vals)
        # Create user
        user = self.env['res.users']
        if res and res.is_school_parent:

            user_vals = {'name': vals.get('name'),
                         'login': vals.get('pid', False),
                         'password': vals.get('pid', False),
                         'partner_id': res.id,
                         'user_type':'parent',
                         'allow_platform_login':vals.get('allow_platform_login', False)
                         }
            user = self.env['res.users'].create(user_vals)
            
            # Assign group of parents to user created
            emp_grp = self.env.ref('base.group_user')
            parent_group = self.env.ref('genext_school.group_school_parent')
            if user:
                user.write({'groups_id': [(6, 0, [emp_grp.id, parent_group.id])]})

        # add student teachers as contacts
        # if res and len(res.student_ids):
        #     teachers_ids = []
        #     for student in res.student_ids:
        #         print "class Name ",student.class_id.name
        #         print "class Code ",student.class_id.code
        #         teachers_ids.extend(self._get_class_teachers(student.class_id))
        #     teachers_ids = list(set(teachers_ids))
        #     teachers = self.env['hr.employee'].browse(teachers_ids)
        #     contact_ids = []
        #     for teacher in teachers:
        #         print "hr employee ",teacher.user_id.name
        #         contact_ids.append(self.env['school.contact'].create({'user_id':user.id,'contact_user_id':teacher.user_id}).id  )       
        #         res.write({'contact_ids':[(6, 0, contact_ids)]})


            #     for subject in student.grade_id.subject_ids:
            #         teacher = self.env['hr.employee'].search([('is_school_teacher','=',True),('id','in',subject.teacher_ids.ids)])
            #         teachers.append(teacher.user_id.id)
            #         print "parent res.users id ",user.id
            #         print "parent res.users name ",user.name
            #         print "teacher res.users id ",teacher.user_id.id
            #         print "teacher res.users name ",teacher.user_id.name
            # teachers = list(set(teachers))
            # contacts = []
            # for id in teachers:

            #     contact = self.env['school.contact'].create({'user_id':user.id,'contact_user_id':id})
            #     print "created contact ",contact.id
            #     contacts.append(contact.id)
            # print "AA ",contacts

        return res


    def _get_class_teachers(self, class_id):
        ''' 
            return the ids of teachers who's teaching the given class in current timing periode 
        '''
        teachers = []
        active_academic_year = self.env['academic.year'].search([('active_year','=',True)])
        recs = self.env['time.table'].search([('class_id','=',class_id.id),('academic_year','=',active_academic_year.id)])
        for rec in recs:
            # print "## name of time table ",rec.name
            # print "## name timing_system ",rec.timing_system.name
            # print "## name timing_periode ",rec.timing_periode.name
            timing_period_id = rec.timing_periode

            start_period = str(datetime.now().year)+'-'+timing_period_id.start_date_month
            end_period = str(datetime.now().year)+'-'+timing_period_id.end_date_month
            # convert date string to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                _logger.warning(" FOUND CURRENT PERIODE %s",timing_period_id.name)
                for session in rec.time_table_session_ids:
                    _logger.warning("TEACHER NAME %s",session.teacher_id.name)
                    teachers.append(session.teacher_id.id)
        # remove duplication and return teachers ids
        return list(set(teachers))

 
    @api.multi
    def unlink(self):
        ''' deleting the user related to  res.partner which are school parent'''
        for rec in self:
            if rec.is_school_parent:
                rec.user_ids.unlink()
        return super(Parent, self).unlink()



class StudentAcademicYear(models.Model):
    _name='student.academic.year'
    _description="Object represent All data related to a student in specific adacemic year"
    _order = "sequence"

    name                = fields.Char("Name")
    # Many StudentAcademicYear related to one Student
    student_id          = fields.Many2one('school.student', required=True, delegate=True, ondelete="cascade")

    educational_stage_id  = fields.Many2one('educational.stage',required=True, string="Educational Stage")
    # Many StudentAcademicYear related two one AcademicYear
    academic_year_id    = fields.Many2one('academic.year', required=True, delegate=True, default=lambda self: self.env['academic.year'].search([('active_year','=',True)]))

    orientation_system  = fields.Selection(related='educational_stage_id.orientation_system')

    # Field is not required in case grade is common for all student as in elementary school
    field_id            = fields.Many2one('school.field')

    grade_id            = fields.Many2one('school.grade', required=True, delegate=True)

    class_id            = fields.Many2one('school.class', required=True, delegate=True)

    registration_state               = fields.Selection([
                                        ('draft','Brouillon'),
                                        ('confirmed','Confirmé'),
                                        ('done','Terminé'),
                                        ], default='draft')

    decision            = fields.Selection([
                                        ('in-progress','En Cours'),
                                        ('succeeded', 'Réussi'),
                                        ('refused','Refusé'),
                                        ('retake-session','Session de contrôle')
                                        ], default='in-progress')
    # used to get the last student academic year for a given student
    sequence            = fields.Char("sequence number")

    @api.model
    def create(self, vals):
        if vals.get('student_id',False):
            records = self.env['student.academic.year'].search([('student_id', '=', vals['student_id'])])
            for record in records:
                if record.decision == 'in-progress':
                    raise ValidationError(_('Action cannot be performed. Students current year still in progress!'))

        vals['sequence'] = self.env['ir.sequence'].next_by_code('student.academic.year')
        res =  super(StudentAcademicYear, self).create(vals)
        _logger.warning("CONTENT OF RES %s",res)
        _logger.warning("CONTENT OF RES ID %s",res.id)

        student = self.env['school.student'].search([('id','=',vals['student_id'])])
        student.write({'current_student_academic_year':res.id, 'state':'pre-registred'})
        return res

    @api.multi
    def write(self,vals):
        res = super(StudentAcademicYear, self).write(vals)
        for rec in self:
            if vals.get('class_id',False):
                rec.student_id._compute_class_id()
                _logger.warning('###### UPDATING STUDENT_ACADEMIC_YEAR %s',vals)
        return res

    @api.multi
    def unlink(self):
        for record in self:
            _logger.warning("DELETING student academic year")
            if record.student_id.current_student_academic_year.id == record.id:
                _logger.warning("Changing student state to condidate")
                record.student_id.write({'state':'condidate'})
        return super(StudentAcademicYear, self).unlink()
        
    @api.multi
    def open_update_registration_wizard(self):
        # check if student has completed the current year or not using the decision attribute of student academic year
        if self.current_student_academic_year.id:
            if self.current_student_academic_year.registration_state != 'draft':
                raise ValidationError(_('Action cannot be performed. Students current year is confirmed!'))

        return {
            'type': 'ir.actions.act_window',
            'name': 'Name ',
            'view_mode': 'form', 
            'target': 'new',
            'res_model': 'student.registration',
            'context': {'student_academic_year_id': self.id, 'student_id':self.student_id.id, 'update':True} 
        }



class ClassAcademicYear(models.Model):
    _name='class.academic.year'

    educational_stage_id  = fields.Many2one('educational.stage',required=True, string="Educational Stage")
    # Many StudentAcademicYear related two one AcademicYear
    academic_year_id    = fields.Many2one('academic.year', required=True, delegate=True, default=lambda self: self.env['academic.year'].search([('active_year','=',True)]))

    orientation_system  = fields.Selection(related='educational_stage_id.orientation_system')

    # Field is not required in case grade is common for all student as in elementary school
    #field_id            = fields.Many2one('school.field')

    old_grade_id            = fields.Many2one('school.grade', required=True, delegate=True)

    old_class_id            = fields.Many2one('school.class', required=True, delegate=True)

    new_educational_stage_id  = fields.Many2one('educational.stage',required=True, string="Educational Stage")
    # Many StudentAcademicYear related two one AcademicYear
    new_academic_year_id    = fields.Many2one('academic.year', required=True, delegate=True, default=lambda self: self.env['academic.year'].search([('active_year','=',True)]))

    new_grade_id            = fields.Many2one('school.grade', required=True, delegate=True)

    new_class_id            = fields.Many2one('school.class', required=True, delegate=True)

    field_id                = fields.Many2one('school.field', domain="[('educational_stage_id','=',educational_stage_id)]", ondelete="set null")

    student_ids             = fields.One2many(related='old_class_id.student_ids',domain="[('academic_year_id','=',academic_year_id),('educational_stage_id','=',educational_stage_id),('grade_id','=',old_grade_id),('class_id','=',old_class_id)]")

    student_academic_year_ids  = fields.Many2many('student.academic.year',domain="[('academic_year_id','=',academic_year_id),('educational_stage_id','=',educational_stage_id),('grade_id','=',old_grade_id),('class_id','=',old_class_id)]")


    def change_decision(self):
        if self.student_academic_year_ids:
            for rec in self.student_academic_year_ids:
                rec.decision = 'succeeded'


    def change_niveau(self):
        if self.student_ids:
            for student in self.student_ids:
                in_progress = False
                for year in student.student_academic_year:
                    if year.decision == 'in-progress':
                        in_progress = True
                if in_progress == False:
                    student_academic_year = {
                                        'student_id': student.id,
                                        'academic_year_id':self.new_academic_year_id.id,
                                        'educational_stage_id':self.new_educational_stage_id.id,
                                        'field_id': self.field_id.id,
                                        'grade_id': self.new_grade_id.id, 
                                        'class_id':self.new_class_id.id
                                        }
                    student_academic_year_object = self.env['student.academic.year'].create(student_academic_year)
                    student.write({'student_academic_year': student_academic_year_object,
                               'current_student_academic_year':student_academic_year_object.id, 'state':'pre-registred'})




class Etablissement(models.Model):
    _name = "school.etablissement"
    
    name              = fields.Char(required=True)
    image             = fields.Binary(' ', default=lambda self:self.get_etablissement_image(), store=True)
    @api.model
    def get_etablissement_image(self):
        img_path = ''
        img_path = get_resource_path("genext_school", "static/images", "genext.png")
        with open(img_path, 'rb') as file:
            image = file.read()
        return image.encode('base64')

    user_id         = fields.Many2one('res.users',readonly=True, ondelete='restrict', auto_join=True, string='Related System User', help='User-related data of the school')
    email           = fields.Char('Email')
    phone           = fields.Char('Téléphone')
    site_web        = fields.Char('Site Web')
    country_id      = fields.Many2one(string="Pays",related='user_id.partner_id.country_id', store=True)
    city            = fields.Char("Ville", related='user_id.partner_id.city', store=True)
    zip_code        = fields.Char("Code postal", related='user_id.partner_id.zip', store=True)
    street          = fields.Char("Rue", related='user_id.partner_id.street', store=True)
    school_active          = fields.Boolean("profil active",onchange="test")
    @api.multi
    def test(self):
        obj = self.search([('school_active', '=', True)])
        obj.write({'school_active': False})


class Student(models.Model):
    _name="school.student"
    _description="School Student"
    _inerits={'res.users': 'user_id', 'res.partner':'partner_id'}
    _order = "name"


    @api.depends('user_id')
    @api.multi
    def _compute_res_partner(self):
        for rec in self:
            _logger.warning("### setting partner")
            if rec.user_id and rec.user_id.partner_id:
                _logger.warning("### user partner %s",rec.user_id.partner_id.name)
                rec.partner_id = rec.user_id.partner_id
                rec.partner_id.write({'is_school_student':True, 'name':rec.name,'last_name':rec.last_name,'pid':rec.pid})
                _logger.warning("#### STUDENT PARTNER %s",rec.partner_id.name)
                _logger.warning("#### STUDENT last_name %s",rec.partner_id.last_name)
                _logger.warning("#### STUDENT is_school_student %s",rec.partner_id.is_school_student)
                _logger.warning("#### STUDENT is_school_student %s",rec.partner_id.pid)
                rec.write({'doctor':''})

    partner_id      = fields.Many2one('res.partner', ondelete='restrict', string='Related System partner',store=True, compute="_compute_res_partner")
    user_id         = fields.Many2one('res.users',readonly=True, required=True, ondelete='restrict', auto_join=True, string='Related System User', help='User-related data of the student')
    pid             = fields.Char('Étudiant ID', help='Personal IDentification Number')
    name            = fields.Char("Prénom", related='user_id.name', required=True)
    last_name       = fields.Char("Nom", required=True)
    user_type       = fields.Selection(related='user_id.user_type',store=True)
    email           = fields.Char("Email", related='user_id.email', store=True)
    phone           = fields.Char("Téléphone", related='user_id.partner_id.phone', store=True)
    mobile          = fields.Char("Mobile", related='user_id.partner_id.mobile', store=True)
    image           = fields.Binary("Photos", related="user_id.partner_id.image", default=lambda self:self._get_default_image(), store=True)
    
    # student --> res_user--> res_partner fields 
    
    state_id        = fields.Many2one(related='user_id.partner_id.state_id', string="State", store=True)
    street_1        = fields.Char("Rue", related='user_id.partner_id.street', store=True )   
    city            = fields.Char("Ville", related='user_id.partner_id.city', store=True)
    zip_code        = fields.Char("Code postal", related='user_id.partner_id.zip', store=True)
    country_id      = fields.Many2one(related='user_id.partner_id.country_id', string="Pays", store=True)
    street_2        = fields.Char("Street 2", related='user_id.partner_id.street2', store=True)
    country_id1     = fields.Many2one(related='user_id.partner_id.country_id', string="Nationalité 1 ", store=True)
    country_id2     = fields.Many2one(related='user_id.partner_id.country_id', string="Nationalité 2 ", store=True)

    birth_date      = fields.Date("Date de naissance", required=False, help="Student birth date")
    birthday_city   = fields.Char('Lieu de naissance')
    age             = fields.Integer(compute='_compute_student_age', string='Age',readonly=True)
    gender          = fields.Selection([('Masculin', 'Masculin'), ('Feminin', 'Feminin')],'Genre', default="Masculin")
    state           = fields.Selection([
                            ('condidate', 'Candidat'),
                            ('pre-registred', 'Pré-inscrit'),
                            ('registred', 'Inscrit'),
                            ('graduated', 'Diplômé'),
                            ('alumni', 'Ancien élève'),
                            ('archive', 'Archivé')],
                            'État', readonly=True, default="condidate")

    # contains all student's history
    student_academic_year = fields.One2many('student.academic.year', 'student_id', string="Historique Scolaire")
    # contains student's current registration year data
    current_student_academic_year = fields.Many2one('student.academic.year')
    educational_stage_id  = fields.Many2one(related='current_student_academic_year.educational_stage_id', string='Niveau éducatif')
    academic_year_id    = fields.Many2one(related='current_student_academic_year.academic_year_id')
    field_id            = fields.Many2one(related='current_student_academic_year.field_id')
    grade_id            = fields.Many2one(related='current_student_academic_year.grade_id', string='Niveau scolaire')
    class_id            = fields.Many2one('school.class', compute="_compute_class_id", store=True, string='Classe')
    parent_ids          = fields.Many2many('res.partner', 'res_partner_school_student_rel', 'school_student_id',  'res_partner_id', string="Parents")
    parents_phone       = fields.Many2many('res.partner', string="Téléphone du parent")
    parents_mail        = fields.Many2many('res.partner', string="Email du parent")
    brothers_ids        = fields.Many2many('school.student', compute="_compute_brothers")
    
    # task_ids            = fields.Many2many('school.task')
    task_ids            = fields.Many2many('school.task', 'school_task_school_student_rel')
    exemption_ids 		= fields.One2many('school.exemption', 'student_id', string="Dispenses")


    doctor              = fields.Char(string="Name of doctor")
    designation         = fields.Char(string="Designation")
    phone               = fields.Char(string="Phone")
    blood_group         = fields.Selection([('a','A'),('b','B'),('ab','AB'),('o','O')], default='a', string="Groupe Sanguin")
    height              = fields.Float(string="Height", default=False)
    weight              = fields.Float(string="Weight", default=False)
    eyes                = fields.Boolean(string="Eyes disability", default=False)
    respiratory         = fields.Boolean(string="Respiratory disability", default=False)
    musculoskeletal     = fields.Boolean(string="Musculoskeletal disorders", default=False)
    ears                = fields.Boolean(string="Ears disorder", default=False)
    cardiovascular      = fields.Boolean(string="Cardiovascular", default=False)
    dermatologiques     = fields.Boolean(string="Dermatologiques", default=False)
    nose_throat         = fields.Boolean(string="Nose & Throat", default=False)
    neurologique        = fields.Boolean(string="Neurologique", default=False)
    blood_pressure      = fields.Boolean(string="Blood pressure", default=False)


    old_school          = fields.Char(string="Name")
    registration_number = fields.Char(string="Registration number")
    admission_date      = fields.Date(string="Admission date")
    release_date        = fields.Date(string="Release date")



    legal_responsable           = fields.Many2one('res.partner', require=True, help="Le responsable légal est celui ", string="Responsable légal")
    """ used to store the domain in json format for web_domain_field plugin"""
    parent_ids_domain           = fields.Char(compute="_compute_parent_ids_domain", readonly=True, store=False,)

    payement_ids                = fields.One2many('account.payment', compute='_compute_payement')
    #account_payment_line_ids    = fields.One2many('account.payment.line', compute='_compute_payement')
    # invoice_ids         = fields.One2many('account.invoice')

    payment_state               = fields.Selection([('unpaid','Non payé'),('all_paid','payé')],compute='_compute_payment_state',string="État de paiement")
    payment_state_group_by      = fields.Selection([('unpaid','Non payé'),('all_paid','payé')], default='unpaid') 

    color                       = fields.Char('Color Index', compute="change_color_on_kanban", store=True) 
    # medical_consultation        = fields.One2many('school.medical', 'student_id', string="Consultation Médical")
    #brothers_ids                = fields.Many2many('school.student', compute="_compute_brothers",  store=True, string="brothers")
    allow_platform_login = fields.Boolean(default=False, string='Allow external login')
    random_key = fields.Char(related='user_id.random_key',string='Code de parrainage',store=True)
    contact_ids         = fields.Many2many('hr.employee', compute='_compute_contact_ids')
    @api.multi
    def _compute_brothers(self):
        self.ensure_one()
        if self.parent_ids:
            parent_ids = self.parent_ids.ids
            brothers   = self.env['school.student'].search(['&',('parent_ids','=', parent_ids),('id' ,'!=', self.id)])
            if brothers:
                self.brothers_ids = brothers


    @api.depends('birth_date')
    @api.multi
    def change_color_on_kanban(self):
        current_date = datetime.today()
        _logger.warning("################################# CURRENT DATE %s",current_date)
        for record in self:
            if record.birth_date:
                birth_day_object = datetime.strptime(record.birth_date,DEFAULT_SERVER_DATE_FORMAT)
                if birth_day_object.day == current_date.day and birth_day_object.month == current_date.month:
                    color = 'ffffff'  
                else:
                    color= 'ffffff'  
                record.color = color
        print "################################ color",self.color

    @api.depends('current_student_academic_year')
    @api.multi
    def _compute_class_id(self):
        for rec in self:
            if rec.current_student_academic_year:
                rec.class_id = rec.current_student_academic_year.class_id

    @api.multi
    def _compute_payement(self):
        for rec in self:
            rec.payement_ids = self.env['account.payment'].search([('student_id','=',rec.id)])
            # payment_line_list = []            
            # for payment in rec.payement_ids:
            #     for line in payment.account_payment_line_ids:
            #         payment_line_list.append(line.id)
            
            # if len(payment_line_list):
            #     rec.account_payment_line_ids = [(6, False, [line for line in payment_line_list])]

            #rec.account_payment_line_ids =  rec.payement_ids.account_payment_line_ids


    
    @api.multi
    def write(self,vals):
        # for rec in self:
        #     print "### FROM STUDENT WRITE"
        #     if rec.user_id and rec.user_id.partner_id:
        #         rec.partner_id.write({'is_school_student':True, 'name':rec.name,'last_name':rec.last_name,'pid':rec.pid})
        #         print "### STUDENT PARTNER ",rec.partner_id.id
        #         print "### USER PARTNER ",rec.user_id.partner_id.id
        for rec in self:
            if vals.get('allow_platform_login', False):
               rec.user_id.write({'allow_platform_login': vals.get('allow_platform_login')})
        return super(Student,self).write(vals)


    @api.model
    def create(self, vals):
        """
        Override methode create() to create a res.users object related to this student 
        so he can have the right to access the system with appropriate ACL accoreded to the group he belongs to
        """
        # Many StudentAcademicYear related to one Student
        vals['pid'] = self.env['ir.sequence'].next_by_code('school.student')
        if vals.get('pid', False):
            #_logger.warning("######",vals['pid'])
            user = self.env['res.users'].create({'name':vals['name'], 'login':vals['pid'],'password':vals['pid'],'user_type':'student','allow_platform_login':vals.get('allow_platform_login', False)})
            vals['user_id'] = user.id            
            #print "#### AFTER USER CREATION"
            emp_grp = self.env.ref('base.group_user')
            student_group = self.env.ref('genext_school.group_school_student')
            if user:
                user.write({'groups_id': [(6, 0, [emp_grp.id, student_group.id])]})
        else:
            raise UserError("[PID] Personal Identifier Number is not valid this student won't be saved")

        # if vals.get('parent_ids',False) and not vals.get('legal_responsable',False):
        #     raise UserError("Vous devez sélectionner un parent en tant que responsable légal")
        # else if not vals.get('parent_ids',False):
        #     parent = {
        #         'pid': self.env['ir.sequence'].next_by_code('res.partner'),
        #         'name': vals.get('name',False),
        #         'last_name': vals.get('last_name',False),
        #         'is_school_parent':True,
        #     }
        #     parent = self.env['res.partner'].create(parent)
           
        return  super(Student, self).create(vals)


    @api.multi
    @api.depends('parent_ids')
    def _compute_parent_ids_domain(self):
        """ 
            needed by web_domain_field plugin to correct a bug in odoo javascript domain evaluation
            used to generate the domain in json format to support using Many2many field in domain
            check the module web_domain_field
         """
        for rec in self:
            rec.parent_ids_domain = json.dumps(['&',('id','in',rec.parent_ids.ids), ('is_school_parent','=',True)])


    @api.multi
    def name_get(self):
        """
        @override : methode to change the display_name 
        """
        return [(record.id, record.name+' '+record.last_name)for record in self]


    @api.model
    def _get_default_image(self):
        img_path = ''
        if self.gender == 'Masculin':
            img_path = get_resource_path("genext_school", "static/images", "student_boy.jpg")
        else:
            img_path = get_resource_path("genext_school", "static/images", "student_girl.png")
        with open(img_path, 'rb') as file:
            image = file.read()
        return image.encode('base64')

    @api.onchange('gender')
    def _onchange_gender(self):
        if self.gender == 'Masculin':
            img_path = get_resource_path("genext_school", "static/images", "student_boy.jpg")
        else:
            img_path = get_resource_path("genext_school", "static/images", "student_girl.png")
        with open(img_path, 'rb') as file:
            image = file.read()
        return {'value': {'image':image.encode('base64')}}

    @api.multi
    def set_to_condidate(self):
        for record in self:
            record.state = 'condidate'
        return True

    # To-Do : verify minimum fees is paid
    # student can be called registred if the min amount is payed
    # student can't be called alumni until he pays all his due fees
    # create a wizard for payment [minimum amout, due amout ...]
    @api.multi
    def set_to_registred(self):
        for record in self:
            record.state = 'registred'
        return True

    # To-Do : verify constraints to get graduated [passed all exams, fees paid, ...]
    @api.multi
    def set_to_graduated(self):
        for record in self:
            record.state = 'graduated'
        return True

    @api.multi
    def set_to_alumni(self):
        """
        Method to change student state to alumni [ancien élève]
        """
        for record in self:
            record.state = 'alumni'
        return True

    @api.constrains('birth_date')
    def _check_age(self):
        """
        Check age must be greater than 5 year
        To-Do : move the minimum accepted age to the school_config_settings
        """
        current_date = datetime.today()
        _logger.warning("CURRENT DATE %s",current_date)
        if self.birth_date:
            birth_day_object = datetime.strptime(self.birth_date,DEFAULT_SERVER_DATE_FORMAT)
            age = ((current_date - birth_day_object).days / 365)
            #check if age is greater than 5 years
            min_student_age = self.env['ir.values'].get_default('school.config.settings', 'min_student_age')
            if not min_student_age:
                _logger.warning("STUDENT MIN AGE: %s",min_student_age)
                raise ValidationError(_('You need to set the student min age'))
            else:
                _logger.warning("STUDENT MIN AGE: %s",min_student_age)
            if age <= int(min_student_age):
                raise ValidationError(_("Student age must be greater that 5 years !"))


    @api.multi
    @api.depends('birth_date')
    def _compute_student_age(self):
        """
        Calculate the age based on the birthday date
        """
        current_date = datetime.today()
        for record in self:
            if record.birth_date:
                #Return a datetime corresponding to date_string, parsed according to format
                birth_day_object = datetime.strptime(record.birth_date, DEFAULT_SERVER_DATE_FORMAT)
                age = ((current_date - birth_day_object).days / 365)
                if age > 0:
                    record.age = age
        return True


    @api.multi
    def open_registration_wizard(self):
        # check if student has completed the current year or not using the decision attribute of student academic year
        if self.current_student_academic_year.id:
            if self.current_student_academic_year.decision == 'in-progress':
                raise ValidationError('Action cannot be performed. Students current year still in progress!')
        if not self.user_id.active:
            self.user_id.active = True
        
        for parent in self.parent_ids:
            user_id = self.env['res.users'].search([('partner_id','=',parent.id)])
            print " Parent user ", user_id
            for user in user_id:
                print " Parent user id active", user.active
                if not user.active:
                    user.active = not user.active
                    print " Parent user id active", user.active

        return {
            'type': 'ir.actions.act_window',
            'name': 'Name ',
            'view_mode': 'form', 
            'target': 'new',
            'res_model': 'student.registration',
            'context': {'student_id': self.id} 
        }

    @api.multi
    def set_to_archive(self):
        for record in self:
            if record.user_id.active:
                record.user_id.active = not record.user_id.active   
            for parent in record.parent_ids:
                user_id = self.env['res.users'].search([('partner_id','=',parent.id)])
                print " PARENT USER ID",user_id
                print " Parent user id active", user_id.active
                if user_id.active:
                    user_id.active = not user_id.active
            for line in record.student_academic_year:
                line.unlink()
            record.class_id = False
            record.state = 'archive'


    @api.multi
    def unlink(self):
        users = self.mapped('user_id')
        super(Student,self).unlink()
        return users.unlink()


    @api.multi
    def _compute_payment_state(self):
        for rec in self:
            for year in rec.student_academic_year:
                if year.decision == 'in-progress':
                    if rec.state == 'pre-registred' or rec.state == 'registred':
                        payment_id = self.env['account.payment'].search([('student_id','=',rec.id)])
                        payment_line_id = payment_id.mapped('account_payment_line_ids')
                        payments_months = []
                        for line in payment_line_id:
                            if line.academic_year.active_year == True:
                                if line.product_id.qte_depends_on_periode:
                                    payments_months.append(int(line.month))

                        month = int(datetime.now().strftime("%m"))
                        if not month in payments_months:
                            rec.payment_state = 'unpaid'
                            rec.write({'payment_state_group_by':'unpaid'})
                            #_logger.warning('#####  UNPAID %s',rec.payment_state_group_by)
                        else:
                            rec.payment_state = 'all_paid'
                            rec.write({'payment_state_group_by':'all_paid'})
                            #_logger.warning('#####  ALL_PAID %s',rec.payment_state_group_by)



    school_id     = fields.Many2many('school.etablissement',compute='_get_student_school')
    def _get_student_school(self):
        etablissement_obj= self.env['school.etablissement'].search([])
        for etablissement in etablissement_obj:
            if etablissement.school_active == True:
                self.school_id = etablissement
    

    @api.multi
    def _compute_contact_ids(self):
        for record in self:
            teacher_list = []
            
            teacher_ids = self._get_class_teachers(self.class_id)
            teacher_list.extend(teacher_ids)
            record.contact_ids = self.env['hr.employee'].browse(list(set(teacher_list)))

    def _get_class_teachers(self, class_id):
        ''' 
            return the ids of teachers who's teaching the given class in current timing periode 
        '''
        teachers = []
        active_academic_year = self.env['academic.year'].search([('active_year','=',True)])
        recs = self.env['time.table'].search([('class_id','=',class_id.id),('academic_year','=',active_academic_year.id)])
        for rec in recs:
            # print "## name of time table ",rec.name
            # print "## name timing_system ",rec.timing_system.name
            # print "## name timing_periode ",rec.timing_periode.name
            timing_period_id = rec.timing_periode

            start_period = str(datetime.now().year)+'-'+timing_period_id.start_date_month
            end_period = str(datetime.now().year)+'-'+timing_period_id.end_date_month
            # convert date string to date object
            start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
            end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
            # check in which period we are right now
            if start_period <= datetime.now().date() <= end_period:
                _logger.warning(" FOUND CURRENT PERIODE %s",timing_period_id.name)
                for session in rec.time_table_session_ids:
                    _logger.warning("TEACHER NAME %s",session.teacher_id.name)
                    teachers.append(session.teacher_id.id)
        # remove duplication and return teachers ids
        return list(set(teachers))

class Diploma(models.Model):
    _name="school.diploma"
    _description="A Grade Diploma"

    name               = fields.Char("Name")
    diploma_prototype  = fields.Binary("Diploma Prototype")
    description        = fields.Text("Description of diploma")
    field_id           = fields.Many2one('school.field', ondelete="restrict", string="Field", delegate=True)


class EducationStage(models.Model):
    _name='educational.stage'
    _description='Educational Stages'

   
    name                           = fields.Char(string="Name", required=True)
    code                           = fields.Char(string="Code", required=True)
    initial_final_stage            = fields.Selection([
                                            ('initial-stage','Initial Stage'),
                                            ('regular-stage','Regular Stage'),
                                            ('final-stage','Final Stage'),
                                            ], default='regular-stage', required=True)

    next_stage_id                  = fields.Many2one('educational.stage', string='Next Stage', ondelete="set null")
    previous_stage_id              = fields.Many2one('educational.stage', string='Previous Stage', ondelete="set null")
    orientation_system             = fields.Selection([
                                        ('no-field', 'All Grades have no Fields'),
                                        ('field', 'All Grades Related to a Field'),
                                        ('partial', 'Both'),
                                        ],default='no-field', required=True, help="Choose if this educational stage contains no field system as in elementary school\
                                            or partial as in hight school or fully field system as in university")
    grade_ids                      = fields.One2many('school.grade', 'educational_stage_id')
    field_ids                      = fields.One2many('school.field', 'educational_stage_id')
    timing_system_id               = fields.Many2one('school.timing.system','Timing System', ondelete="set null")
    timing_system_period_ids       = fields.One2many(related='timing_system_id.period_ids')
    evaluation_methode             = fields.Selection([('preschool','Préscolaire'),('not_preschool','Autre')], default="not_preschool", string="Méthode d'évaluation")




class TimingSystem(models.Model):
    _name='school.timing.system'
    _description= "Object representing the timing system for an educational stage."

    name                    = fields.Char(string="Name", required=True)
    period_ids              = fields.One2many('school.timing.system.period','timing_system_id', string="Périodes")
    active_period           = fields.Boolean('Active Period', default=False)



class TimeSystemLine(models.Model):
    _name='school.timing.system.period'
    _description = 'Object representing a period in a timing system'

    name                = fields.Char("Name", required= True)
    # no need to keep this period if it's timing system is deleted --> ondelete='cascade'
    timing_system_id    = fields.Many2one('school.timing.system', ondelete='cascade')

    start_date          = fields.Date('Start Date', required=True)
    # used to hold day/month for search regardless of the year
    start_date_month    = fields.Char('Day Month only')

    end_date            = fields.Date('End Date', required=True)
    # used to hold day/month for search regardless of the year
    end_date_month    = fields.Char('Day Month only')




    @api.model
    def create(self, values):        
        if not values.get('timing_system_id',False):
            raise ValidationError('You need to create this period from the Educational stage form !')

        if not values.get('start_date',False) or not values.get('end_date',False):
            raise ValidationError('Period start date or end date or both are not valide !')
        _logger.warning("### START DATE ")
        # setting start date and end date with day/month only
        values['start_date_month'] = datetime.strptime(values['start_date'], DEFAULT_SERVER_DATE_FORMAT).strftime('%m-%d')
        values['end_date_month'] = datetime.strptime(values['end_date'], DEFAULT_SERVER_DATE_FORMAT).strftime('%m-%d')

        current_start_date = datetime.strptime(values['start_date'], DEFAULT_SERVER_DATE_FORMAT).date()
        current_end_date = datetime.strptime(values['end_date'], DEFAULT_SERVER_DATE_FORMAT).date()

        periods = self.env['school.timing.system.period'].search([('timing_system_id','=',values['timing_system_id'])])
        for period in periods:
            period_start_date = datetime.strptime(period.start_date, DEFAULT_SERVER_DATE_FORMAT).date()
            period_end_date = datetime.strptime(period.start_date, DEFAULT_SERVER_DATE_FORMAT).date()
            if period_start_date < current_start_date < period_end_date or period_start_date < current_end_date < period_end_date:
                raise ValidationError('This period is overlapping with ['+period.name+']')
        return super(TimeSystemLine, self).create(values)

    @api.multi
    def write(self, vals):
        if vals.get('start_date',False):
            self.start_date_month = datetime.strptime(vals['start_date'], DEFAULT_SERVER_DATE_FORMAT).strftime('%m-%d')
        if vals.get('end_date',False):
            self.end_date_month = datetime.strptime(vals['end_date'], DEFAULT_SERVER_DATE_FORMAT).strftime('%m-%d')
        ''' 
            Update the records and then check if anything is not valid [to avoid using vals dict]
            If not valid the ORM will rollback because of raising an exception no need to worry about changed data 
        '''
        res = super(TimeSystemLine,self).write(vals)
        for record in self:
            record_start_date = datetime.strptime(record.start_date, DEFAULT_SERVER_DATE_FORMAT).date()
            record_end_date = datetime.strptime(record.end_date, DEFAULT_SERVER_DATE_FORMAT).date()
            if record_start_date >= record_end_date:
                raise ValidationError('End date must be greater than start date !')
            periods = self.env['school.timing.system.period'].search([('id','!=',record.id)])
            for period in periods:
                period_start_date = datetime.strptime(period.start_date, DEFAULT_SERVER_DATE_FORMAT).date()
                period_end_date = datetime.strptime(period.start_date, DEFAULT_SERVER_DATE_FORMAT).date()
                if period_start_date < record_start_date < period_end_date or period_start_date < record_end_date < period_end_date:
                    raise ValidationError('['+period.name+'] is overlapping with ['+record.name+']')
        return res



class Field(models.Model):

    _name="school.field"
    _description="School Fields"

    name                            = fields.Char("Name")
    code                            = fields.Char("Code")
    diploma_ids                     = fields.One2many('school.diploma', 'field_id', string="Diplomas")
    grade_ids                       = fields.One2many('school.grade', 'field_id', string="Grades")
    educational_stage_id            = fields.Many2one('educational.stage', domain="[('orientation_system','!=', 'no-field')]", required=True)
    


    @api.multi
    def name_get(self):
        
        naming_pattern = self.env['ir.values'].get_default('school.config.settings', 'field_naming_pattern')
        if naming_pattern == 'code-name':
            return [(record.id, record.code +'/'+record.name)for record in self]
        elif naming_pattern == 'name-code':
            return [(record.id, record.name +'/'+record.code)for record in self]
        elif naming_pattern == 'name':
            return [(record.id, record.name)for record in self]
        elif naming_pattern == 'code':
            return [(record.id, record.code)for record in self]



class Grade(models.Model):
    """
    This object has dependencies with the configuration of the school graduation system
    [primary school, secondary school, high school(university)]
    """
    _name='school.grade'
    _description='School Grade'


    @api.depends('field_id', 'educational_stage_id')
    @api.multi
    def _compute_code(self):
        for record in self:
            suffix = ''
            suffix = record.field_id.code if record.field_id else record.educational_stage_id.code
            if record.field_id:
                record.code = unicode(record.name).encode('utf-8')+' '+unicode(record.field_id.code).encode('utf-8') 
            elif record.educational_stage_id:
                record.code = unicode(record.name).encode('utf-8')+' '+unicode(record.educational_stage_id.code).encode('utf-8')
            else:
                record.code = unicode(record.name).encode('utf-8')



    name               = fields.Char("Grade name", required=True)
    # think about adding compute method to append [Field Code] or [Education stage code] ==> [1ér IFG] or [1ér Elementary]
    code               = fields.Char("Grade's code", compute=_compute_code)
    subject_ids        = fields.Many2many('school.subject', string="Subjects")
    skill_ids          = fields.Many2many('school.skill', string="Compétences")
    initial_final_grade            = fields.Selection([
                                            ('initial-grade','Initial Grade'),
                                            ('regular-grade','Regular Grade'),
                                            ('final-grade','Final Grade'),
                                            ], default='regular-grade', required=True)

    next_grade         = fields.Many2one('school.grade',string="Next Grade", ondelete="set null")
    previous_grade     = fields.Many2one('school.grade',string="Previous Grade", ondelete="set null")

    can_be_failed      = fields.Boolean("Can be failed ?", default=True)

    educational_stage_id   = fields.Many2one('educational.stage', required=True)
    # used to check if field_id attribute is required or not based on educational stage orientation system
    orientation_system     = fields.Selection(related="educational_stage_id.orientation_system")

    """
    If the institution settings is set to university this field needs to be set
    """
    field_id           = fields.Many2one('school.field', string="Field", ondelete="set null", 
                                domain="[('educational_stage_id','=',educational_stage_id)]")
    """
     graduation year is not the final year student can be graduated and move to a next level [licence, master degree, Ph.D.]
     used to list all student diploma 
    """
    graduation_year    = fields.Boolean("Graduation year ?", default=False, help="If student admitted this grade he can obtain a diploma")
    module_ids         = fields.Many2many('school.module', 'school_module_school_grade_rel', 'grade_id', 'module_id')


    @api.multi
    def name_get(self):
        # if grade is not related to any field [elementary school] --> use the educational stage code 
        return [(record.id, record.code)for record in self]

    @api.multi
    def unlink(self):
        # for record in self:
        #     registration_wizards = self.env['student.registration'].search([('educational_stage_id','=',record.id)])
        #     if registration_wizards:
        #         raise ValidationError(_('Befor deleting this grade you need to unregister all students that are registred in this grade'))
        #     registration_wizards = self.env['school.class'].search([('educational_stage_id','=',record.id)])
        return super(Grade,self).unlink()

# To-Do : load the max student number from school config setting
class Class(models.Model):
    _name="school.class"
    _description="School class"

    name                     = fields.Char("Name", required=True)
    code                     = fields.Char("Code", readonly=True, compute='_compute_class_code', store=True)
    grade_id                 = fields.Many2one('school.grade',string="Grade", ondelete="cascade", required=True)
    allow_max_student_number = fields.Boolean("Allow max student number")
    max_student_number       = fields.Integer("Max student number")
    student_ids              = fields.One2many('school.student', 'class_id', compute='_compute_students')
    attendance_id            = fields.One2many('school.attendance','class_id')
    @api.one
    def _compute_students(self):
        if self.id:
            students = self.env["school.student"].search([("class_id", "=",self.id)])
            self.student_ids = students

    
    @api.multi
    @api.depends('name','grade_id.code')
    def _compute_class_code(self):
        class_name_prefix = self.env['ir.values'].get_default('school.config.settings', 'class_name_prefix')
        for record in self:
            if record.grade_id != False:
                record.code = unicode(record.grade_id.code).encode('utf-8')+' '+unicode(record.name).encode('utf-8')

    @api.multi
    def name_get(self):
        class_name_prefix = self.env['ir.values'].get_default('school.config.settings', 'class_name_prefix')
        return [(record.id, record.code)for record in self]


class Subject(models.Model):
    _name="school.subject"
    _description="School Subject"

    name                     = fields.Char("Name", required=True)
    code                     = fields.Char("Code", required=True)
    is_practical             = fields.Boolean('Is Practical',help='Check this if subject is practical.')
    teacher_ids              = fields.Many2many('hr.employee')
    grade_ids                = fields.Many2many('school.grade')
    module_ids               = fields.Many2many('school.module', 'school_module_school_subject_rel', 'subject_id', 'module_id')
    is_primary               = fields.Boolean(string="Matière principale", default=False)

    skill_ids                = fields.Many2many('school.subject.skill', string="Compétences")
    coefficient              = fields.Float(string="Coefficient", default=0.0)

class Skills(models.Model):
    _name='school.skill'
    _description = "school skill"
    name                     = fields.Char("Name", required=True)
    code                     = fields.Char("Code", required=True)
    teacher_ids              = fields.Many2many('hr.employee')
    grade_ids                = fields.Many2many('school.grade')
    is_practical             = fields.Boolean(help='Check this if skill is practical.')

    

class ClassRoom(models.Model):
    _name = 'school.classroom'
    _description='School Classroom'


    name                     = fields.Char("Name")
    code                     = fields.Char("Code", help="The code of the classroom can be the same as the name")
    is_practical             = fields.Boolean('Is practical', default=False, help="Check if classroom is for practical subjects")



class Skill(models.Model):
    _name="school.subject.skill"
    _description="Subject Skills"

    name                    = fields.Char("Compétence")
    ponderation             = fields.Integer(string='Pondération')

class student_decision(models.Model):
    _name="student.decision"


    grade_id                 = fields.Many2one('school.grade',string="Grade", ondelete="cascade", required=True)

    student_academic_year    = fields.One2many('student.academic.year', 'student_id', string="Historique Scolaire")



class TeacherSkill(models.Model):
    _name='skill.teacher'

    name       = fields.Char(string="Nom")
    
class TeacherSkill(models.Model):
    _name='skill.teacher.option'

    skill_teacher  = fields.Many2one('skill.teacher',string="Compétence")
    teacher_id = fields.Many2one('hr.employee',string="Enseignant")
    options    = fields.Selection([
                                    ('primary','Principale'),
                                    ('secondary','Secondaire'),
                                          
                                    ], required=True)
class Region(models.Model):
    _name ='school.region'

    name = fields.Char(string="Nom")
    city = fields.Char(string="Ville")
    teacher_ids = fields.Many2many('hr.employee')