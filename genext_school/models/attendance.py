# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.exceptions import ValidationError, Warning
from datetime import datetime
import smtplib
import string
import logging
_logger = logging.getLogger(__name__)


class Attendance(models.Model):

    _name = 'school.attendance'
    _description = 'School Attendance'


    @api.model
    def _compute_default_teacher_id(self):
        teacher = self.env['hr.employee']
        if self.env.user.user_type == 'teacher':
            teacher = self.env['hr.employee'].search([('user_id','=',self.env.user.id)])
        if len(teacher):
            return teacher
        else:
            return False

    @api.depends('session_id')
    @api.multi
    def _compute_name(self):
        for record in self:
            record.name = record.session_id.name


    name            = fields.Char('Name',compute=_compute_name)
    academic_year   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) )
    teacher_id      = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", default=_compute_default_teacher_id)
    session_id      = fields.Many2one('time.table.session')
    timetable_id    = fields.Many2one(related='session_id.time_table_id')
    class_ids       = fields.Many2one(related='timetable_id.class_id') 
    class_id        = fields.Many2one('school.class','attendance_id')
    subject_id      = fields.Many2one(related='session_id.subject_id')
    classroom_id    = fields.Many2one(related='session_id.classroom_id')
    datetime        = fields.Date('Date',default=fields.Date.today())
    attendance_line_ids = fields.One2many('school.attendance.line','attendance_id',string="présence")

    def _default_timing_periode(self):
        # check if there's a selected timing system
        if len(self.timing_system):
            for rec in self.env['school.timing.system.period'].search([('timing_system_id','=',self.timing_system.id)]):
                # reconstruct date with the current year
                start_period = str(datetime.now().year)+'-'+rec.start_date_month
                end_period = str(datetime.now().year)+'-'+rec.end_date_month
                # convert date sting to date object
                start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
                end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
                # check in which period we are right now
                if start_period <= datetime.now().date() <= end_period:
                    self.timing_periode = rec
                    break


    @api.onchange('teacher_id')
    def _onchange_teacher_id(self):
        '''check if the timetable is in the current academic year and current timing system period '''
        if len(self.teacher_id):
            dow = datetime.today().weekday()
            current_time_string = datetime.now().time().strftime('%H:%M:%S')
            current_time_object = datetime.strptime(current_time_string,'%H:%M:%S')
            _logger.warning("DOW %s",dow)
            _logger.warning("CURRENT TIME STRING %s",current_time_string)
            _logger.warning("CURRENT TEACHER ID %s",self.teacher_id)
            for session in self.env['time.table.session'].search([('time_table_id.academic_year','=',self.env['academic.year'].search([('active_year','=',True)]).id)]):# <--- check current year and timing session period here
                _logger.warning("## SESSION TIME TABLE ACADEMIC YEAR %s",session.time_table_id.academic_year.start_date)
                _logger.warning("## SESSION TIMING PERIODE %s",session.timing_periode.name)
                
                start_period = str(datetime.now().year)+'-'+session.timing_periode.start_date_month
                end_period = str(datetime.now().year)+'-'+session.timing_periode.end_date_month
                # convert date sting to date object
                start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
                end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
                # check in which period we are right now
                if start_period <= datetime.now().date() <= end_period:
                    _logger.warning("#### CURRENT PERIODE %s",session.timing_periode.name)
                else:
                    _logger.warning("#### NOT CURRENT PERIODE %s",session.timing_periode.name)

                # try to check day of week and teacher first to avoid unnecessary further test of datetime
                if session.day_of_week == dow and session.teacher_id.id == self.teacher_id.id:
                    _logger.warning("INDSIDE IF ")
                    session_start_time = datetime.strptime(session.start_time,'%H:%M:%S') 
                    session_end_time = datetime.strptime(session.end_time,'%H:%M:%S')
                    if session_start_time <= current_time_object <= session_end_time:
                        self.session_id = session
                        _logger.warning("#### #### CLASS ID %s",session.class_id)
                        break
                _logger.warning("---------------------------------------------")
        attendance_lines = []
        if len(self.class_id):
            students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
            for student in students:
                attendance_lines.append( (0,0,{'name':'name','student_id':student.id,'state':'present'}) )
        else:
            _logger.warning("NO CLASS ID")
        return {'value':{'attendance_line_ids':attendance_lines}}


    @api.onchange('class_id')
    def _onchange_class_id(self):
        attendance_lines = []
        students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
        for student in students:
            attendance_lines.append( (0,0,{'name':'name','student_id':student.id,'state':'present'}) )
        else:
            print "NO CLASS ID"
        return {'value':{'attendance_line_ids':attendance_lines}}



    # @api.model
    # def create(self, vals):
    #     res = super(Attendance,self).create(vals)
    #     _logger.warning("#### sending msg")


    #     send_msg = self.env['message.sent']
    #     for line in res.attendance_line_ids:
    #         if line.state == 'absent':
    #             print "############ ATTENDANCE ON CREATE",line.student_id.name
    #             print "############ STUDENT PARTNER ID ", line.student_id.legal_responsable.id
    #             msg_vals = {'name': "Message lié à l'assiduité",
    #                      'partner_id': line.student_id.legal_responsable.id,
    #                      'content': "Votre enfant est absent aujourd'hui",
    #                      }
    #             send_msg = self.env['message.sent'].create(msg_vals)
    #             print "#################### MSG SENT"

    #     return res



    # class_id        = fields.Many2one('school.class', string="Classe")
    # date            = fields.Date('Date',default=fields.Date.today())
    # attendance_line_ids = fields.One2many('school.attendance.line','attendance_id')

    # _sql_constraints = [('class_id_date_uniq', 'unique(class_id,date)', 'La date doit etre unique! '),]


    # @api.onchange('class_id')
    # def _onchange_class_id(self):
    #     attendance_lines = []
    #     students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
    #     for student in students:
    #         attendance_lines.append( (0,0,{'name':'name','student_id':student.id,'state':'present'}) )
    #     else:
    #         print "NO CLASS ID"
    #     return {'value':{'attendance_line_ids':attendance_lines}}


    def action_for_email_sms_and_notification(self):
        print("*****************Email*******************")
        _logger.warning("#### sending email")
        email_config  = self.env['email.config'].search([],limit=1)
        emailparent=[]
        sender = email_config.sender_email
      
        for line in self.attendance_line_ids:
            for parent in line.student_id.parent_ids:
                if line.state == 'absent':
                    receiver = parent.email
                    emailparent.append(receiver)
          
        From = sender

        receivers=emailparent
        print "receivers#####",receivers
        date = self.datetime
        date = str(date)
        for receiver in receivers:
            for line in self.attendance_line_ids:
                if receiver in line.student_id.parent_ids.email:
                    child = line.student_id
                    childrens = ["\t"+child.name+" "+child.last_name+": "+ child.class_id.name if child.class_id.name else "Non défini"]
                    childrens = ', '.join(set(childrens))
                    Text = "Votre élève a était "+ line.state +" le " + date 

            Body = string.join((
                "From: %s" % From,
                "",
                Text,
                ), "\r\n")
            print(Body)
            try:
              print("-------------------------------------------------------")
              smtpObj = smtplib.SMTP(host='smtp.gmail.com', port=587)
              smtpObj.ehlo()
              smtpObj.starttls()
              smtpObj.ehlo()
              print("+++++++++++")
              smtpObj.login(user=email_config.sender_email, password=email_config.sender_password)
              smtpObj.sendmail(sender, receiver, Body) 
              smtpObj.close()        
              _logger.warning("Successfully sent email")
            except:
              _logger.warning("#### ERRROR while sending email")

        print("here ------------ sms-------------------")
        sender = self.env['school.sms.setup'].search([('state','=','verified')], limit=1)
        if not sender:
            print("Vous devez valider une configuration sms pour envoyer des sms")
           #raise Warning('Vous devez valider une configuration sms pour envoyer des sms')
        else:
            number_list = []
            for line in self.attendance_line_ids:
                for parent in line.student_id.parent_ids:
                    phone = parent.mobile if parent.mobile else parent.phone
                    if phone:
                       number = {'name':'','gender':1,'phone':phone,'prefix':'216','type':'cell'}
                       _logger.warning("### NUMBER %s",number)
                       number_list.append(number)
            message = "Votre éleve a était absent le " + date
            _logger.warning("NUMBERS %s",number_list)
            response = sender.send_lot_sms(message, number_list)
            if response['success'] == True:
               self.env.cr.commit()
               #raise Warning('Messages envoyés avec succès')
               print("Messages envoyés avec succès")
            else:
               #raise Warning('Messages non envoyés')
               print("Messages non envoyés")

        print("**********notification**************************")
        notif = self.env['notification.config'].search([('enabled','=',True)],limit=1)
        if notif:
            for rec in self:
                tokens = []
                attendance = {
                                'date':rec.datetime,
                                'teacher':{
                                    'id':rec.teacher_id.id,
                                    'name':rec.teacher_id.name,
                                    'last_name':rec.teacher_id.last_name,
                                },
                                'class':{
                                    'id':rec.class_id.id,
                                    'name':rec.class_id.name,
                                    'code':rec.class_id.code,
                                },
                            }
                for line in rec.attendance_line_ids:
                    for parent in line.student_id.parent_ids:
                        for token in parent.token_ids:
                            _logger.warning("#### TOKEN %s",parent.token)
                            tokens.append(token.token)
                            data = {'module':'AttendancePage','data':attendance, 'kid':{'id':line.student_id.id,'name':line.student_id.name,'last_name':line.student_id.last_name}}
                            if line.state == "absent":
                                content = {'en':'Your child is absent', 'fr':'Votre enfant est absent'}
                                notif.sendPushNotification(data=data ,content=content, tokens=[token.token])
                            if line.state == "late":
                                content = {'en':'Your child got is late', 'fr':'Votre enfant est en retard'}
                                notif.sendPushNotification(data=data ,content=content, tokens=[token.token])

                            


          

class AttendanceLine(models.Model):
    _name = 'school.attendance.line'
    _description = 'School Attendance Line'


    name            = fields.Char('Nom',default="présence" , readonly=True)
    attendance_id   = fields.Many2one('school.attendance',string="id",readonly=True)
    student_id      = fields.Many2one('school.student',string="Identifiant", readonly=True) # add a domain to filter registred student and student with exception [will pay later]
    state           = fields.Selection([
                                    ('present','Présent'),
                                    ('late','Retard'),
                                    ('absent','Absent')
                                        ],'État',default='present')
    date_time       = fields.Selection([('Toute la journée','Toute la journée'),('Matin','Matin'),('Aprés midi','Aprés midi')],'Durée')
    motif           = fields.Selection([
                                    ('Libelle','Libelle'),
                                    ('Convocation administrative','Convocation administrative'),
                                    ('Divers','Divers'),
                                    ('Exclusion temporaire','Exclusion temporaire'),
                                    ('Infirmerie','Infirmerie'),
                                    ('Maladie avec certificat','Maladie avec certificat'),
                                    ('Maladie sans certificat','Maladie sans certificat'),
                                    ('Motif non encore connu','Motif non encore connu'),
                                    ('Problème de réveil','Problème de réveil'),
                                    ('Problème de transport','Problème de transport'),
                                    ('Raisons familiales','Raisons familiales'),
                                    ('RDV assistante sociale','RDV assistante sociale'),
                                    ('RDV médecin scolaire','RDV médecin scolaire'),
                                    ('RDV médical extérieur','RDV médical extérieur'),
                                    ('RDV psychologue','RDV psychologue'),
                                    ('Réunion (autre)','Réunion (autre)'),
                                    ('Réunion délégués','Réunion délégués'),
                                    ('Sans excuses','Sans excuses'),
                                    ('Sortie scolaire ou pédagogique','Sortie scolaire ou pédagogique'),
                                    ('Stage en entreprise','Stage en entreprise'),
                                    ('Visite médicale','Visite médicale')])













#     @api.model
#     def _compute_default_teacher_id(self):
#         teacher = self.env['hr.employee']
#         if self.env.user.user_type == 'teacher':
#             teacher = self.env['hr.employee'].search([('user_id','=',self.env.user.id)])
#         if len(teacher):
#             return teacher
#         else:
#             return False


#     @api.depends('session_id')
#     @api.multi
#     def _compute_name(self):
#         for record in self:
#             record.name = record.session_id.name

#     name            = fields.Char('Name',compute=_compute_name)
#     academic_year   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) )
#     teacher_id      = fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", default=_compute_default_teacher_id)
#     session_id      = fields.Many2one('time.table.session')
#     timetable_id    = fields.Many2one(related='session_id.time_table_id')
#     class_id        = fields.Many2one(related='timetable_id.class_id')
#     subject_id      = fields.Many2one(related='session_id.subject_id')
#     classroom_id    = fields.Many2one(related='session_id.classroom_id')
#     datetime        = fields.Datetime('Date',default=fields.Datetime.now())
#     attendance_line_ids = fields.One2many('school.attendance.line','attendance_id')


#     def _default_timing_periode(self):
#         # check if there's a selected timing system
#         if len(self.timing_system):
#             for rec in self.env['school.timing.system.period'].search([('timing_system_id','=',self.timing_system.id)]):
#                 # reconstruct date with the current year
#                 start_period = str(datetime.now().year)+'-'+rec.start_date_month
#                 end_period = str(datetime.now().year)+'-'+rec.end_date_month
#                 # convert date sting to date object
#                 start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
#                 end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
#                 # check in which period we are right now
#                 if start_period <= datetime.now().date() <= end_period:
#                     self.timing_periode = rec
#                     break
        

#     @api.onchange('teacher_id')
#     def _onchange_teacher_id(self):
#         '''check if the timetable is in the current academic year and current timing system period '''
#         if len(self.teacher_id):
#             dow = datetime.today().weekday()
#             current_time_string = datetime.now().time().strftime('%H:%M:%S')
#             current_time_object = datetime.strptime(current_time_string,'%H:%M:%S')
#             _logger.warning("DOW %s",dow)
#             _logger.warning("CURRENT TIME STRING %s",current_time_string)
#             _logger.warning("CURRENT TEACHER ID %s",self.teacher_id)
#             for session in self.env['time.table.session'].search([('time_table_id.academic_year','=',self.env['academic.year'].search([('active_year','=',True)]).id)]):# <--- check current year and timing session period here
#                 _logger.warning("## SESSION TIME TABLE ACADEMIC YEAR %s",session.time_table_id.academic_year.start_date)
#                 _logger.warning("## SESSION TIMING PERIODE %s",session.timing_periode.name)
                
#                 start_period = str(datetime.now().year)+'-'+session.timing_periode.start_date_month
#                 end_period = str(datetime.now().year)+'-'+session.timing_periode.end_date_month
#                 # convert date sting to date object
#                 start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
#                 end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
#                 # check in which period we are right now
#                 if start_period <= datetime.now().date() <= end_period:
#                     _logger.warning("#### CURRENT PERIODE %s",session.timing_periode.name)
#                 else:
#                     _logger.warning("#### NOT CURRENT PERIODE %s",session.timing_periode.name)

#                 # try to check day of week and teacher first to avoid unnecessary further test of datetime
#                 if session.day_of_week == dow and session.teacher_id.id == self.teacher_id.id:
#                     _logger.warning("INDSIDE IF ")
#                     session_start_time = datetime.strptime(session.start_time,'%H:%M:%S') 
#                     session_end_time = datetime.strptime(session.end_time,'%H:%M:%S')
#                     if session_start_time <= current_time_object <= session_end_time:
#                         self.session_id = session
#                         _logger.warning("#### #### CLASS ID %s",session.class_id)
#                         break
#                 _logger.warning("---------------------------------------------")
#         attendance_lines = []
#         if len(self.session_id.class_id):
#             students = self.env['school.student'].search([('class_id','=',self.session_id.class_id.id)])
#             for student in students:
#                 attendance_lines.append( (0,0,{'name':'name','student_id':student.id,'state':'present'}) )
#         else:
#             _logger.warning("NO CLASS ID")
#         return {'value':{'attendance_line_ids':attendance_lines}}

            

# class AttendanceLine(models.Model):
#     _name = 'school.attendance.line'
#     _description = 'School Attendance Line'


#     name            = fields.Char('Name',default="attendace line" , readonly=True)
#     attendance_id   = fields.Many2one('school.attendance', readonly=True)
#     student_id      = fields.Many2one('school.student', readonly=True) # add a domain to filter registred student and student with exception [will pay later]
#     state           = fields.Selection([
#                                     ('present','Present'),
#                                     ('late','Late'),
#                                     ('absent','Absent')
#                                         ], default='present')














# class Attendance(models.Model):

#     _name = 'school.attendance'
#     _description = 'School Attendance'


#     @api.model
#     def _compute_default_teacher_id(self):
#         teacher = self.env['hr.employee']
#         if self.env.user.user_type == 'teacher':
#             teacher = self.env['hr.employee'].search([('user_id','=',self.env.user.id)])
#         if len(teacher):
#             return teacher
#         else:
#             return False


#     @api.depends('session_id')
#     @api.multi
#     def _compute_name(self):
#         for record in self:
#             record.name = record.session_id.name

#     name                    =   fields.Char('Name',compute=_compute_name)
#     academic_year           =   fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) )
#     teacher_id              =   fields.Many2one('hr.employee', domain="[('is_school_teacher','=',True)]", default=_compute_default_teacher_id)
#     session_id              =   fields.Many2one('time.table.session')
#     timetable_id            =   fields.Many2one(related='session_id.time_table_id')
#     class_id                =   fields.Many2one(related='timetable_id.class_id', store=True)
#     grade_id                =   fields.Many2one(related='session_id.grade_id')
#     subject_id              =   fields.Many2one(related='session_id.subject_id')
#     classroom_id            =   fields.Many2one(related='session_id.classroom_id')
#     datetime                =   fields.Datetime('Date',default=fields.Datetime.now)
#     attendance_line_ids     =   fields.One2many('school.attendance.line','attendance_id')
#     educational_stage_id    =   fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
#     timing_system_id        =   fields.Many2one(related='educational_stage_id.timing_system_id', store=True)
#     timing_system_periode_id=   fields.Many2one('school.timing.system.period', domain="[('timing_system_id','=',timing_system_id)]", ondelete='set null', string="Période")


#     def _default_timing_periode(self):
#         # check if there's a selected timing system
#         if len(self.timing_system):
#             for rec in self.env['school.timing.system.period'].search([('timing_system_id','=',self.timing_system.id)]):
#                 # reconstruct date with the current year
#                 start_period = str(datetime.now().year)+'-'+rec.start_date_month
#                 end_period = str(datetime.now().year)+'-'+rec.end_date_month
#                 # convert date sting to date object
#                 start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
#                 end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
#                 # check in which period we are right now
#                 if start_period <= datetime.now().date() <= end_period:
#                     self.timing_periode = rec
#                     break
        

#     @api.onchange('teacher_id')
#     def _onchange_teacher_id(self):
#         '''check if the timetable is in the current academic year and current timing system period '''
#         if len(self.teacher_id):
#             dow = datetime.today().weekday()

#             add_hour            = datetime.now() + timedelta(hours=1) 
#             current_time_string = add_hour.time().strftime('%H:%M:%S')

#             #current_time_string = datetime.now().time().strftime('%H:%M:%S')
#             current_time_object = datetime.strptime(current_time_string,'%H:%M:%S')
#             print "DOW ",dow
#             #print "CURRENT TIME STRING ",current_time_string
#             _logger.warning('###### CURRENT TIME STRING %s',current_time_string)
#             print "CURRENT TEACHER ID ",self.teacher_id
#             for session in self.env['time.table.session'].search([('time_table_id.academic_year','=',self.env['academic.year'].search([('active_year','=',True)]).id)]):# <--- check current year and timing session period here
#                 print "## SESSION TIME TABLE ACADEMIC YEAR ",session.time_table_id.academic_year.start_date
#                 print "## SESSION TIMING PERIODE ",session.timing_periode.name
                
#                 start_period = str(datetime.now().year)+'-'+session.timing_periode.start_date_month
#                 end_period = str(datetime.now().year)+'-'+session.timing_periode.end_date_month
#                 # convert date sting to date object
#                 start_period = datetime.strptime(start_period, DEFAULT_SERVER_DATE_FORMAT).date()
#                 end_period = datetime.strptime(end_period, DEFAULT_SERVER_DATE_FORMAT).date()
#                 # check in which period we are right now
#                 if start_period <= datetime.now().date() <= end_period:
#                     print "#### CURRENT PERIODE ",session.timing_periode.name
#                 else:
#                     print "#### NOT CURRENT PERIODE ",session.timing_periode.name

#                 # try to check day of week and teacher first to avoid unnecessary further test of datetime
#                 if session.day_of_week == dow and session.teacher_id.id == self.teacher_id.id:
#                     print "INDSIDE IF "
#                     session_start_time = datetime.strptime(session.start_time,'%H:%M:%S') 
#                     session_end_time = datetime.strptime(session.end_time,'%H:%M:%S')
#                     if session_start_time <= current_time_object <= session_end_time:
#                         self.session_id = session
#                         print "#### #### CLASS ID ",session.class_id
#                         break
#                 print "---------------------------------------------"
#         attendance_lines = []
#         if len(self.session_id.class_id):
#             students = self.env['school.student'].search([('class_id','=',self.session_id.class_id.id)])
#             for student in students:
#                 attendance_lines.append( (0,0,{'name':'name','student_id':student.id,'state':'present'}) )
#         else:
#             print "NO CLASS ID"    
#         return {'value':{'attendance_line_ids':attendance_lines}}


#     @api.onchange('class_id')
#     def _onchange_class_id(self):
#         attendance_lines = []
#         if len(self.class_id):
#             students = self.env['school.student'].search([('class_id','=',self.class_id.id)])
#             for student in students:
#                 attendance_lines.append( (0,0,{'name':'name','student_id':student.id,'state':'present'}) )
#         else:
#             print "NO CLASS ID"
#         return {'value':{'attendance_line_ids':attendance_lines}}

            

# class AttendanceLine(models.Model):
#     _name = 'school.attendance.line'
#     _description = 'School Attendance Line'


#     name            = fields.Char('Name',default="attendace line" , readonly=True)
#     attendance_id   = fields.Many2one('school.attendance', readonly=True)
#     student_id      = fields.Many2one('school.student') # add a domain to filter registred student and student with exception [will pay later]
#     state           = fields.Selection([
#                                     ('present','Present'),
#                                     ('late','Late'),
#                                     ('absent','Absent')
#                                         ], default='present')