# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json
import base64

_logger = logging.getLogger(__name__)

class Evaluation(models.Model):
    _inherit   = 'school.evaluation'


    attachment_ids                      = fields.Many2many("ir.attachment")



class Bulletin(models.Model):
    _name = 'student.bulletin'
    _description = 'Bulletin of student'


    name                                = fields.Char('Nom', default="Report.pdf")
    student_id                          = fields.Many2one('school.student', string="Étudiants")
    class_id                            = fields.Many2one('school.class', string="Classe")
    periode_config_id                   = fields.Many2one('periode.config')
    module_id                           = fields.One2many('dummy.module.module', 'student_bulletin_id')
    admin_comment_id                    = fields.One2many('dummy.admin.comment', 'student_bulletin_id')
    evaluation_methode                  = fields.Selection([('preschool','Préscolaire'),('not_preschool','Autre')], default="not_preschool", string="Méthode d'évaluation")
    

    annual_calendar_id                  = fields.Many2one('annual.calendar', string='Années scolaire')
    semester_id                         = fields.Many2one('annual.periode', string="Semestre")
    periode_id                          = fields.Many2one('annual.periode', string="évaluation")
    attachment_ids                      = fields.One2many(comodel_name="ir.attachment", inverse_name="res_id", compute="_add_attachment")
   
    @api.multi
    def _add_attachment(self):
        self.attachment_ids = self.env['ir.attachment'].search([('res_model','=','student.bulletin'),('res_id','=',self.id)])


    @api.multi
    def send_bulletin_notification(self):
        # self._add_attachment()
        for rec in self:    
            file = rec.attachment_ids and max(rec.attachment_ids)
            # print "###### datas ",el.datas
            # print "###### name ",el.name
            # print "###### datas_fname ",el.datas_fname
            # print "###### res_name ",el.res_name
            # print "###### res_field ",el.res_field
            # print "###### type ",el.type
            # print "###### url ",el.url
            # print "###### store_fname ",el.store_fname
            # print "###### mimetype ",el.mimetype


            evaluation_dict = {
                'name':'Bulletin étape 1',
                'class_id':rec.class_id.id,
                'student_id':rec.student_id.id,
                'file':file.datas,
                'file_name':file.datas_fname,
            }
            # print "#### CREATING DOCUMENT *************************** ",rec.student_id.name
            evaluation_id = self.env['school.evaluation'].create(evaluation_dict)
            evaluation_id.set_to_confirmed()
        return True




    # @api.multi 
    # def getUrl(self):
    #     print "####### getURL "
    #     pdf = self.env['report'].sudo().get_pdf([invoice.id], 'account.report_invoice')
    #     self.env['ir.attachment'].create({
    #         'name': 'Bulletin',
    #         'type': 'binary',
    #         'datas': base64.encodestring(pdf),
    #         'res_model': 'account.invoice',
    #         'res_id': invoice.id
    #         'mimetype': 'application/x-pdf'
    #     })



    #     for rec in self:
    #         for file in message.file_ids:
    #         _logger.warning('####### FILE NAME %s',file.file_name)
    #         _logger.warning('####### FILE NAME %s',file.description)
    #         domain = [
    #             ('res_model', '=', file._name),
    #             ('res_field', '=', 'file'),
    #             ('res_id', '=', file.id),
    #         ]
    #         attachment = request.env['ir.attachment'].search(domain)
    #         base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
    #         url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+file.file_name
    #         url = url.replace(" ", "_")
    #         _logger.warning('#### URL of document %s',url)
    #         file_list.append({
    #                 'description':file.description,
    #                 'file_name':file.file_name,
    #                 'url':url,
    #             })
    #         _logger.warning('##### FILE url %s',url)



class AdminComment(models.Model):
    _name = 'dummy.admin.comment'


    student_bulletin_id                 = fields.Many2one('student.bulletin')
    educational_stage_id                = fields.Many2one('educational.stage', string="Cycles")
    grade_id                            = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]")
    class_id                            = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]",)


    annual_calendar_id                  = fields.Many2one('annual.calendar', string="Année scolaire")
    semestre_id                         = fields.Many2one('annual.periode', domain="[('educational_stage_id','=',educational_stage_id)]", string="Semestre")
    periode_id                          = fields.Many2one('annual.periode', domain="['&',('last_level','=',True),('parent_id','=',semestre_id)]", string="Evaluation")
    admin_comment_type_id               = fields.Many2one('admin.comment.type', string="Type de commentaire")
    admin_comment_line_ids              = fields.One2many('dummy.admin.comment.line','admin_comment_id', string="Commentaires")
    evaluation_methode                  = fields.Selection([('preschool','Préscolaire'),('not_preschool','Autre')], default="not_preschool", string="Méthode d'évaluation")



class DummyCommentLine(models.Model):
    _name = 'dummy.admin.comment.line'

    admin_comment_id                    = fields.Many2one('dummy.admin.comment')
    student_id                          = fields.Many2one('school.student', string="Étudiants", readonly=True)
    comment                             = fields.Text('Commentaire')






class Module(models.Model):
    _name = 'dummy.module.module'

    order_value                     = fields.Integer(default=0)
    name                            = fields.Char('Nom', default="Report.pdf")
    student_bulletin_id             = fields.Many2one('student.bulletin')
    educational_stage_ids           = fields.Many2many('educational.stage', string="Cycles", required=True)
    skill_ids                       = fields.One2many('dummy.module.skill', 'module_id', string="Cométences")
    ponderation                     = fields.Integer('Pondération', default=0)
    evaluated                       = fields.Boolean('A évaluer', default=False)
    average                         = fields.Integer('Moyenne du groupe pour toutes les compétences',compute='_compute_average')
    result                          = fields.Integer('résultat disciplinaire',compute='_compute_average')
    teacher_id                      = fields.Many2one('hr.employee')

    @api.multi
    @api.depends('skill_ids')
    def _compute_average(self):
        for rec in self:
            average = 0.0
            for line in rec.skill_ids:
                average+=line.average

            if len(rec.skill_ids):
                average = average/len(rec.skill_ids)
            # print "######### computing average ",average
            rec.average = average * 5

            result = 0.0
            for line in rec.skill_ids:
                # print "###### --------------- skill name ",line
                if not line.is_skill:
                    if line.note != 0:
                        result+=line.note * (100/float(line.note_max))
                    # print "######### result note  ",line.note
                    # print "######### result note_max  ",line.note_max
                    # print "######### result ponderation  ",line.ponderation
                    # print "######### result not skill  ",result
                else:
                    if line.note != 0:
                        result+=(line.note * (float(line.ponderation)/100)) * (100/float(line.note_max))
            #         print "######### result note  ",line.note
            #         print "######### result note_max  ",line.note_max
            #         print "######### result ponderation  ",line.ponderation
            #         print "######### result IS skill  ",result
            # print "######### computing result ",result                    
            rec.result = result

class Skill(models.Model):
    _name = 'dummy.module.skill'

    name                = fields.Char('Nom',default="Report.pdf")
    ponderation         = fields.Integer('Pondération', default=0)
    module_id           = fields.Many2one('dummy.module.module', string="Module", ondelete="cascade")
    note_max            = fields.Integer('Note maximale')
    is_skill            = fields.Boolean('Une compétence ?', default=True)
    mark                = fields.Many2one('module.mark', sting="Remarque")
    skill_id            = fields.Many2one('module.skill')
    average             = fields.Float('Moyenne du groupe')
    average_tmp         = fields.Integer('Moyenne du groupe',compute='_compute_note')
    note                = fields.Float('Note')
    note_tmp            = fields.Integer('Note_TMP', compute='_compute_note')
    teacher_id          = fields.Many2one('hr.employee')
    to_skip             = fields.Boolean('Ignore', default=False)
    fake_ponderation    = fields.Integer('Fake ponderation', default=0,)

    @api.depends('note')
    @api.multi
    def _compute_note(self):
        for rec in self:
            if rec.module_id.student_bulletin_id.evaluation_methode == 'not_preschool':
                rec.note_tmp = rec.note * (100/rec.note_max)
                rec.average_tmp = rec.average * (100/rec.note_max)

