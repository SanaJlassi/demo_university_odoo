# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)


class SchoolMedicalConsultation(models.Model):
    _name="school.medical"
    _description="Medical Consultation"

    @api.multi
    @api.depends('student_id')
    def _compute_consutation(self):
        for rec in self:
            # if rec.student_id:
            #     ids = []
            #     if rec.id:
            #         for id in rec.student_id.consultation_ids.ids:
            #             if id != rec.id:
            #                 ids.append(id)
            #         rec.consultation_ids = [6,False,ids]
            #     else:
            rec.consultation_ids = rec.student_id.consultation_ids.ids


    name                        = fields.Char("Passage Infirmerie")
    nature                      = fields.Selection([('accident','Accident'), ('physical_discomfort','Malaise physique'), ('psychic_disorder','Trouble psychique')], default="physical_discomfort",string="Nature")
    student_id                  = fields.Many2one('school.student', string="Étudiants", required=True)
    start_date                  = fields.Datetime(default=fields.Datetime.now(), string='Date de début', required=True)
    end_date                    = fields.Datetime(default=fields.Datetime.now(), string='Date de fin', required=True)
    consultation_place          = fields.Selection([('Couloir','Couloir'),
                                                ("Cour de l'école","Cour de l'école"),
                                                ('Salle de classe',' Salle de classe'),
                                                ('Salle de gym','Salle de gym'),
                                                ('Piscine','Piscine'),
                                                ('Escalier','Escalier'),
                                                ('Refectoire','Refectoire'),
                                                ('Toilette','Toilette'),
                                                ('Vestiaire','Vestiaire'),
                                                ('Autre','Autre'),
                                                ],string="Lieu")
    symptom                     = fields.Selection([('Allergie','Allergie'),
                                                ('Angoisse','Angoisse'),
                                                ('Blessure','Blessure'),
                                                ('Blessure aux lèvres','Blessure aux lèvres'),
                                                ('Blessure au nez','Blessure au nez'),
                                                ('Blessure profonde','Blessure profonde'),
                                                ('Brûlure','Brûlure'),
                                                ('Conjonctivite','Conjonctivite'),
                                                ('Coupure','Coupure'),
                                                ('Crise de nerfs','Crise de nerfs'),
                                                ('Diarrhée','Diarrhée'),
                                                ('Douleur','Douleur'),
                                                ('Douleur aux doigt','Douleur aux doigt'),
                                                ('Douleur cheville','Douleur cheville'),
                                                ('Douleur coude','Douleur coude'),
                                                ('Douleur jambe','Douleur jambe'),
                                                ('Douleur poignet','Douleur poignet'),
                                                ('Douleur sous les côtes','Douleur sous les côtes'),
                                                ('Epistaxis','Epistaxis'),
                                                ('État de grippe','État de grippe'),
                                                ('Fatigue','Fatigue'),
                                                ('Fièvre','Fièvre'),
                                                ('Foulure','Foulure'),
                                                ('Fracture','Fracture'),
                                                ('Hyperglycémie','Hyperglycémie'),
                                                ('Hypoglycémie','Hypoglycémie'),
                                                ('Male à la gorge','Male à la gorge'),
                                                ("Male à l'oreil","Male à l'oreil"),
                                                ('Male au dos','Male au dos'),
                                                ('Male au genoux','Male au genoux'),
                                                ('Male au machoir','Male au machoir'),
                                                ('Male au pied','Male au pied'),
                                                ('Male au testicule','Male au testicule'),
                                                ('Maux de tête','Maux de tête'),
                                                ('Maux de ventre','Maux de ventre'),
                                                ('Membre cassé','Membre cassé'),
                                                ('Nausée','Nausée'),
                                                ('Pansement mouillée','Pansement mouillée'),
                                                ('Plaie','Plaie'),
                                                ('Plaie superficielle','Plaie superficielle'),
                                                ('Reflux gastrique','Reflux gastrique'),
                                                ('Sous traitement','Sous traitement'),
                                                ('Tachycardie','Tachycardie'),
                                                ('Toux','Toux'),
                                                ('Vertige','Vertige'),
                                                ('Vomissement','Vomissement'),
                                                ('Autre','Autre'),
                                                ], string="Symptôme")

    undertaken_act              = fields.Selection([('Lavage oculaire','Lavage oculaire'),
                                                ('alcool de menthe','Application : alcool de menthe(ventre)'),
                                                ('blessure','Application du creme'),
                                                ('Application du creme','Application du creme'),
                                                ('Désinfection','Désinfection'),
                                                ('Glassage','Glassage'),
                                                ('Lavage des yeux','Lavage des yeux'),
                                                ('Pansement','Pansement'),
                                                ('Pansement alcoolisé','Pansement alcoolisé'),
                                                ('Prise de medicament','Prise de medicament'),
                                                ('Prise des signes vitaux','Prise des signes vitaux'),
                                                ('Repos','Repos'),
                                                ('Tamponnage nasal','Tamponnage nasal'),
                                                ("Prise d'un calmant","Prise d'un calmant"),
                                                ('Autre','Autre'),
                                                ], string="Actes Entrepris")
    intervention_type           = fields.Selection([('brief','Bréve'),('extended','Prolongée'),('temporary','Temporaire')], string="Type de l'intervention")
    intervention_decision       = fields.Selection([('hospitalization','Hospitalisation'),('back home','Retour au domicile'),('back to class','Retour en classe')], string="Issu de l'intervention")
    description                 = fields.Text(string="Détails de la visite")
    accompanist_type            = fields.Selection([('parent','Parent'),('teacher','Enseignant'), ('student','Élève')], default='teacher', string="Accompagnateur")
    partner_ids                 = fields.Many2many('res.partner', string="-" ,domain="[('is_school_teacher','=',True)]")
    vaccin_ids                  = fields.One2many(related="student_id.vaccin_ids", string="Vaccins")
    allergy_ids                 = fields.One2many(related="student_id.allergy_ids", string="Allergies")
    exemption_ids               = fields.One2many(related="student_id.exemption_ids", string="Dispenses")
    consultation_ids            = fields.One2many('school.medical',compute=_compute_consutation, string="Historique")


   

    @api.model
    def create(self, vals):
        print "##########  CREATE ",vals
        return super(SchoolMedicalConsultation,self).create(vals)

    @api.multi
    def write(self,vals):
        print "##### WRITE ",vals
        return super(SchoolMedicalConsultation,self).write(vals)


    @api.onchange('accompanist_type')
    def _onchange_accompanist_type(self):
        self.partner_ids = False
        if self.accompanist_type == 'parent':
            return {'domain': {'partner_ids': [('is_school_parent', '=', True)]}}
        elif self.accompanist_type == 'student':
            return {'domain': {'partner_ids': [('is_school_student', '=', True)]}}
        else:
            return {'domain': {'partner_ids': [('is_school_teacher', '=', True)]}}

    @api.multi
    def name_get(self):
        return [(record.id, record.student_id.name+' '+record.student_id.last_name+'/'+record.nature)for record in self]
    

class SchoolVaccine(models.Model):
    _name="school.vaccine"
    _description="Vaccination"

    name                        = fields.Char("Vaccin", required=True)
    date                        = fields.Date(string="Date", default=fields.Date.today())
    student_id                  = fields.Many2one('school.student', string="Étudiants")


class SchoolAllergy(models.Model):
    _name="school.allergy"
    _description="Allergy"

    name                        = fields.Char("Allergie", required=True)
    date                        = fields.Date(string="Date", default=fields.Date.today())
    student_id                  = fields.Many2one('school.student', string="Étudiants")


class Student(models.Model):
    _inherit="school.student"

    vaccin_ids                  = fields.One2many('school.vaccine', 'student_id', string='Vaccin' )
    allergy_ids                 = fields.One2many('school.allergy', 'student_id', string='Allergie')
    consultation_ids            = fields.One2many('school.medical', 'student_id', string='Passage Infirmerie')




