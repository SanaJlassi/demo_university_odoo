# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SalespersonWizard(models.TransientModel):
    _name = "bulletin.wizard"
    _description = "Salesperson wizard"
    
    grade_id                    = fields.Many2one('school.grade',string="Niveau scolaire")
    class_id                    = fields.Many2one('school.class', string="Classe", domain="[('grade_id','=',grade_id)]", required=True)
    academic_year               = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]) , required=True, string="Année scolaire")
    educational_stage_id        = fields.Many2one(related='grade_id.educational_stage_id', string="Niveau éducative")
    annual_calendar_id          = fields.Many2one('annual.calendar', string='Années scolaire', required=True)
    periode_id                  = fields.Many2one('annual.periode', string="Semestre", required=True, 
                                    domain="['&',('annual_calendar_id','=',annual_calendar_id),('last_level','=',False)]")
    step_id                     = fields.Many2one('annual.periode', string="étape", required=True, 
                                    domain="['&',('parent_id','=',periode_id),('last_level','=',True)]")
    
    state                       = fields.Selection([('step_1','step_1'),('step_2','step_2'),('step_3','step_3'),('step_4','step_4'),('step_5','step_5')], default="step_1")

    student_bulletin_id         = fields.Many2many('student.bulletin')

    def prepare_dummy_module(self, student_id, class_id, module_id):
        dummy_mudule = []
        for rec in module_id:
            note_list = []
            teacher_id = None
            for skill in rec.skill_ids:
                note = self.env['class.note'].get_notes(student_id.id, class_id.id, rec.id, skill.id)
                teacher_id = note.teache_id
                student_note = note.note_line_ids.filtered(lambda r: student_id.id == r.student_id.id)
                student_mark = note.presconote_line_ids.filtered(lambda r: student_id.id == r.student_id.id)
                # print "#-|----------------------- ", student_note.student_id.name
                # print "#-|----------------------- ", student_note.note
                # print "#-|----------------------- ", student_mark.mark
                # print "#-|----------------------- ", student_mark.mark.name
                note_list.append((0,0,{
                    'name':skill.name,
                    'ponderation':skill.ponderation,
                    'note_max':skill.note_max,
                    'is_skill':skill.is_skill,
                    'average':note.average,
                    'skill_id':note.skill_id.id,
                    'note':student_note.note,
                    'mark':student_mark.mark.id,
                    'teacher_id':note.teache_id.id,
                    'to_skip' :skill.to_skip,
                    'fake_ponderation':skill.fake_ponderation,
                }))

            # print "####### note_list ",note_list
            print "##### TEACHER ID ",teacher_id.id
            print "##### TEACHER NAME ",teacher_id.name
            dummy_mudule.append((0,0,{
                'order_value':rec.order_value,
                'name':rec.name,
                'educational_stage_ids':rec.educational_stage_ids.ids,
                'ponderation':rec.ponderation,
                'evaluated':rec.evaluated,
                'skill_ids':note_list,
                'teacher_id ':teacher_id.id,
            }))
        return dummy_mudule


    def prepare_dummy_admin_comment(self, student_id, class_id):
        comments = self.env['admin.comment']
        if self.educational_stage_id.evaluation_methode == 'preschool':
            # print "### GET PRESCO COMMENT "
            comments = self.env['admin.comment'].get_presco_comment(student_id.id, class_id.id, self.annual_calendar_id.id, self.periode_id.id)
        else:
            # print "### GET NOT PRESCO COMMENT "
            comments = self.env['admin.comment'].get_not_presco_comment(student_id.id, class_id.id, self.annual_calendar_id.id, self.periode_id.id)

        student_comment = comments.admin_comment_line_ids.filtered(lambda r: student_id.id == r.student_id.id)
        # print "---------------- STUDENT NAME ",student_id.name
        # print "---------------- comments ",student_comment.comment
        comment_list = []
        comment_list.append((0,0,{
            'student_id':student_comment.student_id.id,
            'comment':student_comment.comment,
        }))

        # print "####### note_list ",comment_list
        dummy_comment = []
        dummy_comment.append((0,0,{
            'admin_comment_line_ids':comment_list,
        }))
        return dummy_comment



    @api.multi
    def check_report(self):
        periode_config_id = self.env['periode.config'].search([('educational_stage_id','=',self.educational_stage_id.id)])
        # print "######### periode_config_id ",periode_config_id.name
        module_id = self.env['module.module'].get_modules_contains_cycle(self.educational_stage_id.id)
        # print "######### module ",module_id
        student_id = self.env['school.student'].search([('class_id','=',self.class_id.id)])
        
        student_bulletin_id = self.env['student.bulletin']
        bulletin = []
        for student in student_id:
            # dummy_module = self.prepare_dummy_module(student, self.class_id, module_id)
            student_bulletin_id += self.env['student.bulletin'].create({
                'student_id':student.id,
                'class_id':student.class_id.id,
                'evaluation_methode':self.educational_stage_id.evaluation_methode,
                'periode_config_id':periode_config_id.id,
                # 'module_id':[(6, 0, [rec.id for rec in module_id])],
                'module_id':self.prepare_dummy_module(student, self.class_id, module_id),
                'admin_comment_id':self.prepare_dummy_admin_comment(student, self.class_id),
                'annual_calendar_id':self.annual_calendar_id.id,
                'semester_id':self.periode_id.id,
                'periode_id':self.step_id.id,
                'evaluation_methode':self.educational_stage_id.evaluation_methode,
                })

        # print "##### student_bulletin_id ",student_bulletin_id
        self.write({'student_bulletin_id':[(6, 0, [student_bulletin_id.ids])]})
        data = {}
        data['form'] = self.read(['student_bulletin_id'])[0]
        # print "###### check_report ",data
        # return self._print_report(data)

    def _print_report(self, data):
        # print "###### before  _print_report ",data
        data['form'].update(self.read(['student_bulletin_id']))
        # print "###### _print_report ",data
        return self.env['report'].get_action(self, 'school_exams.report_bulletin_test', data=data)
    

    @api.onchange('exam_exam_ids')
    def _onchange_exam_exam_ids(self):
        for rec in self.exam_exam_ids:
            pass
            # print "##### NAME OF EXAM ",rec.name

    @api.multi
    def next_action(self):
        if self.state == 'step_1':
            self.write({'state':'step_2'})
        elif self.state == 'step_2':
            self.write({'state':'step_3'})
        elif self.state == 'step_3':
            self.write({'state':'step_4'})
        elif self.state == 'step_4':
            self.write({'state':'step_5'})
        return {
            'type': "ir.actions.do_nothing",
        }


    @api.multi
    def previous_action(self):
        if self.state == 'step_2':
            self.write({'state':'step_1'})
        elif self.state == 'step_3':
            self.write({'state':'step_2'})
        elif self.state == 'step_4':
            self.write({'state':'step_3'})
        elif self.state == 'step_5':
            self.write({'state':'step_4'})
        return {
            'type': "ir.actions.do_nothing",
        }


# class DummyStudentBulletin(models.Model):
#     _name = "dummy.student.bulletin"


#     student_id                          = fields.Many2one('school.student')
#     periode_config_id                   = fields.Many2one('periode.config')
#     module_id                           = fields.Many2many('module.module')

#     annual_calendar_id                  = fields.Many2one('annual.calendar', string='Années scolaire')

    

class DummyModuleNotes(models.Model):
    _name =  'module.note'

    module_id                           = fields.Many2one('module.module')
    average                             = fields.Float('Moyenne du groupe',default=0.0)
    comment                             = fields.Char('comment')


class DummySkillNote(models.Model):
    _name  = 'student.note'

    skill_id                            = fields.Many2one('module.skill')
    note                                = fields.Float('note')

