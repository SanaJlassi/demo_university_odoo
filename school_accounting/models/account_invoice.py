# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import odoo.addons.decimal_precision as dp
from odoo.tools.amount_to_text import amount_to_text_fr,french_number



class AccountInvoice(models.Model):
    _inherit = 'account.invoice'
    
    fees_id             = fields.Many2many('school.fees', 'school_invoice_fees_rel', 'account_invoice_id', 'school_fees_id')
    amount_total = fields.Monetary(string='Total', store=True, readonly=True, digits=dp.get_precision('Product Price'), compute='_compute_amount')
    amount_tax = fields.Monetary(string='Tax', store=True, readonly=True, digits=dp.get_precision('Product Price'), compute='_compute_amount')
    amount_untaxed = fields.Monetary(string='Untaxed Amount', store=True, readonly=True, digits=dp.get_precision('Product Price'), compute='_compute_amount', track_visibility='always')
    ref_draft_invoice   = fields.Char(string="Référence Facture brouillon", readonly=True)

    student_id                    = fields.Many2one('school.student')

    class_id                      = fields.Many2one('school.class', string="Classe", compute="_compute_class" , store=True)
    parent_ids                    = fields.Many2many(related='student_id.parent_ids')
    student_pid                   = fields.Char(related='student_id.pid')

    @api.depends('student_id')
    @api.multi
    def _compute_class(self):
        for rec in self:
            if rec.student_id.class_id:
                rec.class_id = rec.student_id.class_id
        print"#########",rec.class_id
        
    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        result = super(AccountInvoice, self)._onchange_partner_id()
        product_ids = []
        if self.partner_id.is_school_parent:
            for student in self.partner_id.student_ids:
                for rec in self.env['school.fees'].search([('grade_id','=',student.grade_id.id)]):
                    for product in rec.obligatory_fees_ids:
                        product_product = self.env['product.product'].search([('product_tmpl_id','=',product.id)])
                        product_ids.append((0,0, {'student_id':student.id,'product_id':product_product.id, 'quantity':1}))
                    for product in rec.optional_fees_ids:
                        product_product = self.env['product.product'].search([('product_tmpl_id','=',product.id)])
                        product_ids.append((0,0, {'student_id':student.id, 'product_id':product_product.id, 'quantity':1}))
                        
        result['value'] = {'invoice_line_ids':product_ids}
        return  result

    @api.multi
    def amount_to_text_fr(self, amount, currency='Dinars'):
        amount = '%.3f' % amount
        units_name = currency
        list = str(amount).split('.')
        start_word = french_number(abs(int(list[0])))
        end_word = french_number(int(list[1]))
        #cents_number = int(list[1])
        #cents_name = (cents_number > 1) and ' Millimes' 
        final_result = start_word +' '+units_name+' '+ end_word +' '+' Millimes'
        return final_result
    #@api.multi
    #def amount_to_text_fr(self, amount, currency='Dinars'):
      #return amount_to_text_fr(amount, currency)

    @api.onchange('invoice_line_ids')
    def _change_invoice_lines(self):
        ''' 
            dummy method to trigger _onchange_product_id() of each invoice_line
            in order to calculate the unit_price and subtotals ... 
        '''
        for el in self.invoice_line_ids:
           el._onchange_product_id()
           
    @api.model
    def create(self, vals):
        vals['ref_draft_invoice'] = self.env['ir.sequence'].next_by_code('account.invoice')
        #_logger.warning('#### REF DRAFT INVOICE: %s', vals['ref_draft_invoice'])
        return super(AccountInvoice, self).create(vals)



class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    student_id          = fields.Many2one('school.student', string="Élève")
    price_subtotal = fields.Monetary(string='Amount', store=True, readonly=True, digits=dp.get_precision('Product Price'), compute='_compute_price')



