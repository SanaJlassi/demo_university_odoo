# -*- coding: utf-8 -*-
import odoo
from odoo import http
from odoo import fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.http import content_disposition, dispatch_rpc, request
import json
import base64
import urllib2
import urllib
from datetime import datetime
import logging
import json

_logger = logging.getLogger(__name__)


class SchoolSanction(http.Controller):

    HEADER_GET = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Credentials':'True',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'GET',
            }

    HEADER_POST = {
                'Cache-Control': 'no-cache', 
                'Content-Type': 'JSON; charset=utf-8',
                'Access-Control-Allow-Origin':  '*',
                'Access-Control-Allow-Methods': 'POST',
                'Access-Control-Allow-Credentials':'True'
            }


    @http.route('/api/sanction/types', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_sanction_types(self, **kwargs):
        response = {'success':False, 'data':None}
        sanction_type_id = request.env['school.sanction.type'].search([])
        
        sanction_type_list = []
        for sanction_type in sanction_type_id:
            sanction_type_list.append(
                {
                    'id':sanction_type.id,
                    'name':sanction_type.name,
                    'description':sanction_type.description,
                }
            )
        response['success'] = True
        response['data'] = sanction_type_list
        return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)



    @http.route('/api/teacher/sanctions', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_teacher_sanctions(self, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        teacher_id = request.env['hr.employee'].search([('partner_id','=',request.env.user.partner_id.id)])
        if not teacher_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Teacher found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)     

        sanction_id = request.env['school.sanction'].search([('teacher_id','=',teacher_id.id)])
        sanction_list = []

        for sanction in sanction_id:
            student_list = []
            for student in sanction.student_ids:
                student_list.append({
                    'id':student.id,
                    'name':student.name,
                    'last_name':student.last_name,
                    })
            _logger.warning('STUDENTS %s',student_list)
            file ={}
            if sanction.file:
                domain = [
                    ('res_model', '=', sanction._name),
                    ('res_field', '=', 'file'),
                    ('res_id', '=', sanction.id),
                ]
                attachment = request.env['ir.attachment'].search(domain)
                base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                _logger.warning('#### file id %s',attachment.id)
                _logger.warning('#### file id %s',sanction.file_name)
                url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+sanction.file_name
                url = url.replace(" ", "_")
                _logger.warning('#### URL of document %s',url)
                file['filename'] = sanction.file_name
                file['url'] = url
            sanction_list.append(
                {
                    'id':sanction.id,
                    'title':sanction.name,
                    'description':sanction.description,
                    'date':sanction.date,
                    'state':sanction.state,
                    'sanction_type':{
                        'id':sanction.sanction_type_id.id,
                        'name':sanction.sanction_type_id.name,
                        'description':sanction.sanction_type_id.description,
                    },
                    'teacher':{
                        'id':sanction.teacher_id.id,
                        'name':sanction.teacher_id.name,
                        'last_name':sanction.teacher_id.last_name,
                    },
                    'student_ids':student_list,
                    'class':{
                        'id':sanction.class_id.id,
                        'name':sanction.class_id.name,
                        'code':sanction.class_id.code,
                    },
                    'file':file,
                }
            )

        response['success'] = True
        response['data'] = sanction_list
        return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)



    @http.route('/api/student/<int:student_id>/sanctions', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_sanctions(self, student_id, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)     

        # sanction_id = request.env['school.sanction'].search([('student_id','=',student_id.id),('state','=','confirmed')])
        sanction_id = request.env['school.sanction'].search([('state','=','confirmed')])
        sanction_ids = []
        for sanction in sanction_id:
            if student_id.id in sanction.student_ids.ids:
                sanction_ids.append(sanction)

        sanction_list = []
        for sanction in sanction_ids:
            file ={}
            if sanction.file:
                domain = [
                    ('res_model', '=', sanction._name),
                    ('res_field', '=', 'file'),
                    ('res_id', '=', sanction.id),
                ]
                attachment = request.env['ir.attachment'].search(domain)
                base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                _logger.warning('#### file id %s',attachment.id)
                _logger.warning('#### file id %s',sanction.file_name)
                url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+sanction.file_name
                url = url.replace(" ", "_")
                _logger.warning('#### URL of document %s',url)
                file['filename'] = sanction.file_name
                file['url'] = url
            sanction_list.append(
                {
                    'id':sanction.id,
                    'title':sanction.name,
                    'description':sanction.description,
                    'date':sanction.date,
                    'sanction_type':{
                        'id':sanction.sanction_type_id.id,
                        'name':sanction.sanction_type_id.name,
                        'description':sanction.sanction_type_id.description,
                    },
                    'teacher':{
                        'id':sanction.teacher_id.id,
                        'name':sanction.teacher_id.name,
                        'last_name':sanction.teacher_id.last_name,
                    },
                    'student':{
                        'id':student_id.id,
                        'name':student_id.name,
                        'last_name':student_id.last_name,
                    },
                    'class':{
                        'id':sanction.class_id.id,
                        'name':sanction.class_id.name,
                        'code':sanction.class_id.code,
                    },
                    'file':file,
                }
            )

        response['success'] = True
        response['data'] = sanction_list
        return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)


    @http.route('/api/student/sanctions', type='http', auth="user", methods=['POST'],  csrf=False)
    def create_sanctions(self, **kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_teacher'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        sanction_json = None
        if not request.params.get('sanction',False):
            response['success'] = False
            response['error'] = {'code':404, 'message':'sanction object not submited'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        _logger.warning('### DATA RECIEVED :  (%s).', request.params.get('sanction',False))
        try:
            sanction_json = json.loads(request.params.get('sanction',False))
        except ValueError, e:
            response['success'] = False
            response['error'] = {'code':404, 'message':'Unable to deserialize sanction object'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        if 'title' not in sanction_json:
            response['success'] = False
            response['error'] = {'code':404, 'message':'title is missing'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        if 'sanction_type_id' not in sanction_json:
            response['success'] = False
            response['error'] = {'code':404, 'message':'sanction_type is missing'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        if 'student_ids' not in sanction_json:
            response['success'] = False
            response['error'] = {'code':404, 'message':'student_id is missing'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        if 'all_student' not in sanction_json:
            response['success'] = False
            response['error'] = {'code':404, 'message':'all_student is missing'}         
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)    

        file = False
        if 'file' in sanction_json:
            try:
                filename = sanction_json['file']['name']
                base64 = sanction_json['file']['base64'].split("base64,")
                # get the second part of the string after 'base64,'   
                base64 = base64[1]
                base64 = str(base64)
                file = {"file":base64, "file_name":filename}
            except IndexError:
                response['success'] = False
                response['error'] = {'code':404, 'message':'student_id is missing'} 

        # student_id = request.env['school.student'].browse(int(sanction_json['student_id']))
        partner_id = request.env.user.partner_id
        teacher_id = request.env['hr.employee'].search([('partner_id','=',partner_id.id)])
        _logger.warning('########################## TEACHER PARTNER  :  (%s).', teacher_id.name)

        list_student = []
        if sanction_json['all_student'] == True:
            class_id = request.env['school.class'].search([('id','=',sanction_json['class_id'])])
            list_student = [(6,False,[y for y in class_id.student_ids.ids])]
        else:
            list_student = [(6,False,[y for y in sanction_json['student_ids']])]
        

        sanction_id = {
            'name':sanction_json['title'],
            'description':sanction_json['description'] if 'description' in sanction_json else '',
            'date':fields.Datetime.now(),
            'sanction_type_id':sanction_json['sanction_type_id'],
            'teacher_id':teacher_id.id,
            # 'student_id':student_id.id,
            'student_ids':list_student,
            'class_id':sanction_json['class_id'],
            'file':file['file'] if file else False,
            'file_name':file['file_name'] if file else False,
        }

        res = request.env['school.sanction'].create(sanction_id)

        # print "######## creating a sanction ",res.name

        response['success'] = True if len(res) else False
        response['data'] = res.name if len(res) else False
        return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)
        
    @http.route('/api/student/<int:student_id>/sanctions/academic_year/<int:academic_year_id>/timing_system_periode/<int:timing_system_periode_id>', type='http', auth="user", methods=['GET'],  csrf=False)
    def get_sanctions_by_academic_year_period(self, student_id,academic_year_id,timing_system_periode_id,**kwargs):
        response = {'success':False, 'data':None}
        if not request.env.user.has_group('genext_school.group_school_parent'):
            response['success'] = False
            response['error'] = {'code':404, 'message':'Logged user is not allowed'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)

        student_id = request.env['school.student'].browse(student_id)
        if not student_id:
            response['success'] = False
            response['error'] = {'code':404, 'message':'No Student found with the given id'} 
            return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)     
        academic_years=request.env['academic.year'].search([('id','=',academic_year_id)])
        timing_periode=request.env['school.timing.system.period'].search([('id','=',timing_system_periode_id)])
        # sanction_id = request.env['school.sanction'].search([('student_id','=',student_id.id),('state','=','confirmed')])
        sanction_id = request.env['school.sanction'].search([('state','=','confirmed')])
        sanction_ids = []
        for sanction in sanction_id:
            if student_id.id in sanction.student_ids.ids:
                if academic_years.start_date <= sanction.date <= academic_years.end_date:
                    if timing_periode.start_date <= sanction.date <= timing_periode.end_date:
                        sanction_ids.append(sanction)

        sanction_list = []
        for sanction in sanction_ids:
            file ={}
            if sanction.file:
                domain = [
                    ('res_model', '=', sanction._name),
                    ('res_field', '=', 'file'),
                    ('res_id', '=', sanction.id),
                ]
                attachment = request.env['ir.attachment'].search(domain)
                base_url = request.env['ir.config_parameter'].sudo().get_param('web.base.url', 'http://localhost:8069')
                _logger.warning('#### file id %s',attachment.id)
                _logger.warning('#### file id %s',sanction.file_name)
                url = base_url+'/web/content/ir.attachment/'+str(attachment.id)+'/datas/'+sanction.file_name
                url = url.replace(" ", "_")
                _logger.warning('#### URL of document %s',url)
                file['filename'] = sanction.file_name
                file['url'] = url
            sanction_list.append(
                {
                    'id':sanction.id,
                    'title':sanction.name,
                    'description':sanction.description,
                    'date':sanction.date,
                    'sanction_type':{
                        'id':sanction.sanction_type_id.id,
                        'name':sanction.sanction_type_id.name,
                        'description':sanction.sanction_type_id.description,
                    },
                    'teacher':{
                        'id':sanction.teacher_id.id,
                        'name':sanction.teacher_id.name,
                        'last_name':sanction.teacher_id.last_name,
                    },
                    'student':{
                        'id':student_id.id,
                        'name':student_id.name,
                        'last_name':student_id.last_name,
                    },
                    'class':{
                        'id':sanction.class_id.id,
                        'name':sanction.class_id.name,
                        'code':sanction.class_id.code,
                    },
                    'file':file,
                }
            )

        response['success'] = True
        response['data'] = sanction_list
        return http.request.make_response(json.dumps(response),SchoolSanction.HEADER_GET)