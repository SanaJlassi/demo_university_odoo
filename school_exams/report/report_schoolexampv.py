# -*- coding: utf-8 -*-
from datetime import datetime
import time
from odoo import api, models
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import logging
_logger = logging.getLogger(__name__)


class ExamPV(models.AbstractModel):
    _name = 'report.school_exams.report_schoolexampv'

    def _get_subjects(self, academic_year, module_id):
        subject_list = []
        module = self.env['school.module'].search([('academic_year_id','=',academic_year.id), ('id','=',module_id.id)])
        if len(module):
            subject_list = module.mapped('subject_ids')
        return len(subject_list)

    def _get_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id, student_id):
        notes_list = []
        # print "################ GET NOTE 1"
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        notes_list = self.env['school.exam.note'].search(domain)
        if len(notes_list):
            for line in notes_list : 
                note_id = line.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
                average = note_id.average
            return average
        else:
            return '-'


   
   
    
   
    def _compute_moyenne_class(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id,subject_id):
        average_list = []
        somme =0.0
        nbeleve=0
        moyenne=0.0
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)] 
        average_list = self.env['school.exam.note'].search(domain)
        average_id = average_list.mapped('exam_notes_line_ids')
        if average_id:
            for line in average_id:
                somme=somme + line.average
                nbeleve= nbeleve + 1
            moyenne=somme/nbeleve
            return moyenne
        else:
            return '-'    


    def _compute_max_averge_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id,subject_id): 
        average_list=[]
        averagesubject=0.0
        domain =[('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        average_list = self.env['school.exam.note'].search(domain)
        average_id = average_list.mapped('exam_notes_line_ids')
        if average_id:
           averagesubject=average_id.mapped('average')
           return max(averagesubject)



    def _compute_min_averge_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id,subject_id): 
        average_list=[]
        averagesubject=0.0
        domain =[('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
        average_list = self.env['school.exam.note'].search(domain)
        average_id = average_list.mapped('exam_notes_line_ids')
        if average_id:
           averagesubject=average_id.mapped('average')
           return min(averagesubject)


    def _sum_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,module_id,student_id):
        sum_module=0.0
        for subject in module_id.subject_ids:
            notes_list = []
            domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject.id)]
            notes_list = self.env['school.exam.note'].search(domain)
            if len(notes_list):
                for line in notes_list : 
                    note_id = line.mapped('exam_notes_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
                    average = note_id.average
                sum_module = sum_module + average 
        return sum_module 
    def _max_sum_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,module_id):
        max_sum=[]
        for student_id in class_id.student_ids:
            sum_module=self._sum_module(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,module_id,student_id)
            max_sum.append(sum_module)
        return max(max_sum)


    def _min_sum_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,module_id):
        min_sum=[]
        for student_id in class_id.student_ids:
            sum_module=self._sum_module(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id,module_id,student_id)
            min_sum.append(sum_module)
        return min(min_sum)

    def _compute_max_sum_modules(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id):
        max_sum = 0
        for student_id in class_id.student_ids:
            sum_module = self._get_sum_modules(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id)
            if sum_module > max_sum :
                max_sum = sum_module
        return max_sum     
    def _compute_min_sum_modules(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id):   
        min_sum=[]
        for student_id in class_id.student_ids:
            sum_module = self._get_sum_modules(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id)
            min_sum.append(sum_module)
        return min(min_sum)
    ######## average period class
    def _compute_class_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id):
        avergeperiodclass=0.0
        sum_avergeperiod=0.0
        nbeleve=0.0
        sump=0.0
        for student_id in class_id.student_ids:
            sum_avergeperiod=self._get_average_period(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id)
            sump=sump+sum_avergeperiod
            nbeleve=nbeleve+1
        avergeperiodclass=sump/nbeleve
        return avergeperiodclass
         ###### max averge period
    def _compute_max_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id):
        max_sum=[]
        for student_id in class_id.student_ids:
            averge_period=self._get_average_period(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id)
            max_sum.append(averge_period)
        return max(max_sum)
    
    def _compute_min_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id):
        min_averageperiod=[]
        for student_id in class_id.student_ids:
            average_period = self._get_average_period(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id)
            min_averageperiod.append(average_period )
        return min(min_averageperiod)
    # def _get_max_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
    #     notes_list = []
    #     # print "################ GET NOTE 2"
    #     domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
    #     # print "################ DOMAIN 2 ",domain
    #     notes_list = self.env['school.subject.average'].search(domain)
    #     # print "################ NOTES LIST 2 ",notes_list
    #     if len(notes_list):
    #         note_id = notes_list.mapped('subject_average_line_ids')
    #         # print "############################### NOTE ID 2 ",note_id
    #         average = note_id.mapped('average')
    #         # print "############################### AVERAGE LIST ",average
    #         return max(average)
    #     else:
    #         return '-'

    # def _get_min_average_subject(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
    #     notes_list = []
    #     # print "################ GET NOTE 2"
    #     domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('subject_id','=',subject_id.id)]
    #     # print "################ DOMAIN 2 ",domain
    #     notes_list = self.env['school.subject.average'].search(domain)
    #     # print "################ NOTES LIST 2 ",notes_list
    #     if len(notes_list):
    #         note_id = notes_list.mapped('subject_average_line_ids')
    #         # print "############################### NOTE ID 2 ",note_id
    #         average = note_id.mapped('average')
    #         # print "############################### AVERAGE LIST ",average
    #         return min(average)
    #     else:
    #         return '-'


    # def _get_appreciation_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id):
    #     domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('module_id','=',module_id.id)] 
    #     module_list = self.env['exam.appreciation.module'].search(domain)
    #     if len(module_list):
    #         appreciation_id = module_list.mapped('comment_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
    #         return appreciation_id.comment
    #     else:
    #         return '-'

    # def _get_appreciation_council(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id):
    #     domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)] 
    #     appreciation_list = self.env['exam.appreciation.council'].search(domain)
    #     if len(appreciation_list):
    #         appreciation_id = appreciation_list.mapped('comment_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
    #         return appreciation_id.comment
    #     else:
    #         return '-'
    #else:
        #return '-'
    def _get_average_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id),('module_id','=',module_id.id)]
        notes_list = self.env['exam.appreciation.module'].search(domain)
        if len(notes_list):
            note_id = notes_list.mapped('module_average_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return note_id.average
        #else:
           # return '-'

    def _get_class_average_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id):
        sum_averge = 0.0
        nb_eleves = 0
        for student_id in class_id.student_ids:
            sum_averge += self._get_average_module(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id)
            nb_eleves += 1
        return round(sum_averge/nb_eleves , 2)    
    def _max_average_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id):
        max_sum=[]
        for student_id in class_id.student_ids:
            average_module=self._get_average_module(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id)
            max_sum.append(average_module)
        return max(max_sum)
    def _min_average_module(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id):
        min_sum=[]
        for student_id in class_id.student_ids:
            average_module=self._get_average_module(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, module_id, student_id)
            min_sum.append(average_module)
        return min(min_sum)

    def _get_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
        notes_list = self.env['exam.appreciation.council'].search(domain)
        if len(notes_list):
            note_id = notes_list.mapped('period_average_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return note_id.average
        else:
            return '-'

    def _get_sum_modules(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id):
        notes_list = []
        sum_module = 0
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
        notes_list = self.env['exam.appreciation.module'].search(domain)
        if len(notes_list):
            for line in notes_list:
                note_id = line.mapped('module_average_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
                sum_module += note_id.average
            return sum_module
        else:
            return '-'

    def _get_student_rank(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, student_id):
        notes_list = []
        domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
        notes_list = self.env['exam.appreciation.council'].search(domain)
        if len(notes_list):
            line = notes_list.mapped('period_average_line_ids').filtered(lambda r: r.student_id.id == student_id.id)
            return line.rank
        else:
            return '-'

    # def _get_max_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
    #     notes_list = []
    #     # print "################ GET NOTE 2"
    #     domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
    #     # print "################ DOMAIN 2 ",domain
    #     notes_list = self.env['exam.appreciation.council'].search(domain)
    #     # print "################ NOTES LIST 2 ",notes_list
    #     if len(notes_list):
    #         note_id = notes_list.mapped('period_average_line_ids')
    #         # print "############################### NOTE ID 2 ",note_id
    #         average = note_id.mapped('average')
    #         # print "############################### AVERAGE LIST ",average
    #         return max(average)
    #     else:
    #         return '-'

    # def _get_min_average_period(self,academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, class_id, subject_id):
    #     notes_list = []
    #     # print "################ GET NOTE 2"
    #     domain = [('academic_year','=',academic_year.id),('educational_stage_id','=',educational_stage_id.id),('periode_config_id','=',periode_config_id.id),('periode_ids','=',periode_ids.id),('grade_id','=',grade_id.id),('class_id','=',class_id.id)]
    #     # print "################ DOMAIN 2 ",domain
    #     notes_list = self.env['exam.appreciation.council'].search(domain)
    #     # print "################ NOTES LIST 2 ",notes_list
    #     if len(notes_list):
    #         note_id = notes_list.mapped('period_average_line_ids')
    #         # print "############################### NOTE ID 2 ",note_id
    #         average = note_id.mapped('average')
    #         # print "############################### AVERAGE LIST ",average
    #         return min(average)
    #     else:
    #         return '-'



    @api.model
    def render_html(self, docids, data=None):
        used_context = data['form'].get('used_context', {})
        print "##### render_html"
        academic_year                 = 'academic_year' in data['form'] and data['form']['academic_year'] or False
        educational_stage_id          = 'educational_stage_id' in data['form'] and data['form']['educational_stage_id'] or False
        periode_config_id             = 'periode_config_id' in data['form'] and data['form']['periode_config_id'] or False
        periode_ids                   = 'periode_ids' in data['form'] and data['form']['periode_ids'] or False
        grade_id                      = 'grade_id' in data['form'] and data['form']['grade_id'] or False
        class_id                      = 'class_id' in data['form'] and data['form']['class_id'] or False


        grade_id                      = self.env['school.grade'].search([('id','=',grade_id[0])])
        educational_stage_id          = self.env['educational.stage'].search([('id','=',educational_stage_id[0])])
        class_id                      = self.env['school.class'].search([('id','=',class_id[0])])
        periode_ids                   = self.env['school.exam.periode'].search([('id','=',periode_ids[0])])
        academic_year                 = self.env['academic.year'].search([('id','=',academic_year[0])])
        periode_config_id             = self.env['school.exam.periode.config'].search([('id','=',periode_config_id[0])])
        student_id                    = self.env['school.student'].search([('class_id','=',class_id.id)])
        print "##### render_html 2"
        docargs = {
            'doc_ids': docids,
            'doc_model': self.env['school.student'],
            'data': data,
            'docs': student_id,
            'time': time,
            'academic_year': academic_year,
            'educational_stage_id':educational_stage_id,
            'periode_config_id': periode_config_id,
            'periode_ids': periode_ids,
            'grade_id': grade_id,
            'class_id': class_id,
            'subjects_len': self._get_subjects,
            'average_subject': self._get_average_subject,
            # 'max_average_subject': self._get_max_average_subject,
            # 'min_average_subject' : self._get_min_average_subject,
            # 'appreciation_module': self._get_appreciation_module,
            'average_module': self._get_average_module,
            'class_average_module': self._get_class_average_module,
            'max_average_module'  :self._max_average_module,
            'min_average_module'  :self._min_average_module,
            'average_period': self._get_average_period,
            # 'max_average_period': self._get_max_average_period,
            # 'min_average_period': self._get_min_average_period,
            # 'appreciation_council': self._get_appreciation_council,
            'sum_modules': self._get_sum_modules,
            'student_rank': self._get_student_rank,
            'avergeclass': self._compute_moyenne_class,
            'maxaverage_subject': self._compute_max_averge_subject,
            'minaverage_subject': self._compute_min_averge_subject,
            'max_sum_modules'   : self._compute_max_sum_modules,
            'min_sum_modules'   : self._compute_min_sum_modules, 
            'avergeclass_period' :self._compute_class_average_period, 
            'max_average_period' :self._compute_max_average_period,
            'min_average_period' :self._compute_min_average_period,
            'sum_module'         :self._sum_module,
            'max_sum_module'     :self._max_sum_module,
            'min_sum_module'     :self._min_sum_module,
            }
        report = self.env['report'].render('school_exams.report_schoolexampv', docargs)
        print "############# hello"
        return report
