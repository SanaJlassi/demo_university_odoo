# -*- coding: utf-8 -*-
from odoo import _
from odoo import models, fields, api
from odoo.exceptions import UserError, ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from odoo.modules import get_resource_path
import time
from datetime import date, datetime
import logging
import json

_logger = logging.getLogger(__name__)

class ExamType(models.Model):
    _name = 'school.exam.type'
    _description = 'Exam Type'

    name                        = fields.Char("Nom")
    type                        = fields.Selection([('Évaluation','Évaluation'),
                                                ('Examen','Examen'),
                                                ('Oral','Oral'),
                                                ('TP','TP'),
                                                ('DS','DS'),
                                                ('DC','DC'),
                                                ],string="Type d'examen", required=True)
    educational_stage_id        = fields.Many2many('educational.stage', string="Cycle", required=True)
    coefficient                 = fields.Float(string='Coefficient', default=0.0, required=True)
    scale                       = fields.Float(string='Barème', default=20, required=True)

    _sql_constraints = [('exam_type_coeff', 'unique(type, coefficient, scale)',"Ce type est déjà saisi")]

    @api.multi
    def name_get(self):
        return [(record.id, record.type)for record in self]
    

class ExamPeriodeConfig(models.Model):
    _name = 'school.exam.periode.config'


    name                            = fields.Char('Nom', required=True)
    academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
    periode_ids                     = fields.One2many('school.exam.periode','periode_config_id', string="Périodes")
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)

    @api.multi
    def name_get(self):
        return [(record.id, record.academic_year.code+'/'+record.name )for record in self]

    _sql_constraints = [('exam_period_config', 'unique(academic_year, educational_stage_id)',"Cette configuration est déjà saisie")] 

class ExamPeriode(models.Model):
    _name = 'school.exam.periode'

    name                    = fields.Char('Nom du période', required=True)
    code                    = fields.Char('Code du période', required=True)
    periode_config_id       = fields.Many2one('school.exam.periode.config')
    academic_year           = fields.Many2one(related='periode_config_id.academic_year')
    educational_stage_id    = fields.Many2one(related='periode_config_id.educational_stage_id')
    start_date              = fields.Date('Date début', required=True)
    end_date                = fields.Date('Date fin', required=True)
    exam_type_ids           = fields.Many2many('school.exam.type', string="Type examen", domain="[('educational_stage_id','=',educational_stage_id)]")


    @api.one
    @api.constrains('start_date', 'end_date')
    def _check_dates(self):
        if self.start_date >= self.end_date:
            raise ValidationError("La date de fin de période doit être supérieure à la date de début de période")
      







##################################### à éliminer ##################################
class ExamSubjectConfig(models.Model):
    _name = 'school.exam.subject.config'

    name                            = fields.Char("Nom")
    academic_year                   = fields.Many2one('academic.year', default=lambda self: self.env['academic.year'].search([('active_year','=',True)]), string="Année scolaire" )
    educational_stage_id            = fields.Many2one('educational.stage', string="Cycle", required=True)
    grade_id                        = fields.Many2one('school.grade', string="Niveau scolaire", domain="[('educational_stage_id','=',educational_stage_id)]",required=True)
    subject_id                      = fields.Many2one('school.subject', string="Matières", required=True)
    periode_config_id               = fields.Many2one('school.exam.periode.config', string="Session", domain="[('educational_stage_id','=',educational_stage_id), ('academic_year','=',academic_year)]", required=True)
    periode_ids                     = fields.Many2one('school.exam.periode', string="Période", domain="[('periode_config_id','=',periode_config_id)]", required=True)
    exam_type_ids                   = fields.Many2many('school.exam.type',string="Type examen", domain="[('educational_stage_id','=',educational_stage_id)]", required=True)    

    @api.multi
    def name_get(self):
        return [(record.id, record.periode_config_id.name+'/'+record.periode_ids.name+'/'+record.grade_id.name+'/'+record.subject_id.name)for record in self]

    _sql_constraints = [('exam_subject_config', 'unique(academic_year, educational_stage_id, periode_config_id, periode_ids, grade_id, subject_id)',"Cette configuration est déjà saisie")] 

class EducationStage(models.Model):
    _inherit='school.subject'

    exam_subject_ids           = fields.One2many('school.exam.subject.config', 'subject_id', string="Subject Exam Config")